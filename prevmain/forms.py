'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
'''




from django.forms import ModelForm,Textarea
from django import forms
from prevmain.models import *
import datetime
from django.core.exceptions import ValidationError
class PMScheduleForm(ModelForm):
	class Meta:
		model = PMSchedule
		exclude = ('created_by_user','company','section','equipment_section','is_blockwise',)
	def __init__(self, sites, *args, **kwargs):
        	super(PMScheduleForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites


class PMAlertForm(ModelForm):
	class Meta:
		model = PMAlert
		exclude = ('user','site','pmschedule','completed_date','due_date','reason_for_delay','shutdown','new_date','block','section','equipment',)
    	def __init__(self, *args, **kwargs):
	        super(PMAlertForm, self).__init__(*args, **kwargs)
		instance = getattr(self, 'instance', None)
		today = datetime.datetime.now().date()
		'''
        	if instance.due_date > today:
            		self.fields['status'].widget.attrs['readonly'] = True
            		self.fields['report'].widget.attrs['readonly'] = True
            		self.fields['observations'].widget.attrs['readonly'] = True
            		self.fields['corrective_action'].widget.attrs['readonly'] = True
            		#self.fields['spares'].widget.attrs['readonly'] = True
            		self.fields['remarks'].widget.attrs['readonly'] = True
		'''
	def clean(self):
		checklists = PMChecklist.objects.filter(pm_alert = self.instance,is_completed=False)
		if checklists:
			raise ValidationError("Please update all checklist items before closing the PM")

class PMChecklistForm(ModelForm):
        class Meta:
                model = PMChecklist
                exclude = ('pm_alert',)


        def __init__(self, *args, **kwargs):
                super(PMChecklistForm, self).__init__(*args, **kwargs)
		instance = getattr(self, 'instance', None)

            	self.fields['checklist'].widget.attrs['readonly'] = True
		today = datetime.datetime.now().date()
                try:
			self.fields['user_input_value'].queryset = PossibleValue.objects.filter(checklist = instance.checklist)
		except:
			pass
	'''
	def clean_user_input_value(self):
		value = self.cleaned_data['user_input_value']
		if not value:
			raise ValidationError("Status cannot be blank")
		return value
	'''
class PMAlertDelayForm(ModelForm):
	class Meta:
		model = PMAlert
		exclude = ('user','site','pmschedule','completed_date','due_date','block','section','equipment',)


class ScheduleSearchForm(forms.Form):
	site = forms.ModelChoiceField(queryset = None)
	equipment = forms.ModelChoiceField(queryset = None,required=False)
	def __init__(self,sites, *args, **kwargs):
        	super(ScheduleSearchForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites
        	self.fields['equipment'].queryset = Equipment.objects.filter(site__in=sites)

class FullScheduleForm(forms.Form):
	site = forms.ModelChoiceField(queryset = None)
	year = forms.ModelChoiceField(queryset = None)
	def __init__(self,sites, *args, **kwargs):
        	super(FullScheduleForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites
        	self.fields['year'].queryset = Years.objects.all()



class ReportForm(forms.Form):
	TYPES = (
		   ('---','---'),
		   ('dgr','Generation'),
		   ('breakdown','Plantdown'),
		   ('griddown','Griddown'),
		   ('radiation','Insolation'),
		   ('pm','PM'),
		   ('slippage','Slippage'),
		   ('reminders','Reminders'),
		   ('cleaning','Module Cleaning'),
		   ('ort','ORT Agenda'),
		  )
	report_type= forms.ChoiceField(choices=TYPES,)

	TYPES = (
		   ('all','All'),
		   ('compliances','Compliances'),
		   ('warranty','warranty'),
		   ('jmr','JMR/Invoice'),
		  )

	category = forms.ChoiceField(choices=TYPES,required=False,widget=forms.Select(attrs={'class':'field-hidden field-category'}))

	site = forms.ModelChoiceField(queryset = None,required=False)
	equipment = forms.ModelChoiceField(queryset = None,required=False, widget=forms.Select(attrs={'class':'field-hidden field-equipment'}))
	CHOICES = (
		   ('---','---'),
		   ('daily','Daily'),
		   ('monthly','Day Range'),
		   ('yearly','Monthly'),
		   ('finyearly','Financial Year'),
		   ('yearwise','Yearly'),
		  )
	format = forms.ChoiceField(choices=CHOICES,)
	YEARS = (('2012','2012'),
		   ('2013','2013'),
		   ('2014','2014'),
		   ('2015','2015'),
		   ('2016','2016'),
		   ('2017','2017'),
		   ('2018','2018'),
		   ('2019','2019'),
		   ('2020','2020'),
		   ('2021','2021'),
		   ('2022','2022'),
		   ('2023','2023'),
		   ('2024','2024'),
		   ('2025','2025'),
		  )
	year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-year'}))
	from_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-from-year'}))
	to_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-to-year'}))

	finyear = forms.ModelChoiceField(queryset = None,required=False, widget=forms.Select(attrs={'class':'field-hidden field-fin-year'}))

	from_date = forms.DateField(required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-from'}))	
	to_date = forms.DateField(required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-to'}))	

	def __init__(self,sites, *args, **kwargs):	
        	super(ReportForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites
        	self.fields['equipment'].queryset = Equipment.objects.all()
        	self.fields['finyear'].queryset = Years.objects.all()
	
	def clean(self): 
        	format = self.cleaned_data['format']
		if format == "weekly" or format == "monthly":
			if not self.cleaned_data['to_date'] or not self.cleaned_data['from_date']:
    				raise forms.ValidationError("Please enter From and To date before generating report")
		if format == "---":
				
    			raise forms.ValidationError("Please choose a valid format")
		if format == "daily":
			if not self.cleaned_data['from_date']:
    				raise forms.ValidationError("Please enter From date before generating report")

		if format == "yearly":
			if not self.cleaned_data['year']:
    				raise forms.ValidationError("Please choose a year for yearly report")


class PMRForm(ModelForm):
	class Meta:
		model = PMR
		fields = ('schedule',)	
