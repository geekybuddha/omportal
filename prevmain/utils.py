from .models import UserCompany,EnergySite,UserSite
			
def get_user_sites(user):			
	usercompany = UserCompany.objects.get(user=user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	return sites


