'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
'''




from django.shortcuts import render
import datetime
from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.http import JsonResponse
from django.forms.formsets import formset_factory
from django.forms import modelformset_factory
from prevmain.forms import *
#from energysite.models import UserCompany,EnergySite,Company,UserSite,SiteNotification
from django.db.models import Q,F
from django.core.mail import send_mail
from .models import *
from issues.models import *
from django.template.loader import render_to_string
import math
import calendar
from django.conf import settings
from .utils import *
import simplejson
import xlsxwriter
from monthdelta import monthdelta
from django.forms  import inlineformset_factory



FROM_EMAIL = settings.DEFAULT_FROM_EMAIL




def create_pm(pm):

	site = pm.site
	frequency = pm.frequency
	equipment = pm.equipment
	financial_year = pm.financial_year
	is_block = False
	#is_blockwise = form.cleaned_data['is_blockwise']
	location = ""
	if is_block:
		location = "block"
	if location == "block":
		blocks = Block.objects.filter(site=site)
		rem_blocks = Block.objects.filter(site=site)
		total_block = blocks.count()
		is_block = True

	c = calendar.Calendar()

	to_date = datetime.date(int(financial_year.end),3,31)
	date = pm.start_date
	day = datetime.timedelta(days=1)


	kk = 1  #counter

	'''
	if frequency == "fortnightly":
		per_day = math.ceil(total_block/14.0)
	if frequency == "monthly":
		per_day = math.ceil(total_block/30.0)
	if frequency == "quarterly":
		per_day = math.ceil(total_block/90.0)
	if frequency == "halfyearly":
		per_day = math.ceil(total_block/180.0)
	if frequency == "yearly":
		per_day = math.ceil(total_block/365.0)
	if per_day < 1:
		per_day = 1
	'''
	reports = []
	from_date = date
	cycle_done = False
	equipments = pm.equipment.all()
	for equipment in equipments:
		date = pm.start_date
		while date < to_date:
		
			data = {}
			pm_alert = PMAlert(pmschedule=pm,equipment=equipment,due_date=date,status="due",site=site)
			pm_alert.save()
		
			'''
			ids = rem_blocks.values_list('id', flat=True)
			if is_block:
				current_blocks = Block.objects.filter(id__in=[o for o in ids])[:per_day]
			else:
				current_blocks = EquipmentSection.objects.filter(equipment=equipment,id__in=[o for o in ids])[:per_day]
				#if equipment_section:
				#	current_blocks = EquipmentSection.objects.filter(id=equipment_section.id)
				#else:
				#	current_blocks = EquipmentSection.objects.filter(equipment=equipment)
			ids2 = current_blocks.values_list('id', flat=True)

			rem_blocks = rem_blocks.exclude(id__in=[n for n in ids2])
			data['date'] = date
			data['blocks'] = current_blocks
			if is_block:
				for block in current_blocks:
					pm_alert = PMAlert(pmschedule=pm,due_date=date,block=block,status="due",site=site)
					pm_alert.save()
			else:
				date2 = date
				for block in current_blocks:
					pm_alert = PMAlert(pmschedule=pm,due_date=date,section=block,status="due",site=site)
					pm_alert.save()
					date2 = date2+day

			reports.append(data)

			date = date+day
			'''
			if frequency == "daily":
				freq = 1
				date = date + datetime.timedelta(days=freq)
		
			if frequency == "weekly":
				freq = 7
				date = date + datetime.timedelta(days=freq)
		
			if frequency == "fortnightly":
				freq = 14
				date = date + datetime.timedelta(days=freq)
			if frequency == "quarterly":
				freq = 90
				date = date + datetime.timedelta(days=freq)
			if frequency == "halfyearly":
				freq = 180
				date = date + datetime.timedelta(days=freq)
			if frequency == "monthly":
				date = date + monthdelta(1)

			if frequency == "yearly":
				date = date + monthdelta(12)
			if frequency == "one_time":
				date = to_date
		
			kk = kk+1







@login_required
def create_pm_schedule(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)


	form = PMScheduleForm(sites=sites)
	if request.method == "POST":
		form = PMScheduleForm(sites=sites,data=request.POST,files=request.FILES)
		if form.is_valid():
			pm = form.save(commit=False)
			pm.created_by_user = request.user
			pm.company = usercompany.company
			pm.date = 1
			pm.save()
			form.save_m2m()
			create_pm(pm)
			'''
			site = form.cleaned_data['site']

			frequency = form.cleaned_data['frequency']
			equipment = form.cleaned_data['equipment']
			financial_year = form.cleaned_data['financial_year']
			is_block = False
                        equipment_section = form.cleaned_data['equipment_section']
			if equipment_section:
				equipment_sections = EquipmentSection.objects.filter(id=equipment_section.id)
			else:
				equipment_sections = EquipmentSection.objects.filter(equipment=equipment)
			#is_blockwise = form.cleaned_data['is_blockwise']
			location = ""
			if is_block:
				location = "block"
			if location == "block":
				blocks = Block.objects.filter(site=site)
				rem_blocks = Block.objects.filter(site=site)
				total_block = blocks.count()
				is_block = True
			else:
				blocks = EquipmentSection.objects.filter(equipment=equipment)
				if equipment_section:
					rem_blocks = EquipmentSection.objects.filter(id=equipment_section.id)
				else:
					rem_blocks = EquipmentSection.objects.filter(equipment=equipment)
				total_block = blocks.count()

			c = calendar.Calendar()

			to_date = datetime.date(int(financial_year.end),3,31)
			date = form.cleaned_data['start_date']
			day = datetime.timedelta(days=1)


			kk = 1  #counter
			if frequency == "fortnightly":
				per_day = math.ceil(total_block/14.0)
			if frequency == "monthly":
				per_day = math.ceil(total_block/30.0)
			if frequency == "quarterly":
				per_day = math.ceil(total_block/90.0)
			if frequency == "halfyearly":
				per_day = math.ceil(total_block/180.0)
			if frequency == "yearly":
				per_day = math.ceil(total_block/365.0)
			if per_day < 1:
				per_day = 1
			reports = []
			from_date = date
			cycle_done = False
			while date < to_date:
				data = {}
				ids = rem_blocks.values_list('id', flat=True)
				if is_block:
					current_blocks = Block.objects.filter(id__in=[o for o in ids])[:per_day]
				else:
					current_blocks = EquipmentSection.objects.filter(equipment=equipment,id__in=[o for o in ids])[:per_day]
					#if equipment_section:
					#	current_blocks = EquipmentSection.objects.filter(id=equipment_section.id)
					#else:
					#	current_blocks = EquipmentSection.objects.filter(equipment=equipment)
				ids2 = current_blocks.values_list('id', flat=True)

				rem_blocks = rem_blocks.exclude(id__in=[n for n in ids2])
				data['date'] = date
				data['blocks'] = current_blocks
				if is_block:
					for block in current_blocks:
						pm_alert = PMAlert(pmschedule=pm,due_date=date,block=block,status="due",site=site)
						pm_alert.save()
				else:
					date2 = date
					for block in current_blocks:
						pm_alert = PMAlert(pmschedule=pm,due_date=date,section=block,status="due",site=site)
						pm_alert.save()
						date2 = date2+day

				reports.append(data)

				date = date+day

				if frequency == "fortnightly":
					if kk > 14:
						kk = 0
						if is_block:
							rem_blocks = Block.objects.filter(site=site)
						else:
							if equipment_section:
								rem_blocks = EquipmentSection.objects.filter(id=equipment_section.id)
							else:
								rem_blocks = EquipmentSection.objects.filter(equipment=equipment)
				if frequency == "quarterly":
					if kk > 90:
						kk = 0
						if is_block:
							rem_blocks = Block.objects.filter(site=site)
						else:
							if equipment_section:
								rem_blocks = EquipmentSection.objects.filter(id=equipment_section.id)
							else:
								rem_blocks = EquipmentSection.objects.filter(equipment=equipment)
				if frequency == "halfyearly":
					if cycle_done == False:
						if date > (from_date + datetime.timedelta(days=180)):
							cycle_done = True
							if is_block:
								rem_blocks = Block.objects.filter(site=site)
							else:
								if equipment_section:
									rem_blocks = EquipmentSection.objects.filter(id=equipment_section.id)
								else:
									rem_blocks = EquipmentSection.objects.filter(equipment=equipment)
					freq = int((180-total_block)/total_block)
					date = date + datetime.timedelta(days=freq)
				if frequency == "monthly":
					if cycle_done == False:
						if date > (from_date + datetime.timedelta(days=30)):
							cycle_done = True
							if is_block:
								rem_blocks = Block.objects.filter(site=site)
							else:
								if equipment_section:
									rem_blocks = EquipmentSection.objects.filter(id=equipment_section.id)
								else:
									rem_blocks = EquipmentSection.objects.filter(equipment=equipment)
					freq = int((30-total_block)/total_block)
					date = date + datetime.timedelta(days=freq)


				if frequency == "yearly":
					if cycle_done == False:
						if date > (from_date + datetime.timedelta(days=365)):
							cycle_done = True
							if is_block:
								rem_blocks = Block.objects.filter(site=site)
							else:
								if equipment_section:
									rem_blocks = EquipmentSection.objects.filter(id=equipment_section.id)
								else:
									rem_blocks = EquipmentSection.objects.filter(equipment=equipment)
					freq = int((365-total_block)/total_block)
					date = date + datetime.timedelta(days=freq)
				
				kk = kk+1
				'''
			messages.success(request, 'PM Schedule Created Successfully!')
			#return HttpResponseRedirect('/pm/schedules/')

	return render(request,'prevmain/create_pm_schedule.html',{'form':form},)


@login_required
def site_pm_alerts(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	form  = ScheduleSearchForm(sites)
	site_id = request.session.get('current_site')
	try:
		site = EnergySite.objects.get(pk=site_id)
	except:
		site = sites[0]

	today = PMAlert.objects.filter(site__in=sites,due_date=today_date)
	upcoming = PMAlert.objects.filter(site__in=sites,due_date__gt=today_date,due_date__month=today_date.month).order_by('due_date')
	due = PMAlert.objects.filter(site__in=sites,due_date__lte=today_date).filter(~Q(status= "completed")).order_by('due_date')
	completed = PMAlert.objects.filter(site__in=sites,due_date__lte=today_date,due_date__month=today_date.month,status="completed").order_by('due_date')


	if request.method == "POST":
		form = ScheduleSearchForm(sites=sites,data=request.POST)
		if form.is_valid():
			site = form.cleaned_data['site']
			equipment = form.cleaned_data['equipment']
			text = "Showing results for Plant - %s" % site.name
			messages.success(request, text)
			if equipment:
				today = PMAlert.objects.filter(site=site,due_date=today_date,pmschedule__equipment=equipment)
				upcoming = PMAlert.objects.filter(site=site,due_date__gt=today_date,pmschedule__equipment=equipment)
				due = PMAlert.objects.filter(site=site,due_date__lt=today_date,pmschedule__equipment=equipment).filter(~Q(status= "completed"))
				completed = PMAlert.objects.filter(site=site,due_date__lt=today_date,status="completed",pmschedule__equipment=equipment)


			else:
				today = PMAlert.objects.filter(site=site,due_date=today_date)
				upcoming = PMAlert.objects.filter(site=site,due_date__gt=today_date)
				due = PMAlert.objects.filter(site=site,due_date__lt=today_date).filter(~Q(status= "completed"))
				completed = PMAlert.objects.filter(site=site,due_date__lt=today_date,status="completed")
	return render(request,'prevmain/site_pm_alerts.html',{'today':today,'upcoming':upcoming,'due':due,'completed':completed,'form':form},
		  				)



@login_required
def pmschedules(request):
	usercompany = UserCompany.objects.get(user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)


	events = PMAlert.objects.filter(pmschedule__site__in=sites)
	form  = ScheduleSearchForm(sites)
	if request.method == "POST":
		form = ScheduleSearchForm(sites=sites,data=request.POST)
		if form.is_valid():
			events = PMAlert.objects.filter(pmschedule__site=form.cleaned_data['site'])
			equipment = form.cleaned_data['equipment']
			if equipment != "":
				events = events.filter(pmschedule__equipment=equipment)
			return render(request,'prevmain/schedule.html',{'events':events,'form':form},
		  				)

	return render(request,'prevmain/pmschedules.html',{'events':events,'form':form},
		  				)

@login_required
def full_schedule(request):
	usercompany = UserCompany.objects.get(user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)
	equipments = Equipment.objects.all()
	reports = []
	form = FullScheduleForm(sites=sites)
	if request.method == "POST":
		form = FullScheduleForm(sites=sites,data=request.POST)
		if form.is_valid():
			site = form.cleaned_data['site']
			years = form.cleaned_data['year']
			from_date = datetime.date(int(years.start),4,1)
			to_date = datetime.date(int(years.end),3,31)
			for equipment in equipments:
				data = {}
				data['equipment'] =   equipment
				data['alerts'] = PMAlert.objects.filter(pmschedule__equipment=equipment,site=site,due_date__range=[from_date,to_date])
				reports.append(data)
	return render(request,'prevmain/full_schedule.html',{'reports':reports,'form':form},
			)






@login_required
def update_pm(request,pm_id):
	pm_alert = get_object_or_404(PMAlert,pk=pm_id)
	form = PMAlertForm(instance=pm_alert)
 	date = datetime.datetime.now().date()
	days = (date - pm_alert.due_date).days

    	qset = PMChecklist.objects.filter(pm_alert=pm_alert) #or however your getting your Points to modify
	if not qset:
		try:
			checks = ChecklistItem.objects.filter(equipment_catagory = pm_alert.equipment.equipment_catagory,pm_type=pm_alert.pmschedule.pm_type)
			for check in checks:
				pmchecklist = PMChecklist(pm_alert = pm_alert,checklist = check,is_completed = False)
				pmchecklist.save()
		except:
			pass

	if days >=3:
		form = PMAlertDelayForm(instance=pm_alert)

	if request.method=="POST":
		if days >=3:
			form = PMAlertDelayForm(data=request.POST,files=request.FILES,instance=pm_alert)
		else:
			form = PMAlertForm(data=request.POST,files=request.FILES,instance=pm_alert)
		if form.is_valid():

			pm = form.save(commit=False)
			pm.completed_date = datetime.datetime.now()
			pm.user = request.user
			pm.save()

			messages.success(request, 'PM updated successfully!')
	return render(request,'prevmain/update_pm_alert.html',{'form':form,'pm':pm_alert},
		  				)

@login_required
def view_pmalert(request,pm_id):
	pm_alert = get_object_or_404(PMAlert,pk=pm_id)
	return render(request,'prevmain/view_pmalert.html',{'pm':pm_alert},


		  				)

@login_required
def reports(request):
	usercompany = UserCompany.objects.get(user=request.user)
	sites = get_user_sites(request.user)
	form = ReportForm(sites=sites)

	type = ""
    	if request.GET.get('type'):
		type = request.GET['type']

		if type == "generation":
			TYPES = (
			   ('dgr','Generation'),
			   ('radiation','Insolation'),
			   ('cleaning','Module Cleaning'),
			  )

			form.fields['report_type'].choices = TYPES
		if type == "breakdown":
			TYPES = (
			   ('breakdown','Plant Down'),
			   ('griddown','Grid Down'),
			  )

			form.fields['report_type'].choices = TYPES
		if type == "pm":
			TYPES = (
			   ('pm','PM'),
			   ('slippage','Slippage'),
			  )

			form.fields['report_type'].choices = TYPES
		if type == "reminders":
			TYPES = (
			   ('reminders','Reminders'),
			  )

			form.fields['report_type'].choices = TYPES
		if type == "ort":
			TYPES = (
			   ('ort','ORT Agenda'),
			  )

			form.fields['report_type'].choices = TYPES

			FORMATS = (
			   ('daily','Daily'),
			  )

			form.fields['format'].choices = FORMATS



	if request.method == "POST":
		initial = {}
		for key in request.POST.iterkeys():
			initial[key] = request.POST.get(key)

		initial = simplejson.dumps(initial)

		#saved_search.apply_async(kwargs={'initial':initial,'user':request.user,'url':request.get_full_path()})

		form = ReportForm(sites=sites,data=request.POST)
		if form.is_valid():
			from_date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			if not to_date:
				to_date = from_date
			if form.cleaned_data['site']:
				site = form.cleaned_data['site']
				sites = EnergySite.objects.filter(id = form.cleaned_data['site'].id)
			else:

				site = 'All'
			type = form.cleaned_data['report_type']
			format = form.cleaned_data['format']
			equipment = form.cleaned_data['equipment']
			if format == "yearly":
				year = form.cleaned_data['year']
				allmonths = []
				months = ['January','February','March','April','May','June','July','August','September','October','November','December']
				i = 0
				while i <= 11:

					reports = {}
					reports['month'] = months[i]
					if type == "slippage":
						reports['pmalerts'] = PMAlert.objects.filter(site__in=sites,due_date__month=i+1,due_date__year=year).filter(Q(due_date__lt=F('completed_date'))| Q(due_date__lt=datetime.datetime.now())).order_by('due_date')

					if type == "history":
						reports['pmalerts'] = PMAlert.objects.filter(pmschedule__equipment = equipment,site__in=sites,due_date__month=i+1,due_date__year=year)

					else:
						reports['pmalerts'] = PMAlert.objects.filter(site__in=sites,due_date__month=i+1,due_date__year=year)
					if reports['pmalerts']:
						reports['total_completed'] = reports['pmalerts'].count()
					allmonths.append(reports)
					i = i +1
				if type == "slippage":
					return render(request,'prevmain/reports_pmalerts_slippage.html',{'form':form,'reports':allmonths,'format':format,'year':year,'report_site': site},
						)
				else:
					return render(request,'prevmain/reports_pmalerts.html',{'form':form,'reports':allmonths,'format':format,'year':year,'report_site': site},
						)

			if format == "yearwise":
				from_year = int(form.cleaned_data['from_year'])
				to_year = int(form.cleaned_data['to_year'])
				year = from_year
				allmonths = []
				while year <= to_year:

					reports = {}
					reports['month'] = year
					if type == "slippage":
						reports['pmalerts'] = PMAlert.objects.filter(site__in=sites,due_date__year=year).filter(Q(due_date__lt=F('completed_date'))| Q(due_date__lt=datetime.datetime.now())).order_by('due_date')

					if type == "history":

						reports['pmalerts'] = PMAlert.objects.filter(pmschedule__equipment=equipment,site__in=sites,due_date__year=year)
					else:

						reports['pmalerts'] = PMAlert.objects.filter(site__in=sites,due_date__year=year)
					if reports['pmalerts']:
						reports['total_completed'] = reports['pmalerts'].count()
					allmonths.append(reports)
					year = year +1
				if type == "slippage":
					return render(request,'prevmain/reports_pmalerts_slippage.html',{'form':form,'reports':allmonths,'format':format,'from_year':from_year,'to_year':to_year,'report_site': site},
						)
				else:
					return render(request,'prevmain/reports_pmalerts.html',{'form':form,'reports':allmonths,'format':format,'from_year':from_year,'to_year':to_year,'report_site': site},
						)


			if format == "daily":


				if type == "slippage":
					allmonths = PMAlert.objects.filter(site__in=sites,due_date=from_date).filter(Q(due_date__lt=F('completed_date'))| Q(due_date__lt=datetime.datetime.now())).order_by('due_date')

					return render(request,'prevmain/reports_pmalerts_daily_slippage.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
					)
				if type == "history":

					allmonths = PMAlert.objects.filter(pmschedule__equipment=equipment,site__in=sites,due_date=from_date).order_by('due_date')
				else:

					allmonths = PMAlert.objects.filter(site__in=sites,due_date=from_date).order_by('due_date')
				return render(request,'prevmain/reports_pmalerts_daily.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
					)


			if format == "monthly":

				if type == "slippage":
					allmonths = PMAlert.objects.filter(site__in=sites,due_date__range=[from_date,to_date]).filter(Q(due_date__lt=F('completed_date'))| Q(due_date__lt=datetime.datetime.now())).order_by('due_date')
					return render(request,'prevmain/reports_pmalerts_daily_slippage.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
					)
				if type == "history":

					allmonths = PMAlert.objects.filter(pmschedule__equipment=equipment,site__in=sites,due_date__range=[from_date,to_date]).order_by('due_date')
					return render(request,'prevmain/reports_pmalerts_daily.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
					)


				else:
					allmonths = PMAlert.objects.filter(site__in=sites,due_date__range=[from_date,to_date]).order_by('due_date')
					return render(request,'prevmain/reports_pmalerts_daily.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
					)

		else:
			print form.errors
		return render(request,'prevmain/reports_pmalerts.html',{'form':form,},
				)

	return render(request,'prevmain/reports.html',{'sites':sites,'form':form,'type':type},
				)





def create_pm_alert(sender, instance, **kwargs):
	frequency = instance.frequency
	date = instance.date
	site = instance.site
	start_date = datetime.date(int(instance.financial_year.start),4,int(date))
	end_date = datetime.date(int(instance.financial_year.end),3,int(date))
	if frequency == "every_month":
		day = datetime.timedelta(days=30)
		while start_date <= end_date:
			pmalert = PMAlert(pmschedule=instance,site=site,status="due",due_date=start_date)
			pmalert.save()
			start_date = start_date+day
	if frequency == "every_quarter":
		day = datetime.timedelta(days=90)
		while start_date <= end_date:
			pmalert = PMAlert(pmschedule=instance,site=site,status="due",due_date=start_date)
			pmalert.save()
			start_date = start_date+day
	if frequency == "every_six_months":
		day = datetime.timedelta(days=180)
		while start_date <= end_date:
			pmalert = PMAlert(pmschedule=instance,site=site,status="due",due_date=start_date)
			pmalert.save()
			start_date = start_date+day
	if frequency == "every_year":
		day = datetime.timedelta(days=365)
		while start_date <= end_date:
			pmalert = PMAlert(pmschedule=instance,site=site,status="due",due_date=start_date)
			pmalert.save()
			start_date = start_date+day


from django.db.models.signals import post_save

# register the signal
#post_save.connect(create_pm_alert, sender=PMSchedule, dispatch_uid="pm_alert")

def pmstatus_change(sender, instance, **kwargs):
	created = datetime.datetime.now()
	sites = UserSite.objects.filter(site=instance.site)
	for site in sites:
		if site.site.notificationsettings.pm_status_change:
			notification = "Status of PM for %s, due on %s is changed to %s by %s. <br> Remarks: %s" % (instance.pmschedule.equipment,instance.due_date,instance.status,instance.user,instance.remarks)
			notice = SiteNotification(notification=notification,site=site.site,is_read=False,created=created,user=site.user)
			notice.save()
			message_body = notification

        		html_content = render_to_string("notifications/notification_email.html", {"user":site.user,"notice":message_body})
			send_mail('[AkshyaPower] : PM Status Change Notification', message_body, FROM_EMAIL,
				    [site.user.email], fail_silently=False,html_message=html_content)




post_save.connect(pmstatus_change, sender=PMAlert, dispatch_uid="pm_status_change")




@login_required
def pmr(request):
	usercompany = UserCompany.objects.get(user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)


	form  = PMRForm()
	if request.method == "POST":
		form = PMRForm(data=request.POST)
		if form.is_valid():
			pm = form.cleaned_data['schedule']
			create_pm(pm)
			return render(request,'prevmain/pmr.html',{'form':form},
		  				)

	return render(request,'prevmain/pmr.html',{'form':form},
		  				)



def summary_xl(sites,range="weekly"):

	if range == "weekly":

		start_date = datetime.datetime.now().date() - datetime.timedelta(days=5)
	if range == "mtd":
		start_date = datetime.date(datetime.datetime.now().date().year,datetime.datetime.now().date().month,1)
	end_date = datetime.datetime.now().date()
	reports = []
	for site in sites:
		data = {}
		due = PMAlert.objects.filter(site=site,due_date__lte=end_date,due_date__gte=start_date).filter(~Q(status= "completed")).count()
		completed = PMAlert.objects.filter(site=site,due_date__lte=end_date,status="completed",due_date__gte=start_date).count()
		total = PMAlert.objects.filter(site=site,due_date__lte=end_date,due_date__gte=start_date).count()
		data['due']  = due
		data['total'] = total
		data['completed'] = completed
		data['site'] = site
		data['comp_per']  = round((float(completed)/float(total))*100.00,2)
		reports.append(data)
	due = PMAlert.objects.filter(site__in=sites,due_date__lte=end_date,due_date__gte=start_date).filter(~Q(status= "completed")).count()
	completed = PMAlert.objects.filter(site__in=sites,due_date__lte=end_date,status="completed",due_date__gte=start_date).count()
	total = PMAlert.objects.filter(site__in=sites,due_date__lte=end_date,due_date__gte=start_date).count()
	comp_per = round((float(completed)/float(total))*100.00,2)

	date_range = '%s to %s' % (start_date.strftime('%d-%m-%Y'),end_date.strftime('%d-%m-%Y'))
	date = datetime.datetime.now().date()	
	file_date = "Weekly-PM-Summary-"+str(date.strftime('%d-%m-%Y'))
        filename = "xlssheets/" + "%s.xlsx" % (file_date)
        xl_url = settings.MEDIA_URL + filename
        workbook = xlsxwriter.Workbook(settings.MEDIA_ROOT + filename)
        worksheet = workbook.add_worksheet("PM Summary")

        worksheet.set_column(0, 106, 15)
        worksheet.set_row(2, 20)

        format = workbook.add_format({'bold': True})
        format.set_bg_color("#00ccff")
        format.set_align('center')
        format.set_align('vcenter')
        format.set_border(1)


        format2 = workbook.add_format({'bold': True})
        format2.set_bg_color("#ff9900")
        format2.set_align('center')
        format2.set_align('vcenter')
        format2.set_border(1)

        format3 = workbook.add_format({'bold': True})
        format3.set_bg_color("#ff9900")
        format3.set_align('center')
        format3.set_align('vcenter')
        format3.set_border(1)

        row = 0
        col = 0

 	worksheet.merge_range(
                         row, 0, row, 5, "PM Summary Report", format2)


	worksheet.write(row+1,col,"Date:",format3)
	worksheet.write(row+1,col+1,date_range,format3)

 	worksheet.merge_range(
                         row+1, 1, row+1,5, date_range, format3)


	row = 2
	worksheet.write(row,col,"Site",format)
	worksheet.write(row,col+1,"Zone",format)
	worksheet.write(row,col+2,"Total PM Task",format)
	worksheet.write(row,col+3,"Completed",format)
	worksheet.write(row,col+4,"Pending",format)
	worksheet.write(row,col+5,"% Completed",format)
	row = row+1
	for report in reports:
		worksheet.write(row,col,report['site'].city)
		worksheet.write(row,col+1,report['site'].zone)
		worksheet.write(row,col+2,report['total'])
		worksheet.write(row,col+3,report['completed'])
		worksheet.write(row,col+4,report['due'])
		worksheet.write(row,col+5,report['comp_per'])
		row = row+1
	worksheet.write(row,col,"Total:")
	worksheet.write(row,col+1,"-")
	worksheet.write(row,col+2,total)
	worksheet.write(row,col+3,completed)
	worksheet.write(row,col+4,due)
	worksheet.write(row,col+5,comp_per)

        workbook.close()  # workbook close
	return {'xl_url':xl_url,'reports':reports}

@login_required
def pm_summary(request):
	usercompany = UserCompany.objects.get(user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	if request.GET['report'] == "aging":
		xl_url = aging_xl(sites,"mtd")
	else:
		
		xl_url = summary_xl(sites)
	
	return HttpResponseRedirect(xl_url['xl_url'])




def aging_xl(sites,range="weekly"):

	if range == "weekly":

		start_date = datetime.datetime.now().date() - datetime.timedelta(days=5)
	if range == "mtd":
		start_date = datetime.date(datetime.datetime.now().date().year,datetime.datetime.now().date().month,1)
	end_date = datetime.datetime.now().date()
	reports = []
	today = datetime.datetime.now().date()
	dues = PMAlert.objects.filter(site__in=sites,due_date__lte=end_date,due_date__gte=start_date).filter(~Q(status= "completed"))
	for due in dues:
		data = {}
		if (today - due.due_date ).days >= 3:
			data['age'] = (due.due_date - today)
			data['alert'] = due
			reports.append(data)

	date_range = '%s to %s' % (start_date.strftime('%d-%m-%Y'),end_date.strftime('%d-%m-%Y'))
	date = datetime.datetime.now().date()	
	file_date = "Aging-"+str(date.strftime('%d-%m-%Y'))
        filename = "xlssheets/" + "%s.xlsx" % (file_date)
        xl_url = settings.MEDIA_URL + filename
        workbook = xlsxwriter.Workbook(settings.MEDIA_ROOT + filename)
        worksheet = workbook.add_worksheet("PM Aging Summary")

        worksheet.set_column(0, 106, 15)
        worksheet.set_row(2, 20)

        format = workbook.add_format({'bold': True})
        format.set_bg_color("#00ccff")
        format.set_align('center')
        format.set_align('vcenter')
        format.set_border(1)


        format2 = workbook.add_format({'bold': True})
        format2.set_bg_color("#ff9900")
        format2.set_align('center')
        format2.set_align('vcenter')
        format2.set_border(1)

        format3 = workbook.add_format({'bold': True})
        format3.set_bg_color("#ff9900")
        format3.set_align('center')
        format3.set_align('vcenter')
        format3.set_border(1)

        row = 0
        col = 0

 	worksheet.merge_range(
                         row, 0, row, 5, "Due PM Aging Report", format2)


	worksheet.write(row+1,col,"Date:",format3)
	worksheet.write(row+1,col+1,date_range,format3)

 	worksheet.merge_range(
                         row+1, 1, row+1,5, date_range, format3)


	row = 2
	worksheet.write(row,col,"Site",format)
	worksheet.write(row,col+1,"Zone",format)
	worksheet.write(row,col+2,"Equipment",format)
	worksheet.write(row,col+3,"Block",format)
	worksheet.write(row,col+4,"Due Date",format)
	worksheet.write(row,col+5,"Delay in Days",format)
	row = row+1
	for report in reports:
		worksheet.write(row,col,report['alert'].site.city)
		worksheet.write(row,col+1,report['alert'].site.zone)
		worksheet.write(row,col+2,report['alert'].equipment.name)
		try:
			worksheet.write(row,col+3,report['alert'].pmschedule.block.block)
		except:
			worksheet.write(row,col+3,"-")
			
		worksheet.write(row,col+4,str(report['alert'].due_date))
		worksheet.write(row,col+5,report['age'])
		row = row+1

        workbook.close()  # workbook close
	return {'xl_url':xl_url,'reports':reports}

@login_required
def pm_save_checklist(request,pm_alert):
	#PMChecklistFormSet = inlineformset_factory(PMAlert,PMChecklist,exclude = ('pm_alert',))
	alert_pm = PMAlert.objects.get(pk = pm_alert)
	#formset = PMChecklistFormSet(instance = alert_pm)


	PMChecklistFormSet = modelformset_factory(PMChecklist,exclude=('pm_alert',),form=PMChecklistForm)
    	qset = PMChecklist.objects.filter(pm_alert=alert_pm) #or however your getting your Points to modify
	if not qset:
		checks = ChecklistItem.objects.filter(equipment_catagory = alert_pm.equipment.equipment_catagory,pm_type=alert_pm.pmschedule.pm_type)
		for check in checks:
			pmchecklist = PMChecklist(pm_alert = alert_pm,checklist = check,is_completed = False)
			pmchecklist.save()
	
    	qset = PMChecklist.objects.filter(pm_alert=alert_pm) #or however your getting your Points to modify
    	formset = PMChecklistFormSet(queryset = qset)

	if request.method == "POST":

    		formset = PMChecklistFormSet(queryset = qset,data=request.POST)
		if formset.is_valid():
			formset.save()
			#url = "/pm/checklist/%d/" % alert_pm.id
			#return HttpResponseRedirect(url)	
	return render(request,'prevmain/checklist.html',{'formset':formset,'pm':alert_pm})

@login_required
def checklists(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	checklists = ChecklistItem.objects.all()
	return render(request,'prevmain/checklists.html',{'checklists':checklists})





@login_required
def pm_checklists(request,pm_alert):
	pm_alert = PMAlert.objects.get(pk=pm_alert)
	usercompany = get_object_or_404(UserCompany,user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	checklists = PMChecklist.objects.filter(pm_alert=pm_alert)
	return render(request,'prevmain/pm_checklists.html',{'checklists':checklists,'pm':pm_alert})
