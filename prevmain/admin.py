'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, 
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE 
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.                
'''




from django.contrib import admin
from prevmain.models import *
import bulk_admin

admin.site.register(Location)
admin.site.register(EquipmentSection)
admin.site.register(Years)
admin.site.register(EnergySite)
admin.site.register(Company)
admin.site.register(UserSite)
admin.site.register(Block)
admin.site.register(NotificationSettings)
admin.site.register(PMChecklist)
admin.site.register(PossibleValue)
admin.site.register(EquipmentCatagory)

@admin.register(PMSchedule)
class PMScheduleAdmin(admin.ModelAdmin):
        list_display = ('site','frequency','start_date',)
	list_filter = ('site',)

@admin.register(Equipment)
class EquipmentAdmin(admin.ModelAdmin):
        list_display = ('name','equipment_catagory',)
	list_filter = ('equipment_catagory',)
	list_editable = ('equipment_catagory',)



@admin.register(PMAlert)
class PMAlertAdmin(admin.ModelAdmin):
        list_display = ('site','pmschedule','due_date','status')
	list_filter = ('site','pmschedule','status',)

@admin.register(UserCompany)
class UserCompanyAdmin(admin.ModelAdmin):
        list_display = ('user','company','role','zone')
	list_filter = ('company',)


@admin.register(ChecklistItem)
class ChecklistItemAdmin(bulk_admin.BulkModelAdmin):
        list_display = ('checklist_name','pm_type','equipment_catagory')
        list_filter = ('pm_type','equipment_catagory')
