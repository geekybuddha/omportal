'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
'''




from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from smart_selects.db_fields import ChainedForeignKey

class Company(models.Model):
	name = models.CharField(max_length=50,help_text="Your company's registered name for the records")
	website = models.URLField(max_length=255,help_text="Please include http in the url ")
	contact_email = models.EmailField(max_length=255, help_text="Enter a valid email address of contact person")
	contact_number = models.CharField(help_text="Enter Mobile Number of contact person",max_length=10)
	city = models.CharField(max_length=255, help_text="Enter city where head office is located")
	country = models.CharField(max_length=255,help_text="Enter the country name")
	address = models.TextField(help_text="Enter full address for the records")
	logo = models.ImageField(upload_to="logos")
	def __unicode__(self):
                return '%s' % (self.name)



class EnergySite(models.Model):
	name = models.CharField(max_length=255,help_text="Please enter name for the site")
	city = models.CharField(max_length=255,help_text="Please enter city/town name for the site")
        location = models.CharField(max_length=100,help_text="Please click on the location in the map to get co-ordiantes")
	capacity = models.FloatField(help_text="Please enter site DC capacity in MW")
	ac_capacity = models.FloatField(help_text="Please enter site AC capacity in MW")
	tvm_mf = models.FloatField(help_text="Please enter multiplication factor to be used in DGR calculuation")
	no_of_blocks = models.IntegerField(help_text="Please enter no. of blocks")
	ppa = models.FloatField(help_text="Enter PPA price in Rs. per unit",verbose_name="Tariff")
	address = models.TextField(help_text="Please enter full site address for the communication")
	STATES = (
		('Andaman and Nicobar Islands', 'Andaman and Nicobar Islands'),
		('Andhra Pradesh' ,'Andhra Pradesh'),
		('Arunachal Pradesh','Arunachal Pradesh'),
		('Assam','Assam'),
		('Bihar	','	Bihar'),
		('Chandigarh','Chandigarh'),
		('Chhattisgarh','	Chhattisgarh'),
		('Dadra and Nagar Haveli','	Dadra and Nagar Haveli'),
		('Daman and Diu','	Daman and Diu'),
		('Delhi','	Delhi'),
		('Goa','Goa'),
		('Gujarat','Gujarat'),
		('Haryana','Haryana'),
		('Himachal Pradesh','	Himachal Pradesh'),
		('Jammu and Kashmir','	Jammu and Kashmir'),
		('Jharkhand','Jharkhand'),
		('Karnataka','Karnataka '),
		('Kerala','Kerala'),
		('Lakshadweep','Lakshadweep'),
		('Madhya Pradesh','	Madhya Pradesh'),
		('Maharashtra','Maharashtra'),
		('Manipur','	Manipur'),
		('Meghalaya','	Meghalaya'),
		('Mizoram','	Mizoram'),
		('Nagaland','	Nagaland'),
		('Orissa','	Orissa'),
		('Pondicherry','	Pondicherry'),
		('Punjab','	Punjab'),
		('Rajasthan','	Rajasthan'),
		('Sikkim','	Sikkim'),
		('Tamil Nadu','	Tamil Nadu'),
		('Tripura','	Tripura'),
		('Uttaranchal','	Uttaranchal'),
		('Uttar Pradesh','	Uttar Pradesh'),
		('West Bengal','	West Bengal'),
	)
	state = models.CharField(choices=STATES,max_length=100)
	company = models.ForeignKey(Company)
	is_grouped = models.BooleanField(default=False)
	cubicles_per_inverter = models.IntegerField(default=1)
	datalogger_site_name = models.CharField(max_length=100)
        monitoring_url = models.URLField(default="https://scada.akshyapower.com")
        site_picture = models.ImageField(upload_to="site_pictures")
      	zone = models.CharField(default="none",max_length=100)
	def __unicode__(self):
                return '%s' % (self.city)

class Block(models.Model):
	site = models.ForeignKey(EnergySite)
	company = models.ForeignKey(Company)
	block = models.CharField(max_length=255)
	def __unicode__(self):
                return "%s" % self.block

class BlockExtras(models.Model):
	block = models.OneToOneField(Block)
	ac_capacity = models.FloatField()
	dc_capacity = models.FloatField()



class UserSite(models.Model):
	user = models.ForeignKey(User)
	site = models.ForeignKey(EnergySite,help_text="Choose the site you want to assign to this user")
	ROLES = (('manager','Manager'),
		 ('incharge','Incharge'),
		 ('client','Client'),
		)
	role = models.CharField(choices=ROLES,max_length=50,help_text="Choose the role of the user")


class UserCompany(models.Model):
	user = models.OneToOneField(User)
	company = models.ForeignKey(Company)
	is_viewer = models.BooleanField(default=False)
	is_admin = models.BooleanField(default=False)
	is_stock_admin = models.BooleanField(default=False)
	is_client = models.BooleanField(default=False)
        ROLES = (('manager','Manager'),
		 ('incharge','Incharge'),
		 ('zonal_head','zonal_head'),
		 ('client','Client'),
		)
	role = models.CharField(choices=ROLES,max_length=50,help_text="Choose the role of the user")
        zone = models.CharField(max_length=100,null=True,blank=True)





class SiteNotification(models.Model):
	site = models.ForeignKey(EnergySite)
	notification = models.CharField(max_length=255)
	details = models.TextField(blank=True)
	action_url = models.CharField(max_length=255,blank=True)
	is_read = models.BooleanField(default=False)
	created = models.DateTimeField()
	user = models.ForeignKey(User)
	category = models.CharField(max_length=255,blank=True,null=True)
	def __unicode__(self):
                return 'site_alert'



class Location(models.Model):
	name = models.CharField(max_length=40)
	def __unicode__(self):
                return '%s' % (self.name)

class EquipmentCatagory(models.Model):
        name = models.CharField(max_length = 100)
        def __unicode__(self):
                return '%s' % (self.name)

class Equipment(models.Model):
	name = models.CharField(max_length=40)
	location = models.ForeignKey(Location)
	site = models.ForeignKey(EnergySite,blank=True,null=True)
	equipment_catagory = models.ForeignKey(EquipmentCatagory, blank = True, null = True)
	def __unicode__(self):
                return '%s' % (self.name)

	


class Section(models.Model):
	location =models.ForeignKey(Location)
	name = models.CharField(max_length=40)
	def __unicode__(self):
                return '%s' % (self.name)

class EquipmentSection(models.Model):
	equipment =models.ForeignKey(Equipment)
	name = models.CharField(max_length=40)
	def __unicode__(self):
                return '%s' % (self.name)



class Years(models.Model):
	YEAR = (('2013','2013'),
		 ('2014','2014'),
		 ('2015','2015'),
		 ('2016','2016'),
		 ('2017','2017'),
		 ('2018','2018'),
		 ('2019','2019'),
		 ('2020','2020'),
		 ('2021','2021'),
		 ('2022','2022'),
		 ('2023','2023'),
		 ('2024','2024'),
		 ('2025','2025'),
		)

	start = models.CharField(choices=YEAR,max_length=50)
	end = models.CharField(choices=YEAR,max_length=50)
	def __unicode__(self):
                return '%s - %s' % (self.start,self.end)


class PMSchedule(models.Model):
	financial_year = models.ForeignKey(Years)
	site = models.ForeignKey(EnergySite)
        block = models.ForeignKey(Block,
	blank=True,
	null=True
        )


	#location = models.ForeignKey(Location)
        equipment = models.ManyToManyField(
        Equipment,
        )
	section = ChainedForeignKey(
        Section,
        chained_field="location",
        chained_model_field="location",
        show_all=False,
        auto_choose=True,
	blank=True,
	null=True
        )


	equipment_section = ChainedForeignKey(
        EquipmentSection,
        chained_field="equipment",
        chained_model_field="equipment",
        show_all=False,
        auto_choose=True,
	blank=True,
	null=True
        )


	details = models.CharField(max_length=100)
	FREQ = (
		('daily','daily'),
		('weekly','weekly'),
		('fortnightly','Fortnightly'),
		 ('monthly','Monthly'),
		 ('quarterly','Quarterly'),
		 ('halfyearly','Halfyearly'),
		 ('yearly','Yearly'),
		 ('one_time','one_time'),
		)
	frequency = models.CharField(choices=FREQ,max_length=50)
	pm_type = models.CharField(choices=FREQ,max_length=50, blank = True, null = True)
	DATES = (('1','1'),
		 ('2','2'),
		 ('3','3'),
		 ('4','4'),
		 ('5','5'),
		 ('6','6'),
		 ('7','7'),
		 ('8','8'),
		 ('9','9'),
		 ('10','10'),
		 ('11','11'),
		 ('12','12'),
		 ('13','13'),
		 ('14','14'),
		 ('15','15'),
		 ('16','16'),
		 ('17','17'),
		 ('18','18'),
		 ('19','19'),
		 ('20','20'),
		 ('21','21'),
		 ('22','22'),
		 ('23','23'),
		 ('24','24'),
		 ('25','25'),
		 ('26','26'),
		 ('27','27'),
		 ('28','28'),
		 ('29','29'),
		 ('30','30'),
		)
	start_date = models.DateField()


	procedure = models.FileField(upload_to="procedures")
	checklist = models.FileField(upload_to="checklist")
	company = models.ForeignKey(Company)
	created_by_user = models.ForeignKey(User)
	is_blockwise = models.BooleanField(default=False)
	def __unicode__(self):
                return '%s-%s-%s' % (self.site.name,self.equipment.name,str(self.start_date))

class PMAlert(models.Model):
	pmschedule = models.ForeignKey(PMSchedule)
	site = models.ForeignKey(EnergySite)
	LEVELS = (('due','due'),
		 ('in_progress','In Progress'),
		 ('delayed','Delayed'),
		 ('completed','Completed'),)
	status = models.CharField(choices = LEVELS,max_length=40)
	due_date = models.DateField()
	equipment = models.ForeignKey(Equipment,null=True,blank=True)
	report = models.FileField(upload_to="pmreports",null=True,blank=True)
	observations = models.TextField(blank=True,verbose_name="Abnormal Observations")
	corrective_action = models.TextField(blank=True)
	reason_for_delay = models.TextField(blank=True,null=True)


	user = models.ForeignKey(User,null=True,blank=True)
	remarks = models.TextField(blank=True)
	completed_date = models.DateField(null=True,blank=True)
	block = models.ForeignKey(Block,null=True,blank=True)
	section = models.ForeignKey(EquipmentSection,null=True,blank=True)
        spares_used = models.BooleanField(default=False)
	created = models.DateTimeField(auto_now=True)
	def __unicode__(self):
                return '%s - %s - %s' % (self.site,self.pmschedule.equipment,self.due_date)
	@property
	def checklist_items(self):
		return PMChecklist.objects.filter(pm_alert=self).count()
	@property
	def due_checklist_items(self):
		return PMChecklist.objects.filter(pm_alert=self,is_completed=False).count()



class CompletedPM(models.Model):
	pmalert = models.ForeignKey(PMAlert)
	user = models.ForeignKey(User)
	site = models.ForeignKey(EnergySite)
	date_time = models.DateTimeField()


def generate_pm_alert(sender, instance, **kwargs):
	company = instance.company
	sites = EnergySite.objects.filter(company=company)
	pmalert = PMAlert(site=instance.site,status='due',due_date=instance.start_date,pmschedule=instance)
	pmalert.save()
class NotificationSettings(models.Model):
	site = models.OneToOneField(EnergySite)
	pm_status_change = models.BooleanField(default=False)


class PMR(models.Model):
	schedule = models.ForeignKey(PMSchedule)


class ChecklistItem(models.Model):
	checklist_name =  models.TextField(help_text="Enter the checklist of PM task")
	equipment_catagory = models.ForeignKey(EquipmentCatagory, help_text = "Enter equipment catagory")
        correct_value = models.CharField(max_length=100, help_text= "Correct value of the related PM checklist")
	TYPES = (('daily','Daily'),
                 ('weekly','Weekly'),
                 ('fortnightly','Fortnightly'),
		 ('monthly','Monthly'),
		 ('quarterly','Quarterly'),
                 ('halfyearly','Halfyearly'),
                 ('yearly','Yearly'),
		 ('premonsoon','PreMonsoon'),
                 ('2_3year','2_3Year'),
                                 
		)
        pm_type = models.CharField(choices=TYPES,max_length=50,help_text="Choose frequency type of the PM")
	def __unicode__(self):
		return '%s' % (self.checklist_name)

class PossibleValue(models.Model):
	value = models.CharField(max_length=100,help_text="Choose from predefined values")
	checklist = models.ForeignKey(ChecklistItem,null=True,blank=True)
	def __unicode__(self):
		return '%s' % (self.value)


class PMChecklist(models.Model):
	pm_alert = models.ForeignKey(PMAlert)
	checklist = models.ForeignKey(ChecklistItem)
	user_input_value = models.ForeignKey( PossibleValue,null=True,blank=True, help_text= "Enter the status of PM checklist",verbose_name="Status")
	remark = models.CharField(max_length=200,help_text="Enter remark of related PM checklist", blank = True, null = True)
	is_completed = models.BooleanField(default = False)
	def __unicode__(self):
		return '%s' % (self.checklist)
def connect_checklist(sender, instance, created, **kwargs):
    if created:
	checklists = ChecklistItem.objects.filter(equipment_catagory=instance.equipment.equipment_catagory, pm_type= instance.pmschedule.pm_type)
	#checklists = ChecklistItem.objects.filter(equipment_catagory=instance.equipment.equipment_catagory) # pm_type= instance.pmschedule.pm_type)
	
	for check in checklists:

		pmchecklist = PMChecklist(pm_alert= instance, checklist= check,is_completed=False)
		pmchecklist.save()


post_save.connect(connect_checklist, sender=PMAlert)


def connect_possible_value(sender, instance, created, **kwargs):
    if created:
	possible_value = PossibleValue(value=instance.correct_value,checklist=instance)
	possible_value.save()

	possible_value = PossibleValue(value="Issue detected",checklist=instance)
	possible_value.save()





post_save.connect(connect_possible_value, sender=ChecklistItem)





#post_save.connect(generate_pm_alert, sender=PMSchedule, dispatch_uid="generate_pm _alert")
