'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, 
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE 
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.                
'''




from django.conf.urls import url
from .views import *
urlpatterns = [
 
 
    url(r'^alerts/$', site_pm_alerts, name='pmalerts'),
    url(r'^schedules/$', pmschedules, name='pmschedules'),
    

    url(r'^update_pm/(?P<pm_id>\d+)/$', update_pm, name='update_pm'),
    url(r'^alert/(?P<pm_id>\d+)/$', view_pmalert, name='view_pmalert'),
    url(r'^pm_checklist/(?P<pm_alert>\d+)/$', pm_checklists, name='pm_checklist'),

    url(r'create/$', create_pm_schedule, name='create_pm'),
    url(r'^full_schedule/$', full_schedule, name='full-schedule'),
    url(r'^reports/$', reports, name='reports'),

    url(r'^checklist/(?P<pm_alert>\d+)/$',pm_save_checklist, name='checklist_pmalert'),

    url(r'^pmr/$', pmr, name='pmr'),
    url(r'^summary/$', pm_summary, name='pm_summary'),
    url(r'^checklists/$', checklists, name='checklists'),
] 
