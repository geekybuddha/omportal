from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
from energysite.models import *
import datetime

class DynamicChoiceField(forms.ChoiceField):
    def valid_value(self, value):
        return True
class CompanyForm(ModelForm):
	class Meta:
		model = Company
	        fields = '__all__' 


class EnergySiteForm(ModelForm):
	class Meta:
		model = EnergySite
		exclude = ('company',)

class TiltForm(ModelForm):
	class Meta:
		model = Tilt
		exclude = ('site',)


class HTPanelForm(ModelForm):
	class Meta:
		model = HTPanel
		exclude = ('site',)

class ModuleForm(ModelForm):
	class Meta:
		model = ConnectedModule
		exclude = ('inverter',)


class TransformerForm(ModelForm):
	class Meta:
		model = Transformer
		exclude = ('site',)
	def __init__(self, site, *args, **kwargs):
        	super(TransformerForm, self).__init__(*args, **kwargs)
        	self.fields['htpanel'].queryset=HTPanel.objects.filter(site=site)
	




class UserForm(forms.Form):
	username = forms.CharField(max_length=30)
	first_name = forms.CharField()
	last_name = forms.CharField()
	email=forms.EmailField()
	
	def clean_username(self): # check if username dos not exist before
    		try:
        		User.objects.get(username=self.cleaned_data['username']) #get user from user model
    		except User.DoesNotExist :
        		return self.cleaned_data['username']

    		raise forms.ValidationError("Sorry, this user exists already")


class UserSiteForm(ModelForm):
	class Meta:
		model = UserSite
		exclude = ('user',)
	def __init__(self, company,user, *args, **kwargs):
		self.user = user
        	super(UserSiteForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset=EnergySite.objects.filter(company=company)

	def clean_site(self):
		try:
        		UserSite.objects.get(user=self.user,site=self.cleaned_data['site']) 
    		except UserSite.DoesNotExist :
        		return self.cleaned_data['site']

    		raise forms.ValidationError("Sorry, this this site already assigned to this user")

class GenerationReportForm(forms.Form):
	TYPES = (
		   ('---','---'),
		   ('dgr','Generation'),
		   ('breakdown','Plantdown'),
		   ('griddown','Griddown'),
		   ('radiation','Insolation'),
		   ('pm','PM'),
		   ('slippage','Slippage'),
		   ('reminders','Reminders'),
		   ('cleaning','Module Cleaning'),
		   ('soiling','Soiling'),
		  )
	report_type= forms.ChoiceField(choices=TYPES,)

	site = forms.ModelChoiceField(queryset = None)
	CHOICES = (
		   ('---','---'),
		   ('daily','Daily'),
		   ('monthly','Day Range'),
		   ('yearly','Monthly'),
		   ('yearwise','Yearly'),
		  )
	format = forms.ChoiceField(choices=CHOICES,)
	YEARS = (('2012','2012'),
		   ('2013','2013'),
		   ('2014','2014'),
		   ('2015','2015'),
		   ('2016','2016'),
		   ('2017','2017'),
		   ('2018','2018'),
		   ('2019','2019'),
		   ('2020','2020'),
		   ('2021','2021'),
		   ('2022','2022'),
		   ('2023','2023'),
		   ('2024','2024'),
		   ('2025','2025'),
		  )
	year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-year'}))
	from_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-from-year'}))
	to_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-to-year'}))


	from_date = forms.DateField(help_text="Choose starting date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-from'}))	
	to_date = forms.DateField(help_text="Choose ending date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-to'}))	

	def __init__(self,sites, *args, **kwargs):	
        	super(GenerationReportForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites
	
	def clean(self): 
        	format = self.cleaned_data['format']
		if format == "weekly" or format == "monthly":
			if not self.cleaned_data['to_date'] or not self.cleaned_data['from_date']:
    				raise forms.ValidationError("Please enter From and To date before generating report")
		if format == "---":
				
    			raise forms.ValidationError("Please choose a valid format")
		if format == "daily":
			if not self.cleaned_data['from_date']:
    				raise forms.ValidationError("Please enter From date before generating report")

		if format == "yearly":
			if not self.cleaned_data['year']:
    				raise forms.ValidationError("Please choose a year for yearly report")



class InverterReportForm(forms.Form):
	site = forms.ModelChoiceField(queryset = None)


	from_date = forms.DateField(help_text="Choose starting date of the report")	
	to_date = forms.DateField(help_text="Choose ending date of the report",required=False)	

	def __init__(self,company, *args, **kwargs):	
        	super(InverterReportForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = EnergySite.objects.filter(company=company)
	
	def clean(self): 
		if self.cleaned_data['to_date'] and self.cleaned_data['to_date'] < self.cleaned_data['from_date']:
    			raise forms.ValidationError("To date can't be less than from date")








'''
class GenerationReportForm(forms.Form):
	sites = forms.ModelMultipleChoiceField(
  		widget=forms.SelectMultiple(attrs={'class': 'chzn-select'}),
   		queryset = None)
	date = forms.DateField(help_text="Choose starting date of the report")	
	CHOICES = (('daily','Daily'),
		   ('weekly','Weekly'),
		   ('monthly','Monthly'),
		   ('yearly','Yearly'),
		  )
	duration = forms.ChoiceField(choices=CHOICES,help_text="Choose duration of the report required")
	def __init__(self,company, *args, **kwargs):	
        	super(GenerationReportForm, self).__init__(*args, **kwargs)
        	self.fields['sites'].queryset = EnergySite.objects.filter(company=company)

'''
class BreakdownReportForm(forms.Form):
	sites = forms.ModelMultipleChoiceField(
  		widget=forms.SelectMultiple(attrs={'class': 'chzn-select'}),
   		queryset = None)
	date = forms.DateField(help_text="Choose starting date of the report")	
	TYPES = (('plantdown','Plant Down'),
		   ('griddown','Grid Down'),
		  )
	type = forms.ChoiceField(choices=TYPES,help_text="Choose break down type")
	
	CHOICES = (('daily','Daily'),
		   ('weekly','Weekly'),
		   ('monthly','Monthly'),
		   ('yearly','Yearly'),
		  )
	duration = forms.ChoiceField(choices=CHOICES,help_text="Choose duration of the report required")
	def __init__(self,sites, *args, **kwargs):	
        	super(BreakdownReportForm, self).__init__(*args, **kwargs)
        	self.fields['sites'].queryset = sites


class PlantWiseForm(forms.Form):

	sites = forms.ModelMultipleChoiceField(
  		widget=forms.SelectMultiple(attrs={'class': 'chzn-select'}),
   		queryset = None)
	
	CHOICES = (('daywise','Daywise'),
		   ('monthly','Monthly'),
		   ('yearly','Yearly'),
		  )

	PARA = (
		   ('generation','Generation'),
		('pr','Performance Ratio'),
		('cuf','CUF'),
		   ('radiation','Radiation'),
		   ('losses','Losses'),
		   ('eff','Generation Efficiency'),
		   ('breakdown','Breakdown'),
		   ('revenue','Revenue'),
		  )
	parameter = forms.ChoiceField(choices=PARA,help_text="Choose parameter for comparision")

	duration = forms.ChoiceField(choices=CHOICES,help_text="Choose duration of the report required")
	YEARS = (('2012','2012'),
		   ('2013','2013'),
		   ('2014','2014'),
		   ('2015','2015'),
		   ('2016','2016'),
		   ('2017','2017'),
		   ('2018','2018'),
		   ('2019','2019'),
		   ('2020','2020'),
		   ('2021','2021'),
		   ('2022','2022'),
		   ('2023','2023'),
		   ('2024','2024'),
		   ('2025','2025'),
		  )
	from_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-from-year'}))
	to_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-to-year'}))


	from_date = forms.DateField(help_text="Choose starting date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-from-date'}))	
	to_date = forms.DateField(help_text="Choose ending date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-to-date'}))


	def __init__(self,sites, *args, **kwargs):	
        	super(PlantWiseForm, self).__init__(*args, **kwargs)
        	self.fields['sites'].queryset = sites

class CompareForm(forms.Form):
	sites = forms.ModelMultipleChoiceField(
  		widget=forms.SelectMultiple(attrs={'class': 'chzn-select'}),
   		queryset = None)
	
	CHOICES = (('daywise','Daywise'),
		   ('monthly','Monthly'),
		   ('yearly','Yearly'),
		  )

	PARA = (
		   ('generation','generation'),
		   ('radiation','radiation'),
		  )
	parameter_one = forms.ChoiceField(choices=PARA,help_text="Choose parameter for comparision")
	parameter_two = forms.ChoiceField(choices=PARA,help_text="Choose parameter for comparision")

	duration = forms.ChoiceField(choices=CHOICES,help_text="Choose duration of the report required")
	YEARS = (('2012','2012'),
		   ('2013','2013'),
		   ('2014','2014'),
		   ('2015','2015'),
		   ('2016','2016'),
		   ('2017','2017'),
		   ('2018','2018'),
		   ('2019','2019'),
		   ('2020','2020'),
		   ('2021','2021'),
		   ('2022','2022'),
		   ('2023','2023'),
		   ('2024','2024'),
		   ('2025','2025'),
		  )
	from_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-from-year'}))
	to_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-to-year'}))


	from_date = forms.DateField(help_text="Choose starting date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-from-date'}))	
	to_date = forms.DateField(help_text="Choose ending date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-to-date'}))


	def __init__(self,company, *args, **kwargs):	
        	super(CompareForm, self).__init__(*args, **kwargs)
        	self.fields['sites'].queryset = EnergySite.objects.filter(company=company)




class PMAnalysisForm(forms.Form):
	site = forms.ModelChoiceField(queryset = None)
	
	CHOICES = (('daywise','Daywise'),
		   ('monthly','Monthly'),
		   ('yearly','Yearly'),
		  )
	delay = forms.IntegerField(required=False)
	duration = forms.ChoiceField(choices=CHOICES,help_text="Choose duration of the report required")
	YEARS = (('2012','2012'),
		   ('2013','2013'),
		   ('2014','2014'),
		   ('2015','2015'),
		   ('2016','2016'),
		   ('2017','2017'),
		   ('2018','2018'),
		   ('2019','2019'),
		   ('2020','2020'),
		   ('2021','2021'),
		   ('2022','2022'),
		   ('2023','2023'),
		   ('2024','2024'),
		   ('2025','2025'),
		  )
	from_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-from-year'}))
	to_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-to-year'}))
	

	from_date = forms.DateField(help_text="Choose starting date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-from-date'}))	
	to_date = forms.DateField(help_text="Choose ending date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-to-date'}))
	
	LEVELS = (
		 ('---','---'),
		 ('due','due'),
		 ('in_progress','In Progress'),
		 ('delayed','Delayed'),
		 ('completed','Completed'),)


	status = forms.ChoiceField(choices=LEVELS,required=False,)
	def __init__(self,sites, *args, **kwargs):	
        	super(PMAnalysisForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites





class EquipmentWiseForm(forms.Form):
	sites = forms.ModelChoiceField(queryset = None)
	inverters = forms.ModelMultipleChoiceField(
  		widget=forms.SelectMultiple(attrs={'class': 'chzn-select'}),
   		queryset = None)
	

	CHOICES = (('daywise','Daywise'),
		   ('monthly','Monthly'),
		   ('yearly','Yearly'),
		  )

	duration = forms.ChoiceField(choices=CHOICES,help_text="Choose duration of the report required")
	YEARS = (('2012','2012'),
		   ('2013','2013'),
		   ('2014','2014'),
		   ('2015','2015'),
		   ('2016','2016'),
		   ('2017','2017'),
		   ('2018','2018'),
		   ('2019','2019'),
		   ('2020','2020'),
		   ('2021','2021'),
		   ('2022','2022'),
		   ('2023','2023'),
		   ('2024','2024'),
		   ('2025','2025'),
		  )
	from_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-from-year'}))
	to_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-to-year'}))


	from_date = forms.DateField(help_text="Choose starting date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-from-date'}))	
	to_date = forms.DateField(help_text="Choose ending date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-hidden field-to-date'}))


	def __init__(self,sites, *args, **kwargs):	
        	super(EquipmentWiseForm, self).__init__(*args, **kwargs)
        	self.fields['sites'].queryset = sites
        	self.fields['inverters'].queryset = Inverter.objects.all()





class BreakdownAnalysisForm(forms.Form):
	sites = forms.ModelMultipleChoiceField(
  		widget=forms.SelectMultiple(attrs={'class': 'chzn-select'}),
   		queryset = None)
	CRI = (('all','all'),
		   ('major','major'),
		   ('frequent','frequent'),
		   ('equipmentwise','Equipment Wise'),
		  )
	#CAUSES = (
	#	   ('---','---'),
	#	  )


	#breakdown_cause = DynamicChoiceField(choices=CAUSES,required=False,widget=forms.Select(attrs={'class':''}))
	criteria = forms.ChoiceField(choices=CRI,widget=forms.Select(attrs={'class':''}))

	equipments = forms.ModelMultipleChoiceField(
  		widget=forms.SelectMultiple(attrs={'class': 'chzn-select'}),
   		queryset = None,
		required = False,
		)
	


	CHOICES = (('daywise','Daywise'),
		   ('monthly','Monthly'),
		   ('yearly','Yearly'),
		  )

	duration = forms.ChoiceField(choices=CHOICES,help_text="Choose duration of the report required")
	YEARS = (('2012','2012'),
		   ('2013','2013'),
		   ('2014','2014'),
		   ('2015','2015'),
		   ('2016','2016'),
		   ('2017','2017'),
		   ('2018','2018'),
		   ('2019','2019'),
		   ('2020','2020'),
		   ('2021','2021'),
		   ('2022','2022'),
		   ('2023','2023'),
		   ('2024','2024'),
		   ('2025','2025'),
		  )
	from_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-from-year'}))
	to_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-to-year'}))




	from_date = forms.DateField(help_text="Choose starting date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-from-date'}))	
	to_date = forms.DateField(help_text="Choose ending date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-to-date'}))

	#CHOICES = (('all','all'),
	#	   ('griddown','griddown'),
	#	   ('plantdown','plantdown'),
	#	  )

	#breakdown_type = forms.ChoiceField(choices=CHOICES,widget=forms.Select(attrs={'class':''}))
	TIME = (
		   ('---','---'),
		   ('1','1 hour'),
		   ('2','2 hours'),
		   ('3','3 hours '),
		   ('4','4 hours '),
		   ('5','5 hours '),
		   ('6','6 hours '),
		   ('7','7 hours '),
		   ('8','8 hours '),
		   ('9','9 hours '),
		   ('10','10 hours '),
		   ('11','11 hours '),
		   ('12','12 hours '),
		  )
	segregate_on_time = forms.ChoiceField(choices=TIME,required=False,widget=forms.Select(attrs={'class':'field-hidden'}))
	FREQ = (
		   ('---','---'),
		   ('1 ','1 time'),
		   ('2','2 times '),
		   ('3','3 times '),
		   ('4','4 times '),
		   ('5','5 times '),
		   ('6','6 times '),
		   ('7','7 times '),
		   ('8','8 times '),
		   ('9','9 times '),
		   ('10','10 time '),
		   ('11','11 times'),
		   ('12','12 times'),
		  )

	


	segregate_on_freq = forms.ChoiceField(choices=FREQ,required=False,widget=forms.Select(attrs={'class':'field-hidden'}))
	


	def __init__(self,sites, *args, **kwargs):	
        	super(BreakdownAnalysisForm, self).__init__(*args, **kwargs)
        	self.fields['sites'].queryset = sites
        	self.fields['equipments'].queryset = Equipment.objects.all()



class MessageForm(forms.Form):
	emails = forms.CharField()


