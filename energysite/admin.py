from django.contrib import admin

from energysite.models import *

admin.site.register(Block)
admin.site.register(ModuleType)
admin.site.register(ConnectedModule)
