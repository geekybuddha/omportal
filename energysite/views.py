from __future__ import division
from django.shortcuts import render,render_to_response
from django.template.loader import render_to_string
import datetime
from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required
from energysite.models import *
from energysite.forms import *
from django.forms.models import BaseModelFormSet,BaseFormSet
from django.shortcuts import get_object_or_404
from django.db.models import Count, Min, Sum, Avg,Max
from django.contrib import messages
from django.http import JsonResponse
from django.forms.formsets import formset_factory
from django.forms import modelformset_factory
from prevmain.models import PMSchedule,PMAlert
from django.http import Http404
from datetime import timedelta
from django.core.mail import send_mail
import re
#from account import utils
#from account.hooks import hookset
from django.db.models.signals import post_save
#from django.contrib.sites.models import get_current_site
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.tokens import default_token_generator
from account import signals
from account.conf import settings
from django.utils.http import base36_to_int, int_to_base36
from functools import partial, wraps
from django.forms.formsets import formset_factory
from django.conf import settings
from operator import attrgetter
from itertools import chain
from calendar import monthrange
from django.core.urlresolvers import reverse
from django.contrib.auth.views import password_reset, password_reset_confirm
import calendar
from math import floor
from operator import itemgetter, attrgetter, methodcaller
from django.utils.html import strip_tags
import random, math
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import simplejson
from django.db.models import F
from calendar import monthrange
months = ['January','February','March','April','May','June','July','August','September','October','November','December']


def index(request):
	if request.user.is_authenticated():
		try:
			usercompany = UserCompany.objects.get(user=request.user) 	
			if usercompany.is_admin:
				sites = EnergySite.objects.filter(company=usercompany.company).order_by('state')
				if not sites:
					return HttpResponseRedirect('/new_site/')
			else:
				usersites = UserSite.objects.filter(user=request.user).values('site')
				if usersites:
					sites = EnergySite.objects.filter(pk__in=usersites).order_by('state')
				elif usercompany.is_stock_admin:	
					return HttpResponseRedirect("/stock/closing/")
				else:
					return HttpResponse("Your are not associated with any site. Please contact your admin to assign site rights to you.")
			manage_data = {}
			form = MessageForm()

			if request.method == "POST":
				form = MessageForm(request.POST)
				if form.is_valid():
					emails = form.cleaned_data['emails']
					url = request.POST['morning_report']
					report_date = request.POST['report_date']
					message_subject = "Morning Report for Date - %s" % (report_date)
					html_content = render_to_string("morning_report_email.html",{'user':request.user,'url':url,'date':report_date})
					#message_body = notification
					send_mail(message_subject, html_content, 'info@energym.cloudapp.net',
					    [emails,request.user.email], fail_silently=False,html_message=html_content)	
					return HttpResponse("Email sent")

					
			'''
			we are going to show data based on the date of the last added dgr to a site belonging to the users 

			'''
			
			#date = datetime.datetime.now().date() - timedelta(days=1)
			alldgr = DGR.objects.filter(site__in=sites)

			try:
				date = alldgr.order_by('-date')[0].date
			except:
				date = datetime.datetime.now().date()
    			if request.GET.get('date'):
						
				try:
					date2 =  datetime.datetime.strptime(request.GET['date'], "%m/%d/%Y").date()
					if date2 > date:
						date2 = date
					else:
						date = date2	
				except:
					date = alldgr.order_by('-date')[0].date
			data_date = date 
			next_date = date + timedelta(days=1)
			prev_date = date - timedelta(days=1)

			manage_data['no_of_sites'] = sites.count()
			try:
				manage_data['today'] = round(alldgr.filter(date=date).aggregate(total=Sum('tvm_export_value'))['total'],2)	
			except:
				manage_data['today'] = 0
			try:
				manage_data['month'] = round(alldgr.filter(date__month=date.month).aggregate(total=Sum('tvm_export_value'))['total'],2)
			except:
				manage_data['month'] = 0

			try:
				manage_data['year'] = round(alldgr.filter(date__year=date.year).aggregate(total=Sum('tvm_export_value'))['total'],2)
				manage_data['year'] = manage_data['year']/1000  # Convert into GW
			except:
				manage_data['year'] = 0
			reports = []

			total_capacity = sites.aggregate(capacity=Sum('capacity'))['capacity']
			manage_data['total_capacity'] = total_capacity
			manage_data['total_today_cuf'] = round((manage_data['today']*100)/(total_capacity*24),2)
			manage_data['total_month_cuf'] = round((manage_data['month']*100)/(total_capacity*24*date.day),2)
			d0 = datetime.date(data_date.year, 1, 1) #current financial year
			
			days_so_far = data_date - d0
			try:
				manage_data['total_year_cuf'] = round((manage_data['year']*100*1000)/(total_capacity*24*days_so_far.days),2)
			except:
				
				manage_data['total_year_cuf'] = 0
			total_today_revenue = 0
			total_month_revenue = 0

			filename = "xlssheets/"+"monring-report-%s.xlsx" % str(datetime.datetime.now())
			xl_url = settings.MEDIA_URL+filename
			workbook  = xlsxwriter.Workbook(settings.MEDIA_ROOT+filename)
			worksheet = workbook.add_worksheet("Morning Report")


			bold = workbook.add_format({'bold': True})
			bold.set_border(1)	
			format = workbook.add_format({'bold': True})

			format.set_align('center')
			format.set_align('vcenter')
			format.set_border(1)

			format2 = workbook.add_format({'bold': True})
			format2.set_align('center')
			format2.set_align('vcenter')
				
			format2.set_font_size(20)

			merge_format2 = workbook.add_format({
			    'bold':     True,
			    'border':   6,
			    'align':    'center',
			    'valign':   'vcenter',
			    'fg_color': '#00B0F0',
			})
			merge_format2.set_font_size(20)
			merge_format3 = workbook.add_format({
			    'bold':     True,
			    'border':   6,
			    'align':    'center',
			    'valign':   'vcenter',
			    'fg_color': '#DBEEF4',
			})

			merge_format2.set_font_size(18)


			worksheet.set_column(0, 7, 30)
			

			worksheet.write(0, 0, 'State',bold)
			worksheet.write(0, 1, 'Project',bold)
			worksheet.write(0, 2, 'Operational Capacity',bold)
			worksheet.write(0, 3, 'Net Generation in MWH',bold)
			worksheet.write(0, 4, 'Net CUF in %',bold)
			worksheet.write(0, 5, 'Highest Till Date in MWH',bold)
			worksheet.write(0, 6, 'Highest CUF in %',bold)
			worksheet.write(0, 7, 'Achieved on',bold)
		


			row = 1
			col = 0

			for site in sites:

				data = {}
				try:
					today = DGR.objects.get(site=site,date=date) 
					data['revenue'] = round(today.revenue,2)
					try:
						pr = (today.tvm_export_value)/(site.capacity*today.dgrweather.radiation)
						pr = round(pr*100.00,2)
					except:
						pr = 0

					data['today_generation'] = round(today.tvm_export_value,2)
					cuf = get_cuf_daily(data['today_generation'],site.capacity)
				except DGR.DoesNotExist:
					today = ''
					pr = 0 
					cuf = 0
					data['today_generation'] = 0 
					data['revenue'] = 0

				total = DGR.objects.filter(site=site,date__month = date.month).aggregate(gen=Sum('tvm_export_value'),rad=Sum('dgrweather__radiation'),rev=Sum('revenue'))
				data['month_pr'] = get_performance_ratio(total['gen'],site.capacity,total['rad']) 
				days = monthrange(date.year,date.month)[1]
				try:
					data['month_cuf'] = round((total['gen']*100)/(site.capacity*24*date.day),2)
				except:
					data['month_cuf'] = 0

				max_gen = alldgr.filter(site=site).aggregate(max=Max('tvm_export_value'))['max']	
			
				if max_gen:
					
					data['highest_cuf'] = round(get_cuf_daily(max_gen,site.capacity),2)
					data['highest_gen'] =  round(max_gen,2)
					data['highest_date'] = DGR.objects.get(site=site,tvm_export_value=max_gen)

				data['site'] = site
				
				data['location'] = site.location
				data['today'] = today
				data['pr'] = pr
				data['rank'] = site_rank_day(site,date)
				total_today_revenue = total_today_revenue+data['revenue']
				try:
					data['total'] =round(total['gen'],2)
				except:
					data['total'] = 0
				try:
					data['month_revenue'] = round(total['rev'],2)
				except:
					data['month_revenue'] = total['rev']
				try:
					total_month_revenue = total_month_revenue+data['month_revenue']
				except:
					pass
				data['cuf'] = round(cuf,2)

				#for exporting excel sheet of morning report
				worksheet.write(row, col,  site.state)
				worksheet.write(row, col+1,  site.name)
				worksheet.write(row, col+2,  site.capacity)
				try:
					worksheet.write(row, col+3, data['today'].tvm_export_value)
				except:
					
					worksheet.write(row, col+3, 0)
				worksheet.write(row, col+4,  data['cuf'])
				try:
					worksheet.write(row, col+5,  data['highest_gen'])
					worksheet.write(row, col+6,  data['highest_cuf'])
					worksheet.write(row, col+7,  str(data['highest_date'].date))
				except:
					pass
				row = row+1
				reports.append(data)

			manage_data['revenue_today'] = total_today_revenue	
			manage_data['revenue_month'] = total_month_revenue	

			thirty_day = datetime.timedelta(days=10)
			from_date = data_date - thirty_day
			to_date = date 
			duration = 'daywise'
			parameter = 'generation' 
		
			end_date = data_date
			start_date = datetime.date(data_date.year,data_date.month,1)
			best_revenue = 0
		
			row = row+2	
			cell_range = xl_range(row,col,row,col+3)
			worksheet.merge_range(cell_range, "Total Net Generation for the day in MWH" ,format)
			worksheet.write(row,col+4,manage_data['today'])

			cell_range = xl_range(row+1,col,row+1,col+3)
			worksheet.merge_range(cell_range, "Total Cumulative Net Generation for the month in MWh " ,format)
			worksheet.write(row+1,col+4,manage_data['month'])

			cell_range = xl_range(row+2,col,row+2,col+3)
			worksheet.merge_range(cell_range, "Total Billing Value for the day in Rs.(Crore) " ,format)
			worksheet.write(row+2,col+4,manage_data['revenue_today'])

			manage_data['revenue_best'] = 0
			manage_data['revenue_best_date']= ''

			while start_date <= end_date:
				revenue = DGR.objects.filter(site__in=sites,date=start_date).aggregate(total=Sum('revenue'))['total']	
				if revenue > best_revenue:
					best_revenue= revenue
					manage_data['revenue_best'] = round(revenue,2)
					manage_data['revenue_best_date'] = start_date
				start_date = start_date+datetime.timedelta(days=1)
			
			cell_range = xl_range(row+3,col,row+3,col+3)
			text = "Highest Daily Billing Value for the month  in Rs. (Crores) - %s" % manage_data['revenue_best_date']
			worksheet.merge_range(cell_range, text ,format)
			worksheet.write(row+3,col+4,manage_data['revenue_best'])

			cell_range = xl_range(row+4,col,row+4,col+3)
			text = "Total Cumulative Billing Value for the month in Rs. (Crores)"
			worksheet.merge_range(cell_range, text ,format)
			worksheet.write(row+4,col+4,manage_data['revenue_month'])



			workbook.close() #workbook close
		
			
			reports2 = []
			reports3 = []

			states = (
				'Andaman and Nicobar Islands',
				'Andhra Pradesh', 
				'Arunachal Pradesh',
				'Assam',
				'Bihar	',
				'Chandigarh',
				'Chhattisgarh',
				'Dadra and Nagar Haveli	',
				'Daman and Diu',
				'Delhi',	
				'Goa',
				'Gujarat',
				'Haryana',
				'Himachal Pradesh',
				'Jammu and Kashmir',
				'Jharkhand',
				'Karnataka',
				'Kerala',
				'Lakshadweep',
				'Madhya Pradesh',
				'Maharashtra',
				'Manipur',
				'Meghalaya',
				'Mizoram',
				'Nagaland',
				'Orissa',
				'Pondicherry',
				'Punjab',
				'Rajasthan',
				'Sikkim',
				'Tamil Nadu',
				'Tripura',
				'Uttaranchal',
				'Uttar Pradesh',
				'West Bengal'
			)
	
			#state wise list

			day = datetime.timedelta(days=1)
			start_date = from_date
			while start_date <= to_date:
				master_query = DGR.objects.filter(date=start_date)
				#for state wise
				data2 = {}
				report = []

				for state in states:
				
					sites2 = sites.filter(state=state)
					state_capacity = sites2.aggregate(total=Sum('capacity'))['total']
					if sites2:
						data['capacity'] = state_capacity
						data = {}	
						dgr = master_query.filter(site__in=sites2)
					
						data['site'] = state
						abc = dgr.aggregate(abc=Sum('tvm_import_value')) #import
						xyz = dgr.aggregate(xyz=Sum('tvm_export_value')) #export									
						try:
							total_radiation = round(dgr.aggregate(radiation=Avg('dgrweather__radiation'))['radiation'],2)     #radiation				
						except:
							total_radiation = 0

						if total_radiation:
							data['radiation'] = total_radiation
							data['gen_by_radiation'] = total_radiation*state_capacity
						else:
							data['radiation'] = 0
							data['gen_by_radiation'] = 0
						try:
							total_generation = float(xyz['xyz']) 
							data['generation'] = round(total_generation,2)
							data['cuf'] = round((total_generation*100)/(state_capacity*24),2)

						except:
							total_generation = 0
							data['generation'] = 0	
							data['cuf'] = 0	

						if xyz['xyz']:		
							pr = get_performance_ratio(float(xyz['xyz']),state_capacity,total_radiation)
							data['pr'] = pr
						else:
							pr = 0
							data['pr'] = 0
						report.append(data)
				data2['date'] = start_date
				data2['sites'] = report
				start_date = start_date+day
				reports3.append(data2)

			
			while from_date <= to_date:
				master_query = DGR.objects.filter(date=from_date)
				#for state wise
				data2 = {}
				report = []
	


				# for site wise
				data2 = {}
				report = []
				for site in sites:
					data = {}	
					dgr = master_query.filter(site=site,date=from_date)
				
					data['site'] = site
					abc = dgr.aggregate(abc=Sum('tvm_import_value')) #import
					xyz = dgr.aggregate(xyz=Sum('tvm_export_value')) #export									
					try:
						total_radiation = round(dgr.aggregate(radiation=Sum('dgrweather__radiation'))['radiation'],2)     #radiation				
					except:
						pass

					if total_radiation:
						data['radiation'] = total_radiation
						data['gen_by_radiation'] = total_radiation*site.capacity
					else:
						data['radiation'] = 0
						data['gen_by_radiation'] = 0
					try:
						total_generation = float(xyz['xyz']) 
						data['generation'] = round(total_generation,2)
						data['cuf'] = round((total_generation*100)/(site.capacity*24),2)
		

					except:
						total_generation = 0
						data['generation'] = 0	
						data['cuf'] = 0	
					if xyz['xyz']:		
						pr = get_performance_ratio(float(xyz['xyz']),site.capacity,float(total_radiation))
						data['pr'] = pr
					else:
						pr = 0
						data['pr'] = 0
					report.append(data)
				data2['date'] = from_date
				data2['sites'] = report
				from_date = from_date+day
				reports2.append(data2)


			return render_to_response('manage.html',{'company':usercompany,'reports':reports,'date':data_date,'manage_data':manage_data,'form':form,'next_date':next_date,'prev_date':prev_date,'readings':reports2,'parameter':parameter,'from_date':date,'to_date':to_date,'duration':duration,'usercompany':usercompany,'morning_report':xl_url,'reports3':reports3},
					context_instance=RequestContext(request))
		except UserCompany.DoesNotExist:
			return HttpResponseRedirect('/create_company/')
	else:
		return render_to_response('index.html',{},
					context_instance=RequestContext(request))

@login_required
def site(request,site_id):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	site = get_object_or_404(EnergySite,pk=site_id)
	if site.company.id != usercompany.company.id:
		return Http404

	try:
		UserSite.objects.get(site=site,user=request.user)
	except:
	 	if usercompany.is_admin:
			pass
		else:
			return HttpResponse("You don't have permission to access this site")

	
	request.session['current_site'] = site.id
	dgr = DGR.objects.filter(site=site)
	if not dgr:
		messages.success(request, 'Upload your first DGR to continue!')
		return HttpResponseRedirect("/dgr_upload/")

	usercompany = get_object_or_404(UserCompany,user=request.user)
	date = dgr.order_by('-date')[0].date
	month = date.month
	year = date.year

	monday = date - datetime.timedelta(days=date.weekday())	

	'''
		for displaying aggregate data of site till date
	'''		
	total_import = dgr.filter(date__gte=monday).aggregate(total=Sum('tvm_export_value'))
	total_export = dgr.filter(date__month=month).aggregate(total=Sum('tvm_export_value'))
	try:
		total_import = round(float(total_import['total']),2) #MWH to GWH
	except:
		total_import = 0
	try:
		total_export = round(float(total_export['total']),2) #MWH to GWH
	except:
		total_export = 0


	
	'''
	for insolation and generation graphs of last 15 days
	'''
	report = []
	start_date = date - timedelta(days=15) 
	end_date = date

	day = timedelta(days=1)
	while start_date <= end_date:
		
		data = {}
		try:	
			dgr_day  = DGR.objects.get(date=start_date,site=site)
			try:
				data['insolation'] = dgr_day.dgrweather.radiation
			except:
				data['insolation'] = 0
			data['generation'] = dgr_day.tvm_export_value
			data['month'] = start_date
	
		except DGR.DoesNotExist:
	
			data['insolation'] = 0
			data['generation'] = 0
			data['month'] = start_date
		report.append(data)
		start_date = start_date+day



	'''
	For displaying data of the latest dgr uploaded
	'''		
	latest_dgr = dgr.order_by('-date')[0]
		
	latest = {}	
	latest['dgr'] = latest_dgr
	try:
		latest['pr'] = get_performance_ratio(latest_dgr.tvm_export_value,site.capacity,latest_dgr.dgrweather.radiation)
	except:
		latest['pr'] = 0
	latest['cuf'] = get_cuf_daily(latest_dgr.tvm_export_value,site.capacity)
	plant_down_query = DGRPlantDownTime.objects.filter(dgr=latest_dgr)
	grid_down_query = DGRGridDownTime.objects.filter(dgr=latest_dgr)
	time_hours = grid_down_query.aggregate(total=Sum('total_time'))['total'] #grid
	plant_time_hours = plant_down_query.aggregate(total=Sum('total_time'))['total'] #plant
						
	latest['breakdown'] = plant_down_query
	#grid
	if time_hours!=None:
				
		total_time = convert_time(time_hours) 
	else:
		total_time = "00:00"
	latest['griddown'] = total_time						

	#plant
	if plant_time_hours!=None:
				
		plant_total_time = convert_time(plant_time_hours) 
	else:
		plant_total_time = "00:00"
				
	latest['plantdown'] = plant_total_time

	aggregate_loss = 0		
	for query in plant_down_query:
		aggregate_loss = aggregate_loss + genloss_calculation(query)
	for query in grid_down_query:
		aggregate_loss = aggregate_loss + genloss_calculation(query)


	latest['genloss']  = aggregate_loss 


	#previous date dgr

	try:
		previous_dgr = dgr.order_by('-date')[1]
		latest['prev_dgr'] = previous_dgr
		previous_pr = get_performance_ratio(previous_dgr.tvm_export_value,site.capacity,previous_dgr.dgrweather.radiation)
		if  latest['pr'] >= previous_pr:
			latest['pr_inc'] = latest['pr'] - previous_pr
		else:
		
			latest['pr_dec'] =  previous_pr - latest['pr']
		if  latest['dgr'].tvm_export_value >= previous_dgr.tvm_export_value:
			latest['gen_inc'] = round(latest['dgr'].tvm_export_value - previous_dgr.tvm_export_value,3)
		else:
		
			latest['gen_dec'] = round(previous_dgr.tvm_export_value - latest['dgr'].tvm_export_value,3)
	except:
		pass



	pmalerts = PMAlert.objects.filter(site=site,completed_date__month=latest['dgr'].date.month,completed_date__day=latest['dgr'].date.day,completed_date__year=latest['dgr'].date.year)
	rank = site_rank_day(site,date)

	return render_to_response('dashboard2.html',{'rank':rank,'pmalerts':pmalerts,'year':year,'usercompany':usercompany,'site':site,'import':total_import,'export':total_export,'report':report,'latest':latest,'date':date},
		  context_instance=RequestContext(request))
@login_required
def sites(request):
	company = UserCompany.objects.get(user=request.user)
	sites = EnergySite.objects.filter(company=company)
	return render_to_response('sites.html',{'sites':sites},
		  context_instance=RequestContext(request))


@login_required
def site_info(request,site_id):
	site = get_object_or_404(EnergySite,pk=site_id)
	try:
		tilt = Tilt.objects.get(site=site)
	except:
		tilt = ''
	htpanels = HTPanel.objects.filter(site=site)
	transformers = Transformer.objects.filter(site=site)
	inverters = Inverter.objects.filter(site=site)
	ajb = AJB.objects.filter(site=site)

	return render(request,'energysite/site_info.html',{'site':site,'tilt':tilt,'htpanels':htpanels,'transformers':transformers,'inverters':inverters,'ajb':ajb},
		)





##todo - make a decorator for checking user role and allowing only admin user to access this view
@login_required
def manage_users(request):
	usercompany = UserCompany.objects.get(user=request.user)
	if usercompany.is_admin:

		users = UserCompany.objects.filter(company=usercompany.company)
		all_sites = EnergySite.objects.filter(company=usercompany.company)
		allusers = []
		for thisuser in users:

			data = {}
			user_sites = UserSite.objects.filter(user=thisuser.user)
			data['user'] = thisuser
			data['user_sites'] = user_sites
			allusers.append(data)
		if request.method == "POST":
			req_usercompany = get_object_or_404(UserCompany,user__pk=request.POST['user'])
			if req_usercompany.company.id == usercompany.company.id:
				if 'is_admin' in request.POST:
					req_usercompany.is_admin = request.POST['is_admin']
					req_usercompany.save()
					return HttpResponse("User is now admin")
				else:
					req_usercompany.is_admin = False
					req_usercompany.save()

					return HttpResponse("User is no longer admin")
			else:
				return HttpResponse("fuck off")
		return render_to_response('users.html',{'users':allusers,'sites':all_sites},
			  context_instance=RequestContext(request))

	else:
		return HttpResponse("sorry you are not admin")


@login_required
def make_stock_admin(request):
	usercompany = UserCompany.objects.get(user=request.user)
	if usercompany.is_admin:

		if request.method == "POST":
			req_usercompany = get_object_or_404(UserCompany,user__pk=request.POST['user'])
			if req_usercompany.company.id == usercompany.company.id:
				if 'is_stock_admin' in request.POST:
					req_usercompany.is_stock_admin = request.POST['is_stock_admin']
					req_usercompany.save()
					return HttpResponse("User is now Stock Admin")
				else:
					req_usercompany.is_stock_admin = False
					req_usercompany.save()

					return HttpResponse("User is no longer stock admin")
			else:
				return HttpResponse("fuck off")

	else:
		return HttpResponse("sorry you are not admin")





##todo - make a decorator for checking user role and allowing only admin user to access this view
@login_required
def manage_create_users(request):

        form= UserForm()
	usercompany = get_object_or_404(UserCompany,user=request.user)
	if request.method == "POST":
		form = UserForm(request.POST)
		if form.is_valid():
			user  = User(username=form.cleaned_data['username'],first_name=form.cleaned_data['first_name'],last_name=form.cleaned_data['last_name'],is_active=True,email=form.cleaned_data['email'])
			user.save()
			if user:
				user_company = UserCompany(user=user,company=usercompany.company,is_admin=False)
				user_company.save()
						
				#utils.send_confirmation_email(user.email)

				protocol = getattr(settings, "DEFAULT_HTTP_PROTOCOL", "http")
				current_site = get_current_site(request)
				uid = int_to_base36(user.id)
				token = default_token_generator.make_token(user)
				password_reset_url = "{0}://{1}{2}".format(
					protocol,
					current_site.domain,
					reverse("account_password_reset_token", kwargs=dict(uidb36=uid, token=token))
				)
				ctx = {
					"user": user,
					"current_site": current_site,
					"password_reset_url": password_reset_url,
				}
				hookset.send_password_reset_email([user.email], ctx)

				##send email to activate account
				#message_body = "Your account to mange Solar Power Plant Site has been created by admin. Please visit "/account/reset/" to activate your account. Thanks"	
				#print message_body
				#send_mail('Your EnergyManager account created!', message_body, 'info@energym.cloudapp.net',
				#    [user.email], fail_silently=False)	
				messages.success(request, 'User added successfully! Email sent to the user for further instructions.')
			return HttpResponseRedirect('/manage/users/')	
	return render_to_response('create_users.html',{'form':form},
		  context_instance=RequestContext(request))



##todo - make a decorator for checking user role and allowing only admin user to access this view
@login_required
def manage_assign_site(request,user_id):

	
	assign_user = get_object_or_404(User,pk=user_id)
	usercompany = get_object_or_404(UserCompany,user=request.user)
	assign_user_company = get_object_or_404(UserCompany,user=assign_user)
	if usercompany.company.id == assign_user_company.company.id:
		if usercompany.is_admin:
			form = UserSiteForm(company=usercompany.company,user=assign_user)
			if request.method == "POST":
				form = UserSiteForm(company=usercompany.company,user=assign_user,data=request.POST)
				if form.is_valid():
					usersite = form.save(commit=False)
					usersite.user = assign_user
					usersite.save()
					notification_settings = NotificationSettings(usersite=usersite)
					notification_settings.save() 

					message_body = "You have been assigned the site - %s. You can access it via http://energym.cloudapp.net/site/%s/ ." % (usersite.site.name,usersite.site.id)
					message_subject = "Site Assigned to You: %s" % (usercompany.company.name)
					html_content = render_to_string("notification_email.html",{'user':assign_user,'notice':message_body})
					#message_body = notification
					send_mail(message_subject, message_body, 'info@energym.cloudapp.net',
					    [assign_user.email], fail_silently=False,html_message=html_content)	


					return HttpResponse("Site assigned to the user")

			return render_to_response('assign_site.html',{'form':form,'user_id':assign_user.id,'assign_user':assign_user},
				  context_instance=RequestContext(request))
		else:
			return HttpResponse("You don't have permission to access this area")
	else:
		return HttpResponse("You dont' belong here")

##todo - make a decorator for checking user role and allowing only admin user to access this view
@login_required
def remove_user_site(request,user_id,site_id):
	assign_user = get_object_or_404(User,pk=user_id)
	usercompany = get_object_or_404(UserCompany,user=request.user)
	assign_user_company = get_object_or_404(UserCompany,user=assign_user)
	assigned_site = get_object_or_404(EnergySite,pk=site_id)
	if usercompany.is_admin:
		if assigned_site.company.id == usercompany.company.id:
			UserSite.objects.filter(user=assign_user,site=assigned_site).delete()
			return HttpResponse("deleted")	
		
##todo - make a decorator for checking user role and allowing only admin user to access this view
@login_required
def block_user(request,user_id):

	assign_user = get_object_or_404(User,pk=user_id)
	usercompany = get_object_or_404(UserCompany,user=request.user)
	assign_user_company = get_object_or_404(UserCompany,user=assign_user)
	if usercompany.is_admin:
		if assign_user_company.company.id == usercompany.company.id:
			if assign_user.is_active == True:
				assign_user.is_active = False
				assign_user.save()
				return HttpResponse("User blocked!")	
			else:
				assign_user.is_active = True
				assign_user.save()
				return HttpResponse("User unblocked!")	
	


@login_required
def new_site(request):
	siteform = EnergySiteForm()
	tiltform = TiltForm()
	HtpanelFormSet = formset_factory(HTPanelForm,extra=5)
	htpanelformset = HtpanelFormSet()
	usercompany = UserCompany.objects.get(user=request.user)
	display_text = "Step 1: Basic Site Information"
	if request.method == "POST":
		form = EnergySiteForm(data=request.POST)
		if form.is_valid():
			site = form.save(commit=False)
			site.company = usercompany.company
			site.save()
			request.session['new_site_id'] = site.id
			i=1
			while i<=site.no_of_blocks:
				block_name = "block-%d" % i
				block = Block(site=site,company=site.company,block=block_name)
				block.save()
				i=i+1
			messages.success(request, 'Site information added successfully!')
			return HttpResponseRedirect('/new_site/htpanels/')	
	
	return render(request,'energysite/new_site.html',{'siteform':siteform,'display_text':display_text},)
class RequiredFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

class MyModelFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        super(MyModelFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


@login_required
def save_htpanels(request):
	site_id = request.session.get('new_site_id')
	site = EnergySite.objects.get(pk=site_id)
	
	FormSet = formset_factory(HTPanelForm,formset=RequiredFormSet)
	formset = FormSet()
	display_text = "Step 2: Add HT Panels Information"
	item = "HT Panel Details:"
	if request.method == "POST":
		formset = FormSet(request.POST)
	
		if formset.is_valid():
			for form in formset:
				if form.is_valid():
					data = form.save(commit=False)
					site_id = request.session.get('new_site_id')
					data.site = EnergySite.objects.get(pk=site_id)
					data.save()

			messages.success(request, 'HT Panels details added successfully!')
			return HttpResponseRedirect('/new_site/transformers/')	
	return render_to_response('new_site_formset.html',{'formset':formset,'display_text':display_text,'site':site,'item':item},
		  context_instance=RequestContext(request))



@login_required
def save_transformers(request):
	site_id = request.session.get('new_site_id')
	site = EnergySite.objects.get(pk=site_id)
	FormSet = modelformset_factory(Transformer, formset=MyModelFormSet,exclude=('site',))

	FormSet.form.base_fields['htpanel'].queryset = HTPanel.objects.filter(site=site)
        formset = FormSet(queryset=Transformer.objects.none())
	
	display_text = "Step 3: Add Transformers Information"
	item = "Transformer Details:"
	if request.method == "POST":
		formset = FormSet(request.POST)
	
		if formset.is_valid():
			for form in formset:
				if form.is_valid():
					data = form.save(commit=False)
					site_id = request.session.get('new_site_id')
					data.site = EnergySite.objects.get(pk=site_id)
					data.save()
			messages.success(request, 'Transformers details added successfully!')
			return HttpResponseRedirect('/new_site/inverter/')	
	return render_to_response('new_site_formset.html',{'formset':formset,'display_text':display_text,'item':item,'site':site},
		  context_instance=RequestContext(request))




@login_required
def save_inverter(request):
	site_id = request.session.get('new_site_id')
	site = EnergySite.objects.get(pk=site_id)
	FormSet = modelformset_factory(Inverter, exclude=('site',),formset=MyModelFormSet)
	FormSet.form.base_fields['transformer'].queryset = Transformer.objects.filter(site=site)
	FormSet.form.base_fields['block'].queryset = Block.objects.filter(site=site)
	formset = FormSet(queryset=Inverter.objects.none())
	display_text = "Step 4: Add Inverters Information"
	item = "Inverter Details:"

	if request.method == "POST":

		formset = FormSet(request.POST)
	
		if formset.is_valid():
			for form in formset:
				if form.is_valid():
					data = form.save(commit=False)
					site_id = request.session.get('new_site_id')
					data.site = EnergySite.objects.get(pk=site_id)
					data.save()
	
			messages.success(request, 'Inverters details added successfully!')
			return HttpResponseRedirect('/new_site/module/')	
	return render_to_response('new_site_formset.html',{'formset':formset,'display_text':display_text,'item':item,'site':site},
		  context_instance=RequestContext(request))
@login_required
def save_module(request):
	site_id = request.session.get('new_site_id')
	site = EnergySite.objects.get(pk=site_id)
	display_text = "Step 5: Add Modules"
	item = "Modules Details:"
	all_blocks = []	
	blocks = Block.objects.filter(site=site)
	for block in blocks:
		block_data = {}
		inverters = Inverter.objects.filter(block=block)				
		block_data['inverters'] = inverters
		block_data['block'] = block
		all_blocks.append(block_data)	

	form = ModuleForm()
	if request.method == "POST":
		form = ModuleForm(request.POST)
		if form.is_valid():
			data = form.save(commit=False)
			inverter = get_object_or_404(Inverter,pk=request.POST['inverter'])
			data.inverter = inverter
			data.save()
			messages.success(request, 'Modules details added successfully!')
	return render_to_response('new_module.html',{'form':form,'blocks':all_blocks,'display_text':display_text,'item':item,'site':site},
		  context_instance=RequestContext(request))



#currently deprecated for welspun
@login_required
def save_ajb(request):
	site_id = request.session.get('new_site_id')
	site = EnergySite.objects.get(pk=site_id)
	FormSet = modelformset_factory(AJB, exclude=('site',),formset=MyModelFormSet)
	FormSet.form.base_fields['inverter'].queryset = Inverter.objects.filter(site=site)
	formset = FormSet(queryset=AJB.objects.none())
	display_text = "Step 6: Add AJB Information"
	item = "AJB Details:"


	if request.method == "POST":
		formset = FormSet(request.POST)
	
		if formset.is_valid():
			for form in formset:
				if form.is_valid():
					data = form.save(commit=False)
					site_id = request.session.get('new_site_id')
					data.site = EnergySite.objects.get(pk=site_id)
					data.save()
			messages.success(request, 'AJB details added successfully!')
			return HttpResponseRedirect('/new_site/tilt/')	
	return render_to_response('new_site_formset.html',{'formset':formset,'display_text':display_text,'item':item,'site':site},
		  context_instance=RequestContext(request))


@login_required
def save_tilt(request):
	site_id = request.session.get('new_site_id')
	site = EnergySite.objects.get(pk=site_id)
	display_text = "Step 7: Panel Tilt Information"
	form = TiltForm()
	if request.method == "POST":
		form = TiltForm(data=request.POST)
		if form.is_valid():
			tilt = form.save(commit=False)
			tilt.site = site 
			tilt.save()
			url = "/new_site/ask/"
			messages.success(request, 'Panels tilt angles details added successfully!')
			return HttpResponseRedirect(url)
		
	return render_to_response('new_site.html',{'siteform':form,'display_text':display_text,'site':site},
		  context_instance=RequestContext(request))



@login_required
def create_company(request):
	form = CompanyForm()
	if request.method == "POST":
		form = CompanyForm(data=request.POST,files=request.FILES)
		if form.is_valid():
			company = form.save()
			usercompany = UserCompany(user=request.user,company=company,is_admin=True)
			usercompany.save()	
			messages.success(request, 'Company profile details updated. Now, create site locations to manage.')
			return HttpResponseRedirect('/')
	return render_to_response('create_company.html',{'form':form},
		  context_instance=RequestContext(request))






@login_required
def dgr_upload(request):
	site_id = request.session.get('current_site')
	site = get_object_or_404(EnergySite,pk=site_id)
	usercompany = get_object_or_404(UserCompany,user=request.user)	
	try:
		UserSite.objects.get(site=site,user=request.user)
		
	except UserSite.DoesNotExist:
		if usercompany.is_admin:
			pass
		else:
			return HttpRespons("You don't have rights to access this view")
	
	form = DGRForm(site=site)
	try:
		prev_dgr = DGR.objects.filter(site=site).order_by('-date')[0]
		form.fields['tvm_export'].initial = prev_dgr.tvm_export
		form.fields['tvm_import'].initial = prev_dgr.tvm_import
	except:
		pass	
	
	if request.method == "POST":
		form = DGRForm(site=site,data=request.POST)
		if form.is_valid():
			dgr = form.save(commit=False)
			dgr.site = site
			dgr.upload_time = datetime.datetime.now()
			dgr.user = request.user
			tvm_mf = site.tvm_mf	
			d = dgr.date- timedelta(days=1)
			try:
				previous_dgr = DGR.objects.get(date=d,site=site)

				dgr.tvm_export_value =  (dgr.tvm_export - previous_dgr.tvm_export)*tvm_mf
				dgr.tvm_import_value =  (dgr.tvm_import - previous_dgr.tvm_import)*tvm_mf
			except DGR.DoesNotExist:
				dgr.tvm_export_value =  (dgr.tvm_export)*tvm_mf
				dgr.tvm_import_value =  (dgr.tvm_import)*tvm_mf

			dgr.revenue = dgr.tvm_export_value*site.ppa*1000

			dgr.save()
			url = "/dgr/%s/" %(dgr.id)
			return HttpResponseRedirect(url)


	return render_to_response('dgr_upload.html',{'form':form,'site':site},
		  context_instance=RequestContext(request))



def dgr_inverter(request,dgr_id):
	site_id = request.session.get('current_site')
	site = get_object_or_404(EnergySite,pk=site_id)
	dgr = get_object_or_404(DGR,pk=dgr_id)
	if not dgr.user == request.user:
		raise Http404
		
	inverters = Inverter.objects.filter(site=site)
	blocks = Block.objects.filter(site=site)

	all_blocks = []	
	for block in blocks:
		block_data = {}
		inverters = Inverter.objects.filter(block=block)				
		block_data['inverters'] = inverters
		block_data['block'] = block
		all_blocks.append(block_data)	

	plantdownform = DGRPlantDownForm(site=site)
	griddownform = DGRGridDownForm(site=site)



	return render_to_response('dgr_inverter.html',{'dgr':dgr,'site':site,'inverters':inverters,'plantdownform':plantdownform,'griddownform':griddownform,'blocks':all_blocks},
				context_instance=RequestContext(request))
	
	
def dgr_plantdown(request,dgr_id):
	site_id = request.session.get('current_site')
	site = EnergySite.objects.get(pk=site_id)

	dgr = get_object_or_404(DGR,pk=dgr_id)
	if not dgr.user == request.user:
		raise Http404
		

	plantdownform = DGRPlantDownForm(site=site)
	griddownform = DGRGridDownForm(site=site)



	return render_to_response('dgr_plantdown.html',{'dgr':dgr,'site':site,'plantdownform':plantdownform,},
				context_instance=RequestContext(request))
def dgr_griddown(request,dgr_id):
	site_id = request.session.get('current_site')
	site = EnergySite.objects.get(pk=site_id)

	dgr = get_object_or_404(DGR,pk=dgr_id)
	if not dgr.user == request.user:
		raise Http404
		
	griddownform = DGRGridDownForm(site=site)


	return render_to_response('dgr_griddown.html',{'dgr':dgr,'site':site,'griddownform':griddownform,},
				context_instance=RequestContext(request))
	
def dgr_weather(request,dgr_id):
	site_id = request.session.get('current_site')
	site = EnergySite.objects.get(pk=site_id)

	dgr = get_object_or_404(DGR,pk=dgr_id)
	if not dgr.user == request.user:
		raise Http404
		
	try:
		dgrweather = dgr.dgrweather
	except:
		dgrweather = DGRWeather(dgr=dgr)
		dgrweather.save()
	
	form = DGRWeatherForm(instance=dgrweather)
	if request.method == "POST":
		form = DGRWeatherForm(request.POST,instance=dgrweather)
		if form.is_valid():
			form.save()


	return render_to_response('dgr_weather.html',{'dgr':dgr,'site':site,'dgrweather':dgrweather,'form':form},
				context_instance=RequestContext(request))


@login_required
def dgr_cleaning(request,dgr_id):
	site_id = request.session.get('current_site')
	site = EnergySite.objects.get(pk=site_id)

	dgr = get_object_or_404(DGR,pk=dgr_id)

	if not dgr.user == request.user:
		raise Http404
		
	form = ModuleCleaningForm(site=site)
	if request.method == "POST":
		form = ModuleCleaningForm(data=request.POST,site=site)
		if form.is_valid():
			try:
				cleaned = ModuleCleaned.objects.get(dgr=dgr,block = form.cleaned_data['block'])
				cleaned.cleaned = form.cleaned_data['cleaned']
				cleaned.save()
			except ModuleCleaned.DoesNotExist:

				data = form.save(commit=False)
				data.dgr = dgr
				data.save()
	return render_to_response('dgr_cleaning.html',{'form':form,'dgr':dgr,'site':site},
		  context_instance=RequestContext(request))




	


@login_required
def save_inverter_reading(request):
	if request.method == "POST":
		form = DGRInverterForm(request.POST)
		if form.is_valid():
			try:
				dgrinverter = DGRInverter.objects.get(dgr=form.cleaned_data['dgr'],inverter=form.cleaned_data['inverter'])
				dgrinverter.generation = form.cleaned_data['generation']
				dgrinverter.save()
			except DGRInverter.DoesNotExist:
				reading = form.save(commit=False)
				reading.save()
			return HttpResponse("sucess")
		else:
			return HttpResponse(form)	

@login_required
def save_plant_down(request):
	site_id = request.session.get('current_site')
	site = EnergySite.objects.get(pk=site_id)


	if request.method == "POST":
		form = DGRPlantDownForm(data=request.POST,site=site)
		if form.is_valid():
			dgr = DGR.objects.get(pk=request.POST['dgr'])
			
			reading = form.save(commit=False)
			reading.dgr = dgr
			reading.save()
			return HttpResponse("sucess")
		else:
			return HttpResponse(form.errors)	



@login_required
def save_grid_down(request):
	site_id = request.session.get('current_site')
	site = EnergySite.objects.get(pk=site_id)


	if request.method == "POST":
		form = DGRGridDownForm(data=request.POST,site=site)
		if form.is_valid():
			dgr = DGR.objects.get(pk=request.POST['dgr']) #since dgr field is not in the form, we need to get it via POST, cleaned_data wouldn't work
			
			reading = form.save(commit=False)
			reading.dgr = dgr
			reading.save()
			return HttpResponse("sucess")
		else:
			return HttpResponse(form)
@login_required
def save_weather(request):
	if request.method == "POST":
		form = HourlyWeatherForm(request.POST)
		if form.is_valid():
			try:
				dgrweather = HourlyWeather.objects.get(dgrweather=form.cleaned_data['dgrweather'],time=form.cleaned_data['time'])
				dgrweather.radiation = form.cleaned_data['radiation']
				dgrweather.temperature = form.cleaned_data['temperature']
				dgrweather.wind_speed = form.cleaned_data['wind_speed']
				dgrweather.save()
			except HourlyWeather.DoesNotExist:
				reading = form.save(commit=False)
				reading.save()
			return HttpResponse("sucess")
		else:
			return HttpResponse(form)	



def save_weather_aggregate(sender, instance, **kwargs):
	dgrweather = instance.dgrweather
	hourlyweather  = HourlyWeather.objects.filter(dgrweather=dgrweather)
	radiation = hourlyweather.aggregate(total_radiation=Sum('radiation'))['total_radiation']
	avg_temperature = hourlyweather.aggregate(avg_temperature=Avg('temperature'))['avg_temperature']
	wind = hourlyweather.aggregate(avg_wind=Avg('wind_speed'))['avg_wind']
	dgrweather.radiation = radiation
	dgrweather.temperature = avg_temperature
	dgrweather.wind_speed = wind
	dgrweather.save()


@login_required
def dgr_report(request):
	form = DateForm()
	if request.method == "POST":
		form = DateForm(request.POST)
		if form.is_valid():
			date = form.cleaned_data['date']
			site_id = request.session.get('current_site')
			site = EnergySite.objects.get(pk=site_id)
			inverters = Inverter.objects.filter(site=site)
			dgr = DGR.objects.get(date=date,site=site)
			d = date- timedelta(days=1)
			previous_dgr = DGR.objects.get(date=d)
			
			readings = []	
			for inverter in inverters:
				data = {}
				data['inverter'] = inverter
				try:
					data['dgr_inverter'] = DGRInverter.objects.get(dgr=dgr,inverter=inverter)
					data['previous_dgr_inverter'] = DGRInverter.objects.get(dgr=previous_dgr,inverter=inverter)
				except:
					pass
				readings.append(data)	

		
			return render_to_response('dgr_report.html',{'form':form,'readings':readings},
				context_instance=RequestContext(request))
		
	return render_to_response('dgr_report.html',{'form':form},
					context_instance=RequestContext(request))
		
##todo - make a decorator for checking user role and allowing only admin user to access this view
@login_required
def manage_reports(request):
	usercompany = UserCompany.objects.get(user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)
	form = GenerationReportForm(sites=sites)	

	type = ""
    	if request.GET.get('type'):
		type = request.GET['type']
		
		if type == "generation":
			TYPES = (
			   ('dgr','Generation'),
			   ('radiation','Insolation'),
			   ('cleaning','Module Cleaning'),
			   ('soiling','Soiling'),
			  )

			form.fields['report_type'].choices = TYPES
		if type == "breakdown":
			TYPES = (
			   ('breakdown','Plant Down'),
			   ('griddown','Grid Down'),
			  )

			form.fields['report_type'].choices = TYPES
		if type == "pm":
			TYPES = (
			   ('pm','PM'),
			   ('slippage','Slippage'),
			  )

			form.fields['report_type'].choices = TYPES
		if type == "reminders":
			TYPES = (
			   ('reminders','Reminders'),
			  )

			form.fields['report_type'].choices = TYPES

	return render_to_response('reports.html',{'sites':sites,'form':form,'type':type},
				context_instance=RequestContext(request))

##todo - make a decorator for checking user role and allowing only admin user to access this view
@login_required
def manage_analysis(request):
	return render_to_response('analysis.html',{},
				context_instance=RequestContext(request))


@login_required
def manage_reports_generation(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)



	form = GenerationReportForm(sites=sites)
	if request.method == "POST":
		form = GenerationReportForm(sites=sites,data=request.POST)
		if not form.is_valid():
			return HttpResponse(form.errors)
		else:
			format = form.cleaned_data['format']
		
			site = form.cleaned_data['site']
			if format == "yearly":
				year = form.cleaned_data['year']
				allmonths = []
				total_import = 0.0
				total_export = 0.0
				total_radiation = 0.0

				total_grid_down_count= 0.0 #grid
				total_grid_down_time = 0.0 #grid

				plant_total_grid_down_count= 0.0 #plant
				plant_total_grid_down_time = 0.0 #plant


				for i in range(12):

					reports = {}
					dgr = DGR.objects.filter(date__month=i+1,date__year=year,site=site)
					totals = DGR.objects.filter(site=site,date__month=i+1,date__year=year).aggregate(total_export=Sum('tvm_export_value'),total_import=Sum('tvm_import_value'),avg_radiation=Sum('dgrweather__radiation'),avg_temp=Avg('dgrweather__temperature'))
					try:
						total_export = total_export+totals['total_export']
					except:
						pass
					try:
						total_import = total_import+totals['total_import']
					except:
						pass
					try:
						total_radiation = total_radiation+totals['avg_radiation']
					except:
						pass
				

					total_grid_down = 0 #grid
					total_time_hours = 0 #grid
				
					plant_total_grid_down = 0 #plant
					plant_total_time_hours = 0 #plant



					report = []
					reports['month'] = months[i]
					try:
						reports['gen_by_rad'] = totals['avg_radiation']*site.capacity
					except:
						reports['gen_by_rad'] = 'Data Not Available'
					for dgrs in dgr:
						data = {}
						griddown_counts = DGRGridDownTime.objects.filter(dgr=dgrs).count() #grid
						plant_griddown_counts = DGRGridDownTime.objects.filter(dgr=dgrs).count() #plant
						time_hours = DGRGridDownTime.objects.filter(dgr=dgrs).aggregate(total=Sum('total_time'))['total'] #grid
						plant_time_hours = DGRPlantDownTime.objects.filter(dgr=dgrs).aggregate(total=Sum('total_time'))['total'] #plant
						
						#grid
						if time_hours!=None:
						
							total_time_hours = time_hours+total_time_hours 
							total_time = convert_time(time_hours) 
						else:
							total_time = "00:00"
						
						total_grid_down =  griddown_counts+total_grid_down #grid

						#plant
						if plant_time_hours!=None:
						
							plant_total_time_hours = plant_time_hours+plant_total_time_hours 
							plant_total_time = convert_time(plant_time_hours) 
						else:
							plant_total_time = "00:00"
						
						total_grid_down =  griddown_counts+total_grid_down #grid
						plant_total_grid_down =  plant_griddown_counts+plant_total_grid_down #plant



						data['dgr']= dgrs
						data['griddown_counts'] = griddown_counts #grid
						data['plant_griddown_counts'] = plant_griddown_counts #plant
						data['total_time'] = total_time
						report.append(data)
					
						
				
						
					total_time = convert_time(total_time_hours) #grid
					plant_total_time = convert_time(plant_total_time_hours) #plant

						
					reports['dgr'] = report
					reports['totals'] = totals
					reports['total_grid_down']= total_grid_down #grid
					reports['total_time_hours'] = total_time #grid
					reports['pr'] = get_performance_ratio(totals['total_export'],site.capacity,totals['avg_radiation'])
					total_grid_down_count = total_grid_down_count+total_grid_down #grid
					total_grid_down_time = total_time_hours+total_grid_down_time #grid

					reports['plant_total_grid_down']= plant_total_grid_down #plant
					reports['plant_total_time_hours'] = plant_total_time #plant

					plant_total_grid_down_count = plant_total_grid_down_count+plant_total_grid_down #plant
					plant_total_grid_down_time = plant_total_time_hours+plant_total_grid_down_time #plant


					allmonths.append(reports)

	
						
				total_time = convert_time(total_grid_down_time) #grid
				plant_total_time = convert_time(plant_total_grid_down_time) #plant

				other_data = {}
				other_data['total_time'] = total_time #grid
				other_data['grid_count'] = total_grid_down_count #grid
				other_data['plant_total_time'] = plant_total_time #plant
				other_data['plant_grid_count'] = plant_total_grid_down_count #plant

				other_data['total_export'] = total_export
				other_data['total_import'] = total_import
				other_data['total_radiation'] = total_radiation
				other_data['pr'] = get_performance_ratio(total_export,site.capacity,total_radiation)

					
				if form.cleaned_data['report_type'] == "radiation":
					return render_to_response('reports_radiation_yearly.html',{'form':form,'year':year,'site':site,'reports':allmonths,'other_data':other_data},
					context_instance=RequestContext(request))
			
				else:
					return render_to_response('reports_generation_yearly.html',{'form':form,'year':year,'site':site,'reports':allmonths,'other_data':other_data},
					context_instance=RequestContext(request))
			if format == "yearwise":
				from_year = int(form.cleaned_data['from_year'])
				to_year = int(form.cleaned_data['to_year'])
				total_import = 0.0
				total_export = 0.0
				total_radiation = 0.0
				allmonths = []
				total_grid_down_count= 0.0 #grid
				total_grid_down_time = 0.0 #grid

				plant_total_grid_down_count= 0.0 #plant
				plant_total_grid_down_time = 0.0 #plant
				year = from_year

				while year <= to_year:

					reports = {}
					dgr = DGR.objects.filter(date__year=year,site=site)
					totals = dgr.aggregate(total_export=Sum('tvm_export_value'),total_import=Sum('tvm_import_value'),avg_radiation=Sum('dgrweather__radiation'),avg_temp=Avg('dgrweather__temperature'))
					try:
						total_export = total_export+totals['total_export']
					except:
						pass
					try:
						total_import = total_import+totals['total_import']
					except:
						pass
					try:
						total_radiation = total_radiation+totals['avg_radiation']
					except:
						pass
				

					total_grid_down = 0 #grid
					total_time_hours = 0 #grid
				
					plant_total_grid_down = 0 #plant
					plant_total_time_hours = 0 #plant



					report = []
					reports['year'] = year
					try:
						reports['gen_by_rad'] = totals['avg_radiation']*site.capacity
					except:
						reports['gen_by_rad'] = 'Data Not Available'
					for dgrs in dgr:
						data = {}
						griddown_counts = DGRGridDownTime.objects.filter(dgr=dgrs).count() #grid
						plant_griddown_counts = DGRGridDownTime.objects.filter(dgr=dgrs).count() #plant
						time_hours = DGRGridDownTime.objects.filter(dgr=dgrs).aggregate(total=Sum('total_time'))['total'] #grid
						plant_time_hours = DGRPlantDownTime.objects.filter(dgr=dgrs).aggregate(total=Sum('total_time'))['total'] #plant
						
						#grid
						if time_hours!=None:
						
							total_time_hours = time_hours+total_time_hours 
							total_time = convert_time(time_hours) 
						else:
							total_time = "00:00"
						
						total_grid_down =  griddown_counts+total_grid_down #grid

						#plant
						if plant_time_hours!=None:
						
							plant_total_time_hours = plant_time_hours+plant_total_time_hours 
							plant_total_time = convert_time(plant_time_hours) 
						else:
							plant_total_time = "00:00"
						
						total_grid_down =  griddown_counts+total_grid_down #grid
						plant_total_grid_down =  plant_griddown_counts+plant_total_grid_down #plant



						data['dgr']= dgrs
						data['griddown_counts'] = griddown_counts #grid
						data['plant_griddown_counts'] = plant_griddown_counts #plant
						data['total_time'] = total_time
						report.append(data)
					
						
				
						
					total_time = convert_time(total_time_hours) #grid
					plant_total_time = convert_time(plant_total_time_hours) #plant

						
					reports['dgr'] = report
					reports['totals'] = totals
					reports['total_grid_down']= total_grid_down #grid
					reports['total_time_hours'] = total_time #grid
					reports['pr'] = get_performance_ratio(totals['total_export'],site.capacity,totals['avg_radiation'])
					total_grid_down_count = total_grid_down_count+total_grid_down #grid
					total_grid_down_time = total_time_hours+total_grid_down_time #grid

					reports['plant_total_grid_down']= plant_total_grid_down #plant
					reports['plant_total_time_hours'] = plant_total_time #plant

					plant_total_grid_down_count = plant_total_grid_down_count+plant_total_grid_down #plant
					plant_total_grid_down_time = plant_total_time_hours+plant_total_grid_down_time #plant

					year = year+1
					allmonths.append(reports)
					
						
						
				total_time = convert_time(total_grid_down_time) #grid
				plant_total_time = convert_time(plant_total_grid_down_time) #plant

				other_data = {}
				other_data['total_time'] = total_time #grid
				other_data['grid_count'] = total_grid_down_count #grid
				other_data['plant_total_time'] = plant_total_time #plant
				other_data['plant_grid_count'] = plant_total_grid_down_count #plant

				other_data['total_export'] = total_export
				other_data['total_import'] = total_import
				other_data['total_radiation'] = total_radiation
				other_data['pr'] = get_performance_ratio(total_export,site.capacity,total_radiation)

					
				if form.cleaned_data['report_type'] == "radiation":
					return render_to_response('reports_radiation_yearly.html',{'format':format,'form':form,'year':year,'site':site,'reports':allmonths,'other_data':other_data,'from_year':from_year,'to_year':to_year},
					context_instance=RequestContext(request))
			
				else:
					return render_to_response('reports_generation_yearly.html',{'format':format,'form':form,'year':year,'site':site,'reports':allmonths,'other_data':other_data,'from_year':from_year,'to_year':to_year},
					context_instance=RequestContext(request))



			if format == "daily":
				date = form.cleaned_data['from_date']
				inverters = Inverter.objects.filter(site=site)
				try:
					dgr = DGR.objects.get(date=date,site=site)
					d = date- timedelta(days=1)
					previous_dgr = DGR.objects.get(date=d,site=site)
					total_plant_time = 0
					total_grid_time = 0

					readings = []	
					for inverter in inverters:
						data = {}
						data['inverter'] = inverter
						try:
							data['dgr_inverter'] = DGRInverter.objects.get(dgr=dgr,inverter=inverter)
						except:
							data['dgr_inverter'] = "-"
						plant_down_time = DGRPlantDownTime.objects.filter(dgr=dgr,inverter=inverter).aggregate(total_time=Sum('total_time'))['total_time']
						#grid_down_time = DGRGridDownTime.objects.filter(dgr=dgr,equipment=inverter).aggregate(total_time=Sum('total_time'))['total_time']
						if plant_down_time!=None:
							data['total_plant_time'] = convert_time(plant_down_time)
							total_plant_time = total_plant_time+plant_down_time
							data['pa'] = round(((12-plant_down_time)/12)*100,2)
						else:
							data['total_plant_time'] = "0"	
							data['pa'] = 100.00
						#if grid_down_time!=None:
						#	data['total_grid_time'] = convert_time(grid_down_time)
						#	total_grid_time = total_grid_time+grid_down_time
						#	data['ga'] = round(((12-grid_down_time)/12)*100,2)
						#else:
						#	data['total_grid_time'] = "0"
						#	data['ga'] = 100.00
					#except:
						#	pass
						try:	
							data['previous_dgr_inverter'] = DGRInverter.objects.get(dgr=previous_dgr,inverter=inverter)
						except:
							pass
						readings.append(data)	

					totals = {}
					
					totals['total_generation'] = dgr.tvm_export_value
					totals['total_yesterday_generation'] = previous_dgr.tvm_export_value
					totals['total_plant_time'] = convert_time(total_plant_time)
					totals['total_grid_time'] = convert_time(total_grid_time)
					totals['dgr'] = dgr
					totals['plant_down_list'] = DGRPlantDownTime.objects.filter(dgr=dgr)
					totals['grid_down_list'] = DGRGridDownTime.objects.filter(dgr=dgr)
					totals['previous_dgr'] = previous_dgr
					totals['export_difference'] = dgr.tvm_export-previous_dgr.tvm_export
					totals['import_difference'] = dgr.tvm_import-previous_dgr.tvm_import

					
					if form.cleaned_data['report_type']== "radiation":
						radiation_data = dgr.dgrweather
						hourly_data = HourlyWeather.objects.filter(dgrweather=radiation_data)
						return render_to_response('reports_radiation_daily.html',{'from_date':date,'site':site,'readings':radiation_data,'hourlydata':hourly_data},
						context_instance=RequestContext(request))
		
					else:
						return render_to_response('reports_generation_daily.html',{'form':form,'date':date,'site':site,'readings':readings,'totals':totals,'dgr':dgr},
						context_instance=RequestContext(request))
				except DGR.DoesNotExist:
					
					return HttpResponse('No data available for selected date')
			# report format is day wise list, like monthly report
			if format == "monthly"  or format == "weekly":
				date = form.cleaned_data['from_date']
				end_date = form.cleaned_data['to_date']
				reports = {}	
				dgr = DGR.objects.filter(site=site,date__range=[date,end_date]).order_by('date')
				totals = DGR.objects.filter(site=site,date__range=[date,end_date]).aggregate(total_export=Sum('tvm_export_value'),total_import=Sum('tvm_import_value'),total_rad=Sum('dgrweather__radiation'))
				total_grid_down = 0
				total_time_hours = 0
				total_plant_down = 0
				plant_total_time_hours = 0

				report = []
				for dgrs in dgr:
					data = {}
					griddown_counts = DGRGridDownTime.objects.filter(dgr=dgrs).count()
					plantdown_counts = DGRPlantDownTime.objects.filter(dgr=dgrs).count()
					time_hours = DGRGridDownTime.objects.filter(dgr=dgrs).aggregate(total=Sum('total_time'))['total']
					plant_time_hours = DGRPlantDownTime.objects.filter(dgr=dgrs).aggregate(total=Sum('total_time'))['total']
					
					if time_hours!=None:

					
						total_time_hours = time_hours+total_time_hours
						total_time =  convert_time(time_hours)
					else:
						total_time = "00:00"
					
					total_grid_down =  griddown_counts+total_grid_down

					#plant
					if plant_time_hours!=None:
					
						plant_total_time_hours = plant_time_hours+plant_total_time_hours 
						plant_total_time = convert_time(plant_time_hours) 
					else:
						plant_total_time = "00:00"
					

					total_plant_down =  plantdown_counts+total_plant_down


					data['dgr']= dgrs
					data['pr'] = get_performance_ratio(dgrs.tvm_export_value,site.capacity,dgrs.dgrweather.radiation)
					data['cuf'] = get_cuf_daily(dgrs.tvm_export_value-dgrs.tvm_import_value,site.capacity)
					data['griddown_counts'] = griddown_counts
					data['total_time'] = total_time
					data['plantdown_counts'] = plantdown_counts
					data['plant_total_time'] = plant_total_time


					data['grid_availability'] = 100 - (float(total_time_hours)*100)/12 
					data['inverter_total'] = DGRInverter.objects.filter(dgr=dgrs).aggregate(total=Sum('generation'))['total']	
					data['inverter_avg'] = DGRInverter.objects.filter(dgr=dgrs).aggregate(total=Avg('generation'))['total']	
					report.append(data)
			
					
				total_time = convert_time(total_time_hours)
				plant_total_time = convert_time(plant_total_time_hours)

					
				reports['dgr'] = report
				reports['site'] = site
				reports['totals'] = totals
				reports['total_grid_down']= total_grid_down
				reports['total_plant_down']= total_plant_down
				reports['total_time_hours'] = total_time
				reports['plant_total_time_hours'] = plant_total_time
				reports['pr'] = get_performance_ratio(totals['total_export'],site.capacity,totals['total_rad'])


				
				'''
				 excel sheet generation

				'''
				
				filename = "xlssheets/dgr/"+str(form.cleaned_data['from_date'])+"-"+str(form.cleaned_data['to_date'])+"-"+str(site.id)+".xlsx"
				xl_url = settings.MEDIA_URL+filename
				workbook  = xlsxwriter.Workbook(settings.MEDIA_ROOT+filename)
				worksheet = workbook.add_worksheet("Summary")
				worksheet1 = workbook.add_worksheet("Main Meter Readings")
				worksheet2 = workbook.add_worksheet("DGR Backup")
				worksheet3 = workbook.add_worksheet("Inverter Comparision")
				worksheet4 = workbook.add_worksheet("Breakdown Sheet")
				worksheet5 = workbook.add_worksheet("Solar Radiation Data")
				worksheet6 = workbook.add_worksheet("Grid Unavailability")
				worksheet7 = workbook.add_worksheet("Module Cleaning")


			        bold = workbook.add_format({'bold': True})
				bold.set_border(1)	
				#worksheet.merge_range('A1:B1', 'Capacity')
			 	format = workbook.add_format({'bold': True})

				format.set_align('center')
				format.set_align('vcenter')
				format.set_border(1)

				format2 = workbook.add_format({'bold': True})
				format2.set_align('center')
				format2.set_align('vcenter')
					
				format2.set_font_size(20)

				merge_format2 = workbook.add_format({
				    'bold':     True,
				    'border':   6,
				    'align':    'center',
				    'valign':   'vcenter',
				    'fg_color': '#00B0F0',
				})
				merge_format2.set_font_size(20)
				merge_format3 = workbook.add_format({
				    'bold':     True,
				    'border':   6,
				    'align':    'center',
				    'valign':   'vcenter',
				    'fg_color': '#DBEEF4',
				})

				merge_format2.set_font_size(18)


				worksheet.set_column(0, 6, 30)
				worksheet1.set_column(0, 200, 22)
				worksheet2.set_column(0, 200, 22)
				worksheet3.set_column(0, 200, 22)
				worksheet4.set_column(0, 200, 22)
				worksheet5.set_column(0, 200, 22)
				worksheet6.set_column(0, 200, 22)
				worksheet7.set_column(0, 200, 22)
				

				worksheet.insert_image(0,0,settings.MEDIA_ROOT+site.company.logo.name,{'y_scale': 0.8,'x_scale':0.8})
				worksheet.set_row(0,20)
				worksheet.set_row(1,20)
				worksheet.set_row(2,20)
				worksheet.set_row(3,20)
				worksheet.set_row(4444,20)
				worksheet.merge_range('B1:F1', site.company.name,merge_format2)
				worksheet.merge_range('B2:F2',"SITE:  "+site.name,merge_format2)
				worksheet.merge_range('B3:F3',"CAPACITY:  "+str(site.capacity)+"MW",merge_format3)
				worksheet.merge_range('B4:F4',"FROM DATE:  "+str(date)+"   TO DATE:  "+str(end_date),format)
				worksheet.merge_range('B5:F5',"DGR REPORT",merge_format3)
	
				worksheet.write(6, 0, 'Date',bold)
				worksheet.write(6, 1, 'Generation in MWH',bold)
				worksheet.write(6, 2, 'PR',bold)
				worksheet.write(6, 3, 'CUF %',bold)
				worksheet.write(6, 4, 'Grid Outage',bold)
				worksheet.write(6, 5, 'Breakdown (MWH)',bold)
			
				worksheet5.merge_range("A1:D1","Solar Radiation and Weather Report",merge_format2)	
				worksheet6.merge_range("A1:I1","Grid Unavailability Status",merge_format2)	
				worksheet5.set_row(0,30)
				worksheet6.set_row(0,30)
				worksheet5.write(1, 0, 'Date',bold)
				worksheet5.write(1, 1, 'Solar Radiation',bold)
				worksheet5.write(1, 2, 'Wind Speed',bold)
				worksheet5.write(1, 3, 'Avg. Temperature',bold)


				row = 1
				col = 0
				row2 = 7
				#worksheet  = summary 
				for data in report:
					date = str(data['dgr'].date)
					time = str(data['dgr'].upload_time)
					worksheet.write(row2, col,  date)
					worksheet.write(row2, col+1,  round(data['dgr'].tvm_export_value,2))
					worksheet.write(row2, col+2,  round(data['pr'],2))
				
					worksheet.write(row2, col+3,  data['cuf'])
					worksheet.write(row2, col+4,  data['total_time'])

					worksheet1.write(row+2, col,  date)
					worksheet1.write(row+2, col+1,  time)
					worksheet1.write(row+2, col+2,  round(data['dgr'].tvm_export,2))
					worksheet1.write(row+2, col+3,  round(data['dgr'].tvm_import,2))
					worksheet1.write(row+2, col+4,  round(data['dgr'].tvm_export_value,2))
					worksheet1.write(row+2, col+5,  round(data['dgr'].tvm_import_value,2))
					worksheet1.write(row+2, col+6,  data['dgr'].note)
					
					worksheet5.write(row+1, col,  date)
					worksheet5.write(row+1, col+1,  data['dgr'].dgrweather.radiation)
					worksheet5.write(row+1, col+2,  data['dgr'].dgrweather.wind_speed)
					worksheet5.write(row+1, col+3,  data['dgr'].dgrweather.temperature)
						
					row2 = row2+1

					row = row+1

				#worksheet 1 = main meter readings 
			
			       	
				worksheet1.set_row(0,25)
				worksheet1.merge_range('A1:G1', 'MAIN METER READING', merge_format2)
				worksheet1.merge_range('A2:B2', 'Capacity:'+str(site.ac_capacity),format)
				worksheet1.merge_range('C2:D2', 'Main Meter Readings',format)
				worksheet1.merge_range('E2:F2', 'Calculated Values',format)



				worksheet1.write(2, 0, 'Date',bold)
				worksheet1.write(2, 1, 'Time',bold)
				worksheet1.write(2, 2, 'Export(MWh)',bold)
				worksheet1.write(2, 3, 'Import MWH',bold)
				worksheet1.write(2, 4, 'Daily Generation (MWh)',bold)
				worksheet1.write(2, 5, 'Daily Import (MWh)',bold)
				worksheet1.write(2, 6, 'Comments',bold)


				#worksheet 2 = DGR backup

				worksheet2.write(5, 0, 'Date',bold)
				worksheet3.write(5, 0, 'Date',bold)
				col = 1
				col2 = 1
				row = 5
				inverters = Inverter.objects.filter(site=site)
				blocks = Block.objects.filter(site=site)
				inverterdata = []
				date = form.cleaned_data['from_date']
				end_date = form.cleaned_data['to_date']

				for inverter in inverters:
					data = {}
					data['inverter'] = inverter
				
					data['dgrinverters'] = DGRInverter.objects.filter(inverter=inverter,dgr__date__range=[date,end_date])
					inverterdata.append(data)

				for block in blocks:
					cell_range = xl_range(3,col2,3,col2+1)
					cell_range2 = xl_range(3,col2,3,col2+4)
					worksheet2.merge_range(cell_range, block.block,format)
					worksheet3.merge_range(cell_range2,block.block,format)
					col2 = col2+2
					
				for inverter in inverterdata:
					worksheet2.write(row, col, inverter['inverter'].name,bold)
					worksheet3.write(row, col, inverter['inverter'].name,bold)
					row2 = 6
					for readings in inverter['dgrinverters']:
						avg = DGRInverter.objects.filter(dgr=readings.dgr).aggregate(avg=Avg('generation'))['avg']
						worksheet2.write(row2, col, readings.generation)
						worksheet2.write(row2, 0, str(readings.dgr.date))
						worksheet3.write(row2, col+3, avg-readings.generation)
						worksheet3.write(row2, col, readings.generation)
						worksheet3.write(row2, 0, str(readings.dgr.date))

						row2 = row2+1
					col = col+1
				worksheet3.set_row(0,20)
				worksheet3.merge_range("A1:E1","Individual Inverters Generation Report",merge_format2)
				worksheet2.set_row(0,30)
				worksheet3.set_row(0,30)
				worksheet2.merge_range("A1:C1","Individual Inverters Generation Report",merge_format2)
				worksheet3.write(5, col, 'Avg Generation (MWh)',bold)
				worksheet2.write(4, 0,"Connected Load",bold)
				worksheet3.write(4, 0,"Connected Load",bold)

				worksheet2.write(3, col,"Total",bold)
				worksheet2.write(3, col+1,"Gross MWh",bold)
				worksheet2.write(3, col+2,"Import MWh",bold)
				worksheet2.write(3, col+3,"Import %",bold)
				worksheet2.write(3, col+4,"Line Loses",bold)
				worksheet2.write(3, col+5,"% Plant Availability",bold)
				worksheet2.write(3, col+6,"% Grid Availability",bold)
				worksheet2.write(3, col+7,"% Generation Availability",bold)
				worksheet2.write(3, col+8,"Daily Radiation",bold)
				worksheet2.write(3, col+9,"Plant Capacity Connected",bold)
				worksheet2.write(3, col+10,"% PR",bold)
				worksheet2.write(3, col+11,"% CUF",bold)

				cell_range = xl_range(0,col,1, col+11)


				worksheet2.merge_range(cell_range, 'Daily',format)

				# sheet3
				row = 5
				col2 = col+1
				 
				for inverter in inverterdata:
					worksheet3.write(row, col2, inverter['inverter'].name,bold)
					col2 = col2+1

				row = 6
				for data in report:
					worksheet2.write(row, col,data['inverter_total'])
					worksheet3.write(row, col,data['inverter_avg']) #sheet 3
					worksheet2.write(row, col+1,data['dgr'].tvm_export_value)
					worksheet2.write(row, col+2,data['dgr'].tvm_import_value)
					try:
						worksheet2.write(row, col+3,data['dgr'].tvm_import_value/data['dgr'].tvm_export_value)
					except:
						worksheet2.write(row, col+3,'-')
					worksheet2.write(row, col+4,"N/A")
					worksheet2.write(row, col+5,"")
					worksheet2.write(row, col+6,data['grid_availability'])
					worksheet2.write(row, col+7,"")
					worksheet2.write(row, col+8,data['dgr'].dgrweather.radiation)
					worksheet2.write(row, col+9,site.ac_capacity)
					worksheet2.write(row, col+10,data['pr'])
					worksheet2.write(row, col+11,data['cuf'])
					row = row+1

				#worksheet2.write(3, col+12,"Total",bold)
				#worksheet2.write(3, col+13,"Gross MWh",bold)
				#worksheet2.write(3, col+14,"Import MWh",bold)
				#worksheet2.write(3, col+15,"Import %",bold)
				#worksheet2.write(3, col+16,"Line Loses",bold)
				#worksheet2.write(3, col+17,"% Plant Availability",bold)
				#worksheet2.write(3, col+18,"% Grid Availability",bold)
				#worksheet2.write(3, col+19,"% Generation Availability",bold)
				#worksheet2.write(3, col+20,"Daily Radiation",bold)
				#worksheet2.write(3, col+21,"Plant Capacity Connected",bold)
				#worksheet2.write(3, col+22,"% PR",bold)
				#worksheet2.write(3, col+23,"% CUF",bold)
				
				cell_range = xl_range(0,col+12,1, col+23)


				#worksheet2.merge_range(cell_range, 'Monthly',format)





				#sheet 4

				col = -1
				worksheet4.set_row(0,30)
				merge_format = workbook.add_format({
				   'bold':     True,
				   'border':   6,
				   'align':    'center',
				   'valign':   'vcenter',
				   'fg_color': '#D7E4BC',
				})

				worksheet4.merge_range("A1:V1","BREAKDOWN SHEET",merge_format)
				worksheet4.write(1, col+1,"Date",bold)	
				worksheet4.write(1, col+2,"Month",bold)
				worksheet4.write(1, col+3,"Location",bold)	
				worksheet4.write(1, col+4,"Equipment",bold)
				worksheet4.write(1, col+5,"Section",bold)
				worksheet4.write(1, col+6,"Sub Category",bold)
				worksheet4.write(1, col+7,"Frame",bold)
				worksheet4.write(1, col+8,"SMU No.",bold)
				worksheet4.write(1, col+9,"AJB No.",bold)	
				worksheet4.write(1, col+10,"BD From(Time)24 hours format",bold)	
				worksheet4.write(1, col+11,"BD up to (Time) 24 hours format",bold)	
				worksheet4.write(1, col+12,"BD Duration (hours)",bold)	
				worksheet4.write(1, col+13,"Generation Loss (Yes/No)",bold)	
				worksheet4.write(1, col+14,"Capacity down due to BD (MW)",bold)	
				worksheet4.write(1, col+15,"Generation Lost(MWh)",bold)	
				worksheet4.write(1, col+16,"Plant Downtime %",bold)	
				worksheet4.write(1, col+17,"Error  Code",bold)	
				worksheet4.write(1, col+18,"Fault Cause/ Description",bold)	
				worksheet4.write(1, col+19,"Action Taken",bold)	
				worksheet4.write(1, col+20,"Reason of Breakdown",bold)	
				worksheet4.write(1, col+21,"Present Status",bold)	
				worksheet4.write(1, col+22,"Attended By",bold)	

				date = form.cleaned_data['from_date']
				end_date = form.cleaned_data['to_date']

	
				breakdown = DGRPlantDownTime.objects.filter(dgr__date__range=[date,end_date])
				col = -1
				row = 2
				for bd in breakdown:
					worksheet4.write(row, col+1,str(bd.dgr.date))	
					worksheet4.write(row, col+2,bd.dgr.date.month)
					#worksheet4.write(row, col+3,bd.location.name)	
					worksheet4.write(row, col+4,bd.equipment.name)
					try:
						worksheet4.write(row, col+5,bd.sub_category.name)
					except:
						worksheet4.write(row, col+5,"-")
					worksheet4.write(row, col+6,"-")
					worksheet4.write(row, col+7,bd.frame)
					worksheet4.write(row, col+8,bd.smu_no)
					worksheet4.write(row, col+9,bd.ajb)	
					worksheet4.write(row, col+10,bd.from_time)	
					worksheet4.write(row, col+11,bd.to_time)	
					worksheet4.write(row, col+12,bd.total_time)	
					worksheet4.write(row, col+13,"Yes")	
					worksheet4.write(row, col+14,"")
					genloss = genloss_calculation(bd)	
					worksheet4.write(row, col+15,genloss)	
					worksheet4.write(row, col+16,"")	
					worksheet4.write(row, col+17,bd.error_code)	
					worksheet4.write(row, col+18,bd.cause)	
					worksheet4.write(row, col+19,bd.action_taken)	
					worksheet4.write(row, col+20,bd.reason)	
					worksheet4.write(row, col+21,bd.present_status)	
					worksheet4.write(row, col+22,bd.attended_by)	
					row = row+1

				#sheet 6 = grid availability
				col = -1
				
				worksheet6.write(1, col+1,"Date",bold)	
				worksheet6.write(1, col+2,"Location",bold)	
				worksheet6.write(1, col+3,"BD From(Time)24 hours format",bold)	
				worksheet6.write(1, col+4,"BD up to (Time) 24 hours format",bold)	
				worksheet6.write(1, col+5,"BD Duration (hours)",bold)	
				worksheet6.write(1, col+6,"Fault Cause/ Description",bold)	
				worksheet6.write(1, col+7,"Action Taken",bold)	
				worksheet6.write(1, col+8,"Present Status",bold)	
				worksheet6.write(1, col+9,"Remarks",bold)	


	
				breakdown = DGRGridDownTime.objects.filter(dgr__date__range=[date,end_date])
				col = -1
				row = 2
				for bd in breakdown:
					worksheet6.write(row, col+1,str(bd.dgr.date))	
					worksheet6.write(row, col+2,bd.location)	
					worksheet6.write(row, col+3,bd.from_time)	
					worksheet6.write(row, col+4,bd.to_time)	
					worksheet6.write(row, col+5,bd.total_time)	
					worksheet6.write(row, col+6,bd.cause)	
					worksheet6.write(row, col+7,bd.action_taken)	
					worksheet6.write(row, col+8,bd.present_status)	
					worksheet6.write(row, col+9,bd.remarks)	
					row = row+1


			 	#sheet 7 - module cleaning
				all_blocks = []
				for block in blocks:
					data = {}
					modules = ConnectedModule.objects.filter(inverter__block=block).aggregate(total=Sum('quantity'))['total']
					day = datetime.timedelta(days=1)
					cleaning_reports = []
					while date <= end_date:
						data = {}
						cleaned = ModuleCleaned.objects.filter(block=block,dgr__date=date).aggregate(cleaned=Sum('cleaned'))['cleaned']
						if not cleaned:
							cleaned = 0
						prev_cleaned = ModuleCleaned.objects.filter(block=block,dgr__date=date-day).aggregate(cleaned=Sum('cleaned'))['cleaned']
						if not prev_cleaned:
							prev_cleaned = 0
						data['cleaned'] = cleaned
						if prev_cleaned+cleaned <= modules and cleaned != 0:
							data['cumulative'] = prev_cleaned+cleaned
							data['balance'] = modules - (prev_cleaned+cleaned)
						else:
							data['cumulative'] = cleaned
							try:
								data['balance'] = modules - cleaned
							except:			
								data['balance'] = modules
	
	
						data['date'] = date
						date = date+day
						cleaning_reports.append(data)
		
					data['block'] = block
					data['reports'] = cleaning_reports
					data['total'] = modules
					all_blocks.append(data)
				col = 0

				worksheet7.merge_range("A1:E1","Module Cleaning Report",merge_format3)
				worksheet7.set_row(0,30)
				for block in all_blocks:
					
					worksheet7.write(3, 0, "Date",bold)	
					worksheet7.write(3, col+1,"Cleaned",bold)	
					worksheet7.write(3, col+2,"Cumulative Cleaned",bold)	
					worksheet7.write(3, col+3,"Balance",bold)	
					worksheet7.write(3, col+4,"Total Cleaned per day",bold)	
					cell_range = xl_range(2,col+1,2,col+4)
					worksheet7.merge_range(cell_range, "Block - "+block['block'].block,format)	
					row = 4
					for data in block['reports']:
						worksheet7.write(row, 0,str(data['date']))	
						worksheet7.write(row, col+1,data['cleaned'])	
						worksheet7.write(row, col+2,data['cumulative'])	
						worksheet7.write(row, col+3,data['balance'])	
						worksheet7.write(row, col+4,data['cleaned'])	
						row = row+1		
					col = col+5
				workbook.close() #workbook close
				if "standalone" in request.POST:
					return render_to_response('dgr_standalone.html',{'xl_url': xl_url,'form':form,'reports':reports,'from_date':date,'site':site,'to_date':end_date},
					context_instance=RequestContext(request))


				if form.cleaned_data['report_type'] == "radiation":
		
					return render_to_response('reports_radiation.html',{'xl_url': xl_url,'form':form,'reports':reports,'from_date':date,'site':site,'to_date':end_date},
					context_instance=RequestContext(request))
				else:
					return render_to_response('reports_generation.html',{'xl_url': xl_url,'form':form,'reports':reports,'from_date':date,'site':site,'to_date':end_date},
					context_instance=RequestContext(request))
	return render_to_response('reports_generation.html',{'form':form,},
				context_instance=RequestContext(request))


'''
@login_required
def manage_reports_generation(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	form = GenerationReportForm(company=usercompany.company)
	if request.method == "POST":
		form = GenerationReportForm(company=usercompany.company,data=request.POST)
		if form.is_valid():
			date = form.cleaned_data['date']
			sites = form.cleaned_data['sites']
			duration = form.cleaned_data['duration']
			reports = []
			if duration == "daily":
					end_date = ''
			if duration == "weekly":
					end_date = date+timedelta(days=7)
			if duration == "monthly":
					end_date = add_months(date,1) 
			if duration == "yearly":
					end_date = add_months(date,12) 
				
				
			for site in sites:
				data = {}
				if duration == "daily":
					dgr = DGR.objects.filter(site=site,date=date)
					totals = DGR.objects.filter(site=site,date=date).aggregate(total_export=Sum('tvm_export_value'),total_import=Sum('tvm_import_value'))
				if duration == "weekly":
					end_date = date+timedelta(days=7)
					dgr = DGR.objects.filter(site=site,date__range=[date,end_date])
					totals = DGR.objects.filter(site=site,date__range=[date,end_date]).aggregate(total_export=Sum('tvm_export_value'),total_import=Sum('tvm_import_value'))
					
					
				if duration == "monthly":
					end_date = add_months(date,1) 
					dgr = DGR.objects.filter(site=site,date__range=[date,end_date])
					totals = DGR.objects.filter(site=site,date__range=[date,end_date]).aggregate(total_export=Sum('tvm_export_value'),total_import=Sum('tvm_import_value'))
	
				if duration == "yearly":
					end_date = add_months(date,12) 
					dgr = DGR.objects.filter(site=site,date__range=[date,end_date])
					totals = DGR.objects.filter(site=site,date__range=[date,end_date]).aggregate(total_export=Sum('tvm_export_value'),total_import=Sum('tvm_import_value'))
	

				if dgr:
					data['dgr'] = dgr
					data['site'] = site		
					data['totals'] = totals
					reports.append(data)
		return render_to_response('reports_generation.html',{'form':form,'reports':reports,'end_date':end_date,'date':date,'sites':sites,'duration':duration},
				context_instance=RequestContext(request))
	return render_to_response('reports_generation.html',{'form':form,},
				context_instance=RequestContext(request))
'''

@login_required
def manage_reports_cleaning(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)

	form = GenerationReportForm(sites=sites)

	if request.method == "POST":
		form = GenerationReportForm(sites=sites,data=request.POST)
		if form.is_valid():
			date = form.cleaned_data['from_date']
			site = form.cleaned_data['site']
			end_date = form.cleaned_data['to_date']
			year = form.cleaned_data['year']
			from_year = int(form.cleaned_data['from_year'])
			to_year = int(form.cleaned_data['to_year'])
			format = form.cleaned_data['format']
			reports = []

			if format == "yearly":
				blocks = Block.objects.filter(site=site)
				for block in blocks:

					data2 = {}
					total_modules = ConnectedModule.objects.filter(inverter__block=block).aggregate(total=Sum('quantity'))['total']
					report = []
					for i in range(12):
						data = {}
						cleaned = ModuleCleaned.objects.filter(block=block,dgr__date__year=year,dgr__date__month=i+1).aggregate(total=Sum('cleaned'))['total']
						try:
							data['cleaned_times'] = cleaned/total_modules	
						except:
							data['cleaned_times'] = 0	
						data['month'] = months[i]
						report.append(data)
					data2['report'] = report
					data2['block'] = block
					reports.append(data2)
				
				return render_to_response('reports_module_cleaning_yearly.html',{'form':form,'reports':reports,'site':site,'year':year},
				context_instance=RequestContext(request))
			if format == "yearwise":
				blocks = Block.objects.filter(site=site)
				for block in blocks:

					data2 = {}
					total_modules = ConnectedModule.objects.filter(inverter__block=block).aggregate(total=Sum('quantity'))['total']
					report = []
					year = from_year
					while year <= to_year:
						data = {}
						cleaned = ModuleCleaned.objects.filter(block=block,dgr__date__year=year).aggregate(total=Sum('cleaned'))['total']
						try:
							data['cleaned_times'] = cleaned/total_modules	
						except:
							data['cleaned_times'] = 0	
						data['month'] = year
						year = year+1
						report.append(data)
					data2['report'] = report
					data2['block'] = block
					reports.append(data2)
				
				return render_to_response('reports_module_cleaning_yearly.html',{'form':form,'reports':reports,'site':site,'from_year':from_year,'to_year':to_year,'format':format},
				context_instance=RequestContext(request))


			if format == "daily":
				blocks = Block.objects.filter(site=site)
				for block in blocks:
					data2 = {} 
					cleaned = ModuleCleaned.objects.filter(dgr__date=date,dgr__site=site,block=block)
					all_cleaned = cleaned.aggregate(total=Sum('cleaned'))['total']
					for cleaning in cleaned:
						data = {} 
						data['modules'] = ConnectedModule.objects.filter(inverter__block=block).aggregate(total=Sum('quantity'))['total']
						total_cleaned = ModuleCleaned.objects.filter(block=block,dgr__date__lte=cleaning.dgr.date).aggregate(total=Sum('cleaned'))['total']	
						try:
							data['accumulated'] = total_cleaned%data['modules']
						except:
							data['accumulated'] = 0
						try:

							data['remaining'] = data['modules'] - data['accumulated']
						except:
							data['remaining'] = data['modules']
						data['today'] = cleaning
						data2['reports'] = data
					data2['block'] = block
					reports.append(data2)

				return render_to_response('reports_module_cleaning.html',{'form':form,'reports':reports,'site':site,'from_date':date,'to_date':end_date,},
				context_instance=RequestContext(request))
	
			if format == "monthly":
				blocks = Block.objects.filter(site=site)
				for block in blocks:

					report = {} 
					data2 = [] 
					cleaned = ModuleCleaned.objects.filter(dgr__date__range=[date,end_date],dgr__site=site,block=block).order_by('dgr__date')
					all_cleaned = cleaned.aggregate(total=Sum('cleaned'))['total']
					for cleaning in cleaned:
						data = {} 

						data['modules'] = ConnectedModule.objects.filter(inverter__block=block).aggregate(total=Sum('quantity'))['total']
						total_cleaned = ModuleCleaned.objects.filter(block=block,dgr__date__lte=cleaning.dgr.date).aggregate(total=Sum('cleaned'))['total']	
						try:
							data['accumulated'] = total_cleaned%data['modules']
						except:
							data['accumulated'] = 0
						try:
							data['remaining'] = data['modules'] - data['accumulated']
						except:	
							data['remaining'] = data['modules']
						data['today'] = cleaning
						data2.append(data)
					report['block'] = block
					report['data'] = data2
					reports.append(report)

				return render_to_response('reports_module_cleaning_monthly.html',{'form':form,'reports':reports,'site':site,'from_date':date,'to_date':end_date,},
				context_instance=RequestContext(request))
	

				return render_to_response('reports_module_cleaning_monthly.html',{'form':form,'reports':reports,'site':site,'from_date':date,'to_date':end_date,'total':total_modules,'cleaned':all_cleaned},
				context_instance=RequestContext(request))
	
				
				return render_to_response('reports_module_cleaning.html',{'form':form,'reports':reports,'site':site,'from_date':date,'to_date':end_date,'total':total_modules,'cleaned':total_cleaned},
				context_instance=RequestContext(request))
	return render_to_response('reports_module_cleaning.html',{'form':form,},
				context_instance=RequestContext(request))



@login_required
def manage_reports_breakdown(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)

	form = GenerationReportForm(sites=sites)

	if request.method == "POST":
		form = GenerationReportForm(sites=sites,data=request.POST)
		if form.is_valid():
			format = form.cleaned_data['format']
		
			site = form.cleaned_data['site']
			if format == "monthly"  or format == "weekly" or format == "daily":
				date = form.cleaned_data['from_date']
				reports = {}	
				total_grid_down = 0
				total_time_hours = 0
				report = []

				if format != "daily":
					end_date = form.cleaned_data['to_date']
						
					griddown = DGRGridDownTime.objects.filter(dgr__site=site,dgr__date__range=[date,end_date])
					plantdown = DGRPlantDownTime.objects.filter(dgr__site=site,dgr__date__range=[date,end_date])

					aggregate_loss = 0
					for event in plantdown:
						data = {}
						try:
							data['genloss'] =  genloss_calculation(event)
						except:
							data['genloss'] = 0
						data['per'] = (event.total_time/12)*100
						data['query'] = event
						report.append(data)	


				else:
					griddown = DGRGridDownTime.objects.filter(dgr__site=site,dgr__date = date)
					plantdown = DGRPlantDownTime.objects.filter(dgr__site=site,dgr__date=date)
					end_date = ''
					for event in plantdown:
						data = {}
						try:
							data['genloss'] =  genloss_calculation(event)
						except:
							data['genloss'] = 0
						data['query'] = event
						data['per'] = (event.total_time/12)*100
						report.append(data)	


					
				if form.cleaned_data['report_type'] == "breakdown":
					return render_to_response('reports_plantdown.html',{'form':form,'reports':report,'from_date':date,'site':site,'to_date':end_date},
						context_instance=RequestContext(request))
				else:
					return render_to_response('reports_breakdown.html',{'form':form,'reports':griddown,'from_date':date,'site':site,'to_date':end_date},
						context_instance=RequestContext(request))
			if format == "yearwise":
				from_year = int(form.cleaned_data['from_year'])
				to_year = int(form.cleaned_data['to_year'])
				allmonths = []
				total_import = 0.0
				total_export = 0.0
				total_radiation = 0.0

				total_grid_down_count= 0.0 #grid
				total_grid_down_time = 0.0 #grid

				plant_total_grid_down_count= 0.0 #plant
				plant_total_grid_down_time = 0.0 #plant

				total_genloss = 0.0
				grid_total_genloss = 0.0
				
				year = from_year	
				while year <= to_year:

					reports = {}
					dgr = DGR.objects.filter(date__year=year,site=site)
					totals = DGR.objects.filter(site=site,date__year=year).aggregate(total_export=Sum('tvm_export_value'),total_import=Sum('tvm_import_value'),avg_radiation=Sum('dgrweather__radiation'))
					try:
						total_export = total_export+totals['total_export']
					except:
						pass
					try:
						total_import = total_import+totals['total_import']
					except:
						pass
			
					try:
						total_radiation = total_radiationt+totals['avg_radiation']
					except:
						pass
	
					total_grid_down = 0 #grid
					total_time_hours = 0 #grid
				
					plant_total_grid_down = 0 #plant
					plant_total_time_hours = 0 #plant
				
					plant_down_query = DGRPlantDownTime.objects.filter(dgr=dgr)	
					grid_down_query = DGRGridDownTime.objects.filter(dgr=dgr)	

					aggregate_loss = 0
					for query in plant_down_query:
						try:
							aggregate_loss = aggregate_loss + genloss_calculation(query)
						except:
							pass
					reports['genloss']  = aggregate_loss 

					total_genloss = total_genloss+aggregate_loss
					
					grid_aggregate_loss = 0
					for query in grid_down_query:
						try:
							grid_aggregate_loss = grid_aggregate_loss + genloss_calculation(query)
						except:
							pass
					reports['grid_genloss']  = grid_aggregate_loss 

					grid_total_genloss = grid_total_genloss+grid_aggregate_loss
					


					report = []
					reports['month'] = year
					for dgrs in dgr:
						data = {}
						griddown_counts = DGRGridDownTime.objects.filter(dgr=dgrs).count() #grid
						plant_griddown_counts = DGRPlantDownTime.objects.filter(dgr=dgrs).count() #plant
						time_hours = DGRGridDownTime.objects.filter(dgr=dgrs).aggregate(total=Sum('total_time'))['total'] #grid
						plant_time_hours = DGRPlantDownTime.objects.filter(dgr=dgrs).aggregate(total=Sum('total_time'))['total'] #plant
						
						#grid
						if time_hours!=None:
						
							total_time_hours = time_hours+total_time_hours 
							total_time = convert_time(time_hours) 
						else:
							total_time = "00:00"
						
						total_grid_down =  griddown_counts+total_grid_down #grid

						#plant
						if plant_time_hours!=None:
						
							plant_total_time_hours = plant_time_hours+plant_total_time_hours 
							plant_total_time = convert_time(plant_time_hours) 
						else:
							plant_total_time = "00:00"
						
						total_grid_down =  griddown_counts+total_grid_down #grid
						plant_total_grid_down =  plant_griddown_counts+plant_total_grid_down #plant



						data['dgr']= dgrs
						data['griddown_counts'] = griddown_counts #grid
						data['plant_griddown_counts'] = plant_griddown_counts #plant
						data['total_time'] = total_time
						report.append(data)
					
						
				
						
					total_time = convert_time(total_time_hours) #grid
					plant_total_time = convert_time(plant_total_time_hours) #plant

						
					reports['dgr'] = report
					reports['totals'] = totals
					reports['total_grid_down']= total_grid_down #grid
					reports['total_time_hours'] = total_time #grid

					total_grid_down_count = total_grid_down_count+total_grid_down #grid
					total_grid_down_time = total_time_hours+total_grid_down_time #grid

					reports['plant_total_grid_down']= plant_total_grid_down #plant
					reports['plant_total_time_hours'] = plant_total_time #plant
					reports['plant_availability'] = round(100 - (float(plant_total_time_hours*100))/(12*30),2)
					reports['grid_availability'] = round(100 - (float(total_time_hours)*100)/(12*30),2)


					plant_total_grid_down_count = plant_total_grid_down_count+plant_total_grid_down #plant
					plant_total_grid_down_time = plant_total_time_hours+plant_total_grid_down_time #plant


					allmonths.append(reports)

					year = year+1	
						
					total_time = convert_time(total_grid_down_time) #grid
					plant_total_time = convert_time(plant_total_grid_down_time) #plant

				other_data = {}
				other_data['total_time'] = total_time #grid
				other_data['grid_count'] = total_grid_down_count #grid
				other_data['plant_total_time'] = plant_total_time #plant
				other_data['plant_grid_count'] = plant_total_grid_down_count #plant
				other_data['genloss'] = total_genloss
				other_data['grid_genloss'] = grid_total_genloss
				
				if form.cleaned_data['report_type'] == "breakdown":
					return render_to_response('reports_breakdown_yearly.html',{'form':form,'from_year':from_year,'to_year':to_year,'site':site,'reports':allmonths,'other_data':other_data,'format':format},
					context_instance=RequestContext(request))
				else:
					return render_to_response('reports_griddown_yearly.html',{'form':form,'from_year':from_year,'to_year':to_year,'site':site,'reports':allmonths,'other_data':other_data,'format':format},
					context_instance=RequestContext(request))



			if format == "yearly":
				year = form.cleaned_data['year']
				allmonths = []
				months = ['January','February','March','April','May','June','July','August','September','October','November','December']
				total_import = 0.0
				total_export = 0.0
				total_radiation = 0.0

				total_grid_down_count= 0.0 #grid
				total_grid_down_time = 0.0 #grid

				plant_total_grid_down_count= 0.0 #plant
				plant_total_grid_down_time = 0.0 #plant

				total_genloss = 0.0
				grid_total_genloss = 0.0
				
				
				for i in range(12):

					reports = {}
					dgr = DGR.objects.filter(site=site,date__month=i+1,date__year=year)
					totals = DGR.objects.filter(site=site,date__month=i+1,date__year=year).aggregate(total_export=Sum('tvm_export_value'),total_import=Sum('tvm_import_value'),avg_radiation=Sum('dgrweather__radiation'))
					try:
						total_export = total_export+totals['total_export']
					except:
						pass
					try:
						total_import = total_import+totals['total_import']
					except:
						pass
			
					try:
						total_radiation = total_radiationt+totals['avg_radiation']
					except:
						pass
	
					total_grid_down = 0 #grid
					total_time_hours = 0 #grid
				
					plant_total_grid_down = 0 #plant
					plant_total_time_hours = 0 #plant
				
					plant_down_query = DGRPlantDownTime.objects.filter(dgr=dgr)	
					grid_down_query = DGRGridDownTime.objects.filter(dgr=dgr)	

					aggregate_loss = 0
					for query in plant_down_query:
						try:
							aggregate_loss = aggregate_loss + genloss_calculation(query)
						except:
							pass
					reports['genloss']  = aggregate_loss 

					total_genloss = total_genloss+aggregate_loss
					
					grid_aggregate_loss = 0
					for query in grid_down_query:
						try:
							grid_aggregate_loss = grid_aggregate_loss + genloss_calculation(query)
						except:
							pass
					reports['grid_genloss']  = grid_aggregate_loss 

					grid_total_genloss = grid_total_genloss+grid_aggregate_loss
					


					report = []
					reports['month'] = months[i]
					for dgrs in dgr:
						data = {}
						griddown_counts = DGRGridDownTime.objects.filter(dgr=dgrs).count() #grid
						plant_griddown_counts = DGRPlantDownTime.objects.filter(dgr=dgrs).count() #plant
						time_hours = DGRGridDownTime.objects.filter(dgr=dgrs).aggregate(total=Sum('total_time'))['total'] #grid
						plant_time_hours = DGRPlantDownTime.objects.filter(dgr=dgrs).aggregate(total=Sum('total_time'))['total'] #plant
						
						#grid
						if time_hours!=None:
						
							total_time_hours = time_hours+total_time_hours 
							total_time = convert_time(time_hours) 
						else:
							total_time = "00:00"
						
						total_grid_down =  griddown_counts+total_grid_down #grid

						#plant
						if plant_time_hours!=None:
						
							plant_total_time_hours = plant_time_hours+plant_total_time_hours 
							plant_total_time = convert_time(plant_time_hours) 
						else:
							plant_total_time = "00:00"
						
						total_grid_down =  griddown_counts+total_grid_down #grid
						plant_total_grid_down =  plant_griddown_counts+plant_total_grid_down #plant



						data['dgr']= dgrs
						data['griddown_counts'] = griddown_counts #grid
						data['plant_griddown_counts'] = plant_griddown_counts #plant
						data['total_time'] = total_time
						report.append(data)
					
						
				
						
					total_time = convert_time(total_time_hours) #grid
					plant_total_time = convert_time(plant_total_time_hours) #plant

						
					reports['dgr'] = report
					reports['totals'] = totals
					reports['total_grid_down']= total_grid_down #grid
					reports['total_time_hours'] = total_time #grid

					total_grid_down_count = total_grid_down_count+total_grid_down #grid
					total_grid_down_time = total_time_hours+total_grid_down_time #grid

					reports['plant_total_grid_down']= plant_total_grid_down #plant
					reports['plant_total_time_hours'] = plant_total_time #plant
					reports['plant_availability'] = round(100 - (float(plant_total_time_hours*100))/(12*30),2)
					reports['grid_availability'] = round(100 - (float(total_time_hours)*100)/(12*30),2)


					plant_total_grid_down_count = plant_total_grid_down_count+plant_total_grid_down #plant
					plant_total_grid_down_time = plant_total_time_hours+plant_total_grid_down_time #plant


					allmonths.append(reports)

	
						
					total_time = convert_time(total_grid_down_time) #grid
					plant_total_time = convert_time(plant_total_grid_down_time) #plant

				other_data = {}
				other_data['total_time'] = total_time #grid
				other_data['grid_count'] = total_grid_down_count #grid
				other_data['plant_total_time'] = plant_total_time #plant
				other_data['plant_grid_count'] = plant_total_grid_down_count #plant
				other_data['genloss'] = total_genloss
				other_data['grid_genloss'] = grid_total_genloss
				
				if form.cleaned_data['report_type'] == "breakdown":
					return render_to_response('reports_breakdown_yearly.html',{'form':form,'year':year,'site':site,'reports':allmonths,'other_data':other_data},
					context_instance=RequestContext(request))
				else:
					return render_to_response('reports_griddown_yearly.html',{'form':form,'year':year,'site':site,'reports':allmonths,'other_data':other_data},
					context_instance=RequestContext(request))

	
	return render_to_response('reports_plantdown.html',{'form':form,},
				context_instance=RequestContext(request))

	
@login_required
def manage_reports_inverter(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	form = InverterReportForm(company=usercompany.company)
	if not usercompany.is_admin:
		usersites = UserSite.objects.filter(user=request.user).values("site")
		sites = EnergySite.objects.filter(pk__in=usersites)
	
		
		form.fields['site'].queryset = sites


	if not usercompany.is_admin:
		usersites = UserSite.objects.filter(user=request.user).values("site")
		sites = EnergySite.objects.filter(pk__in=usersites)
	
		
		form.fields['site'].queryset = sites


	if request.method == "POST":
		form = InverterReportForm(company=usercompany.company,data=request.POST)
		if form.is_valid():
			from_date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			if not to_date:			
				to_date = from_date
		
			site = form.cleaned_data['site']


			inverters = Inverter.objects.filter(site=site)

			total_plant_time = 0
			total_grid_time = 0
			plant_count = 0
			grid_count = 0
			readings = []	
			for inverter in inverters:
				data = {}
				data['inverter'] = inverter
				#try:
				data['dgr_inverter'] = DGRInverter.objects.filter(dgr__date__range=[from_date,to_date],inverter=inverter).aggregate(total_generation=Sum('generation'))['total_generation']
				plant_down_time = DGRPlantDownTime.objects.filter(dgr__date__range=[from_date,to_date],inverter=inverter).aggregate(total_time=Sum('total_time'))['total_time']
				data['plant_down_count'] = DGRPlantDownTime.objects.filter(dgr__date__range=[from_date,to_date],inverter=inverter).count()
				data['grid_down_count'] = DGRGridDownTime.objects.filter(dgr__date__range=[from_date,to_date],inverter=inverter).count()
				plant_count = plant_count + data['plant_down_count'] 
				grid_count = grid_count + data['grid_down_count'] 
				grid_down_time = DGRGridDownTime.objects.filter(dgr__date__range=[from_date,to_date],inverter=inverter).aggregate(total_time=Sum('total_time'))['total_time']
				if plant_down_time!=None:
					data['total_plant_time'] = convert_time(plant_down_time)
					total_plant_time = total_plant_time+plant_down_time
				if grid_down_time!=None:
					data['total_grid_time'] = convert_time(grid_down_time)
					total_grid_time = total_grid_time+grid_down_time
			
				readings.append(data)	

			totals = {}
			
			totals['generation'] = DGRInverter.objects.filter(dgr__date__range=[from_date,to_date],inverter__site=site).aggregate(total_generation=Sum('generation'))['total_generation']
			totals['total_plant_time'] = convert_time(total_plant_time)
			totals['total_grid_time'] = convert_time(total_grid_time)
			totals['grid_count'] = grid_count
			totals['plant_count'] = plant_count
			
			return render_to_response('reports_inverter_yearly.html',{'form':form,'from_date':from_date,'to_date':to_date,'site':site,'readings':readings,'totals':totals},
					context_instance=RequestContext(request))


	return render_to_response('reports_inverter_yearly.html',{'form':form,},
				context_instance=RequestContext(request))




@login_required
def manage_reports_pm(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)

	form = GenerationReportForm(sites=sites)



	if request.method == "POST":
		form = GenerationReportForm(sites=sites,data=request.POST)
		if form.is_valid():
			from_date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			if not to_date:
				to_date = from_date
			site = form.cleaned_data['site']
			type = form.cleaned_data['report_type']		
			format = form.cleaned_data['format']
			if format == "yearly":
				year = form.cleaned_data['year']
				allmonths = []
				months = ['January','February','March','April','May','June','July','August','September','October','November','December']
				i = 0
				while i <= 11:

					reports = {}
					reports['month'] = months[i]
					if type == "slippage":
						reports['pmalerts'] = PMAlert.objects.filter(site=site,due_date__month=i+1,due_date__year=year).filter(Q(due_date__lt=F('completed_date'))| Q(due_date__lt=datetime.datetime.now())).order_by('due_date')
	
					else:
						reports['pmalerts'] = PMAlert.objects.filter(site=site,due_date__month=i+1,due_date__year=year)
					if reports['pmalerts']:
						reports['total_completed'] = reports['pmalerts'].count()
					allmonths.append(reports)
					i = i +1 
				if type == "slippage":
					return render_to_response('reports_pmalerts_slippage.html',{'form':form,'reports':allmonths,'format':format,'year':year,'report_site': site},
						context_instance=RequestContext(request))
				else:
					return render_to_response('reports_pmalerts.html',{'form':form,'reports':allmonths,'format':format,'year':year,'report_site': site},
						context_instance=RequestContext(request))

			if format == "yearwise":
				from_year = int(form.cleaned_data['from_year'])
				to_year = int(form.cleaned_data['to_year'])
				year = from_year
				allmonths = []
				while year <= to_year:

					reports = {}
					reports['month'] = year
					if type == "slippage":
						reports['pmalerts'] = PMAlert.objects.filter(site=site,due_date__year=year).filter(Q(due_date__lt=F('completed_date'))| Q(due_date__lt=datetime.datetime.now())).order_by('due_date')
	

					else:
							
						reports['pmalerts'] = PMAlert.objects.filter(site=site,due_date__year=year)
					if reports['pmalerts']:
						reports['total_completed'] = reports['pmalerts'].count()
					allmonths.append(reports)
					year = year +1 
				if type == "slippage":
					return render_to_response('reports_pmalerts_slippage.html',{'form':form,'reports':allmonths,'format':format,'from_year':from_year,'to_year':to_year,'report_site': site},
						context_instance=RequestContext(request))
				else:
					return render_to_response('reports_pmalerts.html',{'form':form,'reports':allmonths,'format':format,'from_year':from_year,'to_year':to_year,'report_site': site},
						context_instance=RequestContext(request))


			if format == "daily":

				
				if type == "slippage":
					allmonths = PMAlert.objects.filter(site=site,due_date=from_date).filter(Q(due_date__lt=F('completed_date'))| Q(due_date__lt=datetime.datetime.now())).order_by('due_date')
	
					return render_to_response('reports_pmalerts_daily_slippage.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
					context_instance=RequestContext(request))
				else:
					allmonths = PMAlert.objects.filter(site=site,due_date=from_date).order_by('due_date')
					return render_to_response('reports_pmalerts_daily.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
					context_instance=RequestContext(request))

	
			if format == "monthly":

				if type == "slippage":
					allmonths = PMAlert.objects.filter(site=site,due_date__range=[from_date,to_date]).filter(Q(due_date__lt=F('completed_date'))| Q(due_date__lt=datetime.datetime.now())).order_by('due_date')
					return render_to_response('reports_pmalerts_daily_slippage.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
					context_instance=RequestContext(request))
				else:
					allmonths = PMAlert.objects.filter(site=site,due_date__range=[from_date,to_date]).order_by('due_date')
					return render_to_response('reports_pmalerts_daily.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
					context_instance=RequestContext(request))

		else:
			print form.errors
	return render_to_response('reports_pmalerts.html',{'form':form,},
				context_instance=RequestContext(request))

@login_required
def manage_reports_reminders(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)

	form = GenerationReportForm(sites=sites)



	if request.method == "POST":
		form = GenerationReportForm(sites=sites,data=request.POST)
		if form.is_valid():
			from_date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			if not to_date:
				to_date = from_date
			site = form.cleaned_data['site']
		
			format = form.cleaned_data['format']
			if format == "yearly":
				year = form.cleaned_data['year']
				allmonths = []
				months = ['January','February','March','April','May','June','July','August','September','October','November','December']
				i = 0
				while i <= 11:

					reports = {}
					reports['month'] = months[i]
					reports['pmalerts'] = ReminderAlert.objects.filter(site=site,due_date__month=i+1,due_date__year=year)
					if reports['pmalerts']:
						reports['total_completed'] = reports['pmalerts'].count()
					allmonths.append(reports)
					i = i +1 
				return render_to_response('reminders/reports.html',{'form':form,'reports':allmonths,'format':format,'year':year,'report_site': site},
				context_instance=RequestContext(request))
			if format == "yearwise":
				from_year = int(form.cleaned_data['from_year'])
				to_year = int(form.cleaned_data['to_year'])
				year = from_year
				allmonths = []
				while year <= to_year:

					reports = {}
					reports['month'] = year
					reports['pmalerts'] = ReminderAlert.objects.filter(site=site,due_date__year=year)
					if reports['pmalerts']:
						reports['total_completed'] = reports['pmalerts'].count()
					allmonths.append(reports)
					year = year +1 
				return render_to_response('reminders/reports.html',{'form':form,'reports':allmonths,'format':format,'from_year':from_year,'to_year':to_year,'report_site': site},
				context_instance=RequestContext(request))


			if format == "daily":
				allmonths = ReminderAlert.objects.filter(site=site,completed_date__year=from_date.year,completed_date__month=from_date.month,completed_date__day=from_date.day).order_by('due_date')
				return render_to_response('reminders/reports_daily.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
				context_instance=RequestContext(request))
	
			if format == "monthly":
				allmonths = ReminderAlert.objects.filter(site=site,completed_date__range=[from_date,to_date]).order_by('due_date')
			return render_to_response('reminders/reports_daily.html',{'form':form,'reports':allmonths,'format':format,'from_date':from_date,'to_date':to_date,'report_site': site},
				context_instance=RequestContext(request))
		else:
			print form.errors
	return render_to_response('reminders/reports.html',{'form':form,},
				context_instance=RequestContext(request))


def reset_confirm(request, uidb36=None, token=None):

    return password_reset_confirm(request, template_name='reset.html',
        uidb36=uidb36, token=token, post_reset_redirect=reverse('login'))


def reset(request):

    messages.success(request, 'Reset link sent to your email. Please check your inbox!')
    return password_reset(request, template_name='registration/reset.html',
        email_template_name='registration/reset_email.html',
        subject_template_name='registration/reset_subject.txt',
        post_reset_redirect=reverse('login'))

@login_required
def manage_plantwise(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	company = get_object_or_404(UserCompany,user=request.user).company

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)
	
	form = PlantWiseForm(sites=sites)

	PARA = (
		   ('generation','Generation'),
		('pr','Performance Ratio'),
		('cuf','CUF'),
		   ('radiation','Radiation'),
		   ('losses','Losses'),
		   ('eff','Generation Efficiency'),
		   ('revenue','Revenue'),
		  )
	form.fields['parameter'].choices = PARA
	breakdown_form = BreakdownAnalysisForm(sites=sites)
	losses_form = LossesForm(sites=sites)
	equipmentwise_form = EquipmentWiseForm(sites=sites)

	pmform = PMAnalysisForm(sites=sites)
	type = "generation"
	alerts = []
	if request.method == "POST":
		form = PlantWiseForm(sites=sites,data=request.POST)
		if form.is_valid():
			sites = form.cleaned_data['sites']
			from_date = form.cleaned_data['from_date']
			date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			from_year = form.cleaned_data['from_year']
			to_year = form.cleaned_data['to_year']
			duration = form.cleaned_data['duration']
			parameter = form.cleaned_data['parameter']
			start_date = form.cleaned_data['from_date']
			end_date = form.cleaned_data['to_date']

		else:
			print form.errors
		

	else:

		thirty_day = datetime.timedelta(days=10)
		sites = sites
		to_date = DGR.objects.filter(site__in=sites).order_by('-date')[0].date
		from_date = to_date - thirty_day
		date = from_date 
		start_date  = from_date
		end_date = to_date
		duration = 'daywise'
		parameter = 'generation' 
		if request.GET.get('type'):
			type = request.GET['type']
		
			if type == "generation":
				parameter =  "generation"
			if type == "breakdown":
				parameter = "breakdown"
			if type == "pm":
				parameter = "pm"
				deviation = 3
				deviation_count = datetime.timedelta(days=3)
				day = datetime.timedelta(days=1)
				today = datetime.datetime.now().date()
				pmalert = PMAlert.objects.filter(site__in=sites,due_date__range=[from_date,to_date])
				for alert in pmalert:
					data = {}
					if deviation:
						if alert.status != "completed":
							days = today - alert.due_date 
							if days >= deviation_count:
								data['delay'] = days
								data['alert'] = alert
								alerts.append(data)
						else:
							days = alert.completed_date.date() - alert.due_date
							if days >= deviation_count:
								data['delay'] = days
								data['alert'] = alert
								alerts.append(data)

					else:
						if alert.status != "completed":
							days = today - alert.due_date 
							data['delay'] = days
							data['alert'] = alert
							alerts.append(data)

						else:
							days = alert.completed_date.date() - alert.due_date
							data['delay'] = days
							data['alert'] = alert
							alerts.append(data)
			if type == "reminders":
				parameter = "reminders"
				deviation = 3
				deviation_count = datetime.timedelta(days=3)
				day = datetime.timedelta(days=1)
				today = datetime.datetime.now().date()

				pmalert = ReminderAlert.objects.filter(site__in=sites,due_date__range=[from_date,to_date])
				for alert in pmalert:
					data = {}
					if deviation:
						if alert.status != "completed":
							days = today - alert.due_date 
							if days >= deviation_count:
								data['delay'] = days
								data['alert'] = alert
								alerts.append(data)
						else:
							days = alert.completed_date.date() - alert.due_date
							if days >= deviation_count:
								data['delay'] = days
								data['alert'] = alert
								alerts.append(data)

					else:
						if alert.status != "completed":
							days = today - alert.due_date 
							data['delay'] = days
							data['alert'] = alert
							alerts.append(data)

						else:
							days = alert.completed_date.date() - alert.due_date
							data['delay'] = days
							data['alert'] = alert
							alerts.append(data)



			if type == "simulated":
				parameter = "generation"
		
				PARA = (
				   ('generation','Generation'),
				   ('pr','Performance Ratio'),
				   ('radiation','Radiation'),
				  )

				form.fields['parameter'].choices = PARA


	reports = []

	if duration == "yearly":
		start_date = datetime.date(int(from_year),1,1)
		end_date = datetime.date(int(to_year),12,31)
		while int(from_year) <= int(to_year):
			data2 = {}
			report = []
			for site in sites:
				data = {}	
				dgr = DGR.objects.filter(site=site,date__year=from_year)
			
				data['site'] = site
				abc = dgr.aggregate(abc=Sum('tvm_import_value')) #import
				xyz = dgr.aggregate(xyz=Sum('tvm_export_value')) #export
				try:
					total_radiation = round(dgr.aggregate(radiation=Sum('dgrweather__radiation'))['radiation'],2)     #radiation
					data['radiation'] = total_radiation
				except:
					data['radiation'] = 0
					total_radiation = 0

				if xyz['xyz'] and abc['abc']:
					total_generation = float(xyz['xyz']) 
					data['generation'] = round(total_generation,2)
	

				else:
					total_generation = 0
					data['generation'] = 0	
				if xyz['xyz']:		
					pr = get_performance_ratio(float(xyz['xyz']),site.capacity,float(total_radiation))
					data['pr'] = pr
				else:
					data['pr'] = 0
					pr = 0 
			
				data['revenue'] = dgr.aggregate(total=Sum('revenue'))['total']
				if calendar.isleap(int(from_year)): 
					days = 366
				else:
					days = 365
				data['cuf'] = round((total_generation*100)/(site.capacity*24*days),2)
				#plant breakdown

				plant_down_query = DGRPlantDownTime.objects.filter(dgr=dgr)
				data['plant_down_count'] = plant_down_query.count()
				plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']
				if plant_down_time:
					data['plant_down_time'] = convert_time(float(plant_down_time))
				else:
					data['plant_down_time'] = "0"
				aggregate_loss = 0
				for query in plant_down_query:
					try:
						aggregate_loss = aggregate_loss + genloss_calculation(query)
					except:
						pass
				data['genloss']  = aggregate_loss 

				#grid breakdown

				grid_down_query = DGRGridDownTime.objects.filter(dgr=dgr)
				data['grid_down_count'] =grid_down_query.count()
				grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
				if grid_down_time:
					data['grid_down_time'] = convert_time(float(grid_down_time))
				else:
					data['grid_down_time'] = "0"	
				aggregate_loss_grid = 0
				for query in grid_down_query:
					try:
						aggregate_loss_grid = aggregate_loss_grid + genloss_calculation(query)				
					except:
						pass
				data['genloss_grid'] = aggregate_loss_grid

				
				# lossless pr, generation etc.

				data['lossless_gen_plant'] = data['generation']+data['genloss']
				data['lossless_gen_grid'] = data['generation']+data['genloss_grid']

				data['lossless_pr_plant'] = get_performance_ratio(data['lossless_gen_plant'],site.capacity,float(total_radiation))
				data['lossless_pr_grid'] = get_performance_ratio(data['lossless_gen_grid'],site.capacity,float(total_radiation))

				#simulated
				#try:
				simu_data = SimulatedData.objects.filter(site=site,year=from_year).aggregate(gen=Sum('generation'),rad=Sum('solar_insolation'))
				if simu_data['gen'] != None:
					data['simu_gen'] = simu_data['gen'] 
					data['simu_rad'] = simu_data['rad']
					data['simu_pr'] = get_performance_ratio(data['simu_gen'],site.capacity,data['simu_rad'])
				else:
					data['simu_gen'] = 0 
					data['simu_rad'] = 0
					data['simu_pr'] = 0
				try:
					data['eff'] = data['pr']/data['simu_pr']	
				except:
					data['eff'] = 0
				
				report.append(data)
			data2['year'] = from_year
			data2['sites'] = report
			from_year = int(from_year)+1
			reports.append(data2)

	if duration == "monthly":
		start_date = datetime.date(int(from_year),1,1)
		end_date = datetime.date(int(from_year),12,31)

		for i in range(12):
			data2 = {}
			report = []
			for site in sites:
				data = {}	
				dgr = DGR.objects.filter(site=site,date__month=i+1,date__year=from_year)
			
				data['site'] = site
				abc = dgr.aggregate(abc=Sum('tvm_import_value')) #import
				xyz = dgr.aggregate(xyz=Sum('tvm_export_value')) #export
				total_radiation = dgr.aggregate(radiation=Sum('dgrweather__radiation'))['radiation']     #radiation
				if total_radiation:
					data['radiation'] = total_radiation
				else:
					data['radiation'] = 0

				try:
					total_generation = float(xyz['xyz'])
					data['generation'] = round(total_generation,2)
	

				except:
					total_generation = 0
					data['generation'] = 0
				try:		
					pr = get_performance_ratio(float(xyz['xyz']),site.capacity,float(total_radiation))
					data['pr'] = pr
				except:
					pr = 0
					data['pr'] = 0

				days = monthrange(int(from_year),i+1)[1]

				data['cuf'] = round((total_generation*100)/(site.capacity*days*24),2)
				data['revenue'] = dgr.aggregate(total=Sum('revenue'))['total']
				#plant breakdown

				plant_down_query = DGRPlantDownTime.objects.filter(dgr=dgr)
				data['plant_down_count'] = plant_down_query.count()
				plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']
				if plant_down_time:
					data['plant_down_time'] = convert_time(float(plant_down_time))
				else:
					data['plant_down_time'] = 0
				aggregate_loss = 0
				for query in plant_down_query:
					try:
						aggregate_loss = aggregate_loss + genloss_calculation(query)
					except:
						pass
				data['genloss']  = aggregate_loss 

				#grid breakdown

				grid_down_query = DGRGridDownTime.objects.filter(dgr=dgr)
				data['grid_down_count'] =grid_down_query.count()
				grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
				if grid_down_time:
					data['grid_down_time'] = convert_time(float(grid_down_time))

				else:
					data['plant_down_time'] = 0	
				aggregate_loss_grid = 0
				for query in grid_down_query:
					try:
						aggregate_loss_grid = aggregate_loss_grid + genloss_calculation(query)				
					except:
						pass
				data['genloss_grid'] = aggregate_loss_grid


				report.append(data)
				#simulated
				#try:
					
				month = monthToNum(i+1)
				simu_data = SimulatedData.objects.filter(site=site,year=from_year,month=month).aggregate(gen=Sum('generation'),rad=Sum('solar_insolation'))
				if simu_data['gen'] != None:
					data['simu_gen'] = simu_data['gen'] 
					data['simu_rad'] = simu_data['rad']
					data['simu_pr'] = get_performance_ratio(data['simu_gen'],site.capacity,data['simu_rad'])
				else:
					data['simu_gen'] = 0 
					data['simu_rad'] = 0
					data['simu_pr'] = 0
				try:
					data['eff'] = data['pr']/data['simu_pr']	
				except:
					data['eff'] = 0



			data2['month'] = months[i]
			data2['sites'] = report
			reports.append(data2)




	#daily based format
	if duration == "daywise":
		day = datetime.timedelta(days=1)
		while from_date <= to_date:
			data2 = {}
			report = []
			for site in sites:
				data = {}	
				dgr = DGR.objects.filter(site=site,date=from_date)
			
				data['site'] = site
				abc = dgr.aggregate(abc=Sum('tvm_import_value')) #import
				xyz = dgr.aggregate(xyz=Sum('tvm_export_value')) #export
				try:
					total_radiation = round(dgr.aggregate(radiation=Sum('dgrweather__radiation'))['radiation'],2)     #radiation
					data['radiation'] = total_radiation
					data['gen_by_radiation'] = total_radiation*site.capacity
				except:
					data['radiation'] = 0
					data['gen_by_radiation'] = 0
					total_radiation = 0
				try:
					total_generation = float(xyz['xyz'])
					data['generation'] = round(total_generation,2)
					data['cuf'] = round((total_generation*100)/(site.capacity*24),2)
	

				except:
					total_generation = 0
					data['generation'] = 0	
					data['cuf'] = 0	
				if xyz['xyz']:		
					pr = get_performance_ratio(float(xyz['xyz']),site.capacity,float(total_radiation))
					data['pr'] = pr
				else:
					pr = 0
					data['pr'] = 0

				data['revenue'] = dgr.aggregate(total=Sum('revenue'))['total']
				#plant breakdown

				plant_down_query = DGRPlantDownTime.objects.filter(dgr=dgr)
				data['plant_down_count'] = plant_down_query.count()
				plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']

				aggregate_radiation = 0
				for query in plant_down_query:
					try:
						aggregate_radiation = aggregate_radiation + genloss_calculation(query)				
					except:
						pass

				data['genloss'] = aggregate_radiation

				if plant_down_time:
					data['plant_down_time'] = convert_time(float(plant_down_time))

				#grid breakdown

				grid_down_query = DGRGridDownTime.objects.filter(dgr=dgr)
				data['grid_down_count'] =grid_down_query.count()
				grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
				if grid_down_time:
					data['grid_down_time'] = convert_time(float(grid_down_time))

				else:
					data['grid_down_time'] = 0	
				aggregate_radiation_grid = 0
				for query in grid_down_query:
					try:
						aggregate_radiation_grid = aggregate_radiation_grid + genloss_calculation(query)				
					except:
						pass

				data['genloss_grid'] = aggregate_radiation_grid
				
				#simulated
				#try:
				days = monthrange(from_date.year,from_date.month)[1]
				month = monthToNum(from_date.month)
						
				simu_data = SimulatedData.objects.filter(site=site,year=from_date.year,month=month).aggregate(gen=Sum('generation'),rad=Sum('solar_insolation'))

				#print simu_data['gen'] 
				if simu_data['gen'] != None:
					data['simu_gen'] = round(float(simu_data['gen'])/days,3)
				else:
					data['simu_gen'] = 0
				if simu_data['rad'] != None:
					data['simu_rad'] = round(float(simu_data['rad'])/days,3)
				else:
					data['simu_rad'] = 0
				if simu_data['gen'] != None:
					data['simu_pr'] = round(get_performance_ratio(data['simu_gen'],site.capacity,data['simu_rad']),3)
				else:
					data['simu_pr'] = 0
				try:
					data['eff'] = data['pr']/data['simu_pr']	
				except:
					data['eff'] = 0


				
				report.append(data)
			data2['date'] = from_date
			data2['sites'] = report
			from_date = from_date+day
			reports.append(data2)
	total_gen = 0
	total_rad = 0
	total_rev = 0
	total_cap = 0
	loss = 0
	final = []
	overall = {}
	days = end_date - start_date
	for site in sites:
		data = {}
		dgr = DGR.objects.filter(site=site,date__range=[start_date,end_date])	
		total = dgr.aggregate(gen=Sum('tvm_export_value'),rad=Sum('dgrweather__radiation'),rev=Sum('revenue'))	
		try:
			gen = round(total['gen'],2)
		except:
			gen = 0
		try:
			rad = round(total['rad'],2)
		except:
			rad = 0
		revenue = total['rev']
		if gen:
			total_gen = gen+total_gen		
		
		if rad:
			total_rad = rad + total_rad
		if revenue:
			total_rev = revenue+total_rev

		total_cap = site.capacity + total_cap
		pr = get_performance_ratio(gen,site.capacity,rad)
		data['site'] = site
		data['gen'] = gen
		data['pr'] = pr
		data['rad'] = rad
		data['rev'] = revenue
		try:
			data['cuf'] = round((gen*100)/(site.capacity*24*days.days),2)
		except:
			data['cuf'] = 0
		#plant breakdown

		plant_down_query = DGRPlantDownTime.objects.filter(dgr__in=dgr)
		data['plant_down_count'] = plant_down_query.count()
		data['plant_down_time'] = plant_down_query.aggregate(total=Sum('total_time'))['total']
		data['genloss'] = PlantGenLoss.objects.filter(plantdown__in=plant_down_query).aggregate(total=Sum('loss'))['total']
		try:
			loss = data['genloss']+loss
		except:
			pass	

		final.append(data)
		
	total_pr = get_performance_ratio(total_gen,total_cap,total_rad)
	overall['pr'] = total_pr
	overall['gen'] = total_gen
	overall['rad'] = total_rad	
	overall['rev'] = total_rev
	overall['loss'] = loss
	try:
		overall['cuf'] = round((total_gen*100)/(total_cap*24*days.days),2)
	except:
		overall['cuf'] = 0

	if request.method == "POST":
		if 'simulated' in request.POST:
			return render_to_response('simulated.html',{'form':form,'readings':reports,'parameter':parameter,'from_date':date,'to_date':to_date,'duration':duration,'from_year':from_year,'to_year':form.cleaned_data['to_year']},
				context_instance=RequestContext(request))
		else:
			return render_to_response('analysis-ajax.html',{'form':form,'readings':reports,'parameter':parameter,'from_date':date,'to_date':to_date,'duration':duration,'from_year':form.cleaned_data['from_year'],'to_year':form.cleaned_data['to_year'],'final':final,'overall':overall},
				context_instance=RequestContext(request))

	else:

		return render_to_response('plantwise.html',{'alerts':alerts,'form':form,'pmform': pmform,'breakdown_form':breakdown_form,'equipmentwise_form':equipmentwise_form,'losses_form':losses_form,'readings':reports,'parameter':parameter,'from_date':date,'to_date':to_date,'duration':duration,'type':type,'final':final,'overall':overall},
			context_instance=RequestContext(request))

@login_required
def manage_losses(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	company = get_object_or_404(UserCompany,user=request.user).company

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)

	form = LossesForm(sites=sites)


	if request.method == "POST":
		form =PlantWiseForm(sites=sites,data=request.POST)
		if form.is_valid():
			sites = form.cleaned_data['sites']
			from_date = form.cleaned_data['from_date']
			date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			from_year = form.cleaned_data['from_year']
			to_year = form.cleaned_data['to_year']
			duration = form.cleaned_data['duration']
			parameter = form.cleaned_data['parameter']
	

	else:

		thirty_day = datetime.timedelta(days=10)
		sites = sites
		from_date = datetime.datetime.now().date() - thirty_day
		date = from_date 
		to_date = datetime.datetime.now().date()
		duration = 'daywise'
		parameter = 'generation' 
		


	reports = []

	if duration == "yearly":
		while int(from_year) <= int(to_year):
			data2 = {}
			report = []
			for site in sites:
				data = {}	
				dgr = DGR.objects.filter(site=site,date__year=from_year)
			
				data['site'] = site
				abc = dgr.aggregate(abc=Sum('tvm_import_value')) #import
				xyz = dgr.aggregate(xyz=Sum('tvm_export_value')) #export
				total_radiation = dgr.aggregate(radiation=Sum('dgrweather__radiation'))['radiation']     #radiation
				if total_radiation:
					data['radiation'] = total_radiation
				else:
					data['radiation'] = 0


				if xyz['xyz'] and abc['abc']:
					total_generation = float(xyz['xyz']) 
					data['generation'] = round(total_generation,2)
	

				else:
					total_generation = 0
					data['generation'] = 0	
				if xyz['xyz']:		
					pr = get_performance_ratio(float(xyz['xyz']),site.capacity,float(total_radiation))
					data['pr'] = pr
				else:
					data['pr'] = 0
					pr = 0 
				#plant breakdown

				plant_down_query = DGRPlantDownTime.objects.filter(dgr=dgr)
				data['plant_down_count'] = plant_down_query.count()
				plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']
				if plant_down_time:
					data['plant_down_time'] = convert_time(float(plant_down_time))
				else:
					data['plant_down_time'] = "0"
				aggregate_loss = 0
				for query in plant_down_query:
					aggregate_loss = aggregate_loss + genloss_calculation(query)
				data['genloss']  = aggregate_loss 

				#grid breakdown

				grid_down_query = DGRGridDownTime.objects.filter(dgr=dgr)
				data['grid_down_count'] =grid_down_query.count()
				grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
				if grid_down_time:
					data['grid_down_time'] = convert_time(float(grid_down_time))
				else:
					data['grid_down_time'] = "0"	
				aggregate_loss_grid = 0
				for query in grid_down_query:
					aggregate_loss_grid = aggregate_loss_grid + genloss_calculation(query)				
				data['genloss_grid'] = aggregate_loss_grid

				
				# lossless pr, generation etc.

				data['lossless_gen_plant'] = data['generation']+data['genloss']
				data['lossless_gen_grid'] = data['generation']+data['genloss_grid']

				data['lossless_pr_plant'] = get_performance_ratio(data['lossless_gen_plant'],site.capacity,float(data['radiation']))
				data['lossless_pr_grid'] = get_performance_ratio(data['lossless_gen_grid'],site.capacity,float(data['radiation']))

				data['lossless_pr_plant_per'] = data['lossless_pr_plant'] - data['pr']   
				data['lossless_pr_grid_per']   = data['lossless_pr_grid'] - data['pr']   

				
				report.append(data)
			data2['year'] = from_year
			data2['sites'] = report
			from_year = int(from_year)+1
			reports.append(data2)

	

	#daily based format
	if duration == "daywise":
		day = datetime.timedelta(days=1)
		while from_date <= to_date:
			data2 = {}
			report = []
			for site in sites:
				data = {}	
				dgr = DGR.objects.filter(site=site,date=from_date)
			
				data['site'] = site
				abc = dgr.aggregate(abc=Sum('tvm_import_value')) #import
				xyz = dgr.aggregate(xyz=Sum('tvm_export_value')) #export
				total_radiation = dgr.aggregate(radiation=Sum('dgrweather__radiation'))['radiation']     #radiation
				if total_radiation:
					data['radiation'] = total_radiation
					data['gen_by_radiation'] = total_radiation*site.capacity
				else:
					data['radiation'] = 0
					data['gen_by_radiation'] = 0
				try:
					total_generation = float(xyz['xyz'])
					data['generation'] = round(total_generation,2)
					data['cuf'] = round((total_generation*100)/(site.capacity*24*1000),2)
	

				except:
					total_generation = 0
					data['generation'] = 0	
					data['cuf'] = 0	
				if xyz['xyz']:		
					pr = get_performance_ratio(float(xyz['xyz']),site.capacity,float(total_radiation))
					data['pr'] = pr
				else:
					pr = 0
					data['pr'] = 0
				#plant breakdown

				plant_down_query = DGRPlantDownTime.objects.filter(dgr=dgr)
				data['plant_down_count'] = plant_down_query.count()
				plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']

				aggregate_radiation = 0
				for query in plant_down_query:
					try:
						aggregate_radiation = aggregate_radiation + genloss_calculation(query)				
					except:
						pass

				data['genloss'] = aggregate_radiation

				if plant_down_time:
					data['plant_down_time'] = convert_time(float(plant_down_time))

				#grid breakdown

				grid_down_query = DGRGridDownTime.objects.filter(dgr=dgr)
				data['grid_down_count'] =grid_down_query.count()
				grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
				if grid_down_time:
					data['grid_down_time'] = convert_time(float(grid_down_time))

				else:
					data['grid_down_time'] = 0	
				aggregate_radiation_grid = 0
				for query in grid_down_query:
					try:
						aggregate_radiation_grid = aggregate_radiation_grid + genloss_calculation(query)				
					except:
						pass

				data['genloss_grid'] = aggregate_radiation_grid
				
				# lossless pr, generation etc.

				data['lossless_gen_plant'] = data['generation']+data['genloss']
				data['lossless_gen_grid'] = data['generation']+data['genloss_grid']

				data['lossless_pr_plant'] = get_performance_ratio(data['lossless_gen_plant'],site.capacity,float(data['radiation']))
				data['lossless_pr_grid'] = get_performance_ratio(data['lossless_gen_grid'],site.capacity,float(data['radiation']))

				data['lossless_pr_plant_per'] = data['lossless_pr_plant'] - data['pr']   
				data['lossless_pr_grid_per']   = data['lossless_pr_grid'] - data['pr']   

				
				report.append(data)
			data2['date'] = from_date
			data2['sites'] = report
			from_date = from_date+day
			reports.append(data2)

	return render_to_response('losses.html',{'form':form,'readings':reports,'parameter':parameter,'from_date':date,'to_date':to_date,'duration':duration},
		context_instance=RequestContext(request))

@login_required
def manage_equipmentwise(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	company = usercompany.company	
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)


	form = EquipmentWiseForm(sites=sites)

	if request.method == "POST":
		form = EquipmentWiseForm(sites=sites,data=request.POST)
		if form.is_valid():
			inverters = form.cleaned_data['inverters']
			from_date = form.cleaned_data['from_date']
			date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			from_year = form.cleaned_data['from_year']
			to_year = form.cleaned_data['to_year']
			duration = form.cleaned_data['duration']
			reports = []
			if duration == "yearly":
				while int(from_year) <= int(to_year):
					data2 = {}
					report = []
					for inverter in inverters:
						data = {}	
						dgr = DGRInverter.objects.filter(inverter=inverter,dgr__date__year=from_year)
					
						data['inverter'] = inverter
						total_generation = dgr.aggregate(generation=Sum('generation'))['generation']     
						if total_generation:
							data['generation'] = total_generation
						else:
							data['generation'] = 0


						#plant breakdown

						plant_down_query = DGRPlantDownTime.objects.filter(inverter=inverter,dgr__date__year=from_year)
						data['plant_down_count'] = plant_down_query.count()
						plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']
						if plant_down_time:
							data['plant_down_time'] = convert_time(float(plant_down_time))
						else:
							data['plant_down_time'] = "-"
						#grid breakdown

						#grid_down_query = DGRGridDownTime.objects.filter(inverter=inverter,dgr__date__year=from_year)
						#data['grid_down_count'] =grid_down_query.count()
						#grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
						#if grid_down_time:
						#	data['grid_down_time'] = convert_time(float(grid_down_time))
						#else:
						#	data['grid_down_time'] = "-"	
						
						
						report.append(data)
					data2['year'] = from_year
					data2['sites'] = report
					from_year = int(from_year)+1
					reports.append(data2)

			
			if duration == "monthly":

				for i in range(12):
					data2 = {}
					report = []
					for inverter in inverters:
						data = {}	
						dgr = DGRInverter.objects.filter(inverter=inverter,dgr__date__month=i+1,dgr__date__year=from_year)
					
						data['inverter'] = inverter
						total_generation = dgr.aggregate(generation=Sum('generation'))['generation']    
						if total_generation:
							data['generation'] = total_generation
						else:
							data['generation'] = 0
						
						#plant breakdown

						plant_down_query = DGRPlantDownTime.objects.filter(inverter=inverter,dgr__date__month=i+1,dgr__date__year=from_year)
						data['plant_down_count'] = plant_down_query.count()
						plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']
						if plant_down_time:
							data['plant_down_time'] = convert_time(float(plant_down_time))
						else:
							data['plant_down_time'] = "-"
	
						#grid breakdown

						#grid_down_query = DGRGridDownTime.objects.filter(equipment=inverter,dgr__date__month=i+1,dgr__date__year=from_year)
						#data['grid_down_count'] =grid_down_query.count()
						#grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
						#if grid_down_time:
						#	data['grid_down_time'] = convert_time(float(grid_down_time))
						#else:
						#	data['grid_down_time'] = "-"
		
						
						
						report.append(data)
					data2['month'] = months[i]
					data2['sites'] = report
					reports.append(data2)




			#daily based format
			if duration == "daywise":
				day = datetime.timedelta(days=1)
				while from_date <= to_date:
					data2 = {}
					report = []
					for inverter in inverters:
						data = {}	
						dgr = DGRInverter.objects.filter(inverter=inverter,dgr__date=from_date)
					
						data['inverter'] = inverter
						total_generation = dgr.aggregate(generation=Sum('generation'))['generation']    
						if total_generation:
							data['generation'] = total_generation
						else:
							data['generation'] = 0
			

						#plant breakdown

						plant_down_query = DGRPlantDownTime.objects.filter(inverter=inverter,dgr__date=from_date)
						data['plant_down_count'] = plant_down_query.count()
						plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']
						if plant_down_time:
							data['plant_down_time'] = convert_time(float(plant_down_time))
						else:
							data['plant_down_time'] = "-"
	
						#grid breakdown

						#grid_down_query = DGRGridDownTime.objects.filter(equipment=inverter,dgr__date=from_date)
						#data['grid_down_count'] =grid_down_query.count()
						#grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
						#if grid_down_time:
						#	data['grid_down_time'] = convert_time(float(grid_down_time))
						#else:
						#	data['grid_down_time'] = "-"
		
						
						
						report.append(data)
					data2['date'] = from_date
					data2['sites'] = report
					from_date = from_date+day
					reports.append(data2)
			return render_to_response('equipmentwise.html',{'form':form,'readings':reports,'from_date':date,'to_date':to_date,'duration':duration},
				context_instance=RequestContext(request))




	return render_to_response('equipmentwise.html',{'form':form,},
				context_instance=RequestContext(request))


@login_required
def manage_analysis_pm(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	company = usercompany.company	
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)


	form = PMAnalysisForm(sites=sites)


	today = datetime.datetime.now().date()

	deviation_count = datetime.timedelta(days=3)
	if request.method == "POST":
		form = PMAnalysisForm(sites=sites,data=request.POST)
		if form.is_valid():
			site = form.cleaned_data['site']
			from_date = form.cleaned_data['from_date']
			date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			from_year = form.cleaned_data['from_year']
			to_year = form.cleaned_data['to_year']
			duration = form.cleaned_data['duration']
			deviation = form.cleaned_data['delay']
			status = form.cleaned_data['status']

			planned = 0
			actual = 0
			delayed = 0
			incomplete = 0
			master = {}
			if deviation:
				deviation_count = datetime.timedelta(days=int(deviation))
			else:
				deviation_count = datetime.timedelta(days=3)
			reports = []
			if duration == "yearly":
				while int(from_year) <= int(to_year):
					data = {}	
					pmalert = PMAlert.objects.filter(site=site,due_date__year=from_year)
				
					data['planned'] = pmalert.count()     
			
					data['actual'] = PMAlert.objects.filter(site=site,due_date__year=from_year,status="completed").count()

					deviated = PMAlert.objects.filter(site=site,due_date__year=from_year,status="completed")
					data['incomplete'] = PMAlert.objects.filter(site=site,due_date__year=from_year).exclude(status="completed").count()
					alerts = []
					for alert in deviated:
						if alert.status != "completed":
							days = today - alert.due_date 
							if days >= deviation_count:
								alerts.append(alert)
						else:
							days = alert.completed_date - alert.due_date
							if days >= deviation_count:
								alerts.append(alert)

					data['year'] = from_year
					data['deviated'] = alerts
					data['deviated_count'] = len(alerts)
					from_year = int(from_year)+1
					planned = data['planned']+planned
					actual = data['actual']+actual
					delayed = data['deviated_count']+delayed
					incomplete = data['incomplete']+incomplete

					reports.append(data)
				master['planned'] = planned
				master['actual'] = actual
				master['delayed'] = delayed
				master['incomplete'] = incomplete
				master['achieve'] = round((actual/float(planned))*100,2)
				return render_to_response('pmanalysis.html',{'master':master,'form':form,'alerts':reports,'from_date':date,'to_date':to_date,'duration':duration,'from_year':form.cleaned_data['from_year'],'to_year':to_year},
				context_instance=RequestContext(request))


			
			if duration == "monthly":
				reports = []
				for i in range(12):
						
					data = {}	
					pmalert = PMAlert.objects.filter(site=site,due_date__year=from_year,due_date__month=i+1)
				
					data['planned'] = pmalert.count()     
				
					data['actual'] = PMAlert.objects.filter(site=site,due_date__year=from_year,due_date__month=i+1,status="completed").count()

					deviated = PMAlert.objects.filter(site=site,due_date__year=from_year,due_date__month=i+1,status="completed")
					data['incomplete'] = PMAlert.objects.filter(site=site,due_date__year=from_year,due_date__month=i+1).exclude(status="completed").count()
					alerts = []
					for alert in deviated:
						if alert.status != "completed":
							days = today - alert.due_date 
							if days >= deviation_count:
								alerts.append(alert)
						else:
							days = alert.completed_date - alert.due_date
							if days >= deviation_count:
								alerts.append(alert)
					data['deviated'] = alerts

					data['deviated_count'] = len(alerts)
					data['month'] = months[i]
					
					planned = data['planned']+planned
					actual = data['actual']+actual
					delayed = data['deviated_count']+delayed
					incomplete = data['incomplete']+incomplete
					reports.append(data)
				master['planned'] = planned
				master['actual'] = actual
				master['delayed'] = delayed
				master['incomplete'] = incomplete
				master['achieve'] = round((actual/planned)*100,2)

				return render_to_response('pmanalysis.html',{'master':master,'form':form,'alerts':reports,'from_date':date,'to_date':to_date,'duration':duration,'from_year':form.cleaned_data['from_year'],'to_year':to_year},
				context_instance=RequestContext(request))




			#daily based format
			if duration == "daywise":
				alerts = []
				day = datetime.timedelta(days=1)
				if status != "---":	
					pmalert = PMAlert.objects.filter(site=site,due_date__range=[from_date,to_date],status=status)
				else:
					pmalert = PMAlert.objects.filter(site=site,due_date__range=[from_date,to_date])
				for alert in pmalert:
					data = {}
					if deviation:
						if alert.status != "completed":
							days = today - alert.due_date 
							if days >= deviation_count:
								if days.days >=0:
									data['delay'] = days
								data['alert'] = alert
								alerts.append(data)
						else:
							days = alert.completed_date - alert.due_date
							if days >= deviation_count:
								if days.days >=0:
									data['delay'] = days
								data['alert'] = alert
								alerts.append(data)

					else:
						if alert.status != "completed":
							days = today - alert.due_date 
							if days.days >=0:
								data['delay'] = days
							data['alert'] = alert
							alerts.append(data)

						else:
							days = alert.completed_date - alert.due_date
							if days.days >=0:
								data['delay'] = days
							data['alert'] = alert
							alerts.append(data)


					

			return render_to_response('pmanalysis.html',{'form':form,'alerts':alerts,'from_date':date,'to_date':to_date,'duration':duration,'from_year':form.cleaned_data['from_year'],'to_year':to_year},
				context_instance=RequestContext(request))




	return render_to_response('pmanalysis.html',{'form':form,},
				context_instance=RequestContext(request))

@login_required
def manage_analysis_reminders(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	company = usercompany.company	
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)


	form = PMAnalysisForm(sites=sites)


	today = datetime.datetime.now().date()

	deviation_count = datetime.timedelta(days=3)
	if request.method == "POST":
		form = PMAnalysisForm(sites=sites,data=request.POST)
		if form.is_valid():
			site = form.cleaned_data['site']
			from_date = form.cleaned_data['from_date']
			date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			from_year = form.cleaned_data['from_year']
			to_year = form.cleaned_data['to_year']
			duration = form.cleaned_data['duration']
			deviation = form.cleaned_data['delay']
			status = form.cleaned_data['status']
			if deviation:
				deviation_count = datetime.timedelta(days=int(deviation))
			else:
				deviation_count = datetime.timedelta(days=3)
			reports = []
			if duration == "yearly":
				while int(from_year) <= int(to_year):
					data = {}	
					pmalert = ReminderAlert.objects.filter(site=site,due_date__year=from_year)
				
					data['planned'] = pmalert.count()     
			
					if status != "---":	
						data['actual'] = ReminderAlert.objects.filter(site=site,due_date__year=from_year,status=status).count()
					else:	
						data['actual'] = ReminderAlert.objects.filter(site=site,due_date__year=from_year,status="completed").count()

					deviated = ReminderAlert.objects.filter(site=site,due_date__year=from_year)
					alerts = []
					for alert in deviated:
						if alert.status != "completed":
							days = today - alert.due_date 
							if days >= deviation_count:
								alerts.append(alert)
						else:
							days = alert.completed_date.date() - alert.due_date
							if days >= deviation_count:
								alerts.append(alert)

					data['year'] = from_year
					data['deviated'] = alerts
					data['deviated_count'] = len(alerts)
					from_year = int(from_year)+1
					reports.append(data)
				return render_to_response('reminders/analysis.html',{'form':form,'alerts':reports,'from_date':date,'to_date':to_date,'duration':duration,'from_year':form.cleaned_data['from_year'],'to_year':to_year},
				context_instance=RequestContext(request))


			
			if duration == "monthly":
				reports = []
				for i in range(12):
						
					data = {}	
					pmalert = ReminderAlert.objects.filter(site=site,due_date__year=from_year,due_date__month=i+1)
				
					data['planned'] = pmalert.count()     
				
					if status != "---":	
						data['actual'] = ReminderAlert.objects.filter(site=site,due_date__year=from_year,due_date__month=i+1,status=status).count()
					else:
						data['actual'] = ReminderAlert.objects.filter(site=site,due_date__year=from_year,due_date__month=i+1,status="completed").count()

					deviated = ReminderAlert.objects.filter(site=site,due_date__year=from_year,due_date__month=i+1).exclude(status="completed")
					alerts = []
					for alert in deviated:
						if alert.status != "completed":
							days = today - alert.due_date 
							if days >= deviation_count:
								alerts.append(alert)
						else:
							days = alert.completed_date - alert.due_date
							if days >= deviation_count:
								alerts.append(alert)
					data['deviated'] = alerts

					data['deviated_count'] = len(alerts)
					data['month'] = months[i]
					reports.append(data)

				return render_to_response('reminders/analysis.html',{'form':form,'readings':reports,'from_date':date,'to_date':to_date,'duration':duration,'from_year':form.cleaned_data['from_year'],'to_year':to_year},
				context_instance=RequestContext(request))




			#daily based format
			if duration == "daywise":
				alerts = []
				day = datetime.timedelta(days=1)
				if status != "---":	
					pmalert = ReminderAlert.objects.filter(site=site,due_date__range=[from_date,to_date],status=status)
				else:
					pmalert = ReminderAlert.objects.filter(site=site,due_date__range=[from_date,to_date])
				for alert in pmalert:
					data = {}
					if deviation:
						if alert.status != "completed":
							days = today - alert.due_date 
							if days >= deviation_count:
								if days.days >=0:
									data['delay'] = days
		
								data['alert'] = alert
								alerts.append(data)
						else:
							days = alert.completed_date.date() - alert.due_date
							if days >= deviation_count:
								if days.days >=0:
									data['delay'] = days
								data['alert'] = alert
								alerts.append(data)

					else:
						if alert.status != "completed":
							days = today - alert.due_date 
							if days.days >= 0:	
								data['delay'] = days
							data['alert'] = alert
							alerts.append(data)

						else:
							days = alert.completed_date.date() - alert.due_date
							if days.days >= 0:
								data['delay'] = days
							data['alert'] = alert
							alerts.append(data)


					

			return render_to_response('reminders/analysis.html',{'form':form,'readings':alerts,'from_date':date,'to_date':to_date,'duration':duration,'from_year':form.cleaned_data['from_year'],'to_year':to_year},
				context_instance=RequestContext(request))




	return render_to_response('reminders/analysis.html',{'form':form,},
				context_instance=RequestContext(request))




from django.db.models import Q

@login_required
def manage_breakdown_analysis(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)

	form = BreakdownAnalysisForm(sites=sites)



	if request.method == "POST":
		form = BreakdownAnalysisForm(sites=sites,data=request.POST)
		if form.is_valid():
			site = form.cleaned_data['sites'][0]
			sites = form.cleaned_data['sites']
			from_date = form.cleaned_data['from_date']
			date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			breakdown_type = "plantdown"
			#breakdown_cause = form.cleaned_data['breakdown_cause']
			criteria = form.cleaned_data['criteria']
			seg_time = form.cleaned_data['segregate_on_time']
			seg_freq = form.cleaned_data['segregate_on_freq']
			if seg_time == "---":
				seg_time = 3
			if seg_freq == "---":
				seg_freq = 3
		

			duration = form.cleaned_data['duration']
			equipments = form.cleaned_data['equipments']
			if not equipments:
				equipments = Equipment.objects.all()

			meta = {}
			meta['breakdown_type'] = "plantdown"
			#meta['breakdown_cause'] = form.cleaned_data['breakdown_cause']
			meta['criteria'] = form.cleaned_data['criteria']
			meta['seg_time'] = form.cleaned_data['segregate_on_time']
			meta['seg_freq'] = form.cleaned_data['segregate_on_freq']
	
			if criteria == "equipmentwise":

				equipments = form.cleaned_data['equipments']
				if not equipments:
					equipments = Equipment.objects.all()
				if duration == "monthly":
					year = form.cleaned_data['from_year']	
					reportings = []
					for equipment in equipments:
						deta2 = {}	
						i = 1 
						reporting = []
						while i <= 12:
							deta = {}
							breakdowns = DGRPlantDownTime.objects.filter(dgr__site__in=sites,equipment=equipment,dgr__date__month=i,dgr__date__year=year)
							counts = breakdowns.count()
							if counts:
								deta['month'] = monthToNum(i)
								deta['count'] = counts
							else:
								deta['month'] = monthToNum(i)
								deta['count'] = 0

							reporting.append(deta)
							i = i+1 
						deta2['equipment'] = equipment
						deta2['report'] = reporting
						reportings.append(deta2)	
					deta2 = {}	
					i = 1 
					reporting = []
					while i <= 12:
						deta = {}
						breakdowns = DGRPlantDownTime.objects.filter(dgr__site__in=sites,dgr__date__month=i,dgr__date__year=year,equipment__in=equipments)
						counts = breakdowns.count()
						if counts:
							deta['month'] = monthToNum(i)
							deta['count'] = counts
						else:
							deta['month'] = monthToNum(i)
							deta['count'] = 0

						reporting.append(deta)
						i = i+1 
					deta2['equipment'] = "Total"
					deta2['report'] = reporting
					reportings.append(deta2)	

							
					alldata = []
					i = 1 
					while i <= 12:
						reports = []
						data3 = {}
						colspan = 0
						j = 1
						for site in sites:
							data2 = {}
							report2 = []
					
							for equip in equipments:
								data4 = {}
								sub_categories = EquipmentSubCategory.objects.filter(equipment=equip)
								print sub_categories
								report = []
								for category in sub_categories:
									print category
									breakdowns = DGRPlantDownTime.objects.filter(dgr__site=site,sub_category=category,dgr__date__month=i,dgr__date__year=year)
									count = breakdowns.count()
									data = {}
									data['category'] = category
									data['site'] = site.id
									data['breakdowns'] = breakdowns
									data['count'] = count
									data['loss']  = PlantGenLoss.objects.filter(plantdown__in=breakdowns).aggregate(total=Sum('loss'))['total']
									data['time'] = convert_time(breakdowns.aggregate(total=Sum("total_time"))['total'])
									#if count > 0:
									if j%2 == 0 :
										data['class'] = "bg-purple"
									else:
										data['class'] = "bg-blue"
	
									report.append(data)
								data4['equipment'] = equip
								colspan = colspan+sub_categories.count()
								data4['main'] = report
								report2.append(data4)
							j= j+1
							data2['site'] = site
							data2['colspan'] = colspan
							data2['data'] = report2
							reports.append(data2)
						data3['date'] = monthToNum(i)
						data3['total_list'] = DGRPlantDownTime.objects.filter(dgr__date__month=i,dgr__date__year=year,dgr__site__in=sites,equipment__in=equipments) 
						data3['total'] = DGRPlantDownTime.objects.filter(dgr__date__month=i,dgr__date__year=year,dgr__site__in=sites,equipment__in=equipments).count() 
						data3['loss'] = PlantGenLoss.objects.filter(plantdown__in=data3['total_list']).aggregate(total=Sum('loss'))['total']
						data3['time'] = convert_time(data3['total_list'].aggregate(total=Sum("total_time"))['total'])
						data3['reports'] = reports
						alldata.append(data3)	
						i = i+1

					reports = []
					for site in sites:
						
						sub_categories = EquipmentSubCategory.objects.filter(equipment__in=equipments)
						report = []
						for category in sub_categories:
							print category
							breakdowns = DGRPlantDownTime.objects.filter(dgr__site=site,sub_category=category,dgr__date__year=year)
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['breakdowns'] = breakdowns
							data['site'] = site.id
							data['count'] = count
							data['loss']  = PlantGenLoss.objects.filter(plantdown__in=breakdowns).aggregate(total=Sum('loss'))['total']
							data['time'] = convert_time(breakdowns.aggregate(total=Sum("total_time"))['total'])
							report.append(data)
						reports.append(report)	
					reports2 = []
					total = 0
					for site in sites:
						
						report = []
						for category in equipments:
							print category
							breakdowns = DGRPlantDownTime.objects.filter(dgr__site=site,equipment=category,dgr__date__year=year)
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['site'] = site.id
							data['breakdowns'] = breakdowns
							data['loss']  = PlantGenLoss.objects.filter(plantdown__in=breakdowns).aggregate(total=Sum('loss'))['total']
							data['time'] = convert_time(breakdowns.aggregate(total=Sum("total_time"))['total'])
							data['count'] = count
							total = total+count
							data['colspan'] = EquipmentSubCategory.objects.filter(equipment=category).count()
							report.append(data)
						reports2.append(report)	

					fulllist = DGRPlantDownTime.objects.filter(dgr__date__year=year,dgr__site__in=sites,equipment__in=equipments)
					loss  = PlantGenLoss.objects.filter(plantdown__in=fulllist).aggregate(total=Sum('loss'))['total']
					time = convert_time(fulllist.aggregate(total=Sum("total_time"))['total'])
					return render_to_response('breakdown_analysis_categorywise.html',{'form':form,'reports':alldata,'totals':reports,'totals2':reports2,'total':total,'all':fulllist,'duration':duration,'loss':loss,'time':time,'criteria':criteria,'reportings':reportings},
						context_instance=RequestContext(request))
				if duration == "yearly":
					alldata = []
					from_year = int(form.cleaned_data['from_year'])
					to_year = int(form.cleaned_data['to_year'])
					from_date = datetime.date(from_year,01,01)
					to_date = datetime.date(to_year,12,31)
					year = from_year
					equipments = form.cleaned_data['equipments']
					if not equipments:
						equipments = Equipment.objects.all()

					while year <= to_year:
						reports = []
						data3 = {}
						colspan = 0
						i = 0
						for site in sites:
							data2 = {}
							report2 = []
							
							for equip in equipments:
								data4 = {}
								sub_categories = EquipmentSubCategory.objects.filter(equipment=equip)
								print sub_categories
								report = []
								for category in sub_categories:
									print category
									breakdowns = DGRPlantDownTime.objects.filter(dgr__site=site,sub_category=category,dgr__date__year=year)
									count = breakdowns.count()
									data = {}
									data['category'] = category
									data['site'] = site.id
									data['breakdowns'] = breakdowns
									data['loss']  = PlantGenLoss.objects.filter(plantdown__in=breakdowns).aggregate(total=Sum('loss'))['total']
									data['time'] = convert_time(breakdowns.aggregate(total=Sum("total_time"))['total'])

									data['count'] = count
									#if count > 0:
									if i%2 == 0 :
										data['class'] = "bg-purple"
									else:
										data['class'] = "bg-blue"
	
									report.append(data)
								data4['equipment'] = equip
								colspan = colspan+sub_categories.count()
								data4['main'] = report
								report2.append(data4)
							i = i+1
							data2['site'] = site
							data2['colspan'] = colspan
							data2['data'] = report2
							reports.append(data2)
						data3['date'] = year
						data3['total_list'] = DGRPlantDownTime.objects.filter(dgr__date__year=year,dgr__site__in=sites,equipment__in=equipments) 
						data3['total'] = DGRPlantDownTime.objects.filter(dgr__date__year=year,dgr__site__in=sites,equipment__in=equipments).count() 
						data3['loss'] = PlantGenLoss.objects.filter(plantdown__in=data3['total_list']).aggregate(total=Sum('loss'))['total']
						data3['time'] = convert_time(data3['total_list'].aggregate(total=Sum("total_time"))['total'])

						data3['reports'] = reports
						alldata.append(data3)	
						year = year+1

					reports = []
					for site in sites:
						
						sub_categories = EquipmentSubCategory.objects.filter(equipment__in=equipments)
						report = []
						for category in sub_categories:
							print category
							breakdowns = DGRPlantDownTime.objects.filter(dgr__site=site,sub_category=category,dgr__date__range=[from_date,to_date])
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['breakdowns'] = breakdowns
							data['loss']  = PlantGenLoss.objects.filter(plantdown__in=breakdowns).aggregate(total=Sum('loss'))['total']
							data['time'] = convert_time(breakdowns.aggregate(total=Sum("total_time"))['total'])


							data['site'] = site.id
							data['count'] = count
							report.append(data)
						reports.append(report)	
					reports2 = []
					total = 0
					for site in sites:
						report = []
						for category in equipments:
							print category
							breakdowns = DGRPlantDownTime.objects.filter(dgr__site=site,equipment=category,dgr__date__range=[from_date,to_date])
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['site'] = site.id
							data['breakdowns'] = breakdowns
							data['count'] = count
							data['loss']  = PlantGenLoss.objects.filter(plantdown__in=breakdowns).aggregate(total=Sum('loss'))['total']
							data['time'] = convert_time(breakdowns.aggregate(total=Sum("total_time"))['total'])

							total = total+count
							data['colspan'] = EquipmentSubCategory.objects.filter(equipment=category).count()
							report.append(data)
						reports2.append(report)	

					fulllist = DGRPlantDownTime.objects.filter(dgr__date__range=[from_date,to_date],dgr__site__in=sites,equipment__in=equipments)
					loss  = PlantGenLoss.objects.filter(plantdown__in=fulllist).aggregate(total=Sum('loss'))['total']
					time = convert_time(fulllist.aggregate(total=Sum("total_time"))['total'])
					return render_to_response('breakdown_analysis_categorywise.html',{'form':form,'reports':alldata,'totals':reports,'totals2':reports2,'total':total,'all':fulllist,'duration':duration,'loss':loss,'time':time,'criteria':criteria},
						context_instance=RequestContext(request))
			
	



	

				if duration == "daywise":
					alldata = []
					while date <= to_date:
						reports = []
						data3 = {}
						colspan = 0
						i = 1
						for site in sites:
							data2 = {}
							report2 = []
					
							for equip in equipments:
								data4 = {}
								sub_categories = EquipmentSubCategory.objects.filter(equipment=equip)
								print sub_categories
								report = []
								for category in sub_categories:
									print category
									breakdowns = DGRPlantDownTime.objects.filter(dgr__site=site,sub_category=category,dgr__date=date)
									count = breakdowns.count()
									data = {}
									data['category'] = category
									data['site'] = site.id
									data['breakdowns'] = breakdowns
									data['count'] = count
									data['loss']  = PlantGenLoss.objects.filter(plantdown__in=breakdowns).aggregate(total=Sum('loss'))['total']
									data['time'] = convert_time(breakdowns.aggregate(total=Sum("total_time"))['total'])


									if i%2 == 0 :
										data['class'] = "bg-purple"
									else:
										data['class'] = "bg-blue"
									print data['class']

									#if count > 0:
									report.append(data)
								data4['equipment'] = equip
								data4['main'] = report
								colspan = colspan+sub_categories.count()
								report2.append(data4)
							i = i+1
							data2['site'] = site
							data2['colspan'] = colspan
							data2['data'] = report2
							reports.append(data2)
						data3['date'] = date
						data3['total_list'] = DGRPlantDownTime.objects.filter(dgr__date=date,dgr__site__in=sites,equipment__in=equipments) 
						data3['total'] = DGRPlantDownTime.objects.filter(dgr__date=date,dgr__site__in=sites,equipment__in=equipments).count() 
						data3['reports'] = reports
						data3['loss'] = PlantGenLoss.objects.filter(plantdown__in=data3['total_list']).aggregate(total=Sum('loss'))['total']
						data3['time'] = convert_time(data3['total_list'].aggregate(total=Sum("total_time"))['total'])


						alldata.append(data3)	
						date = date+datetime.timedelta(days=1)

					reports = []
					for site in sites:
						
						sub_categories = EquipmentSubCategory.objects.filter(equipment__in=equipments)
						report = []
						for category in sub_categories:
							print category
							breakdowns = DGRPlantDownTime.objects.filter(dgr__site=site,sub_category=category,dgr__date__range=[from_date,to_date])
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['breakdowns'] = breakdowns
							data['site'] = site.id
							data['count'] = count
							data['loss']  = PlantGenLoss.objects.filter(plantdown__in=breakdowns).aggregate(total=Sum('loss'))['total']
							data['time'] = convert_time(breakdowns.aggregate(total=Sum("total_time"))['total'])


							report.append(data)
						reports.append(report)	
					reports2 = []
					total = 0
					for site in sites:
						report = []
						for category in equipments:
							print category
							breakdowns = DGRPlantDownTime.objects.filter(dgr__site=site,equipment=category,dgr__date__range=[from_date,to_date])
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['site'] = site.id
							data['breakdowns'] = breakdowns
							data['count'] = count
							data['loss']  = PlantGenLoss.objects.filter(plantdown__in=breakdowns).aggregate(total=Sum('loss'))['total']
							data['time'] = convert_time(breakdowns.aggregate(total=Sum("total_time"))['total'])


							total = total+count
							data['colspan'] = EquipmentSubCategory.objects.filter(equipment=category).count()
							report.append(data)
						reports2.append(report)	

					fulllist = DGRPlantDownTime.objects.filter(dgr__date__range=[from_date,to_date],dgr__site__in=sites,equipment__in=equipments)
					loss  = PlantGenLoss.objects.filter(plantdown__in=fulllist).aggregate(total=Sum('loss'))['total']
					time = convert_time(fulllist.aggregate(total=Sum("total_time"))['total'])
	
					return render_to_response('breakdown_analysis_categorywise.html',{'form':form,'reports':alldata,'totals':reports,'totals2':reports2,'total':total,'all':fulllist,'loss':loss,'time':time,'criteria':criteria},
						context_instance=RequestContext(request))
			

	
			if criteria == "major" or criteria == "frequent":
				#daily based format
				
				result_list = []
				if criteria == "major":
					qs = DGRPlantDownTime.objects.filter(equipment__in=equipments,dgr__site__in=sites,dgr__date__gte=from_date,dgr__date__lte=to_date).filter(total_time__gte=seg_time)

					
					result_list.append(qs)
				if criteria == "frequent":
					for equipment in equipments:
						qs = DGRPlantDownTime.objects.filter(dgr__site__in=sites,equipment=equipment,dgr__date__range=[from_date,to_date])	
						if qs.count() >= int(seg_freq):
							result_list.append(qs)	
				results = []
				loss = 0
				total = 0
				total_time = 0
				for result in result_list:
					for row in result:
						data= {}
						data['loss'] = genloss_calculation(row)
						data['query'] = row
						loss = loss + data['loss']
						total = total+1
						total_time = total_time+row.total_time
						results.append(data)
				meta['total_loss'] = loss
				meta['total'] = total
				meta['total_time'] = convert_time(total_time)
				return render_to_response('breakdown_analysis.html',{'form':form,'readings':results,'from_date':date,'to_date':to_date,'meta':meta},
					context_instance=RequestContext(request))

			
	return render_to_response('breakdown_analysis.html',{'form':form,},
				context_instance=RequestContext(request))


@login_required
def manage_compare(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	company = get_object_or_404(UserCompany,user=request.user).company
	form = CompareForm(company=company)

	if not usercompany.is_admin:
		usersites = UserSite.objects.filter(user=request.user).values("site")
		sites = EnergySite.objects.filter(pk__in=usersites)
	
		
		form.fields['sites'].queryset = sites



	if request.method == "POST":
		form = CompareForm(company=company,data=request.POST)
		if form.is_valid():
			sites = form.cleaned_data['sites']
			from_date = form.cleaned_data['from_date']
			date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			from_year = form.cleaned_data['from_year']
			to_year = form.cleaned_data['to_year']
			duration = form.cleaned_data['duration']
			parameter = form.cleaned_data['parameter_one']
			parameter2 = form.cleaned_data['parameter_two']
			reports = []
			
			if duration == "yearly":
				while int(from_year) <= int(to_year):
					data2 = {}
					report = []
					for site in sites:
						data = {}	
						dgr = DGR.objects.filter(site=site,date__year=from_year)
					
						data['site'] = site
						abc = dgr.aggregate(abc=Sum('tvm_import_value')) #import
						xyz = dgr.aggregate(xyz=Sum('tvm_export_value')) #export
						total_radiation = dgr.aggregate(radiation=Sum('radiation'))['radiation']     #radiation
						if total_radiation:
							data['radiation'] = total_radiation
						else:
							data['radiation'] = 0


						if xyz['xyz'] and abc['abc']:
							total_generation = float(xyz['xyz']) - float(abc['abc'])
							data['generation'] = total_generation
			

						else:
							total_generation = 0
							data['generation'] = 0	
						if xyz['xyz']:		
							pr = get_performance_ratio(float(xyz['xyz']),site.capacity,float(total_radiation))
							data['pr'] = pr
						else:
							data['pr'] = 0
							pr = 0 
						#plant breakdown

						plant_down_query = DGRPlantDownTime.objects.filter(dgr=dgr)
						data['plant_down_count'] = plant_down_query.count()
						plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']
						if plant_down_time:
							data['plant_down_time'] = convert_time(float(plant_down_time))
						else:
							data['plant_down_time'] = "0"
						#grid breakdown

						grid_down_query = DGRGridDownTime.objects.filter(dgr=dgr)
						data['grid_down_count'] =grid_down_query.count()
						grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
						if grid_down_time:
							data['grid_down_time'] = convert_time(float(grid_down_time))
						else:
							data['grid_down_time'] = "0"	
						
						
						report.append(data)
					data2['year'] = from_year
					data2['sites'] = report
					from_year = int(from_year)+1
					reports.append(data2)

			
			if duration == "monthly":

				for i in range(12):
					data2 = {}
					report = []
					for site in sites:
						data = {}	
						dgr = DGR.objects.filter(site=site,date__month=i+1,date__year=from_year)
					
						data['site'] = site
						abc = dgr.aggregate(abc=Sum('tvm_import_value')) #import
						xyz = dgr.aggregate(xyz=Sum('tvm_export_value')) #export
						total_radiation = dgr.aggregate(radiation=Sum('radiation'))['radiation']     #radiation
						if total_radiation:
							data['radiation'] = total_radiation
						else:
							data['radiation'] = 0

						try:
							total_generation = float(xyz['xyz']) - float(abc['abc'])
							data['generation'] = total_generation
			

						except:
							total_generation = 0
							data['generation'] = 0
						if xyz['xyz']:		
							pr = get_performance_ratio(float(xyz['xyz']),site.capacity,float(total_radiation))
							data['pr'] = pr
						else:
							pr = 0
							data['pr'] = 0
						#plant breakdown

						plant_down_query = DGRPlantDownTime.objects.filter(dgr=dgr)
						data['plant_down_count'] = plant_down_query.count()
						plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']
						if plant_down_time:
							data['plant_down_time'] = convert_time(float(plant_down_time))
						else:
							data['plant_down_time'] = 0
						#grid breakdown

						grid_down_query = DGRGridDownTime.objects.filter(dgr=dgr)
						data['grid_down_count'] =grid_down_query.count()
						grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
						if grid_down_time:
							data['grid_down_time'] = convert_time(float(grid_down_time))
		
						else:
							data['plant_down_time'] = 0	
						
						report.append(data)
					data2['month'] = months[i]
					data2['sites'] = report
					reports.append(data2)




			#daily based format
			if duration == "daywise":
				day = datetime.timedelta(days=1)
				while from_date <= to_date:
					data2 = {}
					report = []
					for site in sites:
						data = {}	
						dgr = DGR.objects.filter(site=site,date=from_date)
					
						data['site'] = site
						abc = dgr.aggregate(abc=Sum('tvm_import_value')) #import
						xyz = dgr.aggregate(xyz=Sum('tvm_export_value')) #export
						total_radiation = dgr.aggregate(radiation=Sum('dgrweather__radiation'))['radiation']     #radiation
						if total_radiation:
							data['radiation'] = total_radiation
							data['gen_by_radiation'] = total_radiation*site.capacity
						else:
							data['radiation'] = 0
							data['gen_by_radiation'] = 0
						try:
							total_generation = float(xyz['xyz']) - float(abc['abc'])
							data['generation'] = total_generation
							data['cuf'] = round((total_generation*100)/(site.capacity*24*1000),2)
			

						except:
							total_generation = 0
							data['generation'] = 0	
							data['cuf'] = 0	
						if xyz['xyz']:		
							pr = get_performance_ratio(float(xyz['xyz']),site.capacity,float(total_radiation))
							data['pr'] = pr
						else:
							pr = 0
							data['pr'] = 0
						#plant breakdown

						plant_down_query = DGRPlantDownTime.objects.filter(dgr=dgr)
						data['plant_down_count'] = plant_down_query.count()
						plant_down_time = plant_down_query.aggregate(total=Sum('total_time'))['total']
						if plant_down_time:
							data['plant_down_time'] = convert_time(float(plant_down_time))

						#grid breakdown

						grid_down_query = DGRGridDownTime.objects.filter(dgr=dgr)
						data['grid_down_count'] =grid_down_query.count()
						grid_down_time = grid_down_query.aggregate(total=Sum('total_time'))['total']
						if grid_down_time:
							data['grid_down_time'] = convert_time(float(grid_down_time))
		
						else:
							data['grid_down_time'] = 0	
						
						report.append(data)
					data2['date'] = from_date
					data2['sites'] = report
					from_date = from_date+day
					reports.append(data2)
			return render_to_response('comparision.html',{'form':form,'readings':reports,'parameter2':parameter2,'parameter':parameter,'from_date':date,'to_date':to_date,'duration':duration},
				context_instance=RequestContext(request))


	return render_to_response('plantwise.html',{'form':form,},
				context_instance=RequestContext(request))





def inverter_choices(request, qs=None):
    if qs is None:
        # Change the default QS to your needs
        qs = Inverter.objects.all()

    if request.GET.get('site'):
        qs = qs.filter(site=request.GET.get('site'))

    results = []
    for choice in qs:
        results.append((choice.pk, choice.name))

    return HttpResponse(simplejson.dumps(results))

def causes_choices(request):
    if request.GET.get('site'):
        qs = DGRPlantDownTime.objects.filter(dgr__site=request.GET.get('site'))
        qs1 = DGRGridDownTime.objects.filter(dgr__site=request.GET.get('site'))

        result_list = sorted(  
    		chain(qs, qs1),
    		key=attrgetter('created'))

    	results = []
    	for choice in result_list:
        	results.append((choice.cause, choice.cause))

    	return HttpResponse(simplejson.dumps(results))
    else:
	return HttpResponse("nothing")

def all_notifications(request):
	   usercompany = get_object_or_404(UserCompany,user=request.user)
	   data = {}

	   
	   data['not_count'] = SiteNotification.objects.filter(user=request.user,is_read=False).order_by('-created').count()
	   notifications = SiteNotification.objects.filter(user=request.user).order_by('-created')

	   paginator = Paginator(notifications, 25) # Show 25 contacts per page

	   page = request.GET.get('page')
	   try:
        	data['notifications'] = paginator.page(page)
	   except PageNotAnInteger:
	        # If page is not an integer, deliver first page.
	   	data['notifications'] = paginator.page(1)
	   except EmptyPage:
	        # If page is out of range (e.g. 9999), deliver last page of results.
		datap['notifications'] = paginator.page(paginator.num_pages)


	   return render_to_response('all_notifications.html',{'data':data,},
				context_instance=RequestContext(request))


def alert_read(request,alert,type):
	notification = get_object_or_404(SiteNotification,pk=alert)
	if notification.user == request.user:
		if notification.is_read == True:
			notification.is_read = False
			notification.save()
		else:
			notification.is_read = True
			notification.save()
		return HttpResponse("Marked")

def alert_delete(request,alert,type):
	notification = get_object_or_404(SiteNotification,pk=alert)
	if notification.user == request.user:
		notification.delete()
		return HttpResponse("Deleted")



@login_required
def simulated_data(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	years = ['2015','2016','2017','2018','2019','2020',]


	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)

	all_reports = SimulatedData.objects.filter(site__in=sites).order_by('site')


	form = SimulatedDataForm(sites=sites)

	if request.method == "POST":
		form = SimulatedDataForm(data=request.POST,sites=sites)
		if form.is_valid():
			try:
				data = SimulatedData.objects.get(site=form.cleaned_data['site'],year=form.cleaned_data['year'],month=form.cleaned_data['month'])
				data.solar_radiation = form.cleaned_data['solar_insolation']
				data.generation = form.cleaned_data['generation']	
				data.save()
			except SimulatedData.DoesNotExist:
				reading = form.save()
	return render_to_response('simulated_data.html',{'form':form,'reports':all_reports},
				context_instance=RequestContext(request))
@login_required
def site_documents(request):

	usercompany = get_object_or_404(UserCompany,user=request.user)

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)


	years = ['2015','2016','2017','2018','2019','2020',]

	all_reports = Document.objects.filter(site__in=sites).order_by('due_date')

	form =  DocumentForm(sites=sites)

	if request.method == "POST":
		form = DocumentForm(data=request.POST,sites=sites,files=request.FILES)
		if form.is_valid():
			doc = form.save()
	return render_to_response('documents.html',{'form':form,'reports':all_reports},
				context_instance=RequestContext(request))


				




@login_required
def site_settings(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	current_site = get_object_or_404(EnergySite,pk=request.session['current_site'])
	url = "/site/%d/" % current_site.id
	try:
		usersite = UserSite.objects.get(site=current_site,user=request.user)
	except UserSite.DoesNotExist:
		return HttpResponseRedirect(url)

	settings = NotificationSettings.objects.get(usersite=usersite)
	form = NotificationSettingsForm(instance=settings)

	if request.method == "POST":
		form = NotificationSettingsForm(instance=settings,data=request.POST)
		if form.is_valid():
			data = form.save()
			messages.success(request, 'Notification Settings Save Successfully!')
	return render_to_response('site_settings.html',{'form':form},
				context_instance=RequestContext(request))


@login_required
def send_site_message(request):
	if request.method == "POST":
		site = get_object_or_404(EnergySite,pk=request.POST['message-site'])
		message_text = strip_tags(request.POST['message-text'])
		if message_text != "":
			usersites = UserSite.objects.filter(site=site)
			if usersites:
				for hope in usersites:
					print "wuhoo"
					message_subject = "AkshyaUrja: Message from %s" % request.user.username
					message_content = message_text.replace("Hi, \n","").replace("\n---- Add any extra message here","").replace("--- \nThanks","").replace("\n","")

					html_message = render_to_string("message_email.html",{"user":hope.user,"message":message_content})
					send_mail(message_subject, message_text, 'info@energym.cloudapp.net',
					    [hope.user.email,request.user.email], fail_silently=False,html_message=html_message)	
				return HttpResponse("Message sent to site incharge")
			else:
				return HttpResponse("No site incharge assigned")
		else:
			return HttpResponse("no response")

@login_required
def breakdown_tickets(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)


	plantdowns = DGRPlantDownTime.objects.filter(dgr__site__in=sites,present_status="open")
	griddowns = DGRGridDownTime.objects.filter(dgr__site__in=sites,present_status="open")
        result_list = sorted(  
    		chain(plantdowns, griddowns),
    		key=attrgetter('created'))
	return render_to_response('breakdown_tickets.html',{'results':result_list,},
				context_instance=RequestContext(request))


@login_required
def plantdown_ticket(request,id):
	plantdown = get_object_or_404(DGRPlantDownTime,pk=id)
	
	form = PlantDownCloseForm(instance=plantdown,site=plantdown.dgr.site)
	if request.method == "POST":
		
		form = PlantDownCloseForm(instance=plantdown,data=request.POST,site=plantdown.dgr.site)
		if form.is_valid():
			ticket = form.save()
			messages.success(request, 'Breakdown Ticket Status Updated!')

			return HttpResponseRedirect('/breakdown_tickets/')
		

	return render_to_response('breakdown_ticket.html',{'form':form,'plantdown':plantdown},
				context_instance=RequestContext(request))

@login_required
def griddown_ticket(request,id):
	plantdown = get_object_or_404(DGRGridDownTime,pk=id)
	
	form = GridDownCloseForm(instance=plantdown,site=plantdown.dgr.site)
	if request.method == "POST":
		
		form = GridDownCloseForm(instance=plantdown,data=request.POST,site=plantdown.dgr.site)
		if form.is_valid():
			ticket = form.save()
			messages.success(request, 'Breakdown Ticket Status Updated!')

			return HttpResponseRedirect('/breakdown_tickets/')
		

	return render_to_response('breakdown_ticket.html',{'form':form,'plantdown':plantdown},
				context_instance=RequestContext(request))

@login_required
def manage_reports_soiling(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)

	form = GenerationReportForm(sites=sites)
	report_from = ''
	report_to = ''
	reports = []
	duration = ''
	if request.method == "POST":
		form = GenerationReportForm(sites=sites,data=request.POST)
		if form.is_valid():
			from_date = form.cleaned_data['from_date']
			site = form.cleaned_data['site']
			to_date = form.cleaned_data['to_date']
			year = form.cleaned_data['year']
			from_year = int(form.cleaned_data['from_year'])
			to_year = int(form.cleaned_data['to_year'])
			duration = form.cleaned_data['format']


		blocks = Block.objects.filter(site=site)

			
		# cleaned block =  a block which has all modules cleaned at least once in last 7 days from a particular date
		# ideal block = a cleaned block with max generation on particular date is an ideal block
		# soiling effect = % loss in generation due to non cleaning of a block, the amount of generation lesser than the ideal block 
	

		if duration == "yearly": 
				year = form.cleaned_data['year']
				report_from = "January %s " % year
				report_to = "December %s " % year  
				reports = []
				for i in range(12):

					from_date = datetime.date(int(year),1,1)
					to_date = datetime.date(int(year),12,31)
					date = from_date
				        day = timedelta(days=1)
					
					data = {}	
					total_soiling_loss = 0
					month_soiling = 0
					while date <= to_date:
						soiling_loss = 0

						past_week = date - timedelta(days=7)
						cleaned_blocks = []
						dirty_blocks = []
						for block in blocks:
							ideal_generation = 0
							cleaned_modules = ModuleCleaned.objects.filter(dgr__date__range=(past_week,date),block=block).aggregate(total=Sum('cleaned'))['total']
							total_modules = ConnectedModule.objects.filter(inverter__block=block).aggregate(quantity=Sum('quantity'))['quantity']
							if cleaned_modules > total_modules:
								cleaned_blocks.append(block)
							else:
								dirty_blocks.append(block)
						
						avg_generation = DGRInverter.objects.filter(inverter__block__in=cleaned_blocks,dgr__date=date).aggregate(gen=Avg('generation'))['gen']
						if not avg_generation:
							avg_generation = 0	
							
						for block in dirty_blocks:
							generation = DGRInverter.objects.filter(inverter__block=block).aggregate(total=Sum('generation'))['total']
							soiling_loss = avg_generation - generation 
							total_soiling_loss = total_soiling_loss+soiling_loss
										
						date = date+day		
		
					
					data['date'] = months[i]
					data['soiling'] = total_soiling_loss*1000
					total_generation = DGRInverter.objects.filter(dgr__date__month=i).aggregate(total=Sum('generation'))['total']
					try:
						data['soiling_per'] = (total_soiling_loss/total_generation)*100
					except:
						data['soiling_per'] = 0

					reports.append(data)		

		if duration == "yearwise":
			from_year = int(form.cleaned_data['from_year'])
			to_year = int(form.cleaned_data['to_year'])
			report_from = from_year
			report_to = to_year
			year = from_year
			reports = []
			while year <= to_year:

				from_date = datetime.date(int(year),1,1)
				to_date = datetime.date(int(year),12,31)
				date = from_date
				day = timedelta(days=1)
				
				data = {}	
				total_soiling_loss = 0
				month_soiling = 0
				while date <= to_date:
					soiling_loss = 0

					past_week = date - timedelta(days=7)
					cleaned_blocks = []
					dirty_blocks = []
					for block in blocks:
						ideal_generation = 0
						cleaned_modules = ModuleCleaned.objects.filter(dgr__date__range=(past_week,date),block=block).aggregate(total=Sum('cleaned'))['total']
						total_modules = ConnectedModule.objects.filter(inverter__block=block).aggregate(quantity=Sum('quantity'))['quantity']
						if cleaned_modules > total_modules:
							cleaned_blocks.append(block)
						else:
							dirty_blocks.append(block)
					
					avg_generation = DGRInverter.objects.filter(inverter__block__in=cleaned_blocks,dgr__date=date).aggregate(gen=Avg('generation'))['gen']
					if not avg_generation:
						avg_generation = 0	
						
					for block in dirty_blocks:
						generation = DGRInverter.objects.filter(inverter__block=block).aggregate(total=Sum('generation'))['total']
						soiling_loss = avg_generation - generation 
						total_soiling_loss = total_soiling_loss+soiling_loss
			
					
					date = date+day		
				
				data['date'] = year
				data['soiling'] = total_soiling_loss*1000 #KWH
				total_generation = DGRInverter.objects.filter(dgr__date__year=year).aggregate(total=Sum('generation'))['total']
				try:
					data['soiling_per'] = (total_soiling_loss/total_generation)*100
				except:
					data['soiling_per'] = 0


				reports.append(data)		


				year = year + 1	

		if duration == "daily":
			date = from_date 
			day = timedelta(days=1)
			report_from = date
			report_to = date
			soiling_loss = 0
			data = {}	
			total_soiling_loss = 0
			past_week = date - timedelta(days=7)
			cleaned_blocks = []
			dirty_blocks = []
			for block in blocks:
				ideal_generation = 0
				cleaned_modules = ModuleCleaned.objects.filter(dgr__date__range=(past_week,date),block=block).aggregate(total=Sum('cleaned'))['total']
				total_modules = ConnectedModule.objects.filter(inverter__block=block).aggregate(quantity=Sum('quantity'))['quantity']
				if cleaned_modules > total_modules:
					cleaned_blocks.append(block)
				else:
					dirty_blocks.append(block)
			
			avg_generation = DGRInverter.objects.filter(inverter__block__in=cleaned_blocks,dgr__date=date).aggregate(gen=Avg('generation'))['gen']
			if not avg_generation:
				avg_generation = 0	
				
			for block in dirty_blocks:
				data = {}
				generation = DGRInverter.objects.filter(inverter__block=block).aggregate(total=Sum('generation'))['total']
				soiling_loss = avg_generation - generation 
				total_soiling_loss = total_soiling_loss+soiling_loss
	
				data['date'] = block
				data['soiling'] = soiling_loss*1000	
				data['soiling_per'] = (soiling_loss/generation)*100	
				reports.append(data)		
			



		#daily based format
		if duration == "monthly":
			date = from_date 
			day = timedelta(days=1)
			report_from = date
			report_to = to_date
			while date <= to_date:
				soiling_loss = 0
				soiling = 0
				total_soiling = 0
				data = {}	
				total_soiling_loss = 0
				past_week = date - timedelta(days=7)
				cleaned_blocks = []
				dirty_blocks = []
				for block in blocks:
					ideal_generation = 0
					cleaned_modules = ModuleCleaned.objects.filter(dgr__date__range=(past_week,date),block=block).aggregate(total=Sum('cleaned'))['total']
					total_modules = ConnectedModule.objects.filter(inverter__block=block).aggregate(quantity=Sum('quantity'))['quantity']
					if cleaned_modules > total_modules:
						cleaned_blocks.append(block)
					else:
						dirty_blocks.append(block)
				
				avg_generation = DGRInverter.objects.filter(inverter__block__in=cleaned_blocks,dgr__date=date).aggregate(gen=Avg('generation'))['gen']
				if not avg_generation:
					avg_generation = 0	
					
				for block in dirty_blocks:
					generation = DGRInverter.objects.filter(inverter__block=block).aggregate(total=Sum('generation'))['total']
					soiling_loss = avg_generation - generation 
					total_soiling_loss = total_soiling_loss+soiling_loss
					try:
						soiling = (soiling_loss/avg_generation)*100
					except:
						soiling = 0
					total_soiling = total_soiling+soiling	
	
				data['date'] = date
				data['soiling_per'] = soiling
				if avg_generation == 0:
					data['soiling'] = "No sufficient data"
				else:
					data['soiling'] = total_soiling_loss*1000 #KWH
				
				reports.append(data)		
				
				date = date+day		

	
	return render_to_response('soiling.html',{'form':form,'readings':reports,'from':report_from,'to':report_to,'duration':duration},
			context_instance=RequestContext(request))
	

	


'''
Utility Functions

'''


def add_months(sourcedate,months):
	month = sourcedate.month - 1 + months
	year = sourcedate.year + month / 12
	month = month % 12 + 1
	day = min(sourcedate.day,calendar.monthrange(year,month)[1])
	return datetime.date(year,month,day)

def convert_time(hours):
	try:
		time_minutes = hours * 60
		time_seconds = time_minutes * 60
		hours_part   = floor(hours)
		minutes_part = floor(time_minutes % 60)
		seconds_part = floor(time_seconds % 60)
		total_time = "%s:%s" %(int(hours_part),int(minutes_part))
	except:
		total_time = "00:00"
	return total_time


def get_performance_ratio(tvm_export_value,capacity,radiation):
	try:
		pr = (tvm_export_value)/(capacity*radiation)
		pr = round(pr*100.00,2)
	except:	
		pr = 0
	return pr

def get_cuf_daily(generation,capacity):
	cuf =  round((generation*100)/(capacity*24),2)
	return cuf


def genloss_calculation(hope):
	genloss = 0
	if hope.__class__.__name__== "DGRPlantDownTime":   
		try:
			genloss =  PlantGenLoss.objects.get(plantdown=hope).loss
		except PlantGenLoss.DoesNotExist:
			genloss = 0

	if hope.__class__.__name__ == "DGRGridDownTime":
		try:
			genloss =  GridGenLoss.objects.get(griddown=hope).loss
		except GridGenLoss.DoesNotExist:
			genloss = 0
	return genloss 

def abs_genloss_calculation(hope):	
	start_time = re.split(':',hope.from_time)
	end_time = re.split(':',hope.to_time)

	start_hour  = int(start_time[0])
	end_hour = int(end_time[0])
	try:
		start_minutes = int(start_time[1])
	except:
		start_minutes = 0
	try:
		end_minutes = int(end_time[1])
	except:
		end_minutes = 0
	hour = start_hour
	total_radiation = 0
	genloss = 0
	dgr = hope.dgr
	hourlyweather = HourlyWeather.objects.filter(dgrweather__dgr=dgr)
	
	while hour<=end_hour:
		try:
			radiation = hourlyweather.filter(time=hour)[0].radiation
		except:
			radiation = 0 
		try:
			radiation_plus_one = hourlyweather.filter(time=hour+1)[0].radiation
		except:
			radiation_plus_one = 0
		avg_radiation = (float(radiation)+float(radiation_plus_one))/2
		
		#print hour
		#radiation = hourlyweather.filter(time=hour)[0]
		#radiation_plus_one = hourlyweather.filter(time=hour+1)[0]
		#avg_radiation = (float(radiation.radiation)+float(radiation_plus_one.radiation))/2
		if hour == start_hour:
			minutes = 60-start_minutes 
		elif hour == end_hour:
			minutes = end_minutes
		else:
			minutes = 60
		radiation_per_minute = (avg_radiation)/60
		total_radiation = total_radiation + (minutes*radiation_per_minute)
		hour=hour+1
	if hope.__class__.__name__== "DGRPlantDownTime":   
		try:
			inverter_generation = DGRInverter.objects.get(inverter=hope.inverter,dgr=dgr)
			genloss = round((inverter_generation.generation/(dgr.dgrweather.radiation-total_radiation))*total_radiation,2)
		except DGRInverter.DoesNotExist:
			genloss = 0
	if hope.__class__.__name__ == "DGRGridDownTime":
		if not hope.dgr.tvm_export_value:
			end_date = hope.dgr.date
			date = end_date - timedelta(days=10)
			dgrs = DGR.objects.filter(site=dgr.site,date__range=[date,end_date]).order_by('date')
			prs = []
			for dgr in dgrs:
				pr = get_performance_ratio(dgr.tvm_export_value,dgr.site.capacity,dgr.dgrweather.radiation)			
				prs.append(pr)
			total_pr = 0
			for pr in prs:
				total_pr =+ pr
			pravg = total_pr/len(prs)
			genloss = round((pravg*dgr.site.capacity*dgr.dgrweather.radiation)/100 ,2)
		else:
			
			genloss = round((dgr.tvm_export_value/(dgr.dgrweather.radiation-total_radiation))*total_radiation,2)
	return genloss	




def monthToNum(date):

	return{
		1:'jan',
		2:'feb',
		3:'mar',
		4:'apr',
		5:'may',
		6:'jun',
		7:'jul',
		8:'aug',
		9:'sep', 
		10:'oct',
		11:'nov',
		12:'dec'
	}[date]


def site_rank_day(site,date):
	site_id = site.id
	sites = EnergySite.objects.filter(company = site.company)
	gentargets = []
	for site in sites:
		try:
			generation_target = SimulatedData.objects.get(site=site,month=monthToNum(date.month),year=date.year) 
			data = {}
			days =  monthrange(date.year,date.month)[1]
			day_target = generation_target.generation/days
			try:
				dgr = DGR.objects.get(site=site,date=date)
				gen_target = (dgr.tvm_export_value/day_target)*100
			except DGR.DoesNotExist:
				gen_target = 0
			data['gen_target'] = gen_target
			data['site'] = site
			gentargets.append(data)
		except SimulatedData.DoesNotExist:
			pass
	rankwise = sorted(gentargets,key=lambda x: x['gen_target'],reverse=True)
	i = 1
	ranking = 0
	for rank in rankwise:
		if rank['site'].id == site_id:
			ranking = i
			break
		i=i+1
	ranks = {}
	ranks['site_rank'] = ranking
	ranks['all_ranks'] = rankwise
	return ranks



#all notifications related signals go here


# Notification based on DGR submisson

def dgr_add_notification(sender, instance, **kwargs):
	created = datetime.datetime.now()
	sites = UserSite.objects.filter(site=instance.site)
	for site in sites:
		if site.notificationsettings.dgr_uploaded:
			notification = "DGR of %s for %s uploaded" % (instance.site,str(instance.date))
			notice = SiteNotification(notification=notification,site=instance.site,is_read=False,created=created,user=site.user)
			notice.save()
			message_body = notification

        		html_content = render_to_string("notification_email.html", {"user":site.user,"notice":message_body})
			send_mail('DGR Upload Notification', message_body, 'info@energym.cloudapp.net',
				    [site.user.email], fail_silently=False,html_message=html_content)	


	
		if site.notificationsettings.dgr_generation_target:
			try:
				generation_target = SimulatedData.objects.get(site=instance.site,month=monthToNum(instance.date.month),year=instance.date.year) 
				days =  monthrange(instance.date.year,instance.date.month)[1]
				day_target = generation_target.generation/days
				gen_target = (instance.tvm_export_value/day_target)*100
				if gen_target <= site.notificationsettings.dgr_generation_target:
					notification = "Generation for site %s is %s below target on %s " % (instance.site.name, gen_target,instance.date)
					notice = SiteNotification(notification=notification,site=instance.site,is_read=False,created=created,user=site.user)
					notice.save()
					message_body = notification


        				html_content = render_to_string("notification_email.html", {"user":site.user,"notice":message_body})
					send_mail('Missed Generation Target Notification', message_body, 'info@energym.cloudapp.net',
					    [site.user.email], fail_silently=False,html_message=html_content)	



			except SimulatedData.DoesNotExist:
				print "simulated data not available"

def plant_down_notification(sender, instance, **kwargs):
	created = datetime.datetime.now()
	sites = UserSite.objects.filter(site=instance.dgr.site)

	date = datetime.datetime.now().date()	
	for site in sites:

		if site.notificationsettings.breakdown_frequent:
			d = date- timedelta(days=7)
			freq = DGRPlantDownTime.objects.filter(inverter=instance.inverter,cause=instance.cause,dgr__date__gte=d).count()
			if freq > site.notificationsettings.breakdown_frequent:
				notification = "Frequent breakdown - %s inverter, cause - %s, for more than %s times in last 7 days" % (instance.inverter.name,instance.cause,freq)
				notice = SiteNotification(notification=notification,site=instance.dgr.site,is_read=False,created=instance.dgr.date,user=site.user)
				notice.save()
				message_body = notification
	

        			html_content = render_to_string("notification_email.html", {"user":site.user,"notice":message_body})
				send_mail('Frequent Breakdown Notification', message_body, 'info@energym.cloudapp.net',
				    [site.user.email], fail_silently=False,html_message=html_content)	


		if site.notificationsettings.breakdown_major:
			if instance.total_time >= site.notificationsettings.breakdown_major:
				notification = "Major breakdown -%s - %s on site %s, cause - %s, total time %s" % (instance.inverter.block,instance.inverter.name,instance.dgr.site,instance.cause,instance.total_time)
				notice = SiteNotification(notification=notification,site=instance.dgr.site,is_read=False,created=instance.dgr.date,user=site.user)
				notice.save()
				
				message_body = notification

        			html_content = render_to_string("notification_email.html", {"user":site.user,"notice":message_body})
				send_mail('Major Breakdown Notification', message_body, 'info@energym.cloudapp.net',
				    [site.user.email], fail_silently=False,html_message=html_content)	


def dgrweather_add_notification(sender, instance, **kwargs):
	created = datetime.datetime.now()
	sites = UserSite.objects.filter(site=instance.dgr.site)
	for site in sites:
		if site.notificationsettings.dgr_pr_value:
			pr = get_performance_ratio(instance.dgr.tvm_export_value,instance.dgr.site.capacity,instance.radiation)
			if pr < site.notificationsettings.dgr_pr_value:
				notification = "PR for site is %s on %s. Please look into it " % (pr,instance.dgr.date)
				notice = SiteNotification(notification=notification,site=instance.dgr.site,is_read=False,created=created,user=site.user)
				notice.save()
				message_body = notification

        			html_content = render_to_string("notification_email.html", {"user":site.user,"notice":message_body})
				send_mail('Low PR  Notification', message_body, 'info@energym.cloudapp.net',
				    [site.user.email], fail_silently=False,html_message=html_content)	







def grid_down_notification(sender, instance, **kwargs):

	date = datetime.datetime.now().date()	
	d = date- timedelta(days=7)
	freq = DGRGridDownTime.objects.filter(inverter=instance.inverter,description=instance.description,dgr__date__gte=d).count()
	if freq > 3:
		notification = "%s inverter is tripping for the casuse - %s, for more than 3 times" % (instance.inverter,instance.description)
		notice = SiteNotification(notification=notification,site=instance.dgr.site,is_read=False,created=instance.dgr.date)
		notice.save()
	
		notice2 = CompanyNotification(notification=notification,site=instance.dgr.site,is_read=False,company=instance.dgr.site.company,created=instance.dgr.date)
		notice2.save()


def dgr_not_uploaded():
	sites = EnergySite.objects.all()
	date = datetime.datetime.now().date()
	for site in sites:
		usersite = UserSite.objects.filter(site=site)
		try:
			dgr = DGR.objects.filter(site=site).order_by('-date')[0]
			diff = date - dgr.date 
		except:
			diff = 1
		for query in usersite:
			if diff.days == int(query.notificationsettings.dgr_delayed):
				notification = "DGR for %s of site %s has not been uploaded after %s days. " % (date,site.name,diff.days)
				notice = SiteNotification(notification=notification,site=site,is_read=False,created=date,user=query.user)
				notice.save()
			
				message_body = notification

        			html_content = render_to_string("notification_email.html", {"user":query.user,"notice":message_body})
				send_mail('DGR Delay Notification', message_body, 'info@energym.cloudapp.net',
				    [query.user.email], fail_silently=False,html_message=html_content)	


		
def pm_delayed():
	sites = EnergySite.objects.all()
	for site in sites:
		usersite = UserSite.objects.filter(site=site)
		date = datetime.datetime.now().date()
		pmalerts = PMAlert.objects.filter(site=site,status="due")
		for alert in pmalerts:
			diff = date - alert.due_date
			for query in usersite:
				if diff.days == query.notificationsettings.pm_delayed:
					notification = "PM for %s equipment, location - %s for site % is delayed %s days. " % (alert.equipment,alert.location,site.name,diff.days)
					notice = SiteNotification(notification=notification,site=instance.dgr.site,is_read=False,created=instance.dgr.date,user=query.user)
					notice.save()
				
					message_body = notification

        				html_content = render_to_string("notification_email.html", {"user":query.user,"notice":message_body})
					send_mail('PM Delay Notification', message_body, 'info@energym.cloudapp.net',
					    [query.user.email], fail_silently=False,html_message = html_content)	


		
def document_due_date():
	today = datetime.datetime.now().date()
	documents = Document.objects.filter(due_date=today)
	for document in documents:
		users = UserCompany.objects.filter(company=document.site.company,is_admin=True)
		for user in users:
			notification = "Document %s  due date is today. Please take appropriate action for it. " % (document.title)
		
			notice = SiteNotification(notification=notification,site=document.site,is_read=False,created=datetime.datetime.now(),user=user.user)
			notice.action_url = "/documents/"
			notice.save()

						
			message_body = notification
        		html_content = render_to_string("notification_email.html", {"user":user.user,"notice":message_body})
	
			send_mail('Document Due date is today', message_body, 'info@energym.cloudapp.net',
			    [user.user.email], fail_silently=False,html_message=html_content)	

def save_genloss():
	queries = PlantGenLoss.objects.all().values('plantdown')
	plantdownquery = DGRPlantDownTime.objects.exclude(id__in=queries)
	for result in plantdownquery:
		loss = abs_genloss_calculation(result)
		genloss = PlantGenLoss(plantdown=result,loss=loss)
		genloss.save()

	queries = GridGenLoss.objects.all().values('griddown')
	griddownquery = DGRGridDownTime.objects.exclude(id__in=queries)
	for result in griddownquery:
		loss = abs_genloss_calculation(result)
		genloss = GridGenLoss(griddown=result,loss=loss)
		genloss.save()

def dummy():
	print "here"
	sites = EnergySite.objects.all()
	for site in sites:
		print "Site - %s, id - %d \n" %(site.name,site.id)
	var = raw_input("Please enter site id: ")
	from_d = raw_input("Please enter from date ")
	to = raw_input("Please enter to date ")

	from_d = from_d.split('/')
	from_date = datetime.date(int(from_d[0]),int(from_d[1]),int(from_d[2]))
	to = to.split('/')
	to_date = datetime.date(int(to[0]),int(to[1]),int(to[2]))

	print from_date
	print to_date	
	user = User.objects.get(pk=1)
	if var:
			site = get_object_or_404(EnergySite,pk=int(var))
			date = from_date
			duration = "daywise"
			reports = []
			


			#daily based format
			if duration == "daywise":
				day = datetime.timedelta(days=1)
				i = 300 
				j = 4
				while from_date <= to_date:
					data2 = {}
					report = []
					data = {}	
					tvm_export = random.uniform(i,i+15)
					tvm_import = random.uniform(j,j+2)
					tvm_mf = site.tvm_mf	
					d = from_date- timedelta(days=1)
					try:
						previous_dgr = DGR.objects.get(site=site,date=d)

						tvm_export_value =  ((tvm_export-previous_dgr.tvm_export)*tvm_mf)/1000
						tvm_import_value =  ((tvm_import-previous_dgr.tvm_import)*tvm_mf)/1000
					except DGR.DoesNotExist:
						tvm_export_value = 0
						tvm_import_value = 0
						

					dgr = DGR(site=site,date=from_date,upload_time = datetime.datetime.now(),
						user = user,
						tvm_export = tvm_export,
						tvm_import = tvm_import,
						tvm_export_value = tvm_export_value,
						tvm_import_value = tvm_import_value,
						)

					dgr.save()
					inverters = Inverter.objects.filter(site=site)
					avg_inverter = tvm_export_value/inverters.count()	
					for inverter in inverters:
						dgrinverter = DGRInverter(dgr=dgr,inverter=inverter,generation=avg_inverter)
						dgrinverter.save()
					dgrweather = DGRWeather(dgr=dgr,weather="sunny",wind_speed=random.uniform(4.12,10.72),radiation=random.uniform(3.21,7.9123),temperature=random.randrange(27,40))
					
					dgrweather.save()
					dummy_time = 7
					k = 7
					radiation= .26
					while k <= 19:
						radiation = radiation + random.uniform(.05,.0625)
						if k > 12:
							radiation = radiation - random.uniform(0.080,0.110)
						hourlyweather = HourlyWeather(dgrweather=dgrweather,radiation=radiation,wind_speed=dgrweather.wind_speed,temperature=dgrweather.temperature,time=k)	
						k = k+1
						hourlyweather.save()
					randominverter = inverters.order_by("?").first()
					error_code = ['201','231','X211','Z891','232','298','242','251','222','242','Earth Fault','INV Sleep','8185']
					ajb = ['ajb1','ajb2','ajb3','ajb4','ajb5','ajb6','ajb7','ajb8','ajb9'] 
					smu = ['smu1','smu2','smu3','smu4','smu5','smu6','smu7','smu8','smu9'] 
					cause = ['Controling Wiring Change in Inverter end','HMI hang','DIR.Overcurrent',
						 'DI 14 fauit',
						 'INV In Sleen after grid resuming',
						 'String Cable found failty',
						 'revers power flow']

					reason = ['control Wiring Change in Inverter end',
						  'Auto Rest',
						  'Grid down',
						  'Replace MC-4 Connector and 6 sqr mm cable'
						]

					action_taken = [
						'Co-ordinate with ABB Service Engineer',
						'Rest relay',
						'System reset',
						'Auto Started',
						'Trouble shooting work done then find out string cable shorted with modules structure .'
						]
					present_status  = ['ok', 'testing','down']
					from_time = random.randrange(7,15)
					to_time = random.randrange(1,2) + from_time
					total_time = to_time - from_time	
					
					ii = random.randrange(0,2)
					ik = 0
					while ik <= ii:

						plantdowntime = DGRPlantDownTime(dgr=dgr,inverter=randominverter,frame = "Frame",
								smu_no = random.choice(smu),
								ajb = random.choice(ajb),
								from_time  = from_time ,
								to_time = to_time ,
								total_time = total_time,
								error_code = random.choice(error_code),
								cause = random.choice(cause),
								action_taken = random.choice(action_taken),
								reason = random.choice(reason),
								present_status = random.choice(present_status),
								attended_by = user.username ,
								created = datetime.datetime.now() ,
								block = randominverter.block,
								location = Location.objects.all().order_by("?").first(),
								equipment = Equipment.objects.all().order_by("?").first(),
								section = Section.objects.all().order_by("?").first(),
								)
						plantdowntime.save()
						ik = ik+1

					remarks = [
						'66 kv incomer breaker problem in GSS end',
						'132 kv line shutdown in GSS end',
						'GSS trip for 110 v dc battery maintance',
						'GSS trip for 66 kv incomer jumper connected',
						'grid power shifted 132 kv to 220 kv line,so found phase sequence chages',
						'133 KV GSS Breaker Trip',
						'134 KV GSS Breaker Trip',
						'6.3 MVA Power Transformer Replaced to 12.5 MVA Transformer at GSS End',
						'66 kv Line shutdown',
						'132 kv Line shutdown',
						]
					ii = random.randrange(0,2)
					ik = 0
					while ik <= ii:	
						from_time = random.randrange(10,14)
						to_time = random.randrange(1,2) + from_time
						total_time = to_time - from_time	

						
						griddowntime = DGRGridDownTime(dgr=dgr,
								from_time  = from_time ,
								to_time = to_time ,
								total_time = total_time,
								cause = "Grid Failure",
								action_taken = "Co-ordiante with GSS officials",
								present_status = "Grid Available",
								created = datetime.datetime.now() ,
								location = "GSS Trip",
								remarks = random.choice(remarks)
								)	
						griddowntime.save() 
						ik = ik+1
				from_date = from_date+day
				i=i+200
				j=j+10

			print "dummy data entered for the selected sites and dates"		
			exit()



def create_dummy_site():
	sites = EnergySite.objects.all()
	for site in sites:
		print "Site - %s, id - %d \n" %(site.name,site.id)
	var = raw_input("Please enter site id: ")
	
	site = EnergySite.objects.get(pk=int(var))
	i = 1
	while i <= 50:
		block_name = "block-%d" % i
		block = Block(block=block_name,site=site,company=site.company)
		block.save()
		i = i+1
	blocks = Block.objects.filter(site=site)
	#transformer = Transformer.objects.filter(site=site)[0]
	for block in blocks:
		inv1 = Inverter(name="INV-01", block=block,site=site,model_no="wipro",make="wipro",serial_no="123213",capacity_connected=1158,capacity=1158)
		inv1.save()
		inv2 = Inverter(name="INV-02",block=block,site=site,model_no="wipro",make="wipro",serial_no="123213",capacity_connected=1158,capacity=1158)
		inv2.save()


		
		
