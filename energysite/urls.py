'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, 
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE 
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.                
'''




from django.conf.urls import url
from .views import *
urlpatterns = [
    url(r'^new_site/$', new_site, name='new_site'),
    url(r'^new_site/htpanels/$', save_htpanels, name='save_htpanels'),
    url(r'^new_site/transformers/$', save_transformers, name='save_transformers'),
    url(r'^new_site/inverter/$', save_inverter, name='save_inverter'),
    url(r'^new_site/ajb/$', save_ajb, name='save_ajb'),
    url(r'^new_site/tilt/$', save_tilt, name='save_tilt'),
    url(r'^new_site/module/$',save_module, name='save_module'),
    
    url(r'^site/(?P<site_id>\d+)/$', site, name='dashboard'),
    url(r'^site/(?P<site_id>\d+)/info/$', site_info, name='site_info'),


 
] 
