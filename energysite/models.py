from django.db import models
from django.contrib.auth.models import User
import datetime
from django.conf import settings
from prevmain.models import *

class Tilt(models.Model):
	TYPES = (('fix','Fix'),
		 ('seasonal','Seasonal'),
		)
	type = models.CharField(choices=TYPES,max_length=50,help_text="Choose panel tilt type according to the site design")
	angle = models.FloatField(blank=True,null=True,help_text="Please enter this angle if type is fixed")
	winter_angle = models.FloatField(blank=True,null=True,help_text="Please enter winter angle if type is seasonal")
	summer_angle = models.FloatField(blank=True,null=True,help_text="Please enter summer angle if type is seasonal")
	site = models.OneToOneField(EnergySite,related_name="tilt_site")
class HTPanel(models.Model):
	name = models.CharField(max_length=50,help_text="Please enter name according to your company's nomanclature pattern")
	TYPES = (('incoming','Incoming'),
		 ('outgoing','Outgoing'),
		)
	type = models.CharField(choices=TYPES,max_length=50,help_text="Choose panel type")

	model_no = models.CharField(max_length=50,help_text="Please enter model no. of the panel",blank=True)
	make = models.CharField(max_length=50,blank=True,help_text="Please enter make")
	serial_no = models.CharField(max_length=50,blank=True,help_text="Please enter serial no.")
	capacity = models.FloatField(null=True,blank=True,verbose_name="Rating",help_text="Please enter capacity in KV")
	site = models.ForeignKey(EnergySite,related_name="htpanel_site")
	def __unicode__(self):
                return '%s' % (self.name)


class Transformer(models.Model):
	name = models.CharField(max_length=50,help_text="Please enter name according to your company's nomanclature pattern")
	TYPES = (('auxillary','Auxillary'),
		 ('power','Power'),
		)
	type = models.CharField(choices=TYPES,max_length=50,help_text="Choose transformer type")

	model_no = models.CharField(max_length=50,help_text="Please enter model no. of the panel",blank=True)
	make = models.CharField(max_length=50,blank=True,help_text="Please enter make")
	serial_no = models.CharField(max_length=50,blank=True,help_text="Please enter serial no.")
	capacity = models.FloatField(null=True,blank=True,verbose_name="Rating",help_text="Please enter capacity in KVA")
	site = models.ForeignKey(EnergySite,related_name="transformer_site")
	htpanel = models.ForeignKey(HTPanel,help_text="Choose the HT Panel this transformer is connected to",blank=True,null=True)
	def __unicode__(self):
                return '%s' % (self.name)
class Block(models.Model):
	site = models.ForeignKey(EnergySite,related_name="block_site")
	company = models.ForeignKey(Company,related_name="block_company")
	block = models.CharField(max_length=255)
	def __unicode__(self):
                return self.block



class Inverter(models.Model):
	name = models.CharField(max_length=50,help_text="Please enter name according to your company's nomanclature pattern")
	block = models.ForeignKey(Block,help_text="Choose inverter block")
	capacity_connected = models.FloatField(null=True,blank=True,verbose_name="Capacity",help_text="Please enter capacity connected in KW")
	model_no = models.CharField(max_length=50,help_text="Please enter model no. of the panel",blank=True)
	make = models.CharField(max_length=50,blank=True,help_text="Please enter make")
	serial_no = models.CharField(max_length=50,blank=True,help_text="Please enter serial no.")
	capacity = models.FloatField(null=True,blank=True,verbose_name="Rating",help_text="Please enter inverter rating in KW")
	site = models.ForeignKey(EnergySite)
	transformer = models.ForeignKey(Transformer,help_text="Choose the transformer this inverter is connected to",blank=True,null=True)
	def __unicode__(self):
                return '%s' % (self.name)

class ModuleType(models.Model):
	name = models.CharField(max_length=50,help_text="Please enter name according to your company's nomanclature pattern")
	capacity = models.FloatField(null=True,blank=True,verbose_name="Capacity",help_text="Please enter capacity connected in W")
	def __unicode__(self):
                return '%s Watt' % (self.capacity)

class ConnectedModule(models.Model):
	type = models.ForeignKey(ModuleType,help_text="Choose Module Type")
	inverter = models.ForeignKey(Inverter,help_text="Choose inverter")
	quantity = models.IntegerField(verbose_name="Total Quantity",help_text="Please enter no. of modules of this type connected")
	def __unicode__(self):
                return '%s' % (self.type.capacity)


class AJB(models.Model):
	name = models.CharField(max_length=50,help_text="Please enter name according to your company's nomanclature pattern")
	model_no = models.CharField(max_length=50,help_text="Please enter model no. of the panel",blank=True)
	make = models.CharField(max_length=50,blank=True,help_text="Please enter make")
	serial_no = models.CharField(max_length=50,blank=True,help_text="Please enter serial no.")
	capacity = models.FloatField(null=True,blank=True,help_text="Please enter capacity in KV",verbose_name="Rating")
	site = models.ForeignKey(EnergySite)
	inverter = models.ForeignKey(Inverter,help_text="Choose the inverter this AJB is connected to",blank=True,null=True)
	def __unicode__(self):
                return '%s' % (self.name)




