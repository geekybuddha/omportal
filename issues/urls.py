'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, 
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE 
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.                
'''



from django.conf.urls import url
from .views import *
from .forms import *
urlpatterns = [
 
    url(r'^$', dashboard, name='dashboard'),
    url(r'^search/$', view_issues, name='view_issues'),
    url(r'^contact/$', CreateIssueWizard.as_view([NewIssueForm,NewIssueForm])),
    url(r'^create_issue/$', create_issue, name='create_issue'),
    url(r'^api/create_issue/$', create_issue_api, name='create_issue_api'),
    url(r'^api/update_issue/$', update_issue_api, name='update_issue_api'),
    url(r'^reports/$', reports, name='reports'),
    url(r'^issues/mark/$', mark_issues, name='mark_issues'),
    url(r'^update_issue/(?P<issue_id>\d+)/$', update_issue, name='update_issue'),
    url(r'^issue/(?P<issue_id>\d+)/$', view_issue, name='view_issue'),
    url(r'^create_action/(?P<issue_id>\d+)/$', create_action, name='create_action'),
    url(r'^resolve/(?P<issue_id>\d+)/$', create_resolution, name='create_resolution'),
    url(r'^issue/(?P<issue_id>\d+)/assign/$', assign_issue, name='assign_issue'),
    url(r'^issue/(?P<issue_id>\d+)/pm/$', issue_pm, name='issue_pm'),
    url(r'^issue/(?P<issue_id>\d+)/reopen/$', issue_reopen, name='issue_reopen'),
    url(r'^issue/(?P<issue_id>\d+)/approved/$', issue_approved, name='issue_approved'),
    url(r'^issue/(?P<issue_id>\d+)/set_target_date/$', set_target_date, name='set_target_date'),
    url(r'^issue/escalate/$', escalate, name='escalate'),
] 
