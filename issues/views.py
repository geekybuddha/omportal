'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
'''



import datetime
from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.http import JsonResponse
from django.db.models import Value as V
from django.db.models.functions import Coalesce
from django.db.models import Count, Min, Sum, Avg,Max
from stock.models import ItemRecieved

from prevmain.forms import  ScheduleSearchForm
from prevmain.models import EnergySite,UserCompany,UserSite,SiteNotification
from .forms import *
from .models import *
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework.decorators import *
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated

def monthToNum(date):

	return{
		1:'jan',
		2:'feb',
		3:'mar',
		4:'apr',
		5:'may',
		6:'jun',
		7:'jul',
		8:'aug',
		9:'sep', 
		10:'oct',
		11:'nov',
		12:'dec'
	}[date]


@login_required
def dashboard(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	form  = ScheduleSearchForm(sites)
	#site_id = request.session.get('current_site')
	#try:
	#	site = EnergySite.objects.get(pk=site_id)
	#except:
	#	site = sites[0]

	#today = PMAlert.objects.filter(site=site,due_date=today_date)
	#week = today_date + datetime.timedelta(days=7)
	#upcoming = PMAlert.objects.filter(site=site,due_date__gt=today_date,due_date__lte=week)
	#due = PMAlert.objects.filter(site=site,due_date__lt=today_date).filter(~Q(status= "completed"))
	#completed = PMAlert.objects.filter(site=site,due_date__lt=today_date,status="completed")

	alert_type = request.GET.get('type')
	if alert_type == "my":
		alerts = Complaint.objects.filter(site__in=sites,assigned_to=request.user).order_by('-created')
		messages.success(request, 'showing only issues assigned to you')
	elif alert_type == "urgent":
		alerts = Complaint.objects.filter(site__in=sites,priority__title="critical").order_by('-created')
		messages.success(request, 'showing only urgent issues')
	elif alert_type == "delayed":
		alerts = Complaint.objects.filter(site__in=sites,target_date__lt=today_date).order_by('-created')
		messages.success(request, 'showing only delayed issues')
	else:
		alerts = Complaint.objects.filter(site__in=sites).order_by('-created')
			
	paginator = Paginator(alerts, 25) # Show 25 contacts per page

    	page = request.GET.get('page')
    	try:
        	alerts_list = paginator.page(page)
    	except PageNotAnInteger:
        	# If page is not an integer, deliver first page.
        	alerts_list = paginator.page(1)
    	except EmptyPage:
        	# If page is out of range (e.g. 9999), deliver last page of results.
        	alerts_list = paginator.page(paginator.num_pages)

	return render(request,'issues/dashboard.html',{'alerts':alerts_list,})



@login_required
def create_issue(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)
        #if usercompany.is_client:
        #    form = NewClientIssueForm(sites)
        #else:
	form  = NewIssueForm(sites)
	if request.method == "POST":
                #if usercompany.is_client:
		#    form = NewClientIssueForm(sites=sites,data=request.POST)
                #else:
		form = NewIssueForm(sites=sites,data=request.POST)

		if form.is_valid():
			complaint = form.save(commit=False)
			complaint.company = usercompany.company
			complaint.user = request.user
			complaint.complaint_by = request.user
                        complaint.date_of_complaint = datetime.datetime.now()
                        try:
                            complaint.time_to_detect = (complaint.date_of_complaint - complaint.outage_start).days
                        except:
                            complaint.time_to_detect = 0
                        if usercompany.is_client:
                            complaint.complaint_by = request.user
                            try:
                                usersite = UserSite.objects.get(site=complaint.site,role="incharge") #we need to choose one
                                complaint.assigned_to = usersite.user
                            except:
                                print "no site incharge"
                            try:
                                status = Status.objects.get(title="pending")
                            except:
                                status = Status(title="pending")
                                status.save()
                            complaint.status = status
			complaint.save()
			messages.success(request, 'Issue opened Successfully!')
			if complaint.outage_type.title == "preventive maintenance":
				url = "/complaints/issue/%d/pm/" % complaint.id
				return HttpResponseRedirect(url)
	
			else:
				url = "/complaints/issue/%d/" % complaint.id
				return HttpResponseRedirect(url)


        return render(request,'issues/create_issue.html',{'form':form,'sites':sites},
		  				)
from formtools.wizard.views import SessionWizardView

FORMS = [("address", NewIssueForm),
         ("confirmation",NewIssueForm)]

TEMPLATES = {"0": "issues/create_issue.html",
             "1": "issues/create_issue.html",
             "cc": "checkout/creditcard.html",
             "confirmation": "checkout/confirmation.html"}


class CreateIssueWizard(SessionWizardView):
    #form_list = [NewIssueForm,NewIssueForm,]
    def get_template_names(self):
        return [TEMPLATES[self.steps.current]]

    def done(self, form_list, **kwargs):
        for form in form_list:
            if form.is_valid():
                print "yes"
        return HttpResponseRedirect('/page-to-redirect-to-when-done/')

@login_required
def issue_pm(request,issue_id):
	issue = get_object_or_404(Complaint,pk=issue_id)
	form = IssuePMForm()
	if request.method == "POST":
		form = IssuePMForm(request.POST)
		if form.is_valid():
			issue_pm = form.save(commit=False)
			issue_pm.complaint = issue
			issue_pm.status = Status.objects.get(title="open")
			issue_pm.save()
			messages.success(request, 'Issue opened Successfully!')
			url = "/complaints/issue/%d/" % issue.id
			return HttpResponseRedirect(url)

	return render(request,'issues/issue_pm.html',{'form':form,'complaint':issue},
		  				)



@login_required
def view_issue(request,issue_id):
	issue = get_object_or_404(Complaint,pk=issue_id)
	if not issue.is_read:
		issue.is_read = True
		issue.save()
	actions = Action.objects.filter(complaint=issue).order_by('-id')
	logs = ComplaintLog.objects.filter(complaint=issue).order_by('-id')
	resolutions = ComplaintResolution.objects.filter(complaint=issue)
	pms = ComplaintPM.objects.filter(complaint=issue)	
	if issue.outage_type.title == "Maintenance":
		if not pms:
			url = "/complaints/issue/%d/pm/" % issue.id
			return HttpResponseRedirect(url)
	po = ComplaintPO.objects.filter(complaint=issue)
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	form  = AssignIssueForm(sites)
	target_form = TargetDateForm()
	reopen_form = IssueReopenForm()
	return render(request,'issues/view_issue.html',{'complaint':issue,'logs':logs,'actions':actions,'resolutions':resolutions,'pms':pms,'assignform':form,'po':po,'target_form':target_form,'reopen_form':reopen_form},
		  				)


@login_required
def update_issue(request,issue_id):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	complaint = get_object_or_404(Complaint,pk=issue_id)
	if not complaint.company == usercompany.company:
		return Http404

	form  = UpdateIssueForm(sites,instance=complaint)
	if request.method == "POST":
		form = UpdateIssueForm(sites=sites,instance=complaint,data=request.POST)
		if form.is_valid():
			action = form.save(commit=False)
			action.save()
			messages.success(request, 'Issue updated Successfully!')
			return HttpResponseRedirect('/')


	return render(request,'issues/update_issue.html',{'form':form,'complaint':complaint},
		  				)



@login_required
def view_issues(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	new = Complaint.objects.filter(company=usercompany.company)
        try:
            outage = request.GET['outage']
            outage = Outage.objects.get(pk=outage)
            new = new.filter(outage_type=outage)
        except:
            pass
	today = Complaint.objects.filter(company=usercompany.company,target_date=today_date)
	upcoming = Complaint.objects.filter(company=usercompany.company,target_date__gt=today_date)
	due = Complaint.objects.filter(company=usercompany.company,target_date__lt=today_date)



	form  = IssueSearchForm(sites)
	if request.method == "POST":
		form = IssueSearchForm(sites=sites,data=request.POST)
		if form.is_valid():
			site  = form.cleaned_data['site']
			status = form.cleaned_data['status']
			outage_type = form.cleaned_data['outage_type']
			description = form.cleaned_data['description']
			priority = form.cleaned_data['priority']
			complaint_by = form.cleaned_data['complaint_by']
			from_date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
                        issues = Complaint.objects.filter(site__company=request.user.usercompany.company)
                        if from_date:

                            issues = issues.filter(site__company=request.user.usercompany.company,outage_start__gte=from_date)
                        if from_date and to_date:
                            issues = issues.filter(site__company=request.user.usercompany.company,outage_start__range=[from_date,to_date])

                        if site:
			    issues = Complaint.objects.filter(site=site)
			if status:
				issues = issues.filter(status=status)
			if outage_type:
				issues = issues.filter(outage_type=outage_type)
			if description:
				issues = issues.filter(description=description)
			if priority:
				issues = issues.filter(priority=priority)
			if complaint_by:
				issues = issues.filter(complaint_by=complaint_by)
                        totals = {}
                        totals['count'] = issues.count()
                        totals['genloss'] = issues.aggregate(loss=Sum('genloss'))['loss']
			return render(request,'issues/issues.html',{'form':form,'today':issues,'search':issues,'upcoming':upcoming,'due':due,'totals':totals},
		  				)



	return render(request,'issues/issues.html',{'form':form,'today':new,'search':new,'upcoming':upcoming,'due':due},
		  				)


@login_required
def create_action(request,issue_id):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	complaint = get_object_or_404(Complaint,pk=issue_id)
	if not complaint.company == usercompany.company:
		return Http404

	form  = NewActionForm(sites)
	if request.method == "POST":
		form = NewActionForm(sites=sites,data=request.POST)
		if form.is_valid():
			action = form.save(commit=False)
			action.user = request.user
			action.complaint = complaint
			action.save()
			messages.success(request, 'Action updated Successfully!')
			return HttpResponseRedirect('/')


	return render(request,'issues/create_action.html',{'form':form,'complaint':complaint},
		  				)


@login_required
def create_resolution(request,issue_id):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	complaint = get_object_or_404(Complaint,pk=issue_id)
	if not complaint.company == usercompany.company:
		return Http404

	pms = ComplaintPM.objects.filter(complaint=complaint)
	form  = NewResolutionForm(sites)
	close = False	
	for pm in pms:
		if pm.pmalert.status == "completed":
			close = True	
        pos = ComplaintPO.objects.filter(complaint=complaint)
        print pos
        if pos:
            for po in pos:
                stock = ItemRecieved.objects.filter(created__gte=po.po.created,item=po.po.item)
                if not stock:
                    close = False
                else:
                    close = True
	if request.method == "POST":
		form = NewResolutionForm(sites=sites,data=request.POST)
		if form.is_valid():
			action = form.save(commit=False)
			action.user = request.user
			action.complaint = complaint
			action.save()
			status = Status.objects.get(title="close")
			complaint.status = status
			if not complaint.outage_end:
				complaint.outage_end = action.outage_end
			if not complaint.genloss:
				complaint.genloss = action.generation_loss
			complaint.save()
			messages.success(request, 'Issue closed Successfully!')
			user_action = Action(action_taken="issue closed",work_status="close",complaint=complaint,user=request.user)
			user_action.save()
			return HttpResponseRedirect('/')


        return render(request,'issues/create_resolution.html',{'form':form,'complaint':complaint,'pms':pms,'pos':pos,'close':close},
		  				)



@login_required
def issue_reopen(request,issue_id):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	complaint = get_object_or_404(Complaint,pk=issue_id)
	if not complaint.company == usercompany.company:
		return Http404


	if request.method == "POST":
		form = IssueReopenForm(request.POST)
		if form.is_valid():
			reason = form.cleaned_data['reason']
			action = Action(action_taken=reason,work_status="reopened",complaint=complaint,user=request.user)
			action.save()
			status = Status.objects.get(title="open")
			complaint.status = status
			complaint.save()
			messages.success(request, 'Issue reopened successfully!')

		return HttpResponseRedirect('/')


	else:
		return HttpResponse("this method is not allowed")
@login_required
def issue_approved(request,issue_id):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	complaint = get_object_or_404(Complaint,pk=issue_id)
	if not complaint.company == usercompany.company:
		return Http404


        action = Action(action_taken="Issue Approved",work_status="approved",complaint=complaint,user=request.user)
        action.save()
        status = Status.objects.get(title="open")
        complaint.status = status
        complaint.save()
        messages.success(request, 'Issue approved successfully!')

        return HttpResponseRedirect('/')





@login_required
def mark_issues(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)


	issues = request.GET.get('issues')
	action = request.GET.get('action')
	return HttpResponse(action)
	#issues = issues.split(",")
	for issue in issues:
		complaint = Complaint.objects.get(pk=issue)
		if action == "read":
			complaint.is_read = True
			complaint.save()
		elif action == "unread":
			complaint.is_read = False
			complaint.save()
	message  = "Issues marked %s" % action
	return HttpResponse(message)

@login_required
def assign_issue(request,issue_id):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	issue = get_object_or_404(Complaint,pk=issue_id)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)


	form  = AssignIssueForm(sites)
	if request.method == "POST":
		form = AssignIssueForm(sites=sites,data=request.POST)
		if form.is_valid():
			user = form.cleaned_data['user']
			issue.assigned_to = user
			issue.save()
			created = datetime.datetime.now()
			notification = "Issue #%d for site %s assigned to you" % (issue.id,issue.site)
			notice = SiteNotification(notification=notification,site=issue.site,is_read=False,created=created,user=user)
			notice.save()
			message_body = notification

			html_content = render_to_string("notifications/notification_email.html", {"site_url":settings.SITE_URL,"user":user,"notice":message_body})
			send_mail('O&M Portal :Issue Assigned to you', message_body, settings.DEFAULT_FROM_EMAIL,
				    [user.email], fail_silently=False,html_message=html_content)


			messages.success(request, 'Issue Assigned to User Successfully!')
			return JsonResponse({"message":"success"})
	return render(request,'issues/assign_issue.html',{'form':form,'issue':issue},
		  				)


@login_required
def set_target_date(request,issue_id):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	issue = get_object_or_404(Complaint,pk=issue_id)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)


	form  = TargetDateForm()
	if request.method == "POST":
		form = TargetDateForm(data=request.POST)
		if form.is_valid():
			target_date = form.cleaned_data['target_date']
			issue.target_date = target_date
			issue.save()
			messages.success(request, 'Complaint Target Date Set Successfully!')

			notification = "Target date for Complaint #%d is set to - %s" % (issue.id,str(issue.target_date))
			if issue.assigned_to:
				user = issue.assigned_to
			else:
				user = request.user
			notice = SiteNotification(notification=notification,site=issue.site,is_read=False,created=issue.created,user=user)
			notice.save()
			message_body = notification

			html_content = render_to_string("notifications/notification_email.html", {"site_url":settings.SITE_URL,"user":user,"notice":message_body,'issue':issue})
			send_mail('O&M Portal : Issue Target Date Set', message_body, settings.DEFAULT_FROM_EMAIL,
				    [user.email], fail_silently=False,html_message=html_content)


			return JsonResponse({"message":"success"})



	return render(request,'issues/target_date.html',{'form':form,'issue':issue},
		  				)

@login_required
def escalate(request):
    complaint_id = request.GET.get('id')
    complaint = get_object_or_404(Complaint,pk=complaint_id)
    try:
        escalate = Escalate(complaint=complaint)
        escalate.save()
	return JsonResponse({"message":"Your request to escalate the issue is receieved!"})
    except:
	return JsonResponse({"message":"Please try again later!"})

@login_required
def reports(request):

	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)


	form = IssueReportForm(sites=sites)


	type = "breakdown"
	alerts = []



	if request.method == "POST":

		form = IssueReportForm(sites=sites,data=request.POST)
		if form.is_valid():
			if form.cleaned_data['sites']:
				sites = form.cleaned_data['sites']
			from_date = form.cleaned_data['from_date']
			date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']
			breakdown_type = "plantdown"
			#breakdown_cause = form.cleaned_data['breakdown_cause']
			criteria = form.cleaned_data['criteria']
			seg_time = form.cleaned_data['segregate_on_time']
			seg_freq = form.cleaned_data['segregate_on_freq']
			if seg_time == "---":
				seg_time = 3
			if seg_freq == "---":
				seg_freq = 3


			duration = form.cleaned_data['duration']
			equipments = form.cleaned_data['equipments']
			if not equipments:
				equipments = Description.objects.all()

			meta = {}
			meta['breakdown_type'] = "plantdown"
			#meta['breakdown_cause'] = form.cleaned_data['breakdown_cause']
			meta['criteria'] = form.cleaned_data['criteria']
			meta['seg_time'] = form.cleaned_data['segregate_on_time']
			meta['seg_freq'] = form.cleaned_data['segregate_on_freq']

			if criteria == "equipmentwise":

				equipments = form.cleaned_data['equipments']
				if not equipments:
					equipments = Description.objects.all()
				if duration == "monthly":
					year = form.cleaned_data['from_year']
					reportings = []
					for equipment in equipments:
						deta2 = {}
						i = 1
						reporting = []
						while i <= 12:
							deta = {}
							breakdowns = Complaint.objects.filter(site__in=sites,description=equipment,outage_start__month=i,outage_start__year=year)
							counts = breakdowns.count()
							loss  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
							#time = breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total']
							#deta['time'] = time
							deta['loss'] = loss


							if counts:
								deta['month'] = monthToNum(i)
								deta['count'] = counts
								#deta['time'] = time
								deta['loss'] = loss
							else:
								deta['month'] = monthToNum(i)
								deta['count'] = 0
								deta['time'] = 0
								deta['loss'] = 0

							reporting.append(deta)
							i = i+1
						deta2['equipment'] = equipment
						deta2['report'] = reporting
						reportings.append(deta2)
					deta2 = {}
					i = 1
					reporting = []
					while i <= 12:
						deta = {}
						breakdowns = Complaint.objects.filter(site__in=sites,outage_start__month=i,outage_start__year=year,description__in=equipments)
						counts = breakdowns.count()
						loss  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
						#time = breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total']
						#deta['time'] = time
						deta['loss'] = loss

						if counts:
							deta['month'] = monthToNum(i)
							deta['count'] = counts
							#deta['time'] = time
							deta['loss'] = loss


						else:
							deta['month'] = monthToNum(i)
							deta['count'] = 0
							deta['time'] = 0
							deta['loss'] = 0

						reporting.append(deta)
						i = i+1
					deta2['equipment'] = "Total"
					deta2['report'] = reporting
					reportings.append(deta2)


					alldata = []
					i = 1
					while i <= 12:
						reports = []
						data3 = {}
						colspan = 0
						j = 1
						for site in sites:
							data2 = {}
							report2 = []

							for equip in equipments:
								data4 = {}
								sub_categories = SubDescription.objects.filter(description=equip)
								report = []
								for category in sub_categories:
									breakdowns = Complaint.objects.filter(site=site,sub_description=category,outage_start__month=i,outage_start__year=year)
									count = breakdowns.count()
									data = {}
									data['category'] = category
									data['site'] = site.id
									data['breakdowns'] = breakdowns
									data['count'] = count
									data['loss']  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
									#data['time'] = convert_time(breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])
									#if count > 0:
									if j%2 == 0 :
										data['class'] = "bg-purple"
									else:
										data['class'] = "bg-blue"

									report.append(data)
								data4['equipment'] = equip
								colspan = colspan+sub_categories.count()
								data4['main'] = report
								report2.append(data4)
							j= j+1
							data2['site'] = site
							data2['colspan'] = colspan
							data2['data'] = report2
							reports.append(data2)
						data3['date'] = monthToNum(i)
						data3['total_list'] = Complaint.objects.filter(outage_start__month=i,outage_start__year=year,site__in=sites,description__in=equipments)
						data3['total'] = data3['total_list'].count()
						data3['loss'] = data3['total_list'].aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
						#data3['time'] = convert_time(data3['total_list'].aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])
						data3['reports'] = reports
						alldata.append(data3)
						i = i+1

					reports = []
					for site in sites:

						sub_categories = SubDescription.objects.filter(description__in=equipments)
						report = []
						for category in sub_categories:
							breakdowns = Complaint.objects.filter(site=site,sub_description=category,outage_start__year=year)
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['breakdowns'] = breakdowns
							data['site'] = site.id
							data['count'] = count
							data['loss']  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
							#data['time'] = convert_time(breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])
							report.append(data)
						reports.append(report)
					reports2 = []
					total = 0
					for site in sites:

						report = []
						for category in equipments:
							breakdowns = Complaint.objects.filter(site=site,description=category,outage_start__year=year)
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['site'] = site.id
							data['breakdowns'] = breakdowns
							data['loss']  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
							#data['time'] = convert_time(breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])
							data['count'] = count
							total = total+count
							data['colspan'] = SubDescription.objects.filter(description=category).count()
							report.append(data)
						reports2.append(report)

					fulllist = Complaint.objects.filter(outage_start__year=year,site__in=sites,description__in=equipments)
					loss  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
					#time = convert_time(fulllist.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])
					return render(request,'issues/reports_categorywise.html',{'form':form,'reports':alldata,'totals':reports,'totals2':reports2,'total':total,'all':fulllist,'duration':duration,'loss':loss,'time':'time','criteria':criteria,'reportings':reportings},
						)
				if duration == "yearly":
					alldata = []
					from_year = int(form.cleaned_data['from_year'])
					to_year = int(form.cleaned_data['to_year'])
					from_date = datetime.date(from_year,01,01)
					to_date = datetime.date(to_year,12,31)
					year = from_year
					equipments = form.cleaned_data['equipments']
					if not equipments:
						equipments = Description.objects.all()

					while year <= to_year:
						reports = []
						data3 = {}
						colspan = 0
						i = 0
						for site in sites:
							data2 = {}
							report2 = []

							for equip in equipments:
								data4 = {}
								sub_categories = SubDescription.objects.filter(description=equip)
								report = []
								for category in sub_categories:
									breakdowns = Complaint.objects.filter(site=site,sub_description=category,outage_start__year=year)
									count = breakdowns.count()
									data = {}
									data['category'] = category
									data['site'] = site.id
									data['breakdowns'] = breakdowns
									data['loss']  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
									#data['time'] = convert_time(breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])

									data['count'] = count
									#if count > 0:
									if i%2 == 0 :
										data['class'] = "bg-purple"
									else:
										data['class'] = "bg-blue"

									report.append(data)
								data4['equipment'] = equip
								colspan = colspan+sub_categories.count()
								data4['main'] = report
								report2.append(data4)
							i = i+1
							data2['site'] = site
							data2['colspan'] = colspan
							data2['data'] = report2
							reports.append(data2)
						data3['date'] = year
						data3['total_list'] = Complaint.objects.filter(outage_start__year=year,site__in=sites,description__in=equipments)
						data3['total'] = Complaint.objects.filter(outage_start__year=year,site__in=sites,description__in=equipments).count()
						data3['loss'] = data3['total_list'].aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
						#data3['time'] = convert_time(data3['total_list'].aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])

						data3['reports'] = reports
						alldata.append(data3)
						year = year+1

					reports = []
					for site in sites:

						sub_categories = SubDescription.objects.filter(description__in=equipments)
						report = []
						for category in sub_categories:
							breakdowns = Complaint.objects.filter(site=site,sub_description=category,outage_start__range=[from_date,to_date])
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['breakdowns'] = breakdowns
							data['loss']  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
							#data['time'] = convert_time(breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])


							data['site'] = site.id
							data['count'] = count
							report.append(data)
						reports.append(report)
					reports2 = []
					total = 0
					for site in sites:
						report = []
						for category in equipments:
							breakdowns = Complaint.objects.filter(site=site,description=category,outage_start__range=[from_date,to_date])
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['site'] = site.id
							data['breakdowns'] = breakdowns
							data['count'] = count
							data['loss']  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
							#data['time'] = convert_time(breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])

							total = total+count
							data['colspan'] = SubDescription.objects.filter(description=category).count()
							report.append(data)
						reports2.append(report)

					fulllist = Complaint.objects.filter(outage_start__range=[from_date,to_date],site__in=sites,description__in=equipments)
					loss  = fulllist.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
					#time = convert_time(fulllist.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])
					return render(request,'issues/reports_categorywise.html',{'form':form,'reports':alldata,'totals':reports,'totals2':reports2,'total':total,'all':fulllist,'duration':duration,'loss':loss,'time':'time','criteria':criteria},
						)







				if duration == "daywise":
					alldata = []
					while date <= to_date:
						reports = []
						data3 = {}
						colspan = 0
						i = 1
						for site in sites:
							data2 = {}
							report2 = []

							for equip in equipments:
								data4 = {}
								sub_categories = SubDescription.objects.filter(description=equip)
								report = []
								for category in sub_categories:
									breakdowns = Complaint.objects.filter(site=site,sub_description=category,outage_start=date)
									count = breakdowns.count()
									data = {}
									data['category'] = category
									data['site'] = site.id
									data['breakdowns'] = breakdowns
									data['count'] = count
									data['loss']  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
									#data['time'] = convert_time(breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])


									if i%2 == 0 :
										data['class'] = "bg-purple"
									else:
										data['class'] = "bg-blue"

									#if count > 0:
									report.append(data)
								data4['equipment'] = equip
								data4['main'] = report
								colspan = colspan+sub_categories.count()
								report2.append(data4)
							i = i+1
							data2['site'] = site
							data2['colspan'] = colspan
							data2['data'] = report2
							reports.append(data2)
						data3['date'] = date
						data3['total_list'] = Complaint.objects.filter(outage_start=date,site__in=sites,description__in=equipments)
						data3['total'] = data3['total_list'].count()
						data3['reports'] = reports
						data3['loss'] = data3['total_list'].aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
						#data3['time'] = convert_time(data3['total_list'].aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])


						alldata.append(data3)
						date = date+datetime.timedelta(days=1)

					reports = []
					for site in sites:

						sub_categories = SubDescription.objects.filter(description__in=equipments)
						report = []
						for category in sub_categories:
							breakdowns = Complaint.objects.filter(site=site,sub_description=category,outage_start__range=[from_date,to_date])
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['breakdowns'] = breakdowns
							data['site'] = site.id
							data['count'] = count
							data['loss']  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
							#data['time'] = convert_time(breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])


							report.append(data)
						reports.append(report)
					reports2 = []
					total = 0
					for site in sites:
						report = []
						for category in equipments:
							breakdowns = Complaint.objects.filter(site=site,description=category,outage_start__range=[from_date,to_date])
							count = breakdowns.count()
							data = {}
							data['category'] = category
							data['site'] = site.id
							data['breakdowns'] = breakdowns
							data['count'] = count
							data['loss']  = breakdowns.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
							#data['time'] = convert_time(breakdowns.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])


							total = total+count
							data['colspan'] = SubDescription.objects.filter(description=category).count()
							report.append(data)
						reports2.append(report)

					fullist = Complaint.objects.filter(outage_start__range=[from_date,to_date],site__in=sites,description__in=equipments)
					loss  = fullist.aggregate(total=Coalesce(Sum('genloss'),V(0)))['total']
					#time = convert_time(fulllist.aggregate(total=Coalesce(Sum("total_time"),V(0)))['total'])

					return render(request,'issues/reports_categorywise.html',{'form':form,'reports':alldata,'totals':reports,'totals2':reports2,'total':total,'all':fullist,'loss':loss,'time':'time','criteria':criteria},
						)

			if criteria == "all":
				#daily based format

				if duration == "monthly":

					from_year = int(form.cleaned_data['from_year'])
					from_date = datetime.date(from_year,1,1)
					to_date = datetime.date(from_year,12,31)
				if duration == "yearly":

					from_year = int(form.cleaned_data['from_year'])
					to_year = int(form.cleaned_data['to_year'])
					from_date = datetime.date(from_year,1,1)
					to_date = datetime.date(to_year,12,31)

				result_list = []
				qs = Complaint.objects.filter(description__in=equipments,site__in=sites,outage_start__gte=from_date,outage_start__lte=to_date)
				result_list.append(qs)
				results = []
				loss = 0
				total = 0
				total_time = 0
				for result in result_list:
					for row in result:
						data= {}
						data['loss'] = row.genloss
						data['query'] = row
						loss = loss + data['loss']
						total = total+1
						#total_time = total_time+row.total_time
						results.append(data)
				meta['total_loss'] = loss
				meta['total'] = total
				#meta['total_time'] = convert_time(total_time)
				return render(request,'issues/reports.html',{'form':form,'readings':results,'from_date':date,'to_date':to_date,'meta':meta},
					)


				
			if criteria == "major" or criteria == "frequent":
				#daily based format

				if duration == "monthly":

					from_year = int(form.cleaned_data['from_year'])
					from_date = datetime.date(from_year,1,1)
					to_date = datetime.date(from_year,12,31)
				if duration == "yearly":

					from_year = int(form.cleaned_data['from_year'])
					to_year = int(form.cleaned_data['to_year'])
					from_date = datetime.date(from_year,1,1)
					to_date = datetime.date(to_year,12,31)

				result_list = []
				if criteria == "major":
					qs = Complaint.objects.filter(description__in=equipments,site__in=sites,outage_start__gte=from_date,outage_start__lte=to_date).filter(total_time__gte=seg_time)
					result_list.append(qs)
				if criteria == "frequent":
					for equipment in equipments:
						qs = Complaint.objects.filter(site__in=sites,description=equipment,outage_start__range=[from_date,to_date])
						if qs.count() >= int(seg_freq):
							result_list.append(qs)
				results = []
				loss = 0
				total = 0
				total_time = 0
				for result in result_list:
					for row in result:
						data= {}
						data['loss'] = row.genloss
						data['query'] = row
						loss = loss + data['loss']
						total = total+1
						#total_time = total_time+row.total_time
						results.append(data)
				meta['total_loss'] = loss
				meta['total'] = total
				#meta['total_time'] = convert_time(total_time)
				return render(request,'issues/reports.html',{'form':form,'readings':results,'from_date':date,'to_date':to_date,'meta':meta},
					)


		return render(request,'issues/reports.html',{'form':form,},
				)
	return render(request,'issues/plantwise.html',{'form':form,'type':type,},
			)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def create_issue_api(request):
        user=request.user
        site = EnergySite.objects.get(id=request.GET['site'])
        status = Status.objects.get(title="open")
        details = request.GET['details']
        outage_date = datetime.datetime.strptime(
                request.GET['outage_date'], "%Y-%m-%d %H:%M")

        today_date = datetime.datetime.now()
        try:
            outage_type = Outage.objects.get(title=request.GET['outage_type'])
        except:
            outage_type = Outage(title=request.GET['outage_type'])
            outage_type.save()
        try:
            description = Description.objects.get(title=request.GET['description'])
        except:
            description = Description(title=request.GET['description'],outage=outage_type)
            description.save()

        try:
		sub_description = SubDescription.objects.get(title=request.GET['sub_description'],description=description)
        except:
		sub_description = SubDescription(title=request.GET['sub_description'],description=description)
		sub_description.save()

        priority = Priority.objects.get(title="critical")
       
        try:
		genloss = request.GET['genloss']
	except:
		genloss = 0

        try:
		outage_end = request.GET['outage_end']
        	status = Status.objects.get(title="close")
	except:
		outage_end = 0
        try:
            device = Device.objects.get(name=request.GET['device'],device_type=request.GET['device_type'],site=site)
        except:
            device = Device(name=request.GET['device'],device_type=request.GET['device_type'],make=request.GET['make'],rating=request.GET['rating'],site=site)
            device.save()

        #try:
            
        complaint = Complaint(outage_end=outage_end,genloss=genloss,device=device,site=site,complaint_by=user,date_of_complaint=today_date,outage_start=outage_date,outage_type=outage_type,description=description,sub_description=sub_description,priority=priority,status=status,comment=details)

        complaint.company = site.company
        complaint.time_to_detect = (complaint.date_of_complaint - complaint.outage_start).days
        complaint.save()
        return JsonResponse({'status':'success','message':'Issue Created','ticket':complaint.id})
        #except:
        #    return JsonResponse({'status':'error','message':'Unable to create issue!'})



@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def update_issue_api(request):
        complaint = Complaint.objects.get(id=request.GET['ticket'])
        outage_end = datetime.datetime.strptime(
                request.GET['outage_end'], "%Y-%m-%d %H:%M")
        genloss = request.GET['genloss']
        complaint.outage_end = outage_end
       	status = Status.objects.get(title="close")
	complaint.status = status
        complaint.genloss = genloss
        complaint.save()
        return JsonResponse({'status':'success','message':'Issue Updated','ticket':complaint.id})
        #except:
        #    return JsonResponse({'status':'error','message':'Unable to create issue!'})



def action_created_signal(sender,instance,created,**kwargs):
	if created:
		log = " Action created by %s" % (instance.user.username)
		complaint_log = ComplaintLog(log=log,complaint=instance.complaint)
		complaint_log.save()
	else:
		log = " Action updated by %s" % (instance.user.username)
		complaint_log = ComplaintLog(log=log,complaint=instance.complaint)
		complaint_log.save()


def complaint_created_signal(sender,instance,created,**kwargs):
	if created:
		try:
			log = " Issue created by %s" % (instance.complaint_by.username)
		except:
			log = " Issue generated by system"
		complaint_log = ComplaintLog(log=log,complaint=instance)
		complaint_log.save()

                #send email
                print "in signal"
                #usersites = UserCompany.objects.filter(company=instance.company)
                #for user in usersites:
	        print "sending email"
	        message_subject = "AkshyaPower Portal: %s - New Ticket Created #%d" % (instance.description,instance.id)

	        html_content = render_to_string("notifications/issue_email.html", {"user":"","complaint":instance,'SITE_URL':settings.SITE_URL})
	        send_mail(message_subject, "Issue Created", settings.FROM_EMAIL,
	        ['skbohra123@gmail.com',], fail_silently=False,html_message=html_content)


	else:
		try:
			log = " Issue updated by %s" % (instance.complaint_by.username)
		except:
			log = " Issue generated by system"
		complaint_log = ComplaintLog(log=log,complaint=instance)
		complaint_log.save()




post_save.connect(complaint_created_signal, sender=Complaint, dispatch_uid="complaint_created")
post_save.connect(action_created_signal, sender=Action, dispatch_uid="action_created")



