from django.contrib import admin
from .models import *

admin.site.register(Complaint)
admin.site.register(Outage)
admin.site.register(Description)
admin.site.register(ResolutionType)
admin.site.register(Priority)
admin.site.register(Action)
admin.site.register(Remark)
admin.site.register(Status)
admin.site.register(ComplaintLog)
admin.site.register(ComplaintResolution)
admin.site.register(ComplaintPM)
admin.site.register(ComplaintPO)
admin.site.register(Device)

@admin.register(SubDescription)
class SubDescriptionAdmin(admin.ModelAdmin):
        list_display = ('description','title')

