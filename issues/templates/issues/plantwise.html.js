
{%load staticfiles%}
<script>
$(function(){
     $("select").select2({
    width: "100px",
    placeholder: "Select"
    });

$(".field-hidden").parent().hide();

var duration = $("#id_duration,.field-duration").val();
if (duration == "daywise" ){
	$(".field-from-date").attr("required","required").parent().show();
	$(".field-to-date").attr("required","required").parent().show();
	$(".field-from-year").removeAttr("required").parent().hide();
	$(".field-to-year").removeAttr("required").parent().hide();
}
if (duration == "monthly" ){
	$(".field-from-date").removeAttr("required").parent().hide();
	$(".field-to-date").removeAttr("required").parent().hide();
	$(".field-from-year").attr("required","required").parent().show();
	$(".field-to-year").removeAttr("required").parent().hide();

}
if (duration == "yearly" ){
	$(".field-from-date").removeAttr("required","required").parent().hide();
	$(".field-to-date").removeAttr("required").parent().hide();
	$(".field-from-year").attr("required","required").parent().show();
	$(".field-to-year").attr("required","required").parent().show();

}


$("#id_duration,.field-duration").change(function(){


var duration = $(this).val();
if (duration == "daywise" ){
	$(".field-from-date").attr("required","required").parent().show();
	$(".field-to-date").attr("required","required").parent().show();
	$(".field-from-year").removeAttr("required").parent().hide();
	$(".field-to-year").removeAttr("required").parent().hide();
}
if (duration == "monthly" ){
	$(".field-from-date").removeAttr("required").parent().hide();
	$(".field-to-date").removeAttr("required").parent().hide();
	$(".field-from-year").attr("required","required").parent().show();
	$(".field-to-year").removeAttr("required").parent().hide();

}
if (duration == "yearly" ){
	$(".field-from-date").removeAttr("required","required").parent().hide();
	$(".field-to-date").removeAttr("required").parent().hide();
	$(".field-from-year").attr("required","required").parent().show();
	$(".field-to-year").attr("required","required").parent().show();

}


});


$('.form').ajaxForm({ 
        // target identifies the element(s) to update with the server response 
        target: '.ajax-content', 
	beforeSubmit: showRequest, 
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function() { 
            $('.search-results').fadeIn('slow'); 
	    $(".form-button").text("Go");

	   $.notify("Report loaded", "success"); 
        } 
    }); 


});

// pre-submit callback 
function showRequest(formData, jqForm, options) { 
	$(".form-button").text("Loading..");
    return true; 
} 





</script>

<script>

$(function(){
var duration = $("#id_criteria").val();



if (duration == "major" ){
	$("#id_segregate_on_time").parent().show();
	$("#id_segregate_on_freq").parent().hide();
}
if (duration == "frequent" ){
	$("#id_segregate_on_time").parent().hide();
	$("#id_segregate_on_freq").parent().show();

}
if (duration == "all"){
	$("#id_segregate_on_time").parent().hide();
	$("#id_segregate_on_freq").parent().hide();

}

$("#id_criteria").change(function(){


var duration = $(this).val();



if (duration == "major" ){
	$("#id_segregate_on_time").parent().show();
	$("#id_segregate_on_freq").parent().hide();
}
else if (duration == "frequent" ){
	$("#id_segregate_on_time").parent().hide();
	$("#id_segregate_on_freq").parent().show();

}else{
	$("#id_segregate_on_time").parent().hide();
	$("#id_segregate_on_freq").parent().hide();
}
if (duration == "all" ){
	$("#id_segregate_on_time").parent().hide();
	$("#id_segregate_on_freq").parent().hide();
	
}
else{

}



});








var options = {
    autoclose: true,
    todayHighlight: true,
    defaultViewDate: 'today',
};


//$(".field-from-date,.field-to-date").datepicker(options);
});

</script>

    <script src="/static/plantwise.js" type="text/javascript"></script>


