'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, 
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE 
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.                
'''



from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from prevmain.models import EnergySite,Company,PMAlert
from smart_selects.db_fields import ChainedForeignKey 
from stock.models import PO

class Outage(models.Model):
	title = models.CharField(max_length=120)
	def __unicode__(self):
                return '%s' % (self.title)


class Description(models.Model):
	outage = models.ForeignKey(Outage)
	title = models.CharField(max_length=100)
	def __unicode__(self):
                return '%s' % (self.title)



class SubDescription(models.Model):
	description = models.ForeignKey(Description)
	title = models.CharField(max_length=100)
	def __unicode__(self):
                return '%s' % (self.title)



class Priority(models.Model):
	title = models.CharField(max_length=100)
	def __unicode__(self):
                return '%s' % (self.title)
class Status(models.Model):
	title = models.CharField(max_length=100)
	def __unicode__(self):
                return '%s' % (self.title)

class Device(models.Model):
    name = models.CharField(max_length=100)

    DEV_TYPES = (('meter', 'meter'),
                 ('htpanel', 'htpanel'),
                 ('control_room', 'control_room'),
                 ('transformer', 'transformer'),
                 ('field', 'field'),
                 ('sub_station', 'sub_station'),
                 ('ajb', 'ajb'),
                 ('transmisson_line', 'transmission_line'),
                 ('inverter', 'inverter'),
                 ('env_kit', 'env_kit'))

    device_type = models.CharField(max_length=100, choices=DEV_TYPES)

    make = models.CharField(max_length=100)
    rating = models.FloatField()
    site = models.ForeignKey(EnergySite)

    def __unicode__(self):
            return '%s' % (self.name)
'''
class Issue(models.Model):
    user = models.ForeignKey(User)
    site = models.ForeignKey(EnergySite)
    date = models.DateTimeField()
    details = models.TextField()
    OPTIONS = (('pending','pending'),('approved','approved'),('rejected','rejected'))
    status = models.CharField(max_length=100,choices=OPTIONS)

'''


class Complaint(models.Model):
	date_of_complaint = models.DateTimeField(verbose_name="Date of Filing")
	outage_start = models.DateTimeField(verbose_name="Issue starting time")
	time_to_detect = models.FloatField()
	site =  models.ForeignKey(EnergySite,verbose_name="Select the site this issue belongs to")
	complaint_by = models.ForeignKey(User,related_name="complaint_by",verbose_name="Who raised the issue?",null=True,blank=True)
	outage_type = models.ForeignKey(Outage,verbose_name="Select a category for this issue")
        description = ChainedForeignKey(
        Description,
        chained_field="outage_type",
        chained_model_field="outage",
        show_all=False,
        auto_choose=True,
	blank=True,
	null=True,
        verbose_name = "Type"
        )

        sub_description = ChainedForeignKey(
        SubDescription,
        chained_field="description",
        chained_model_field="description",
        show_all=False,
        auto_choose=True,
	blank=True,
	null=True,
        verbose_name = "Select a sub reason"
        )
        device = ChainedForeignKey(Device,null=True,blank=True,chained_field="site",chained_model_field="site",verbose_name="Location")

	priority = models.ForeignKey(Priority,verbose_name="Please choose the issue priority ")
	comment = models.TextField(verbose_name="Details related to the issue")
	status = models.ForeignKey(Status,verbose_name="What's the current status of the issue?")
	company = models.ForeignKey(Company)
	assigned_to = models.ForeignKey(User,related_name="assigned_to",null=True,blank=True,verbose_name="Do you want to assign this to any user?")
	user = models.ForeignKey(User,related_name="created_user",null=True,blank=True)
	created = models.DateTimeField(auto_now_add=True)
	target_date = models.DateField(null=True,blank=True)
	closing_date = models.DateField(null=True,blank=True)
	outage_end = models.DateTimeField(null=True,blank=True)
        impact_on_gen = models.BooleanField(default=True,verbose_name="Issue impacting generation?")
	capacity_down = models.FloatField(null=True,blank=True,default=0.0)
        fault_code = models.CharField(max_length=100,null=True,blank=True)
        plant_pr = models.FloatField(null=True,blank=True,default=0.0) 
        device_pr = models.FloatField(null=True,blank=True,default=0.0)
        radiation = models.FloatField(null=True,blank=True,default=0.0)
	genloss = models.FloatField(null=True,blank=True,default=0.0)
	is_read = models.BooleanField(default=False)
	def __unicode__(self):
                return '%s' % (self.description)

	def total_time(self):
		try:
			return self.outage_end - self.outage_start
		except:
			return 0

class ComplaintPM(models.Model):
	complaint = models.ForeignKey(Complaint)
	pmalert = models.ForeignKey(PMAlert)
	status = models.ForeignKey(Status)

class ComplaintPO(models.Model):
	complaint = models.ForeignKey(Complaint)
	po = models.ForeignKey(PO)
	status = models.ForeignKey(Status)


class ResolutionType(models.Model):
	title = models.CharField(max_length=100)
	
	def __unicode__(self):
                return '%s' % (self.title)



class Escalate(models.Model):
        complaint  = models.OneToOneField(Complaint)
	created = models.DateTimeField(auto_now_add=True)

class Action(models.Model):
	action_taken = models.TextField() 
	work_status = models.CharField(max_length=100) 
	complaint = models.ForeignKey(Complaint)
	created = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User)
	def __unicode__(self):
                return '%s' % (self.action_taken)



class Remark(models.Model):
	text = models.TextField()
	created = models.DateTimeField(auto_now_add=True)
	complaint = models.ForeignKey(Complaint)
	user = models.ForeignKey(User)
	def __unicode__(self):
                return '%s' % (self.text)



class ComplaintLog(models.Model):
	complaint = models.ForeignKey(Complaint)
	log = models.TextField()
	created = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
                return '%s' % (self.log)

class ComplaintResolution(models.Model):
	resolution = models.ForeignKey(ResolutionType)
	details = models.TextField()
	complaint = models.ForeignKey(Complaint)
	created = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User)
	outage_end = models.DateTimeField(null=True,blank=True,help_text= "Time of Ticket Closing")
	generation_loss = models.FloatField(help_text="Calculated Generation Loss in KWh")
	def __unicode__(self):
                return '%s' % (self.details)


