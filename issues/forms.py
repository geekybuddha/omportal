'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, 
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE 
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.                
'''




from django.forms import ModelForm
from django import forms
from .models import *
from django.contrib.auth.models import User
from datetimewidget.widgets import DateTimeWidget
from prevmain.models import PMAlert

class IssueSearchForm(forms.Form):
	site = forms.ModelChoiceField(queryset = None,required=False)
	status = forms.ModelChoiceField(queryset = None,required=False)	
	outage_type = forms.ModelChoiceField(queryset = None,required=False,label="Category")	
	description = forms.ModelChoiceField(queryset = None,required=False,label="Type")	
	sub_description = forms.ModelChoiceField(queryset = None,required=False,label="Description")	
	priority = forms.ModelChoiceField(queryset = None,required=False)	
	complaint_by = forms.ModelChoiceField(queryset = None,required=False)	

	from_date = forms.DateField(help_text="Choose starting date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-from-date'}))	
	to_date = forms.DateField(help_text="Choose ending date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-to-date'}))


        def __init__(self,sites, *args, **kwargs):	
        	super(IssueSearchForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites 
        	self.fields['outage_type'].queryset = Outage.objects.all()
        	self.fields['description'].queryset = Description.objects.all()
        	self.fields['sub_description'].queryset = SubDescription.objects.all()
        	self.fields['priority'].queryset = Priority.objects.all()
        	self.fields['complaint_by'].queryset = User.objects.all()
        	self.fields['status'].queryset = Status.objects.all()


class NewIssueForm(ModelForm):
	class Meta:
		model = Complaint
		exclude = (
                    'complaint_by', 'fault_code',  'date_of_complaint', 'device_pr','plant_pr','radiation','created','time_to_detect','company','target_date','closing_date','user','outage_end','is_read',)
		widgets = {
	            #Use localization and bootstrap 3
        	    'outage_start': DateTimeWidget(attrs={'id':"outage_start"}, usel10n = True, bootstrap_version=3)
		}
	def __init__(self, sites, *args, **kwargs):
        	super(NewIssueForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites
class NewClientIssueForm(ModelForm):
	class Meta:
		model = Complaint
		exclude = (
                    'date_of_complaint',    'fault_code','complaint_by','status','assigned_to','capacity_down','fault_code','device_pr','plant_pr','radiation','created','time_to_detect','company','target_date','closing_date','user','outage_end','genloss','is_read',)
		widgets = {
	            #Use localization and bootstrap 3
        	    'outage_start': DateTimeWidget(attrs={'id':"outage_start"}, usel10n = True, bootstrap_version=3)
		}
	def __init__(self, sites, *args, **kwargs):
        	super(NewClientIssueForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites

class UpdateIssueForm(ModelForm):
	class Meta:
		model = Complaint
		exclude = ('created','time_to_detect','company','target_date','closing_date','user','outage_end','genloss','is_read',)
		widgets = {
	            #Use localization and bootstrap 3
        	    'date_of_complaint': DateTimeWidget(attrs={'id':"date_of_complaint"}, usel10n = True, bootstrap_version=3),
        	    'outage_start': DateTimeWidget(attrs={'id':"outage_start"}, usel10n = True, bootstrap_version=3)
		}
	def __init__(self, sites, *args, **kwargs):
        	super(UpdateIssueForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites




class NewActionForm(ModelForm):
	class Meta:
		model = Action
		exclude = ('created','complaint','user',)
	def __init__(self, sites, *args, **kwargs):
        	super(NewActionForm, self).__init__(*args, **kwargs)

class IssuePMForm(ModelForm):
	class Meta:
		model = ComplaintPM
		exclude = ('status','complaint')
	def __init__(self, *args, **kwargs):
        	super(IssuePMForm, self).__init__(*args, **kwargs)


class NewResolutionForm(ModelForm):
	class Meta:
		model = ComplaintResolution
		exclude = ('created','complaint','user',)
		widgets = {
	            #Use localization and bootstrap 3
        	    'outage_end': DateTimeWidget(attrs={'id':"outage_end"}, usel10n = True, bootstrap_version=3)
		}

	def __init__(self, sites, *args, **kwargs):
        	super(NewResolutionForm, self).__init__(*args, **kwargs)




class AssignIssueForm(forms.Form):
	user = forms.ModelChoiceField(queryset = None)
	def __init__(self,sites, *args, **kwargs):	
        	super(AssignIssueForm, self).__init__(*args, **kwargs)
        	self.fields['user'].queryset = User.objects.all()
 
class TargetDateForm(forms.Form):
	target_date = forms.DateTimeField()
	def __init__(self, *args, **kwargs):	
        	super(TargetDateForm, self).__init__(*args, **kwargs)
	class Meta:
 		widgets = {
	            #Use localization and bootstrap 3
        	    'target_date': DateTimeWidget(attrs={'id':"target_date"}, usel10n = True, bootstrap_version=3),
		}




class IssueReportForm(forms.Form):


	#all_sites2 = forms.BooleanField(required=False,label="All Sites")
	sites = forms.ModelMultipleChoiceField(required=False,
  		widget=forms.SelectMultiple(attrs={'class': 'chzn-select'}),
   		queryset = None)
	CRI = (('all','All BreakDowns'),
		   ('major','Major BreakDowns'),
		   ('frequent','Frequent BreakDowns'),
		   ('equipmentwise','Category Wise'),
		  )
	#CAUSES = (
	#	   ('---','---'),
	#	  )


	#breakdown_cause = DynamicChoiceField(choices=CAUSES,required=False,widget=forms.Select(attrs={'class':''}))
	criteria = forms.ChoiceField(choices=CRI,widget=forms.Select(attrs={'class':''}))

	equipments = forms.ModelMultipleChoiceField(
  		widget=forms.SelectMultiple(attrs={'class': 'chzn-select'}),
   		queryset = None,
		required = False,
                label="Category"
		)
	


	CHOICES = (('daywise','Daywise'),
		   ('monthly','Monthly'),
		   ('yearly','Yearly'),
		  )

	duration = forms.ChoiceField(choices=CHOICES,label="Time Period")
	YEARS = (('2012','2012'),
		   ('2013','2013'),
		   ('2014','2014'),
		   ('2015','2015'),
		   ('2016','2016'),
		   ('2017','2017'),
		   ('2018','2018'),
		   ('2019','2019'),
		   ('2020','2020'),
		   ('2021','2021'),
		   ('2022','2022'),
		   ('2023','2023'),
		   ('2024','2024'),
		   ('2025','2025'),
		  )
	from_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-from-year'}))
	to_year = forms.ChoiceField(choices=YEARS,required=False,widget=forms.Select(attrs={'class':'field-hidden field-to-year'}))




	from_date = forms.DateField(help_text="Choose starting date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-from-date'}))	
	to_date = forms.DateField(help_text="Choose ending date of the report",required=False,widget=forms.TextInput(attrs={'class':'field-to-date'}))

	#CHOICES = (('all','all'),
	#	   ('griddown','griddown'),
	#	   ('plantdown','plantdown'),
	#	  )

	#breakdown_type = forms.ChoiceField(choices=CHOICES,widget=forms.Select(attrs={'class':''}))
	TIME = (
		   ('---','---'),
		   ('1','1 hour'),
		   ('2','2 hours'),
		   ('3','3 hours '),
		   ('4','4 hours '),
		   ('5','5 hours '),
		   ('6','6 hours '),
		   ('7','7 hours '),
		   ('8','8 hours '),
		   ('9','9 hours '),
		   ('10','10 hours '),
		   ('11','11 hours '),
		   ('12','12 hours '),
		  )
	segregate_on_time = forms.ChoiceField(choices=TIME,required=False,widget=forms.Select(attrs={'class':'field-hidden'}))
	FREQ = (
		   ('---','---'),
		   ('1 ','1 time'),
		   ('2','2 times '),
		   ('3','3 times '),
		   ('4','4 times '),
		   ('5','5 times '),
		   ('6','6 times '),
		   ('7','7 times '),
		   ('8','8 times '),
		   ('9','9 times '),
		   ('10','10 time '),
		   ('11','11 times'),
		   ('12','12 times'),
		  )

	


	segregate_on_freq = forms.ChoiceField(choices=FREQ,required=False,widget=forms.Select(attrs={'class':'field-hidden'}))
	


	def __init__(self,sites, *args, **kwargs):	
        	super(IssueReportForm, self).__init__(*args, **kwargs)
        	self.fields['sites'].queryset = sites
        	self.fields['equipments'].queryset = Description.objects.all()



class IssueReopenForm(forms.Form):
	reason = forms.CharField(max_length=100)
	
