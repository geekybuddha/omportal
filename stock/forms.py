'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, 
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE 
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.                
'''




from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
import datetime
from datetimewidget.widgets import DateTimeWidget
from stock.models import *

class ItemForm(ModelForm):
	class Meta:
		model = StockItem
		exclude = ('company','entry_by',)

	def __init__(self, company, *args, **kwargs):
        	super(ItemForm, self).__init__(*args, **kwargs)
		self.company = company

	#def clean_material_description(self):
        #
    	#	try:
        #		StockItem.objects.get(company=self.company,material_description=self.cleaned_data['material_description']) 
    	#		raise forms.ValidationError("Sorry, this item already exist in the database")
    	#	except StockItem.DoesNotExist :
        #		return self.cleaned_data['material_description']



class ItemIssuedForm(ModelForm):
	class Meta:
		model = ItemIssued
		exclude = ('entry_by','created',)

		widgets = {
		    #Use localization and bootstrap 3
		    'issue_date': DateTimeWidget(attrs={'id':"issue_date"}, usel10n = True, bootstrap_version=3)
		}

	def __init__(self, sites, *args, **kwargs):
        	super(ItemIssuedForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites
	def clean_quantity(self):

    		try:
        		stock = ClosingStock.objects.get(site=self.cleaned_data['site'],item=self.cleaned_data['item'])
			if self.cleaned_data['quantity'] > stock.quantity:
				message = "Sorry only %d quantity in stock" % stock.quantity
    			 	raise forms.ValidationError(message)
			else:
        			return self.cleaned_data['quantity']
    		except ClosingStock.DoesNotExist :
    			 raise forms.ValidationError("Sorry, no stock available for the item.")


class ItemRecievedForm(ModelForm):
	class Meta:
		model = ItemRecieved
		exclude = ('entry_by','created',)

	def __init__(self, sites, *args, **kwargs):
        	super(ItemRecievedForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites


class ClosingStockForm(forms.Form):

	CHOICES = (
		   ('closing','Closing'),
		   ('issued','Outward'),
		   ('rec','Inward'),
		   ('po','PO'),
		   ('aval','Availability'),
		  )
	report_type = forms.ChoiceField(choices=CHOICES,)

	item = forms.ModelChoiceField(queryset = None,required=False)

	from_date = forms.DateField(widget=forms.TextInput(attrs={'class':'field-hidden field-from-date'}),required=False)	
	to_date = forms.DateField(widget=forms.TextInput(attrs={'class':'field-hidden field-to-date'}),required=False)	

	site = forms.ModelChoiceField(queryset = None,required=False,widget=forms.Select(attrs={'class':'field-hidden field-site'}))
	
	def __init__(self,sites,items, *args, **kwargs):	
        	super(ClosingStockForm, self).__init__(*args, **kwargs)
        	self.fields['site'].queryset = sites
        	self.fields['item'].queryset = items
	
	def clean(self): 
		if self.cleaned_data['to_date'] and self.cleaned_data['to_date'] < self.cleaned_data['from_date']:
    			raise forms.ValidationError("To date can't be less than from date")




class POForm(ModelForm):
	class Meta:
		model = PO
		exclude = ('requested_by','approved_by','created','status')

	def __init__(self,sites,items, *args, **kwargs):	
        	super(POForm, self).__init__(*args, **kwargs)
        	self.fields['for_site'].queryset = sites
        	self.fields['item'].queryset = items


class PurchaseOrderForm(ModelForm):
	class Meta:
		model = PurchaseOrder
		exclude = ('requested_by','created')

	def __init__(self,sites, *args, **kwargs):	
        	super(PurchaseOrderForm, self).__init__(*args, **kwargs)
        	self.fields['for_site'].queryset = sites


class POUpdateForm(ModelForm):
	class Meta:
		model = PO
		fields = ('status',)


class PurchaseOrderUpdateForm(ModelForm):
	class Meta:
		model = PurchaseOrder
		fields = ('payment_status','delivery_date')



