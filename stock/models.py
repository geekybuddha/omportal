'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, 
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE 
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.                
'''




from django.db import models
from django.contrib.auth.models import User
from prevmain.models import EnergySite,Company,PMAlert

from django.db.models.signals import post_save
class StockItem(models.Model):
	material_description = models.CharField(max_length=255,verbose_name="Category")
	UNITS = (('pcs','pcs'),
		 ('litre','ltr'),
		 ('meter','mtr'),
		 ('Kg','kg'),
		 ('Pair','pair'),
		)
	
	part_code = models.CharField(max_length=40)	
	specifications = models.CharField(max_length=400)	
	unit_of_measurement = models.CharField(choices=UNITS,max_length=100)
	company = models.ForeignKey(Company)
	
	created = models.DateTimeField(auto_now_add=True)

	remarks = models.TextField()
	entry_by = models.ForeignKey(User)
	def __unicode__(self):
                return '%s-%s' % (self.material_description,self.specifications)

  	class Meta:
	    unique_together = ('material_description', 'part_code','specifications',)
	
class ItemIssued(models.Model):
	site = models.ForeignKey(EnergySite)
	item = models.ForeignKey(StockItem)
	quantity = models.IntegerField()
	created = models.DateTimeField(auto_now_add=True)
	issue_date = models.DateTimeField(verbose_name="Item issuing date")	
	TYPES = (('consumed','consumed'),
		 ('inter_site_sale','Inter Site Sale'),
		 ('inter_site_return','Inter Site Returnable'),
		 ('repair','Sent For Repair'),
		 ('scrap_sale','Scrap Sale'),
		 ('warranty_transfer','Warranty Transfer'),
		)
	transfer_type = models.CharField(choices=TYPES,max_length=100,verbose_name="Reason")
        related_pm = models.ForeignKey(PMAlert,blank=True,null=True)	
	to_site = models.ForeignKey(EnergySite,null=True,blank=True,related_name="to_site")
	remarks = models.TextField(null=True,blank=True,verbose_name="Remarks (if any)")
	entry_by = models.ForeignKey(User)

class ItemRecieved(models.Model):
	site = models.ForeignKey(EnergySite)
	item = models.ForeignKey(StockItem)
	quantity = models.IntegerField()
	created = models.DateTimeField(auto_now_add=True)
	TYPES = (
		 ('inter_site_sale','Inter Site Buy'),
		 ('inter_site_return','Inter Site Returnable'),
		 ('repair','Received From Repair'),
		 ('warranty_transfer','Warranty Transfer Received'),
		 ('opening_stock','Opening Stock'),
		)
	transfer_type = models.CharField(choices=TYPES,max_length=100)
	from_site = models.ForeignKey(EnergySite,null=True,blank=True,related_name="from_site")
	remarks = models.TextField()
	invoice_no = models.CharField(max_length=100)
	purchase_order_no = models.CharField(max_length=100)
	entry_by = models.ForeignKey(User)
	


class ClosingStock(models.Model):
	item = models.ForeignKey(StockItem)
	site = models.ForeignKey(EnergySite)
	quantity = models.IntegerField()
	inward = models.IntegerField()
	outward = models.IntegerField()
	last_updated = models.DateTimeField(auto_now_add=True)

#this is actually PR(Purchase Request)
class PO(models.Model):
	for_site = models.ForeignKey(EnergySite)
	new_item = models.BooleanField(default=False)
	item = models.ForeignKey(StockItem,null=True,blank=True)
	quantity = models.IntegerField()
	created = models.DateTimeField(auto_now_add=True)
	
	TYPES = (('pending','Pending'),
		 ('approved','Approved'),
		 ('cancelled','Cancelled'),
		)
	status = models.CharField(choices=TYPES,max_length=100)
	
	remarks = models.TextField(verbose_name="Reason for Request")
	requested_by = models.ForeignKey(User,related_name="requested_user")
	approved_by = models.ForeignKey(User,related_name="approved_user",null=True,blank=True)

#this is actually PR(Purchase Request)
class PurchaseOrder(models.Model):
	for_site = models.ForeignKey(EnergySite)
	created = models.DateTimeField(auto_now_add=True)
	invoice_no = models.CharField(max_length=100)	
	invoice_date = models.DateField()
	delivery_date = models.DateField()
	TYPES = (('pending','Pending'),
		 ('approved','Approved'),
		 ('cancelled','Cancelled'),
		)
	payment_status = models.CharField(choices=TYPES,max_length=100)
	
	remarks = models.TextField()
	requested_by = models.ForeignKey(User,related_name="po_requested_user")

class PMSpare(models.Model):
        pmalert = models.ForeignKey(PMAlert)
	date_time = models.DateTimeField()
        spare = models.ForeignKey(ItemIssued)



def update_stock(sender, instance, **kwargs):
	site = instance.site
	try:
		stock = ClosingStock.objects.get(item=instance.item,site=site)
		quantity = stock.quantity - instance.quantity
		stock.quantity = quantity
		stock.outward = instance.quantity + stock.outward
		stock.save()
	except ClosingStock.DoesNotExist:
		print "no stock entry?"

def update_stock_rec(sender, instance, **kwargs):
	site = instance.site
	try:
		stock = ClosingStock.objects.get(item=instance.item,site=site)
		quantity = stock.quantity + instance.quantity
		stock.quantity = quantity
		stock.inward = instance.quantity + stock.inward
		stock.save()
	except ClosingStock.DoesNotExist:
		stock = ClosingStock(item=instance.item,site=instance.site,quantity=instance.quantity,inward=instance.quantity,outward=0)
		stock.save()

	
	

post_save.connect(update_stock, sender=ItemIssued, dispatch_uid="update_stock")	
post_save.connect(update_stock_rec, sender=ItemRecieved, dispatch_uid="update_stock_rec")	
