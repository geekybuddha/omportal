'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
'''




from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^new/$', views.new_items, name='stock_index'),
    url(r'^issued/$', views.item_issued, name='item_issued'),
    url(r'^received/$', views.item_rec, name='item_rec'),
    url(r'^closing/$', views.stock_statement, name='stock_statement'),
    url(r'^po/$', views.po, name='po'),
    url(r'^item_names/$', views.item_names, name='item_names'),

    url(r'^po/(?P<po_id>\d+)/$', views.update_po, name='update_po'),

    url(r'^purchase_order/$', views.purchase_order, name='purchase_order'),

    url(r'^purchase_order/(?P<po_id>\d+)/$', views.update_purchase_order, name='update_purchase_order'),



    url(r'^issued/(?P<id>\d+)/pdf/$', views.issued_pdf, name='issued_pdf'),
    url(r'^rec/(?P<id>\d+)/pdf/$', views.rec_pdf, name='rec_pdf'),

]
