'''
 GEEKYBUDDHA TECHNOLOGIES PVT. LTD. CONFIDENTIAL
 Unpublished Copyright (c) 2015-2016 GEEKYBUDDHA TECHNOLOGIES PVT. LTD., All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of GEEKBUDDHA TECHNOLOGIES PVT. LTD.. The intellectual and technical concepts contained
 herein are proprietary to GEEKYBUDDHA TECHNOLOGIES PVT. LTD. and may be covered by INDIAN and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from GEEKYBUDDHA TECHNOLOGIES PVT. LTD..  Access to the source code contained herein is hereby forbidden to anyone except current GEEKYBUDDHA TECHNOLOGIES PVT. LTD. employees, managers or contractors who have executed
 Confidentiality and Non-disclosure agreements explicitly covering such access.

The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  GEEKYBUDDHA TECHNOLOGIES PVT. LTD..   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OFAPPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
'''



import datetime
import cStringIO as StringIO
from cgi import escape
from xhtml2pdf import pisa
from reportlab.pdfgen import canvas
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.forms import modelformset_factory
from django.forms.models import BaseModelFormSet,BaseFormSet
from functools import partial, wraps
from django.contrib import messages
from django.shortcuts import render
from django.db.models import Count, Min, Sum, Avg
from django.shortcuts import get_object_or_404
from prevmain.models import UserCompany,EnergySite,Company,UserSite
from issues.models import Complaint,Outage,Priority,SubDescription,Description,Status,ComplaintPO
from django.db.models import Q
from django.core.mail import send_mail

from datetime import timedelta
from django.template.loader import render_to_string
from django.conf import settings

from .forms import *
FROM_EMAIL = settings.DEFAULT_FROM_EMAIL

class RequiredFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


@login_required
def new_items(request):

	usercompany = UserCompany.objects.get(user=request.user).company
	form = ItemForm(company=usercompany)

	if request.method == "POST":
		form = ItemForm(company=usercompany,data=request.POST)

		if form.is_valid():
			data = form.save(commit=False)
			data.company = usercompany
			data.entry_by = request.user
			data.save()
			messages.success(request, 'Item details added successfully!')
			return HttpResponseRedirect('/stock/closing/')
	return render(request,'stock/new_items.html',{'form':form,},)



@login_required
def item_issued(request):
	#site_id = request.session.get('current_site')
	#site = EnergySite.objects.get(pk=site_id)

	usercompany = get_object_or_404(UserCompany,user=request.user)

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites)

	form = ItemIssuedForm(sites=sites)

	try:
		form.fields['item'].initial = StockItem.objects.get(pk=request.GET['item'])
	except:
		pass
	issued = ItemIssued.objects.filter(site__in=sites).order_by('-created')
	if request.method == "POST":
		form = ItemIssuedForm(sites=sites,data=request.POST)
		if form.is_valid():
			data = form.save(commit=False)
			data.entry_by = request.user
			data.save()

			messages.success(request, 'Item issue entry added successfully!')
			return HttpResponseRedirect('/stock/closing/')
	return render(request,'stock/item_issued.html',{'form':form,'issued':issued},)

@login_required
def item_rec(request):

	usercompany = get_object_or_404(UserCompany,user=request.user)

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)

	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	rec = ItemRecieved.objects.filter(site__in=sites).order_by('created')
	form = ItemRecievedForm(sites=sites)

	try:
		form.fields['item'].initial = StockItem.objects.get(pk=request.GET['item'])
	except:
		pass


	if request.method == "POST":
		form = ItemRecievedForm(sites=sites,data=request.POST)
		if form.is_valid():
			data = form.save(commit=False)
			data.entry_by = request.user
			data.save()

			messages.success(request, 'Item received entry added successfully!')
			return HttpResponseRedirect('/stock/closing/')
	return render(request,'stock/item_rec.html',{'form':form,'rec':rec},)
@login_required
def stock_statement(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	items = StockItem.objects.filter(company=usercompany.company)
	form = ClosingStockForm(sites=sites,items=items)
	report = []
	to_date = datetime.datetime.now().date()
	from_date = to_date - timedelta(days=30)


	for item in items:
		issued = ItemIssued.objects.filter(item=item,site__in=sites,created__range=[from_date,to_date]).exclude(transfer_type="consumed").aggregate(total=Sum('quantity'))['total']
		consumed = ItemIssued.objects.filter(transfer_type="consumed",item=item,site__in=sites).aggregate(total=Sum('quantity'))['total']
		#rec = ItemRecieved.objects.filter(item=item,site__in=sites,created__range=[from_date,to_date]).aggregate(total=Sum('quantity'))['total']
		#rec = ItemRecieved.objects.filter(item=item,site__in=sites,created__range=[from_date,to_date])
		rec = ItemRecieved.objects.filter(item=item,site__in=sites).exclude(transfer_type="opening_stock").aggregate(total=Sum('quantity'))['total']

		issued_prev = ItemIssued.objects.filter(item=item,site__in=sites,created__lt=from_date).aggregate(total=Sum('quantity'))['total']
		rec_prev = ItemRecieved.objects.filter(item=item,site__in=sites,transfer_type="opening_stock").aggregate(total=Sum('quantity'))['total']


		print rec

		if not issued:
			issued = 0
		if not rec:
			rec = 0
		if not issued_prev:
			issued_prev = 0
		if not rec_prev:
			rec_prev = 0
		if not consumed:
			consumed = 0
		data = {}

		opening = rec_prev #- issued_prev
		closing = opening + rec - issued -consumed
		data['item'] = item
		data['opening'] = opening
		data['closing']  = closing
		data['issued'] = issued
		data['consumed'] = consumed
		data['rec'] = rec
		data['sitewise'] = ClosingStock.objects.filter(item=item)
		report.append(data)



	if request.method == "POST":
		form = ClosingStockForm(sites=sites,items=items,data=request.POST)
		if form.is_valid():

			from_date = form.cleaned_data['from_date']
			to_date = form.cleaned_data['to_date']


			if 'site' in request.POST:
				site = form.cleaned_data['site']
			if 'item' in request.POST :
				item = form.cleaned_data['item']
			if 'report_type' in request.POST :
				report_type = form.cleaned_data['report_type']


			if report_type == "aval":
				results = ClosingStock.objects.filter(site__in=sites,item=item)
				total = results.aggregate(total=Sum('quantity'))['total']
				return render(request,'stock/aval.html',{'form':form,'item':item,'results':results,'total':total},)

			if item and site:

				if report_type == "po":
					results = PO.objects.filter(item=item,for_site=site,created__range=[from_date,to_date])
					return render(request,'stock/po_entries.html',{'form':form,'data':results,'from':from_date,'to':to_date,'type':report_type},)
				if report_type == "purchase_order":
					results = PurchaseOrder.objects.filter(for_site=site,created__range=[from_date,to_date])
					return render(request,'stock/purchase_order_entries.html',{'form':form,'data':results,'from':from_date,'to':to_date,'type':report_type},)


				issued = ItemIssued.objects.filter(item=item,site=site,created__range=[from_date,to_date]).exclude(transfer_type="consumed").aggregate(total=Sum('quantity'))['total']
				consumed = ItemIssued.objects.filter(transfer_type="consumed",item=item,site=site,created__range=[from_date,to_date]).aggregate(total=Sum('quantity'))['total']
				rec = ItemRecieved.objects.filter(item=item,site=site,created__range=[from_date,to_date]).exclude(transfer_type="opening_stock").aggregate(total=Sum('quantity'))['total']

				issued_prev = ItemIssued.objects.filter(item=item,site=site,created__lt=from_date).aggregate(total=Sum('quantity'))['total']
				rec_prev = ItemRecieved.objects.filter(transfer_type="opening_stock",item=item,site=site,created__lt=from_date).aggregate(total=Sum('quantity'))['total']

				if not issued:
					issued = 0
				if not consumed:
					consumed = 0

				if not rec:
					rec = 0
				if not issued_prev:
					issued_prev = 0
				if not rec_prev:
					rec_prev = 0
				data = {}

				opening = rec_prev #- issued_prev
				closing = opening + rec - issued - consumed
				data['item'] = item
				data['site'] = site
				data['opening'] = opening
				data['closing']  = closing
				data['issued'] = issued
				data['rec'] = rec
				data['consumed'] = consumed
				if not report_type or report_type == "closing":
					return render(request,'stock/closing_item_site.html',{'form':form,'data':data,'from':from_date,'to':to_date},)

				if report_type == "rec":

					rec = ItemRecieved.objects.filter(item=item,site=site,created__range=[from_date,to_date])
					return render(request,'stock/entries.html',{'form':form,'data':rec,'from':from_date,'to':to_date,'type':'Inward'},)
				if report_type == "issued":

					issued = ItemIssued.objects.filter(item=item,site=site,created__range=[from_date,to_date])
					return render(request,'stock/entries.html',{'form':form,'data':issued,'from':from_date,'to':to_date,'type':'Outward'},)

			if item and not site:
				if report_type == "po":
					results = PO.objects.filter(item=item,for_site__in=sites,created__range=[from_date,to_date])
					return render(request,'stock/po_entries.html',{'form':form,'data':results,'from':from_date,'to':to_date,'type':report_type},)
				if report_type == "purchase_order":
					results = PurchaseOrder.objects.filter(for_site__in=sites,created__range=[from_date,to_date])
					return render(request,'stock/purchase_order_entries.html',{'form':form,'data':results,'from':from_date,'to':to_date,'type':report_type},)


				issued = ItemIssued.objects.filter(item=item,site__in=sites,created__range=[from_date,to_date]).exclude(transfer_type="consumed").aggregate(total=Sum('quantity'))['total']
				consumed = ItemIssued.objects.filter(transfer_type="consumed",item=item,site__in=sites,created__range=[from_date,to_date]).aggregate(total=Sum('quantity'))['total']
				rec = ItemRecieved.objects.filter(item=item,site__in=sites,created__range=[from_date,to_date]).aggregate(total=Sum('quantity'))['total']

				issued_prev = ItemIssued.objects.filter(item=item,site__in=sites,created__lt=from_date).aggregate(total=Sum('quantity'))['total']
				rec_prev = ItemRecieved.objects.filter(item=item,site__in=sites,created__lt=from_date).aggregate(total=Sum('quantity'))['total']

				if not issued:
					issued = 0
				if not consumed:
					consumed = 0

				if not rec:
					rec = 0
				if not issued_prev:
					issued_prev = 0
				if not rec_prev:
					rec_prev = 0
				data = {}

				opening = rec_prev - issued_prev
				closing = opening + rec - issued - consumed
				data['item'] = item
				data['opening'] = opening
				data['closing']  = closing
				data['issued'] = issued
				data['rec'] = rec
				data['consumed'] = consumed
				if not report_type or report_type == "closing":

					return render(request,'stock/closing_item_site.html',{'form':form,'data':data,'from':from_date,'to':to_date},)
				if report_type == "rec":

					rec = ItemRecieved.objects.filter(item=item,site__in=sites,created__range=[from_date,to_date])
					return render(request,'stock/entries.html',{'form':form,'data':rec,'from':from_date,'to':to_date,'type':'Inward'},)
				if report_type == "issued":

					issued = ItemIssued.objects.filter(item=item,site__in=sites,created__range=[from_date,to_date])
    					return render(request,'stock/entries.html',{'form':form,'data':issued,'from':from_date,'to':to_date,'type':'Outward'},)

			if not item and not site:
				if report_type == "po":
					results = PO.objects.filter(for_site__in=sites,created__range=[from_date,to_date])
					return render(request,'stock/po_entries.html',{'form':form,'data':results,'from':from_date,'to':to_date,'type':report_type},)
				if report_type == "purchase_order":
					results = PurchaseOrder.objects.filter(for_site__in=sites,created__range=[from_date,to_date])
					return render(request,'stock/purchase_order_entries.html',{'form':form,'data':results,'from':from_date,'to':to_date,'type':report_type},)


				report = []
				for item in items:
					issued = ItemIssued.objects.filter(item=item,site__in=sites,created__range=[from_date,to_date]).exclude(transfer_type="consumed").aggregate(total=Sum('quantity'))['total']
					consumed = ItemIssued.objects.filter(transfer_type="consumed",item=item,site__in=sites,created__range=[from_date,to_date]).aggregate(total=Sum('quantity'))['total']
					rec = ItemRecieved.objects.filter(item=item,site__in=sites,created__range=[from_date,to_date]).aggregate(total=Sum('quantity'))['total']

					issued_prev = ItemIssued.objects.filter(item=item,site__in=sites,created__lt=from_date).aggregate(total=Sum('quantity'))['total']
					rec_prev = ItemRecieved.objects.filter(item=item,site__in=sites,created__lt=from_date).aggregate(total=Sum('quantity'))['total']

					if not issued:
						issued = 0
					if not rec:
						rec = 0
					if not issued_prev:
						issued_prev = 0
					if not rec_prev:
						rec_prev = 0
					if not consumed:
						consumed = 0
					data = {}

					opening = rec_prev - issued_prev
					closing = opening + rec - issued -consumed
					data['item'] = item
					data['opening'] = opening
					data['closing']  = closing
					data['issued'] = issued
					data['consumed'] = consumed
					data['rec'] = rec
					report.append(data)
				if not report_type or report_type == "closing":
					return render(request,'stock/closing_date.html',{'reports':report,'form':form,'from':from_date,'to':to_date},)
				if report_type == "rec":
					rec = ItemRecieved.objects.filter(site__in=sites,created__range=[from_date,to_date])
					return render(request,'stock/entries.html',{'data':rec,'form':form,'from':from_date,'to':to_date,'type':'Inward'},
						  )
				if report_type == "issued":
					issued = ItemIssued.objects.filter(site__in=sites,created__range=[from_date,to_date])
					return render(request,'stock/entries.html',{'data':issued,'form':form,'from':from_date,'to':to_date,'type':'Outward'},)


			if not item and site:
				if report_type == "po":
					results = PO.objects.filter(for_site=site,created__range=[from_date,to_date])
					return render(request,'stock/po_entries.html',{'form':form,'data':results,'from':from_date,'to':to_date,'type':report_type},)
				if report_type == "purchase_order":
					results = PurchaseOrder.objects.filter(for_site=site,created__range=[from_date,to_date])
					return render(request,'stock/purchase_order_entries.html',{'form':form,'data':results,'from':from_date,'to':to_date,'type':report_type},)


				report = []
				for item in items:
					issued = ItemIssued.objects.filter(item=item,site=site,created__range=[from_date,to_date]).exclude(transfer_type="consumed").aggregate(total=Sum('quantity'))['total']
					consumed = ItemIssued.objects.filter(transfer_type="consumed",item=item,site=site,created__range=[from_date,to_date]).aggregate(total=Sum('quantity'))['total']
					rec = ItemRecieved.objects.filter(item=item,site=site,created__range=[from_date,to_date]).aggregate(total=Sum('quantity'))['total']

					issued_prev = ItemIssued.objects.filter(item=item,site=site,created__lt=from_date).aggregate(total=Sum('quantity'))['total']
					rec_prev = ItemRecieved.objects.filter(item=item,site=site,created__lt=from_date).aggregate(total=Sum('quantity'))['total']

					if not issued:
						issued = 0
					if not consumed:
						consumed = 0

					if not rec:
						rec = 0
					if not issued_prev:
						issued_prev = 0
					if not rec_prev:
						rec_prev = 0
					data = {}

					opening = rec_prev - issued_prev
					closing = opening + rec - issued - consumed
					data['item'] = item
					data['opening'] = opening
					data['closing']  = closing
					data['issued'] = issued
					data['rec'] = rec
					data['consumed'] = consumed
					report.append(data)
				if not report_type or report_type == "closing":
					return render(request,'stock/closing_date.html',{'reports':report,'form':form,'from':from_date,'to':to_date,'site':site},)
				if report_type == "rec":
					rec = ItemRecieved.objects.filter(site=site,created__range=[from_date,to_date])
					return render(request,'stock/entries.html',{'data':rec,'form':form,'from':from_date,'to':to_date,'type':'Inward'},)
				if report_type == "issued":
					issued = ItemIssued.objects.filter(site=site,created__range=[from_date,to_date])
					return render(request,'stock/entries.html',{'data':issued,'form':form,'from':from_date,'to':to_date,'type':'Outward'},)





	date = datetime.datetime.now()
	return render(request,'stock/closing.html',{'data':report,'form':form,'date':date},)


#this is actually PR
@login_required
def po(request):
	try:
		site_id = request.session.get('current_site')
		site = EnergySite.objects.get(pk=site_id)
	except:
		pass

	usercompany = get_object_or_404(UserCompany,user=request.user)

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)

	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

	items = StockItem.objects.filter(company=usercompany.company)
	form = POForm(sites=sites,items=items)
	itemform = ItemForm(company=usercompany.company)
	if request.method == "POST":
		form = POForm(items=items,sites=sites,data=request.POST)
		today  = datetime.datetime.now()
		if form.is_valid():
			data = form.save(commit=False)
			data.requested_by = request.user
			data.status = "pending"
			data.save()
			if form.cleaned_data['new_item']:
				item_form = ItemForm(company=usercompany.company,data=request.POST)
				if item_form.is_valid():

					item = item_form.save(commit=False)
					item.company  = usercompany.company
					item.entry_by = request.user
					item.save()
					data.item  = item
					data.save()
			outage_type = Outage.objects.get(title="Material")
			priority = Priority.objects.get(title="normal") 
			description = Description.objects.get(outage=outage_type,title="Spares")
			sub_description = SubDescription.objects.get(description=description,title="New Spare Request")
			status = Status.objects.get(title="open")
			issue = Complaint(sub_description=sub_description,date_of_complaint=today,outage_start=today,complaint_by=request.user,outage_type=outage_type,priority=priority,comment="System Generated Ticket",company=usercompany.company,time_to_detect=0,description=description,site=data.for_site,status=status,user=request.user)
			issue.save()
			issue_po = ComplaintPO(complaint=issue,po=data,status=status)
			issue_po.save()
			text  = 'Ticket No. #%d  - PR raised. You will be notified when approved or cancelled' % issue.id
			messages.success(request, text)

			'''
			notification  = 'Ticket No. #%d  - PR  raised by site %s, Please take action' % (data.id,data.for_site.name)
			stock_admins = UserCompany.objects.filter(company=usercompany.company,is_stock_admin=True)
			for user in stock_admins:
				notice = SiteNotification(notification=notification,site=data.for_site,is_read=False,created=datetime.datetime.now(),user=user.user)
				url = "/stock/po/%d/" % data.id
				notice.action_url = url
				notice.save()
			'''
			return HttpResponseRedirect('/stock/closing/')
	return render(request,'stock/po.html',{'form':form,'itemform':itemform},)


@login_required
def purchase_order(request):
	try:
		site_id = request.session.get('current_site')
		site = EnergySite.objects.get(pk=site_id)
	except:
		pass

	usercompany = get_object_or_404(UserCompany,user=request.user)

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)
	form = PurchaseOrderForm(sites=sites)
	if request.method == "POST":
		form = PurchaseOrderForm(sites=sites,data=request.POST)
		if form.is_valid():
			data = form.save(commit=False)
			data.requested_by = request.user
			data.save()
			text  = 'Ticket No. #%d  - PO raised. You will be notified when it is updated' % data.id
			messages.success(request, text)

			notification  = 'Ticket No. #%d  - PO  raised for site %s' % (data.id,data.for_site.name)
			stock_admins = UserCompany.objects.filter(company=usercompany.company,is_stock_admin=True)
			for user in stock_admins:
				notice = SiteNotification(notification=notification,site=data.for_site,is_read=False,created=datetime.datetime.now(),user=user.user)
				url = "/stock/purchase_order/%d/" % data.id
				notice.action_url = url
				notice.save()

			return HttpResponseRedirect('/stock/closing/')
	return render(request,'stock/purchase_order.html',{'form':form},)


#this is actually PR
@login_required
def update_po(request,po_id):

	usercompany = get_object_or_404(UserCompany,user=request.user)
	po = get_object_or_404(PO,pk=po_id)

	if usercompany.is_admin or usercompany.is_stock_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)

	if usercompany.company != po.for_site.company:
		return HttpResponse("you are not authorized to access this view")
	form = POUpdateForm(instance=po)
	if request.method == "POST":
		form = POUpdateForm(instance=po,data=request.POST)
		if form.is_valid():
			data = form.save(commit=False)
			data.approved_by = request.user
			data.save()
			text  = 'Ticket No. #%d  - PR updated.' % data.id
			notification  = 'Ticket No. #%d  - PR status changed to  %s, by %s' % (data.id,data.status,data.approved_by.first_name)
			notice = SiteNotification(notification=notification,site=data.for_site,is_read=False,created=datetime.datetime.now(),user=data.requested_by)
			url = "/stock/po/%d/" % data.id
			notice.action_url = url
			notice.save()

			messages.success(request, text)

			return HttpResponseRedirect('/stock/closing/')
	return render(request,'stock/po_update.html',{'form':form,'po':po},)

@login_required
def update_purchase_order(request,po_id):

	usercompany = get_object_or_404(UserCompany,user=request.user)
	po = get_object_or_404(PurchaseOrder,pk=po_id)

	if usercompany.is_admin or usercompany.is_stock_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)

	if usercompany.company != po.for_site.company:
		return HttpResponse("you are not authorized to access this view")
	form = PurchaseOrderUpdateForm(instance=po)
	if request.method == "POST":
		form = PurchaseOrderUpdateForm(instance=po,data=request.POST)
		if form.is_valid():
			data = form.save(commit=False)
			data.save()
			text  = 'Ticket No. #%d  - PO updated.' % data.id
			notification  = 'Ticket No. #%d  - PO status changed to  %s, by %s' % (data.id,data.payment_status,request.user.first_name)
			notice = SiteNotification(notification=notification,site=data.for_site,is_read=False,created=datetime.datetime.now(),user=data.requested_by)
			url = "/stock/purchase_order/%d/" % data.id
			notice.action_url = url
			notice.save()

			messages.success(request, text)

			return HttpResponseRedirect('/stock/closing/')
	return render(request,'stock/purchase_order_update.html',{'form':form,'po':po},)


@login_required
def item_names(request):
    term = request.GET.get('term', None) 	
    if term:
            items = StockItem.objects.filter(material_description__icontains=term).values('material_description','unit_of_measurement','remarks')
    else:
            items = StockItem.objects.all().values('material_description','unit_of_measurement','remarks')
    return JsonResponse(list(items),safe=False)




def render_to_pdf(template_src, context_dict,request):
    template = get_template(template_src)
    context = Context(context_dict)
    html  = template.render(context_dict)
    result = StringIO.StringIO()

    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))



def issued_pdf(request,id):
   issued = get_object_or_404(ItemIssued,pk=id)

   usercompany = get_object_or_404(UserCompany,user=request.user)
   return render_to_pdf(
            'stock/issued_pdf.html',
            {
                'pagesize':'A4',
                'issued': issued,
		'company':usercompany.company
            }
        )


def rec_pdf(request,id):
   rec = get_object_or_404(ItemRecieved,pk=id)

   usercompany = get_object_or_404(UserCompany,user=request.user)
   return render_to_pdf(
            'stock/rec_pdf.html',
            {
                'pagesize':'A4',
                'issued': rec,
		'company':usercompany.company
            },request
        )


def stock_low(sender, instance, **kwargs):
	created = datetime.datetime.now()
	sites = UserSite.objects.filter(site=instance.site)
	if instance.quantity <= 4:
            outage_type = Outage.objects.get(title="Material")
            priority = Priority.objects.get(title="normal") 
            description = Description.objects.get(outage=outage_type,title="Spares")
            sub_description = SubDescription.objects.get(description=description,title="Low Stock")
            status = Status.objects.get(title="open")
            issue = Complaint(sub_description=sub_description,date_of_complaint=created,outage_start=created,complaint_by=None,outage_type=outage_type,priority=priority,comment="System Generated Ticket",company=instance.site.company,time_to_detect=0,description=description,site=instance.for_site,status=status,user=None)
            issue.save()
            issue_po = ComplaintPO(complaint=issue,po=data,status=status)
            issue_po.save()






#post_save.connect(stock_low, sender=ClosingStock, dispatch_uid="stock_low")




