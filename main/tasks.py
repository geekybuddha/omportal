import requests
import json
import time
import datetime
import calendar
import httplib
import urllib



from requests.auth import HTTPBasicAuth
from django.conf import settings
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.core.mail import send_mail
from django.template.loader import render_to_string
from prevmain.models import *
from omportal.celery_tasks import app
from django.db.models import Q,F
from issues.models import *
from django.core.mail import EmailMultiAlternatives
from prevmain.views import summary_xl,aging_xl



@app.task(serializer='pickle')
def daily_pm_emails(*args,**kwargs):
	company = args[0]

	today_date = datetime.datetime.now().date()


	usersites = EnergySite.objects.filter(company__id=company)

        for site in usersites:
	    today = PMAlert.objects.filter(site=site,due_date=today_date).order_by('due_date')
	    due = PMAlert.objects.filter(site=site,due_date__lt=today_date).filter(~Q(status= "completed")).order_by('-due_date')
            message_subject = "[AkshyaPower]: Site: %s - Today's PM Tasks Details" % (site.name)

            ''' 
            status = Status.objects.get(title="open")
            description = Description.objects.get(title="Preventive Maintenance")
            #sub_description = SubDescription.objects.get(title="Delay")
            outage_type = Outage.objects.get(title="Maintenance")
            priority = Priority.objects.get(title="critical")
            complaint = Complaint(site=site,complaint_by=None,date_of_complaint=today_date,outage_start=pm.due_date,outage_type=outage_type,description=description,priority=priority,status=status,comment="Today's Preventive Maintenance Tasks")

            complaint.company = site.company
            complaint.time_to_detect = (complaint.date_of_complaint - complaint.outage_start).days
            complaint.save()
	    '''
	    '''
            for pm in today:
                complaint_pm = ComplaintPM(complaint=complaint,pmalert=pm,status=status)
                complaint_pm.save()
	    '''
            if today or due:
		to = []
		cc = []
		users = UserSite.objects.filter(site=site,role="incharge")

                for user in users:
 			to.append(user.user.email)
		print(users)
		'''
		users =  UserCompany.objects.filter(company=site.company,role="manager")
		print(users)
		print(site)
                for user in users:
			cc.append(user.user.email)
		users =  UserCompany.objects.filter(company=site.company,role="zonal_head",zone=site.zone)
                for user in users:
			cc.append(user.user.email)
		'''
			
		print(to)
		print(cc)
	    	html_content = render_to_string("notifications/notification_email.html", {"user":'',"today":today,"due":due,'SITE_URL':settings.SITE_URL})
	    	msg = EmailMultiAlternatives(message_subject, html_content, settings.FROM_EMAIL, to,cc=cc,bcc=['skbohra123@gmail.com','ritu.ecb@gmail.com',])
	    	msg.attach_alternative(html_content, "text/html")

	    	msg.send()

            else:
                print "no pm today"




@app.task(serializer='pickle')
def daily_pm_done_emails(*args,**kwargs):
	company = args[0]

	today_date = datetime.datetime.now().date() - datetime.timedelta(days=1)


	usersites = EnergySite.objects.filter(company__id=company)

        for site in usersites:
	    today = PMAlert.objects.filter(site=site,completed_date=today_date,status="completed").order_by('due_date')
            message_subject = "[AkshyaPower]: Site: %s - Completed Tasks Details (%s)" % (site.name,str(today_date))

            ''' 
            status = Status.objects.get(title="open")
            description = Description.objects.get(title="Preventive Maintenance")
            #sub_description = SubDescription.objects.get(title="Delay")
            outage_type = Outage.objects.get(title="Maintenance")
            priority = Priority.objects.get(title="critical")
            complaint = Complaint(site=site,complaint_by=None,date_of_complaint=today_date,outage_start=pm.due_date,outage_type=outage_type,description=description,priority=priority,status=status,comment="Today's Preventive Maintenance Tasks")

            complaint.company = site.company
            complaint.time_to_detect = (complaint.date_of_complaint - complaint.outage_start).days
            complaint.save()
	    '''
	    '''
            for pm in today:
                complaint_pm = ComplaintPM(complaint=complaint,pmalert=pm,status=status)
                complaint_pm.save()
	    '''
            if today:
		to = []
		cc = []
		users = UserSite.objects.filter(site=site,role="incharge")

                for user in users:
 			to.append(user.user.email)
		print(users)
		users =  UserCompany.objects.filter(company=site.company,role="manager")
		print(users)
		print(site)

                for user in users:
			cc.append(user.user.email)
		print(to)
		print(cc)
	    	

		users =  UserCompany.objects.filter(company=site.company,role="zonal_head",zone=site.zone)
                for user in users:
			cc.append(user.user.email)


		html_content = render_to_string("notifications/notification_email_completed.html", {"user":'',"today":today,"due":"",'SITE_URL':settings.SITE_URL})
	    	msg = EmailMultiAlternatives(message_subject, html_content, settings.FROM_EMAIL, to,cc=cc,bcc=['skbohra123@gmail.com','ritu.ecb@gmail.com',])
	    	msg.attach_alternative(html_content, "text/html")

	    	msg.send()

            else:
                print "no pm today"




@app.task(serializer='pickle')
def daily_due_issues_emails(*args,**kwargs):
	company = args[0]

	today_date = datetime.datetime.now().date()


	usersites = EnergySite.objects.filter(company__id=company)

        for site in usersites:
	    today = Complaint.objects.filter(site=site,target_date=today_date,status__title="open")
            message_subject = "AkshyaPower Portal:  Close these issues today!"

            

            if today or due:

                users = UserCompany.objects.filter(company__id=company)
                for user in users:

                    html_content = render_to_string("notifications/daily_issues_email.html", {"user":user.user,"today":today,"due":due,'SITE_URL':settings.SITE_URL})
                    send_mail(message_subject, "Issues Alerts", settings.FROM_EMAIL,
                        [user.user.email], fail_silently=False,html_message=html_content)
            else:
                print "no issues today"

'''


Crate issue from delayed PM

'''


@app.task(serializer='pickle')
def pm_delay_issue(*args,**kwargs):
	company = args[0]

	today_date = datetime.datetime.now().date() - datetime.timedelta(days=4)


	usersites = EnergySite.objects.filter(company__id=company)
        print "in pm delay"
        for site in usersites:
            print "in sites"
	    due = PMAlert.objects.filter(site=site,due_date__lt=today_date).filter(~Q(status= "completed"))
            for pm in due:
                try:
                    issue = ComplaintPM.objects.get(pmalert=pm)
                except ComplaintPM.DoesNotExist:
                    print "due pm"
                    status = Status.objects.get(title="open")
                    description = Description.objects.get(title="Preventive Maintenance")
                    sub_description = SubDescription.objects.get(title="Delay")
                    outage_type = Outage.objects.get(title="Maintenance")
                    priority = Priority.objects.get(title="critical")
		    comment = "PM Delayed :  %s  BLock - %s - Due Date - %s " % (pm.equipment.name,pm.pmschedule.block,str(pm.due_date))
                    complaint = Complaint(site=site,complaint_by=None,date_of_complaint=today_date,outage_start=pm.due_date,outage_type=outage_type,description=description,sub_description=sub_description,priority=priority,status=status,comment=comment)

                    complaint.company = site.company
                    complaint.time_to_detect = (complaint.date_of_complaint - complaint.outage_start).days
                    complaint.save()
                    
                    complaint_pm = ComplaintPM(complaint=complaint,pmalert=pm,status=status)
                    complaint_pm.save()



@app.task(serializer='pickle')
def pm_summary(*args,**kwargs):
	company = Company.objects.get(id=args[0])

	sites = EnergySite.objects.filter(company=company)

	summary  = summary_xl(sites,"mtd")
	xl_url = summary['xl_url']
	reports = summary['reports']
    	if xl_url:
		to = []
		cc = []
		users = UserSite.objects.filter(site__company=company,role__in=["incharge","manager"])

		for user in users:
			to.append(user.user.email)
		print(users)
		users =  UserCompany.objects.filter(company=company,role="manager")
		print(users)

		for user in users:
			cc.append(user.user.email)
		print(to)
		print(cc)
		

		users =  UserCompany.objects.filter(company=company,role="zonal_head")
		for user in users:
			cc.append(user.user.email)
		date = datetime.datetime.now().date()
    		message_subject = "%s - %s" % ("Portfolio PM Summary - MTD",str(date))

		html_content = render_to_string("notifications/notification_summary.html", {"reports": reports,"user":'','SITE_URL':settings.SITE_URL})
		msg = EmailMultiAlternatives(message_subject, html_content, settings.FROM_EMAIL, to,cc=cc,bcc=['skbohra123@gmail.com','ritu.ecb@gmail.com',])
		msg.attach_alternative(html_content, "text/html")
		date = datetime.datetime.now().date()
    		file_name = "%s_%s.xlsx" % ("PM_Summary-MTD",str(date))
                data = open("/home/skbohra/code/omportal/"+xl_url, 'rb').read()
                #encoded = base64.b64encode(data)
                mimetype='application/octet-stream'
                msg.attach(file_name,data, mimetype=mimetype)


		msg.send()

	else:
		print "no pm today"




@app.task(serializer='pickle')
def pm_aging(*args,**kwargs):
	company = Company.objects.get(id=args[0])

	sites = EnergySite.objects.filter(company=company)

	summary  = aging_xl(sites,"mtd")
	xl_url = summary['xl_url']
	reports = summary['reports']
    	if xl_url:
		to = []
		cc = []
		users = UserSite.objects.filter(site__company=company,role__in=["incharge","manager"])

		for user in users:
			to.append(user.user.email)
		print(users)
		users =  UserCompany.objects.filter(company=company,role="manager")
		print(users)

		for user in users:
			cc.append(user.user.email)
		print(to)
		print(cc)
		
		cc = []
		users =  UserCompany.objects.filter(company=company,role="zonal_head")
		for user in users:
			cc.append(user.user.email)
		date = datetime.datetime.now().date()
    		message_subject = "%s - %s" % ("Portfolio Delay PM Aging Summary - MTD",str(date))

		html_content = render_to_string("notifications/notification_aging.html", {"reports": reports,"user":'','SITE_URL':settings.SITE_URL})
		msg = EmailMultiAlternatives(message_subject, html_content, settings.FROM_EMAIL, to,cc=cc,bcc=['skbohra123@gmail.com','ritu.ecb@gmail.com',])
		msg.attach_alternative(html_content, "text/html")
		date = datetime.datetime.now().date()
    		file_name = "%s_%s.xlsx" % ("Aging_PM_Summary-MTD",str(date))
                data = open("/home/skbohra/code/omportal/"+xl_url, 'rb').read()
                #encoded = base64.b64encode(data)
                mimetype='application/octet-stream'
                msg.attach(file_name,data, mimetype=mimetype)


		msg.send()

	else:
		print "no pm today"




