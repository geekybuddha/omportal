import requests

from allauth.socialaccount.providers.oauth2.views import (
    OAuth2Adapter,
    OAuth2CallbackView,
    OAuth2LoginView,
)

from .provider import AkshyaPowerProvider


class AkshyaPowerOAuth2Adapter(OAuth2Adapter):
    provider_id = AkshyaPowerProvider.id
    access_token_url = 'http://localhost:9000/o/token/'
    authorize_url = 'http://localhost:9000/o/authorize'
    profile_url = 'http://localhost:9000/api/user/view/1/'
    supports_state = False
    redirect_uri_protocol = 'http'

    def complete_login(self, request, app, token, **kwargs):
	print app
	print token
        response = requests.get(
            self.profile_url,
            params={'access_token': token})
	print response
        extra_data = response.json()
	print extra_data
        #if 'Profile' in extra_data:
        extra_data = {
		'user_id': "1",
		'name': "333",
		'email': "sss@ss.com"
        }
	
        return self.get_provider().sociallogin_from_response(
            request,extra_data)


oauth2_login = OAuth2LoginView.adapter_view(AkshyaPowerOAuth2Adapter)
oauth2_callback = OAuth2CallbackView.adapter_view(AkshyaPowerOAuth2Adapter)
