from allauth.socialaccount.providers.oauth2.urls import default_urlpatterns

from .provider import AkshyaPowerProvider


urlpatterns = default_urlpatterns(AkshyaPowerProvider)
