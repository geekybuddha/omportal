# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from issues.models import *
from issues.forms import *
from prevmain.models import *
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import datetime
from django.http import HttpResponseRedirect
from .forms import *

@login_required
def dashboard(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()
	weekday = today_date.weekday()
	start = today_date - datetime.timedelta(days=today_date.weekday())
	end = start + datetime.timedelta(days=6)

        if usercompany.is_client:
            return HttpResponseRedirect("/client/dashboard/")

        if usercompany.role == "manager":
		return HttpResponseRedirect("/manager/dashboard/")
	elif usercompany.role == "incharge":
		return HttpResponseRedirect("/pm/alerts/")

@login_required
def issue_tracker(request):
		
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()
	weekday = today_date.weekday()
	start = today_date - datetime.timedelta(days=today_date.weekday())
	end = start + datetime.timedelta(days=6)


	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)
        allusers = UserSite.objects.filter(site__company=usercompany.company)


	site_id = request.GET.get('site')
        if site_id:
            sites = EnergySite.objects.filter(pk=site_id)
	view_type = request.GET.get('type')
	if view_type == "issues":
		events = Complaint.objects.filter(site__in=sites)
		pms = None
	elif view_type == "pm":
		pms = PMAlert.objects.filter(site__in=sites)
		events = None
	elif view_type == "my":
		pms = PMAlert.objects.filter(site__in=sites)
		events = Complaint.objects.filter(site__in=sites,assigned_to=request.user)
	elif view_type == "urgent":
		pms = PMAlert.objects.filter(site__in=sites)
		events = Complaint.objects.filter(site__in=sites)
	else:
		events = Complaint.objects.filter(site__in=sites).exclude(status__title="completed")

        open_issues = events.filter(status__title="open").count()
	today_date = datetime.datetime.now().date()


	alert_type = request.GET.get('type')
	if alert_type == "my":
		alerts = Complaint.objects.filter(site__in=sites,assigned_to=request.user).order_by('-created')
		messages.success(request, 'showing only issues assigned to you')
	elif alert_type == "urgent":
		alerts = Complaint.objects.filter(site__in=sites,priority__title="critical").order_by('-created')
		messages.success(request, 'showing only urgent issues')
	elif alert_type == "delayed":
		alerts = Complaint.objects.filter(site__in=sites,target_date__lt=today_date).order_by('-created')
		messages.success(request, 'showing only delayed issues')
        elif alert_type == "open":
                alerts = Complaint.objects.filter(site__in=sites,status__title="open").order_by('-created')
		messages.success(request, 'showing only open issues')
        elif alert_type == "pending":
                alerts = Complaint.objects.filter(site__in=sites,status__title="pending").order_by('-created')
		messages.success(request, 'showing only pending issues')
        elif alert_type == "closed":
                alerts = Complaint.objects.filter(site__in=sites,status__title="close").order_by('-created')
		messages.success(request, 'showing only closed issues')



	else:
		alerts = Complaint.objects.filter(site__in=sites).order_by('-created')

	paginator = Paginator(alerts, 25) # Show 25 contacts per page

    	page = request.GET.get('page')
    	try:
        	alerts_list = paginator.page(page)
    	except PageNotAnInteger:
        	# If page is not an integer, deliver first page.
        	alerts_list = paginator.page(1)
    	except EmptyPage:
        	# If page is out of range (e.g. 9999), deliver last page of results.
        	alerts_list = paginator.page(paginator.num_pages)

        today_date = datetime.datetime.now().date()

        pms = PMAlert.objects.filter(due_date__lte=today_date)[:5]

        paginator2 = Paginator(pms, 25) # Show 25 contacts per page

    	page2 = request.GET.get('page2')
    	try:
        	pms_list = paginator2.page(page2)
    	except PageNotAnInteger:
        	# If page is not an integer, deliver first page.
        	pms_list = paginator2.page(1)
    	except EmptyPage:
        	# If page is out of range (e.g. 9999), deliver last page of results.
        	pms_list = paginator2.page(paginator2.num_pages)

        actions = Action.objects.filter(complaint__company=usercompany.company,created__date=today_date)
        week_date = today_date - datetime.timedelta(days=7)
        week_actions = Action.objects.filter(complaint__company=usercompany.company,created__date__gte=week_date)
        stats = {}
        stats['total_issues'] = Complaint.objects.filter(site__in=sites).count()
        stats['closed'] = Complaint.objects.filter(site__in=sites,status__title="close").count()
        try:
            stats['last_closed'] = Complaint.objects.filter(site__in=sites,status__title="close")[0]
        except:
            stats['last_closed'] = 0
        stats['open_issues'] = Complaint.objects.filter(site__in=sites,status__title="open").count()
        stats['pending_issues'] = Complaint.objects.filter(site__in=sites,status__title="pending").count()
        stats['rejected_issues'] = Complaint.objects.filter(site__in=sites,status__title="rejected").count()
        try:
            stats['issue_per'] = int(stats['closed']*100.00/stats['total_issues'])
        except ZeroDivisionError:
            stats['issue_per'] = 0

        stats['delayed_issues'] = Complaint.objects.filter(site__in=sites,status__title="open",target_date__lte=today_date).count()
        stats['closed_week'] = Complaint.objects.filter(site__in=sites,status__title="close",closing_date__range=[week_date,today_date]).count()
        print week_date
        print today_date
        recently_closed = Complaint.objects.filter(site__in=sites,closing_date__range = [week_date,today_date])
        metrics = []
        categories = Outage.objects.all()
        for category in categories:
            data = {}
            data['count'] = Complaint.objects.filter(site__in=sites,outage_type=category).count()
            data['category'] = category
            metrics.append(data)
        return render(request,"main/dashboard.html",{'categories':metrics,'actions':actions,'week_actions':week_actions,'recently_closed':recently_closed,'pms_list':pms_list,'allusers':allusers,'open_issues':open_issues,'pms':pms,'events':events,'today':today_date,'alerts':alerts,'sites':sites,'stats':stats})




@login_required
def client_dashboard(request):
        usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()
	weekday = today_date.weekday()
	start = today_date - datetime.timedelta(days=today_date.weekday())
	end = start + datetime.timedelta(days=6)

        #if not usercompany.is_client:
        #    return HttpResponseRedirect("/")
	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)

        if request.method == "POST":
            ticket = request.POST['ticket']
            try:
                issue = Complaint.objects.get(pk=ticket)
                url = "/complaints/issue/%d/" % issue.id
                return HttpResponseRedirect(url)
            except:# Complaint.DoesNotExist:
                print "does not exist"



	weekday = today_date.weekday()
	start = today_date - datetime.timedelta(days=today_date.weekday())
	end = start + datetime.timedelta(days=6)

	if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)
        allusers = UserSite.objects.filter(site__company=usercompany.company)

	view_type = request.GET.get('type')
	if view_type == "issues":
		events = Complaint.objects.filter(site__in=sites)
		pms = None
	elif view_type == "pm":
		pms = PMAlert.objects.filter(site__in=sites)
		events = None
	elif view_type == "my":
		pms = PMAlert.objects.filter(site__in=sites)
		events = Complaint.objects.filter(site__in=sites,assigned_to=request.user)
	elif view_type == "urgent":
		pms = PMAlert.objects.filter(site__in=sites)
		events = Complaint.objects.filter(site__in=sites)
	else:
		events = Complaint.objects.filter(site__in=sites).exclude(status__title="completed")

        open_issues = events.filter(status__title="open").count()
	today_date = datetime.datetime.now().date()


	alert_type = request.GET.get('type')
	if alert_type == "my":
		alerts = Complaint.objects.filter(site__in=sites,assigned_to=request.user).order_by('-created')
		messages.success(request, 'showing only issues assigned to you')
	elif alert_type == "urgent":
		alerts = Complaint.objects.filter(site__in=sites,priority__title="critical").order_by('-created')
		messages.success(request, 'showing only urgent issues')
	elif alert_type == "delayed":
		alerts = Complaint.objects.filter(site__in=sites,target_date__lt=today_date).order_by('-created')
		messages.success(request, 'showing only delayed issues')
	else:
		alerts = Complaint.objects.filter(site__in=sites).order_by('-created')

	paginator = Paginator(alerts, 25) # Show 25 contacts per page

    	page = request.GET.get('page')
    	try:
        	alerts_list = paginator.page(page)
    	except PageNotAnInteger:
        	# If page is not an integer, deliver first page.
        	alerts_list = paginator.page(1)
    	except EmptyPage:
        	# If page is out of range (e.g. 9999), deliver last page of results.
        	alerts_list = paginator.page(paginator.num_pages)

        today_date = datetime.datetime.now().date()



        return render(request,"main/client_dashboard.html",{'allusers':allusers,'open_issues':open_issues,'events':events,'today':today_date,'alerts':alerts_list,'sites':sites})


@login_required
def user_profile(request,id):
    user = get_object_or_404(User,pk=id)
    usercompany = get_object_or_404(UserCompany,user=user)

    if usercompany.is_admin:
            sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
    else:
            usersites = UserSite.objects.filter(user=request.user).values('site')
            sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)
    form = UserForm(instance=user)
    if request.method == "POST":

        form = UserForm(instance=user,data=request.POST)
        if form.is_valid():
            form.save()
	    messages.success(request, 'User information updated successfully!')
    return render(request,"main/user-profile.html",{'sites':sites,'user':user,'form':form})


@login_required
def manager_dashboard(request):
	usercompany = get_object_or_404(UserCompany,user=request.user)
	today_date = datetime.datetime.now().date()
	weekday = today_date.weekday()
	start = today_date - datetime.timedelta(days=today_date.weekday())
	end = start + datetime.timedelta(days=6)

        if usercompany.is_client:
            return HttpResponseRedirect("/client/dashboard/")

        if not usercompany.role == "manager":
            return HttpResponseRedirect("/")




        if usercompany.is_admin:
		sites = EnergySite.objects.filter(company=usercompany.company,is_grouped=False)
	else:
		usersites = UserSite.objects.filter(user=request.user).values('site')
		sites = EnergySite.objects.filter(pk__in=usersites,is_grouped=False)
        allusers = UserSite.objects.filter(site__company=usercompany.company)


	view_type = request.GET.get('type')
	if view_type == "issues":
		events = Complaint.objects.filter(site__in=sites)
		pms = None
	elif view_type == "pm":
		pms = PMAlert.objects.filter(site__in=sites)
		events = None
	elif view_type == "my":
		pms = PMAlert.objects.filter(site__in=sites)
		events = Complaint.objects.filter(site__in=sites,assigned_to=request.user)
	elif view_type == "urgent":
		pms = PMAlert.objects.filter(site__in=sites)
		events = Complaint.objects.filter(site__in=sites)
	else:
		events = Complaint.objects.filter(site__in=sites).exclude(status__title="completed")

        open_issues = events.filter(status__title="open").count()
	today_date = datetime.datetime.now().date()


	alert_type = request.GET.get('type')
	if alert_type == "my":
		alerts = Complaint.objects.filter(site__in=sites,assigned_to=request.user).order_by('-created')
		messages.success(request, 'showing only issues assigned to you')
	elif alert_type == "urgent":
		alerts = Complaint.objects.filter(site__in=sites,priority__title="critical").order_by('-created')
		messages.success(request, 'showing only urgent issues')
	elif alert_type == "delayed":
		alerts = Complaint.objects.filter(site__in=sites,target_date__lt=today_date).order_by('-created')
		messages.success(request, 'showing only delayed issues')
	else:
		alerts = Complaint.objects.filter(site__in=sites).order_by('-created')

	paginator = Paginator(alerts, 25) # Show 25 contacts per page

    	page = request.GET.get('page')
    	try:
        	alerts_list = paginator.page(page)
    	except PageNotAnInteger:
        	# If page is not an integer, deliver first page.
        	alerts_list = paginator.page(1)
    	except EmptyPage:
        	# If page is out of range (e.g. 9999), deliver last page of results.
        	alerts_list = paginator.page(paginator.num_pages)

        today_date = datetime.datetime.now().date()

        pms = PMAlert.objects.filter(due_date__lte=today_date)[:5]

        paginator2 = Paginator(pms, 25) # Show 25 contacts per page

    	page2 = request.GET.get('page2')
    	try:
        	pms_list = paginator2.page(page2)
    	except PageNotAnInteger:
        	# If page is not an integer, deliver first page.
        	pms_list = paginator2.page(1)
    	except EmptyPage:
        	# If page is out of range (e.g. 9999), deliver last page of results.
        	pms_list = paginator2.page(paginator2.num_pages)

        actions = Action.objects.filter(complaint__company=usercompany.company,created__date=today_date)
        week_date = today_date - datetime.timedelta(days=7)
        week_actions = Action.objects.filter(complaint__company=usercompany.company,created__date__gte=week_date)
        stats = {}
        stats['total_issues'] = Complaint.objects.filter(site__company=usercompany.company).count()
        stats['closed'] = Complaint.objects.filter(site__company=usercompany.company,status__title="close").count()
        try:
            stats['last_closed'] = Complaint.objects.filter(site__company=usercompany.company,status__title="close")[0]
        except:
            stats['last_closed'] =None

        try:
            stats['open_issues'] = Complaint.objects.filter(site__company=usercompany.company,status__title="open").count()
        except:
            stats['open_issues'] = 0
        try:
            stats['pending_issues'] = Complaint.objects.filter(site__company=usercompany.company,status__title="pending").count()
        except:
            stats['pending_issues'] = 0
        try:
            stats['rejected_issues'] = Complaint.objects.filter(site__company=usercompany.company,status__title="rejected").count()
        except:
            stats['rejected_issues'] = 0

        try:
            stats['issue_per'] = int(stats['closed']*100.00/stats['total_issues'])
        except:
            stats['issue_per'] = 0
        try:
            stats['delayed_issues'] = Complaint.objects.filter(site__company=usercompany.company,status__title="open",target_date__lte=today_date).count()
        except:
            stats['delayed_issues'] = 0
        try:
            stats['closed_week'] = Complaint.objects.filter(site__company=usercompany.company,status__title="close",closing_date__range=[week_date,today_date]).count()
        except:
            stats['closed_week'] = 0
        stats['pending_pm'] = PMAlert.objects.filter(site__company=usercompany.company,due_date__lte=today_date,status="due").count()
        stats['completed_pm'] = PMAlert.objects.filter(site__company=usercompany.company,due_date__lte=today_date,status="completed").count()
        
	print week_date
        print today_date
        recently_closed = Complaint.objects.filter(site__company=usercompany.company,closing_date__range = [week_date,today_date])
        metrics = []
        categories = Outage.objects.all()
        for category in categories:
            data = {}
            data['count'] = Complaint.objects.filter(site__company=usercompany.company,outage_type=category).count()
            data['category'] = category
            metrics.append(data)
        reports = []
        for site in sites:
            data = {}
            complaints = Complaint.objects.filter(site=site)
            data['total'] = complaints.count()
            data['site'] = site
            data['open'] = complaints.filter(status__title = "open").count()
            data['closed'] = complaints.filter(status__title = "close").count()
            data['pending'] = complaints.filter(status__title = "pending").count()
            data['pending_pm'] = PMAlert.objects.filter(site=site,due_date__lte=today_date,status="due").count()
            data['completed_pm'] = PMAlert.objects.filter(site=site,due_date__lte=today_date,status="completed").count()
            reports.append(data)

        escalated = Escalate.objects.filter(complaint__site__in=sites,complaint__status__title="open")

        return render(request,"main/manager_dashboard.html",{'escalated':escalated,'reports':reports,'categories':metrics,'actions':actions,'week_actions':week_actions,'recently_closed':recently_closed,'pms_list':pms_list,'allusers':allusers,'open_issues':open_issues,'pms':pms,'events':events,'today':today_date,'alerts':alerts,'sites':sites,'stats':stats})



