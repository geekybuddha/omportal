# README #

OMPORTAL is django/python based web application for Solar O&M Activities management. Modules includes - 

### Available Modules  ###

* Issue Tracker
* Preventive Maintenance Management
* Stock Management 
* User Roles 

### Screenshots ###
 ![image](https://bitbucket.org/geekybuddha/omportal/raw/master/screenshots/dashboard.png) 
 
 ![image](https://bitbucket.org/geekybuddha/omportal/raw/master/screenshots/issue-tracker.png) 
 
  ![image](https://bitbucket.org/geekybuddha/omportal/raw/master/screenshots/PM-calendar.png) 