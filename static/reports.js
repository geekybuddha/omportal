
$(function(){


$("select").select2();

$("#id_from_date,#id_to_date").datepicker();
$(".field-hidden").parent().hide();

var format = $("#id_format").val();
if (format == "weekly" || format == "monthly"){
	$(".field-from").attr("required","required").parent().show();
	$(".field-to").attr("required","required").parent().show();
	$(".field-year").removeAttr("required").parent().hide();
	$(".field-from-year").removeAttr("required").parent().hide();
	$(".field-to-year").removeAttr("required").parent().hide();
	$(".field-fin-year").removeAttr("required").parent().hide();

}
if (format == "daily" ){
	$(".field-from").attr("required","required").parent().show();
	$(".field-to").removeAttr("required").parent().hide();
	$(".field-year").removeAttr("required").parent().hide();
	$(".field-from-year").removeAttr("required").parent().hide();
	$(".field-to-year").removeAttr("required").parent().hide();
	$(".field-fin-year").removeAttr("required").parent().hide();

}
if (format == "yearly" ){
	$(".field-from").removeAttr("required").parent().hide();
	$(".field-to").removeAttr("required").parent().hide();
	$(".field-year").attr("required","required").parent().show();
	$(".field-from-year").removeAttr("required").parent().hide();
	$(".field-to-year").removeAttr("required").parent().hide();
	$(".field-fin-year").removeAttr("required").parent().hide();


}

if (format == "yearwise" ){
	$(".field-from").removeAttr("required").parent().hide();
	$(".field-to").removeAttr("required").parent().hide();
	$(".field-year").removeAttr("required").parent().hide();
	$(".field-from-year").attr("required","required").parent().show();
	$(".field-to-year").attr("required","required").parent().show();
	$(".field-fin-year").removeAttr("required").parent().hide();
}

if (format == "finyearly" ){
	$(".field-from").removeAttr("required").parent().hide();
	$(".field-to").removeAttr("required").parent().hide();
	$(".field-year").removeAttr("required").parent().hide();
	$(".field-from-year").removeAttr("required").parent().hide();
	$(".field-to-year").removeAttr("required").parent().hide();
	$(".field-fin-year").attr("required","required").parent().show();
}




var type = $("#id_report_type").val();
if (type == "dgr" ){

$("form").attr("action","/manage/reports/generation/");

}
if (type == "breakdown" ){

$("form").attr("action","/manage/reports/breakdown/");

}
if (type == "griddown" ){

$("form").attr("action","/manage/reports/breakdown/");

}
if (type == "pm" || type == "slippage" || type == "history" ){

$("form").attr("action","/pm/reports/");


}

if (type == "history" ){
$("#id_equipment").parent().show();
}

if (type == "radiation" ){

$("form").attr("action","/manage/reports/generation/");

}

if (type == "cleaning" ){

$("form").attr("action","/manage/reports/cleaning/");

}
if (type == "reminders" ){

$("form").attr("action","/manage/reports/reminders/");

$(".field-category").parent().show();
}
if (type == "soiling" ){

$("form").attr("action","/manage/reports/soiling/");

}

if (type == "ort" ){

$("form").attr("action","/manage/reports/ort/");
$("#id_site").removeAttr("required");

}















$("#id_format").change(function(){

var format = $(this).val();
if (format == "weekly" || format == "monthly"){
	$(".field-from").attr("required","required").parent().show();
	$(".field-to").attr("required","required").parent().show();
	$(".field-year").removeAttr("required").parent().hide();
	$(".field-from-year").removeAttr("required").parent().hide();
	$(".field-to-year").removeAttr("required").parent().hide();
	$(".field-fin-year").removeAttr("required").parent().hide();

}
if (format == "daily" ){
	$(".field-from").attr("required","required").parent().show();
	$(".field-to").removeAttr("required").parent().hide();
	$(".field-year").removeAttr("required").parent().hide();
	$(".field-from-year").removeAttr("required").parent().hide();
	$(".field-to-year").removeAttr("required").parent().hide();
	$(".field-fin-year").removeAttr("required").parent().hide();

}
if (format == "yearly" ){
	$(".field-from").removeAttr("required").parent().hide();
	$(".field-to").removeAttr("required").parent().hide();
	$(".field-year").attr("required","required").parent().show();
	$(".field-from-year").removeAttr("required").parent().hide();
	$(".field-to-year").removeAttr("required").parent().hide();
	$(".field-fin-year").removeAttr("required").parent().hide();


}

if (format == "yearwise" ){
	$(".field-from").removeAttr("required").parent().hide();
	$(".field-to").removeAttr("required").parent().hide();
	$(".field-year").removeAttr("required").parent().hide();
	$(".field-from-year").attr("required","required").parent().show();
	$(".field-to-year").attr("required","required").parent().show();
	$(".field-fin-year").removeAttr("required").parent().hide();
}

if (format == "finyearly" ){
	$(".field-from").removeAttr("required").parent().hide();
	$(".field-to").removeAttr("required").parent().hide();
	$(".field-year").removeAttr("required").parent().hide();
	$(".field-from-year").removeAttr("required").parent().hide();
	$(".field-to-year").removeAttr("required").parent().hide();
	$(".field-fin-year").attr("required","required").parent().show();
}




});


$("#id_report_type").change(function(){

var format = $(this).val();
if (format == "dgr" ){

$("form").attr("action","/manage/reports/generation/");

}
if (format == "breakdown" ){

$("form").attr("action","/manage/reports/breakdown/");

}
if (format == "griddown" ){

$("form").attr("action","/manage/reports/breakdown/");

}
if (format == "pm"  || format == "slippage" || format == "history"){

$("form").attr("action","/pm/reports/");

}
if (format == "history" ){


$("#id_equipment").parent().show();
}

if (format == "radiation" ){

$("form").attr("action","/manage/reports/generation/");

}

if (format == "cleaning" ){

$("form").attr("action","/manage/reports/cleaning/");

}

if (format == "reminders" ){

$("form").attr("action","/manage/reports/reminders/");

}

if (format == "soiling" ){

$("form").attr("action","/manage/reports/soiling/");

}

if (format == "ort" ){

$("form").attr("action","/manage/reports/ort/");

$("#id_site").removeAttr("required");
}



});

$('form').ajaxForm({ 
        // target identifies the element(s) to update with the server response 
        target: '.search-results', 
	beforeSubmit: showRequest, 
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function() { 
            $('.search-results').fadeIn('slow'); 
	    $(".form-button").text("Generate");

	   //$.notify("Report loaded", "success"); 
        } 
    }); 


});

// pre-submit callback 
function showRequest(formData, jqForm, options) { 
        $(".saved-search-button").addClass("disabled");
	$(".form-button").text("Loading..");
    return true; 
} 



