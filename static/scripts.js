function displayLoader(element){

$(element).html('<span class="loader"><div class="uil-sunny-css" style="transform:scale(0.2);"><div class="uil-sunny-circle"></div><div class="uil-sunny-light"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div></span>');
}



$(function() {



$("#dataTable").DataTable();
$("#id_from_date,#id_to_date").datepicker();

var active_class = "#"+Cookies.get('active_menu');
$(active_class).addClass("menu-open");
$(".menu-open").find("fa-plus-square-o").removeClass("fa-plus-square-o").addClass("fa-minus-square-o");
$(".accordion-toggle").click(function(){
var classes = $(this).attr("id");
Cookies.set('active_menu', classes);
var active_class = Cookies.get('active_menu');
});

$(".sidebar-menu a").click(function(){
  var active_menu = $(this).parent().data("item");
  Cookies.set("active_menu_item",active_menu);
  window.location = $(this).attr("href");
  return false;
});


var active_menu = Cookies.get("active_menu_item",active_menu);
$('[data-item="'+active_menu+'"]').addClass("active");



function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace(/"/g, '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"';

				// Deliberate 'false', see comment below
        if (false && window.navigator.msSaveBlob) {

						var blob = new Blob([decodeURIComponent(csv)], {
	              type: 'text/csv;charset=utf8'
            });
            
            // Crashes in IE 10, IE 11 and Microsoft Edge
            // See MS Edge Issue #10396033: https://goo.gl/AEiSjJ
            // Hence, the deliberate 'false'
            // This is here just for completeness
            // Remove the 'false' at your own risk
            window.navigator.msSaveBlob(blob, filename);
            
        } else if (window.Blob && window.URL) {
						// HTML5 Blob        
            var blob = new Blob([csv], { type: 'text/csv;charset=utf8' });
            var csvUrl = URL.createObjectURL(blob);

            $(this)
            		.attr({
                		'download': filename,
                		'href': csvUrl
		            });
				} else {
            // Data URI
            var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

						$(this)
                .attr({
               		  'download': filename,
                    'href': csvData,
                    'target': '_blank'
            		});
        }
    }

});

