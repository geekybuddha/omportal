from __future__ import absolute_import

import os

from celery import Celery

from django.conf import settings


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'omportal.settings')
app = Celery('omportal')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(
    CELERY_IMPORTS = ("tasks", )
)
app.conf.update(
    CELERY_TIMEZONE='Asia/Kolkata',
    CELERY_ENABLE_UTC=True,
    BROKER_URL = 'redis://localhost:6379/1',
    CELERY_TASK_SERIALIZER = 'pickle',
)


from celery.schedules import crontab

from kombu.serialization import registry
registry.enable('pickle')
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_RESULT_SERIALIZER = 'pickle'

